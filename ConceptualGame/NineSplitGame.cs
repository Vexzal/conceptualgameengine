﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using BaseEngine;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using BaseEngine;
using BaseEngine.Extensions;
using BaseEngine.Components;

namespace ConceptualGame
{
    public class MemoryBatch
    {
        GraphicsDevice GraphicsDevice;

        VertexPositionColorTexture[] Vertex = new VertexPositionColorTexture[4]
        {
            new VertexPositionColorTexture(Vector3.Zero,Color.White,Vector2.Zero),
            new VertexPositionColorTexture(Vector3.Zero,Color.White,Vector2.Zero),
            new VertexPositionColorTexture(Vector3.Zero,Color.White,Vector2.Zero),
            new VertexPositionColorTexture(Vector3.Zero,Color.White,Vector2.Zero)
        };
        short[] index = new short[] { 0, 1, 2, 1, 3, 2 };
        VertexPositionColorTexture vTL = new VertexPositionColorTexture(Vector3.Zero, Color.White, Vector2.Zero);
        VertexPositionColorTexture vTR = new VertexPositionColorTexture(Vector3.Zero, Color.White, Vector2.Zero);
        VertexPositionColorTexture vBL = new VertexPositionColorTexture(Vector3.Zero, Color.White, Vector2.Zero);
        VertexPositionColorTexture vBR = new VertexPositionColorTexture(Vector3.Zero, Color.White, Vector2.Zero);

        Vector2 texCoordTL = new Vector2();
        Vector2 texCoordBR = new Vector2();
        public MemoryBatch(GraphicsDevice graphics)
        {
            GraphicsDevice = graphics;
        }
        public unsafe void DrawText(string text, SpriteFont font, Rectangle Limit,Vector2 Padding, Effect effect, GameTime gameTime)
        {
            //clip text firs
            var paddedLimit = Limit;
            paddedLimit.Width -= 40;
            paddedLimit.Height -= 30;

            //alright lets setup translation ight with
            var textClip = font.MeasureString(text);
            var placementHeight = paddedLimit.Height / 2 - textClip.Y / 2 + 15;
            float widthClip = textClip.X;
            float heightCLip = textClip.Y;
            if (textClip.X > paddedLimit.Width)
                widthClip = paddedLimit.Width;
            if (textClip.Y > paddedLimit.Height)
                heightCLip = paddedLimit.Height;
            //wait just. okay so we're. we're going to do the thing //the thing was draw, the text using draw rectangles. and I guess OH. OH SHIT.
            //okay if thats all I'm doing I can do the culling in that loop so
            //lets pesudo code it and go to bed
            //rectangle destination
            //for loop
            //  check if destination width will exceed bounds
            //  if so constrain destination
            //  draw texture source position etc                
            //  add bounds to destination position.
            //  //break if out of bounds.
            //end
            //this doesn't do rotation well right now I'll. I'll work on that.
            var Glyphs = font.GetGlyphs();
            Rectangle destination = new Rectangle(0, 0, 0, 0);
            destination.Height = font.LineSpacing;
            for (int i = 0; i < text.Length; i++)
            {
                //destination.Height = font.LineSpacing;

                char c = text[i];
                //destination.Height -= Glyphs[c].Cropping.Y;

                destination.Y = Glyphs[c].Cropping.Y;
                destination.X += (int)(font.Spacing + Glyphs[c].LeftSideBearing);
                //destination.Width = (int)Glyphs[c].Width;
                var source = Glyphs[c].BoundsInTexture;
                destination.Height = source.Height;
                destination.Width = source.Width;
                if ((destination.X + destination.Width) > widthClip)
                {
                    destination.Width = (int)widthClip - destination.X;
                    i = text.Length;//only problem is this is stretching instead of cropping. Hmm 
                    //okay so the problem is source controls cropping so.
                    source.Width -= (int)Glyphs[c].Width - destination.Width;
                }
                //should I just use pointers? I mean I guess
                DrawRectangles(destination, source, font.Texture, gameTime, new Vector3(50, 50, 0) + new Vector3(15, placementHeight, 0), Color.Purple,effect);
                destination.X += destination.Width + (int)Glyphs[c].RightSideBearing;
            }

        }
        public unsafe void DrawRectangles(Rectangle destination, Rectangle source, Texture2D texure, GameTime gameTime, Vector3 translate, Color color,Effect effect)
        {

            texCoordTL.X = source.X * texure.TexelWidth();
            texCoordTL.Y = source.Y * texure.TexelHeight();
            texCoordBR.X = (source.X + source.Width) * texure.TexelWidth();
            texCoordBR.Y = (source.Y + source.Height) * texure.TexelHeight();

            vTL.Position.X = destination.X;
            vTL.Position.Y = destination.Y;
            vTL.Position.Z = 0;
            vTL.Color = color;
            vTL.TextureCoordinate.X = texCoordTL.X;
            vTL.TextureCoordinate.Y = texCoordTL.Y;

            vTR.Position.X = destination.X + destination.Width;
            vTR.Position.Y = destination.Y;
            vTR.Position.Z = 0;
            vTR.Color = color;
            vTR.TextureCoordinate.X = texCoordBR.X;
            vTR.TextureCoordinate.Y = texCoordTL.Y;

            vBL.Position.X = destination.X;
            vBL.Position.Y = destination.Y + destination.Height;
            vBL.Position.Z = 0;
            vBL.Color = color;
            vBL.TextureCoordinate.X = texCoordTL.X;
            vBL.TextureCoordinate.Y = texCoordBR.Y;

            vBR.Position.X = destination.X + destination.Width;
            vBR.Position.Y = destination.Y + destination.Height;
            vBR.Position.Z = 0;
            vBR.Color = color;
            vBR.TextureCoordinate.X = texCoordBR.X;
            vBR.TextureCoordinate.Y = texCoordBR.Y;

            fixed (VertexPositionColorTexture* vertexArrayFixedPtr = Vertex)
            {
                var vAPtr = vertexArrayFixedPtr;
                *(vAPtr + 0) = vTL;
                *(vAPtr + 1) = vTR;
                *(vAPtr + 2) = vBL;
                *(vAPtr + 3) = vBR;
            }

            
            //Vector3 OriginOff = new Vector3(Box2.Width / 2, Box2.Height / 2, 0);
            //Vector3 OriginOff = new Vector3(200, 25, 0);
            Vector3 OriginOff = Vector3.Zero;
            Matrix m = Matrix.CreateOrthographicOffCenter(0, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height, 0, 0, -1);
            Matrix mOff = Matrix.CreateTranslation(OriginOff);
            Matrix mTrans = Matrix.CreateTranslation(translate);
            Matrix m2 = Matrix.Invert(mOff) * Matrix.CreateRotationZ(MathHelper.ToRadians(15)) * mOff * mTrans;
            effect.Parameters["MatrixTransform"].SetValue(m2 * m);
            effect.Parameters["SpriteTexture"].SetValue(texure);
            GraphicsDevice.Textures[0] = texure;
            //GraphicsDevice.RasterizerState = RasterizerState.CullNone;
            GraphicsDevice.RasterizerState = RasterizerState.CullNone;
            effect.CurrentTechnique.Passes[0].Apply();
            GraphicsDevice.DrawUserIndexedPrimitives(
                PrimitiveType.TriangleList,
                Vertex,
                0,
                4,
                index,
                0,
                2,
                VertexPositionColorTexture.VertexDeclaration);
        }
    }
    //lets do split frame next;
    //okay I said split frame but scaling is more important
    public class Frame
    {
        //so the idea is.
        //a frame is a container for other controls.
        //the windows themselves.
        //so.
        //alright so I know 
        Frame Parent;
        Vector2 PositionPercentage;//percentage of parent frame size
        Vector2 SizePercentage;//this is, this is basically the anchor points right?
        Rectangle originFrame; //frame just used as an origin for spritebatch transform//based off texture

        Rectangle internalFrame;//contains real position and size;
        Rectangle ActiveFrame;
        Vector2 Position;
        Vector2 Size;
        Vector2[] AnchorPercentages;//these are 0-1 values determining distance from each cor, heck HECK I need I do only need the 2.
        //because its min percent and max percnt
        Vector4 minMaxAnchorPercentages;//x = min x, y = min y, z = max x, w = max y
        public void Draw(MemoryBatch batch)
        {

        }
        public void Draw(SpriteBatch batch)
        {
            ActiveFrame.X = (int)(internalFrame.X * Parent.minMaxAnchorPercentages.X);
            ActiveFrame.Y = (int)(internalFrame.Y * Parent.minMaxAnchorPercentages.Y);
            ActiveFrame.Width = (int)(internalFrame.Width * Parent.minMaxAnchorPercentages.Z);
            ActiveFrame.Height = (int)(internalFrame.Height * Parent.minMaxAnchorPercentages.W);

            //this is wrong
            //we lack parent information.
            //this would just .5 transform all the dots but its all relative.
            //we need relative transform of parent;
            //unity has this in a transform component so move it there
            //a part of the problem is this is radically more complicated than normal dear imgui.
            //but all of this is behind the scenes and controlled through id functions 
        }
    }
    //we're going to treat all transforms as percentages relative to a thing, SO
    public class CanvasX
    {
        int Width;
        int Height;
        List<UIWindow> windows = new List<UIWindow>();
        SpriteBatch spriteBatch { get; set; }
        public void Draw(GameTime time)
        {
            foreach(UIWindow w in windows)
            {
                for (int i = 0; i < w.SplitCapacity; i++)
                {
                    spriteBatch.Draw(w.texture, w.destinationRectangles[i], w.sourceRectangles[i], Color.White);
                }
            }
        }
    }
    public class RectControl2 : Component
    {
        
        public Vector2 Position { get; set; }//relative offset according to the origin;//so not according to the origin, and thats
        //so like there is no good reason to do that, in the long run really??
        //Vector2 size2Offset;//relative offset of size from anchor.
        public Vector2 Size;//okay this is always just relative to, like its Position offset + Size offset from, anchor
        public float Depth { get; set; }
        public float Rotation { get; set; }
        public Vector2 OriginOffset { get; set; }//distance from draw point to origin point.//actually this is a percentage too right?
        //this is used for rotation information I don't think its actually all that useful elsewise;
        public Vector2 AnchorMin { get; set; }
        public Vector2 AnchorMax { get; set; }
        //so the transform is Anchor(percent of parent) => OffsetPosition;
        //so 
        //size return has various behaviors depending on whether or not the anchors match
        public RectControl2()
        {
            Position = Vector2.Zero;
            Size = Vector2.One;
            Depth = .1f;
            Rotation = 0;
            OriginOffset = Vector2.One / 2;
            AnchorMin = Vector2.One / 2;
            AnchorMax = AnchorMin;
        }

        public Vector2 Origin
        {
            get
            {
                //this means resolution can't use origin at all.
                return Resolution * OriginOffset;//uhhhm, yeah? no wait that won't, I have to solve for, size since size is just an anchor offset. fuck.
            }
        }
        
        public Vector2 Resolution
        {
            get
            {
                //throw new NotImplementedException();//so we'll use this to convert the offset info into a fixed size 
                //alright so size right now is, offset from 
                if(gameObject.Parent != null)
                {
                    var parentVal = gameObject.Parent.GetComponent<RectControl2>().Resolution;
                    var pos = Position + parentVal * AnchorMin;
                    var pos2 = Size + parentVal * AnchorMax;
                    return pos2 - pos;
                }
                //so if no parent its just
                //if no parent the anchors can't do anything so it is just,
                return Size;//I don't think this accounts for, origin offset.
            }
        }
        public Vector2 DrawPosition
        {
            //so. here we go
            get
            {
                if(gameObject.Parent != null)
                {
                    //so lets, just do the things I know
                    var anchorAlign = gameObject.Parent.GetComponent<RectControl2>().Resolution * AnchorMin;//hmm
                    return Position + anchorAlign;
                }
                //throw new NotImplementedException();
                return Position;
            }
        }
        //wait this is just resolution again.
        //public Vector2 DrawSize
        //{
        //    get
        //    {
        //        if(gameObject.Parent != null)
        //        {
        //            var anchorAlgin =
        //        }
        //    }
        //}
    }
    public interface IUserInterfaceComponent
    {
        Texture2D Texture { get; }
        Rectangle[] DestinationRectangles { get; }
        Rectangle[] SourceRectangles { get; }
        Color Color { get; }
        float Rotation { get; }
        Vector2 Origin { get; }
        SpriteEffects Effects { get; }
        float Depth { get; }
    }
    public class RectWindow : Component ,IUserInterfaceComponent
    {
        public RectControl2 Transform { get; set; }
        public Texture2D Texture { get; set; }

        //Texture2D IUserInterfaceComponent.Texture => throw new NotImplementedException();

        public Rectangle[] DestinationRectangles { get; private set; }
        public Rectangle[] SourceRectangles { get; private set; }

        public Color Color { get; set; }

        public float Rotation { get; set; }

        Vector2 IUserInterfaceComponent.Origin => Transform.DrawPosition + Transform.Origin;

        public SpriteEffects Effects { get; set; }
        public float Depth
        {
            get => Transform.Depth;
        }

        public RectWindow(Texture2D tex, Vector4 padding, Color color)
        {
            Texture = tex;
            leftPadding = (int)padding.X;
            rightPadding = (int)padding.Y;
            topPadding = (int)padding.Z;
            bottomPadding = (int)padding.W;
            SourceRectangles = CreatePatches(new Rectangle(0, 0, tex.Width, tex.Height));

            Color = color;
            Effects = SpriteEffects.None;
            //when you get home write the container for this;
        }
        //so the thing is, the ui module would be a rendering and a, update module?
        //hmm okay hold on so this needs to add a. it gets called by the rendering service and by the updatable.
        //what if I go back to services and just have module collections instead?
        //hmmm.
        //lets just scratch up here and go back for it.
       
        public override void Initialize()
        {
            Transform = gameObject.GetComponent<RectControl2>();
            DestinationRectangles = CreatePatches(new Rectangle(Transform.DrawPosition.ToPoint(), Transform.Resolution.ToPoint()));
            base.Initialize();
        }

        public int leftPadding, rightPadding, topPadding, bottomPadding;


        private Rectangle[] CreatePatches(Rectangle rectangle)
        {
            var x = rectangle.X;
            var y = rectangle.Y;
            var w = rectangle.Width;
            var h = rectangle.Height;
            var middleWidth = w - leftPadding - rightPadding;
            var middleHeight = h - topPadding - bottomPadding;
            var bottomY = y + h - bottomPadding;
            var rightX = x + w - rightPadding;
            var leftX = x + leftPadding;
            var topY = y + topPadding;
            var patches = new[]
            {
        new Rectangle(x,      y,        leftPadding,  topPadding),      // top left
        new Rectangle(leftX,  y,        middleWidth,  topPadding),      // top middle
        new Rectangle(rightX, y,        rightPadding, topPadding),      // top right
        new Rectangle(x,      topY,     leftPadding,  middleHeight),    // left middle
        new Rectangle(leftX,  topY,     middleWidth,  middleHeight),    // middle
        new Rectangle(rightX, topY,     rightPadding, middleHeight),    // right middle
        new Rectangle(x,      bottomY,  leftPadding,  bottomPadding),   // bottom left
        new Rectangle(leftX,  bottomY,  middleWidth,  bottomPadding),   // bottom middle
        new Rectangle(rightX, bottomY,  rightPadding, bottomPadding)    // bottom right
    };
            return patches;
        }

    }
    public class UIService
    {
        IUserInterfaceComponent Canvas; //I thought i needed both of these but. Don't I really just need. oh no second is
                                        //second is draw based and this one is update based.
                                        //this stage could just be split into two pieces
                                        //like, have an update componnt and a draw module.
                                        //because like, I don't need a big batch thing they're both using the same thing but for different capacities.
                                        //public SpriteBatch batch;
        public MemoryBatch batch;
        public List<IUserInterfaceComponent> UiComponents = new List<IUserInterfaceComponent>();//this is wrong I think?

        public void Update()
        {
            //haven't implemented update behaviour
        }
        public void Draw(GameTime gameTime)
        {
            //using a 
            //using a like parameter, that sets the clipping rectangle.
            //batch.Begin();
            //batch.
            foreach (IUserInterfaceComponent component in UiComponents)
            {
                for (int i = 0; i < component.SourceRectangles.Length; i++)
                {
                    //so we're missing some stuff here like various draw behaviours
                    //but that would be handled with a, moduled spritebatch drawer.\
                    //batch.Draw(component.Texture, component.DestinationRectangles[i], component.SourceRectangles[i], component.Color);
                    var xcom = component.DestinationRectangles[i];
                    //xcom.Location = (xcom.Location.ToVector2() * new Vector2(.5f, .5f)).ToPoint();
                    //batch.Draw(component.Texture, xcom, component.SourceRectangles[i], component.Color, component.Rotation, Vector2.Zero, component.Effects, 1);
                    //batch.DrawRectangles(component.DestinationRectangles[i],component.SourceRectangles[i],component.Texture,0,component)
                    //batch.DrawRectangles(component.DestinationRectangles[i],component.SourceRectangles[i],component.Texture,gameTime,Vector3.Zero,Color.White,new SpriteEffect()
                }
            }
            //batch.End();
            
        }
    }
    public class UIWindow
    {
        
        public int SplitCapacity
        {
            get => destinationRectangles.Length;
        }
        public Texture2D texture { get; set; }
        Rectangle imageRect;
        public Rectangle[] sourceRectangles { get; set; }
        public Rectangle[] destinationRectangles { get; set; }
        public RectangleTransform transform { get; set; }
       // public BuildRectangles

    }
    public class RectangleTransform : Component
    {
        public float X;
        public float Y;
        public float Width;
        public float Height; //all of these need to be 0 - 1//unless its the root canvas I guess /shrug. what if these aren't
        //alright. SO. these are app percentages, just, we're gonna live there
        //so since we live there
        public Vector4 Anchors;//so. Each of these will return a width or height or x or y percentage?
        //eugYHHHHH what the heck. I can't. I need. I NEED.
        //the thing defines two corners so I define two corners.
        //public Vector2 ParentedPosition
        //{
        //    //this is the top corner, and the other is the bottom, and that works, I ALWAYS HAD 2 CORNERS AUGGHHH
        //    get
        //    {
        //        if (gameObject.Parent != null)
        //        {
        //            return gameObject.Parent.GetComponent<RectangleTransform>().ParentedPosition * new Vector2(Anchors.X, Anchors.Y) + new Vector2(X, Y);
        //        }
        //        //anchors don't mean anything without a parent.
        //        return new Vector2(X, Y);
        //    }
        //}
        //public Vector2 ParentedPosition
        //{
        //    get
        //    {
        //        //alright so everything is percentages.
        //        //eAHHGHHH
                
        //        if(gameObject.Parent != null)
        //        {

                    
        //            return gameObject.GetComponent<RectangleTransform>().ParentedPosition
        //        }
        //    }
        //}
       
        public float ParentWidth
        {
            get
            {
                if(gameObject.Parent != null)
                {
                    return gameObject.Parent.GetComponent<RectangleTransform>().ParentWidth * Width;
                }
                return Width;
            }
        }

        //so when you're getting a parents position.
    }
    public class TransformComponent2D : Component
    {
        public Vector2 Position { get; set; }
        public Vector2 Scale { get; set; }
        public float Rotation { get; set; }
        public Vector4 Anchors { get; set; }
        //okay
        //so.
        //alright no.
        //public Vector2 TopRightAnchor
        //{
        //    //this doens't work does it.
        //    //alright.
        //}

        public Matrix Transform
        {
            get => Matrix.CreateScale(new Vector3(Scale, 0)) *
                Matrix.CreateRotationZ(Rotation) *
                Matrix.CreateTranslation(new Vector3(Position, 0));
        }
        public TransformComponent2D()
        {
            Position = Vector2.Zero;
            Scale = Vector2.One;
            Rotation = 0;
        }
    }
    //remove component in the long term
    //I want this to be stored seperately
    class FrameContainer : Component, INode<FrameContainer>
    {
        public TransformComponent2D transform;
        bool updateFlag = false;

        int width;
        public int Width
        {
            get
            {
                // return destRectangle.Width;
                return width;
            }
            set
            {
                //destRectangle.Width = value;
                width = value;
                updateFlag = true;
            }
        }
        int height;
        public int Height
        {
            get
            {
                //return destRectangle.Height;
                return height;
            }
            set
            {
                
                height = value;
                updateFlag = true;
            }
        }
        public int X
        {
            get
            {
                //return position.X;
                return position.X;
            }
            //set
            //{
            //    //position.X = value;
            //    //position = new Point(value, position.Y);
            //    //destRectangle.X = value;
            //    //transform.Position.X = value;
            set
            {
                transform.Position = new Vector2(value, transform.Position.Y);
                updateFlag = true;
            }  
            //}
        }
        public int Y
        {
            get
            {
                return position.Y;
                //return (int)transform.Position.Y;
            }
            set
            {
                //position.Y = value;               
                //destRectangle.Y = value;
                transform.Position = new Vector2(transform.Position.X,value );

                updateFlag = true;
            }
        }

        int leftPadding;
        public int LeftPadding
        {
            set
            {
                leftPadding = value;
                updateFlag = true;
            }
        }
        int rightPadding;
        public int RightPadding
        {
            set
            {
                rightPadding = value;
                updateFlag = true;
            }
        }
        int topPadding;
        public int TopPadding
        {
            set
            {
                topPadding = value;
                updateFlag = true;
            }
        }
        int bottomPadding;
        public int BottomPadding
        {
            set
            {
                bottomPadding = value;
                updateFlag = true;
            }
        }

        public FrameContainer Parent { get; private set; }

        public List<FrameContainer> Children { get; }

        Rectangle[] sourceRectangles;
        Rectangle[] destinationRectangles;
        Texture2D Texture;
        //Rectangle destRectangle;//I actually want.
        
        //Point position = new Point(0, 0);
        //Point position
        //{
        //    get
        //    {
        //        return destRectangle.Location;//so what helps a lot is we're removing ths, entirely
        //    }
        //    set
        //    {
        //        destRectangle.Location = value;
        //    }
        //}
        Point position
        {
            get
            {
                return transform.Position.ToPoint();
            }
            set
            {
                transform.Position = value.ToVector2();
            }
        }
        public FrameContainer(Vector4 padding, /*Rectangle window*/ int width, int height, Texture2D texture)
        {
            Children = new List<FrameContainer>();
            TopPadding = (int)padding.X;
            BottomPadding = (int)padding.Y;
            RightPadding = (int)padding.Z;
            LeftPadding = (int)padding.W;

            Width = width;
            Height = height;

            //destRectangle = window;
            Texture = texture;

            sourceRectangles = CreatePatches(Texture.Bounds);
            destinationRectangles = CreatePatches(new Rectangle(0, 0, this.width, this.height));
            Anchors = new Vector4(.5f, .5f, .5f, .5f);
        }
        public Vector4 Anchors { get; set; }
        public override void Initialize()
        {
            transform = gameObject.GetComponent<TransformComponent2D>();
            
        }
        private Rectangle[] CreatePatches(Rectangle rectangle)
        {
            var x = rectangle.X;
            var y = rectangle.Y;
            var w = rectangle.Width;
            var h = rectangle.Height;
            var middleWidth = w - leftPadding - rightPadding;
            var middleHeight = h - topPadding - bottomPadding;
            var bottomY = y + h - bottomPadding;
            var rightX = x + w - rightPadding;
            var leftX = x + leftPadding;
            var topY = y + topPadding;
            var patches = new[]
            {
        new Rectangle(x,      y,        leftPadding,  topPadding),      // top left
        new Rectangle(leftX,  y,        middleWidth,  topPadding),      // top middle
        new Rectangle(rightX, y,        rightPadding, topPadding),      // top right
        new Rectangle(x,      topY,     leftPadding,  middleHeight),    // left middle
        new Rectangle(leftX,  topY,     middleWidth,  middleHeight),    // middle
        new Rectangle(rightX, topY,     rightPadding, middleHeight),    // right middle
        new Rectangle(x,      bottomY,  leftPadding,  bottomPadding),   // bottom left
        new Rectangle(leftX,  bottomY,  middleWidth,  bottomPadding),   // bottom middle
        new Rectangle(rightX, bottomY,  rightPadding, bottomPadding)    // bottom right
    };
            return patches;
        }

        private Rectangle PositionAligned(Rectangle destination)
        {
            //destination.Location += position;
            var actualOffset = destination.Location - transform.Position.ToPoint();//this is , I'm so happy
            destination.Location = actualOffset + GetParentedPoint();///mmmmmmm
            return destination;
        }
        //these functions are operating on completely different ideals
        //one is 
        //just fucking update the rectangles with position this is not worth it.
        //wait no that wouldn't work thats not the problem the problem is updating the parented position.//so lets just 
        //okay so I have, nine rectangles with different positions, right?
        //so the thing is all of those need to be offset by a parent value.
        //so we need to get the parent chain value.
        //so active value + 
        private Point GetParentedPoint()
        {
            if(Parent == null)
            {
                //return position;
                return position;
            }
            return position + Parent.GetParentedPoint();
        }
        public Color Color { get; set; }
        //public void Draw(SpriteBatch spriteBatch,GameTime gameTime)
        //{
            
        //    if (updateFlag)
        //    {
        //        Rectangle active;
        //        if (Parent != null)
        //        {
        //            active = new Rectangle(position.X * Parent.transform.Scale.X * )
        //        }
        //        destinationRectangles = CreatePatches(new Rectangle(position.X,position.Y,width,height));
        //    }
            
        //    for (int i = 0; i < sourceRectangles.Length; i++)
        //    {                
        //        spriteBatch.Draw(Texture,PositionAligned(destinationRectangles[i]), sourceRectangles[i], Color);
        //        //so like this might want like custom effects
        //        //on each frame? so custom frames I guess.
        //    }
        //}

        public void AddChild(FrameContainer node)
        {
            node.SetParent(this);
            Children.Add(node);
             
        }

        public void SetParent(FrameContainer node)
        {
            Parent = node;
        }

        public void RemoveChild(FrameContainer node)
        {
            node.RemoveParent();
            Children.Remove(node);
        }

        public void RemoveParent()
        {
            Parent = null;
        }
    }
    //actually for both of these they shouldn't contain draw functions? or should they... :S
    class TextBox//so this would be a, child of control right? or whatever I named the class.
    {
        //so this is a, another control, that will go in windows
        //we need
        Texture2D textBox;
        Rectangle origin;
        Rectangle size;
        string text;
        SpriteFont font;//this isn't relevant, honestly.

        #region sprintstringblock
        Vector2 rotPoint, scale;
        float rotation;
        SpriteEffects effects;
        Color textColor;
        #endregion
        RasterizerState rasterState = new RasterizerState() { ScissorTestEnable = true };

        public TextBox(Texture2D tex, Rectangle drawSize,SpriteFont xFont,Color TexColor)
        {
            text = "";
            font = xFont;
            textBox = tex;
            origin = new Rectangle(0, 0, tex.Width, tex.Height);
            size = drawSize;
            textColor = TexColor;
        }
        public void Draw(SpriteBatch batch, GraphicsDevice GraphicsDevice)
        {
            var textOffset = Vector2.Max(font.MeasureString(text) - size.Size.ToVector2(), Vector2.Zero);
            GraphicsDevice.ScissorRectangle = size;
            batch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, null, null, rasterState);
            //batch.DrawString(font, text, size.Location.ToVector2(), Color.Red);
            batch.Draw(textBox, size, origin, Color.White);
            batch.DrawString(font, text, size.Location.ToVector2() - textOffset, textColor);
            batch.End();


        }
        public void Window_TextInput(object sender, TextInputEventArgs e)
        {
            if (font.Characters.Contains(e.Character))
                text += e.Character;
            if ((e.Key == Keys.Delete || e.Key == Keys.Back) && text.Length > 0)
                text = text.Remove(text.Length - 1);
            //xString += e.Character;
            if (e.Key == (Keys.A))
            {
                var hold = 0;
            }
            //if (char.IsControl(e.Character))
            //{
            //    var hold = 0;
            //}
            //so this event isn't firing on controls down at all.

            if (Keyboard.GetState().IsKeyDown(Keys.RightControl) | Keyboard.GetState().IsKeyDown(Keys.LeftControl))
            {
                var hold = 0;
            }

            if (e.Key == Keys.RightControl)
            {
                var hold = 0;
            }
        }

    }
    class NineSplitGame : CoreGame
    {
        Texture2D GuiTex;
        Texture2D BlockTexture;
        Texture2D TextBox;
        string xString = "";
        SpriteFont xFont;
        Rectangle destRect = new Rectangle(50, 50, 600, 400);
        public NineSplitGame()
        {

        }
        TextBox xBox;
        FrameContainer frame;
        FrameContainer frameChild;

        UIService uiService = new UIService();
        RectWindow ParentWindow;
        RectWindow ChildWindow;
        GameObject ParentObject;
        GameObject ChildObject;
        
        protected override void Initialize()
        {
            ParentExamp();
            base.Initialize();
        }
        protected override void LoadContent()
        {
            
            GuiTex = Content.Load<Texture2D>("DemoUiFrame");
            BlockTexture = Content.Load<Texture2D>("WhiteBoxUI");
            TextBox = Content.Load<Texture2D>("TextBoxTexture");
            xFont = Content.Load<SpriteFont>("File");
            //this.Window.TextInput += Window_TextInput;
           
            ParentWindow = new RectWindow(BlockTexture,Vector4.One,Color.White);
            ChildWindow = new RectWindow(BlockTexture, Vector4.One, Color.Green);
            ChildObject = new GameObject();
            ChildObject.AddComponent(new RectControl2());
            ChildObject.AddComponent(ChildWindow);
            ParentObject = new GameObject();            
            ParentObject.AddComponent(new RectControl2());
            ParentObject.AddComponent(ParentWindow);
            ParentObject.AddChild(ChildObject);
            uiService.UiComponents.Add(ParentWindow);
            uiService.UiComponents.Add(ChildWindow);
            
            var pTransform = ParentObject.GetComponent<RectControl2>();
            pTransform.Position = new Vector2(10, 10);
            pTransform.Size = new Vector2(500, 400);
            var cTransform = ChildObject.GetComponent<RectControl2>();
            cTransform.AnchorMin = new Vector2(0, 0);
            cTransform.AnchorMax = new Vector2(0, 0);
            cTransform.Position = new Vector2(20, 20);
            cTransform.Size = new Vector2(50, 50);
            ParentObject.Initialize();
            ChildObject.Initialize();
            //sourceRectangles = CreatePatches(GuiTex.Bounds);
            //destinationRectangles = CreatePatches(new Rectangle(0, 0, 400, 120));
            // destinationRectangles = CreatePatches(destRect);
            xBox = new TextBox(TextBox, new Rectangle(0, 0, 50, 20), xFont, Color.Black);
            Window.TextInput += xBox.Window_TextInput;
            frame = new FrameContainer(new Vector4(15, 15, 15, 15), /*destRect*/600,400, BlockTexture);
            frame.transform = new TransformComponent2D();
            frame.X = 50;
            frame.Y = 50;
            frame.Color = Color.Gray;
            frameChild = new FrameContainer(new Vector4(15, 15, 15, 15),
                /*new Rectangle(15, 15, 100, 100)*/100,100, BlockTexture);
            frameChild.transform = new TransformComponent2D();
            frameChild.Color = Color.White;
            frameChild.X = 15;
            frameChild.Y = 15;
            
            frame.AddChild(frameChild);
            base.LoadContent();
            //uiService.batch = spriteBatch;
            uiService.batch = new MemoryBatch(GraphicsDevice);
        }



        private void Window_TextInput(object sender, TextInputEventArgs e)
        {
            if (xFont.Characters.Contains(e.Character))
                xString += e.Character;
            if ((e.Key == Keys.Delete || e.Key == Keys.Back) && xString.Length > 0)
                xString = xString.Remove(xString.Length - 1);
            //xString += e.Character;
            if(e.Key == (Keys.A ))
            {
                var hold = 0;
            }
            //if (char.IsControl(e.Character))
            //{
            //    var hold = 0;
            //}
           //so this event isn't firing on controls down at all.
            
            if(Keyboard.GetState().IsKeyDown(Keys.RightControl) | Keyboard.GetState().IsKeyDown(Keys.LeftControl))
            {
                var hold = 0;
            }
            
            if(e.Key == Keys.RightControl)
            {
                var hold = 0;
            }
        }

        protected override void Update(GameTime gameTime)
        {
            if(Keyboard.GetState().IsKeyDown(Keys.Left))
            {
                frame.Width++;
                ParentWindow.Transform.Size.X++;
            }
            if(Keyboard.GetState().IsKeyDown(Keys.Right))
            {
                frame.Width--;
                ParentWindow.Transform.Size.X--;
            }
            if(Keyboard.GetState().IsKeyDown(Keys.Up))
            {
                frame.Height--;
            }
            if(Keyboard.GetState().IsKeyDown(Keys.Down))
            {
                frame.Height++;
            }
            if(Keyboard.GetState().IsKeyDown(Keys.W))
            {
                frame.Y--;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.S))
            {
                frame.Y++;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.A))
            {
                frame.X--;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.D))
            {
                frame.X++;
            }
            
            base.Update(gameTime);
        }
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.DeepPink);
            uiService.Draw(gameTime);
            spriteBatch.Begin();
            
            //frame.Draw(spriteBatch, gameTime);
            //frameChild.Draw(spriteBatch, gameTime);
            spriteBatch.DrawString(xFont, xString, Vector2.Zero, Color.Blue);
            spriteBatch.End();
            xBox.Draw(spriteBatch, GraphicsDevice);
            base.Draw(gameTime);
        }
        private void ParentExamp()
        {
            //Vector2 ParentPosition = new Vector2(0, 0);
            //Vector2 ParentSize = new Vector2(1200, 800);

            //Vector2 MinAnchor = new Vector2(.5f, .5f);
            //Vector2 MaxAnchor = new Vector2(.5f, .5f);
            //Vector2 Position = new Vector2(10, 10);
            //Vector2 Size = new Vector2(100, 30);

            ////so.the problem is
            ////alright so how do we keep the position of the object relative to the center
            ////so the normal state is
            //var activePos = ParentedPosition + Position;
            ////but if say, the size of parent changes to
            ////so what hits me is the position is relative to the anchors.
            ////so.
            //var anchorMiddle = ParentSize * MinAnchor;//okay so the percentage system probably works

            Vector2 windowSize = new Vector2(1200, 800);

            Vector2 ParentPos = new Vector2(0, 0);
            Vector2 ParentSize = new Vector2(1, 1);
            //so this means the size is, alright
            //then. 

            Vector2 Pos = new Vector2(10 / windowSize.X, 10 / windowSize.Y);//should be like .0125 and 0.01875
            Vector2 Size = new Vector2(100 / windowSize.X, 30 / windowSize.Y);
            //this isn't accurate to overall drawing but it works here
            //because this doesn't account for parent cascading.
            //but neither do the values 10 or 100 or 30
            Vector2 MinAnchor = new Vector2(.5f, .5f);
            Vector2 MaxAnchor = new Vector2(.5f, .5f);
            //Vector2 ActualPos = ParentPos * windowSize + Pos * windowSize;//okay no all percentages are relative right?that. Hmm that probably won't work long term.// no no no this doesn't have the. The anchors right?
            //so
            //so the actual position is always going to be a relative distnace from the anchor which is relative to the parent
            //so in our case its always, 
            Vector2 RelativeFromAnchorPos = Pos * windowSize - MinAnchor * windowSize;//so this works here but not, overall?//unless I get rid of percentages which honeslty I should. you start with the known position and return actual pos

            //so lets say all percentages are just from the core window.
            //and we don't have relatives at all.[;;.,m.l;/'
            //actually rle
            //LK>AJ:ALKJFD
            //this is so hard.
            //ALRIGHTR
            Vector2 ActivePos = Pos + ParentPos * MinAnchor;
            ActivePos = ActivePos * windowSize;
            Vector2 ActiveSize = Size + ParentSize * MaxAnchor;
            ActiveSize = ActiveSize * windowSize;
            var clamp = "HELP";

        }

    }
}

﻿using System;

namespace ConceptualGame
{
    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //using (var game = new Game1())
            //using (var game = new BepuExperimentGame())
            //using (var game = new EffectInheritanceForwardTest())
            //using (var game = new PhongRenderingModuleBuild())
            //using (var game = new NineSplitGame())
            //using (var game = new NineSliceRotationGame())
            using(var game = new ImGuiExampleGame())
                game.Run();
        }
    }
}

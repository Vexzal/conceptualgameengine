﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using BaseEngine;
namespace ConceptualGame
{
    class GuiScratch
    {
    }
    //so.
    //lets start with, 
    //a basic list of things I need.
    //so a gui is composed of various components
    //you have windows
    //you have buttons
    //you have different controls like
    //sliders, text fields, number fields.
    //trees, you have trees.
    //drag and drop.
    //alright lets start with
    //windows have texture libraries that determine how things are drawn.
    //you have a base model that controls render from
    //we are rendering, 2d models here so neaaat
    //so this is very different from pixel aligned things

    //alright lets start with I want boxes that are drawn relative to other boxes
    public class Canvas
    {
        //so like  the canvas is the container that positions and manages controls and windows and what not.
        public Vector2 Position { get; set; }
        public readonly Vector2 Resolution;
        public Vector2 Scale; //alright so we've got a lower anchro but lets not even, lets not even start there.
        //as this window is scaled around we like update the scale.
        Vector2 LowerCornerAnchor { get; set; }
        List<Window> controls = new List<Window>();

        public void Draw()
        {
            //draw window

            foreach (Window c in controls)
            {
                c.Draw(Matrix.CreateScale(new Vector3(Scale, 1)));//so. so.
            }
        }

        //alrightc neat so. 
    }
    //so a nine slice thing needs, the follow
    public interface INSlice
    {

    }
    public interface IFrame
    {
        int LeftPadding { get; set; }
        int RightPadding { get; set; }
        int TopPadding { get; set; }
        int BottomPadding { get; set; }
    }
    public interface IControl : INode<IControl>
    {
        Vector2 Position { get; set; }//I don't want this. I want a transform component. oh wait no this is the inteface this pulls from the transform
        Vector2 Resolution { get; set; }
        Texture2D Texture { get; set; }
        
        //Model Frame { set; }
        //IControl Parent { get; set; }
        Matrix Scale { get; set; }
        
        //List<IControl> Children { get; set; }//so this is really what I want, right?
    }
    public class Control : IControl
    {
        public Vector2 Position { get; set; }
        public Vector2 Resolution { get; set; }
        public Texture2D Texture { get; set; }
        public Model Frame { get; set; }
        public Matrix Scale { get; set; }

        public IControl Parent { get; }

        public List<IControl> Children { get; }



        public void AddChild(IControl node)
        {
            throw new NotImplementedException();
        }

        public void RemoveChild(IControl node)
        {
            throw new NotImplementedException();
        }

        public void RemoveParent()
        {
            throw new NotImplementedException();
        }

        public void SetParent(IControl node)
        {
            throw new NotImplementedException();
        }
        public void GenerateFrame(float height, float width, float xMargin, float yMargin, ref ModelMeshPart part)
        {
            
            part.VertexBuffer.SetData(new VertexPositionTexture[1] { new VertexPositionTexture(Vector3.Zero,Vector2.Zero)});
            part.IndexBuffer.SetData(new int[1] { 0 });
            part.NumVertices = 8;
            part.PrimitiveCount = 10;
            //can I do this in the effect itself.
            //alright so like, no, not really....
            //you're not just moving the uv. or. can you??
            //hmm ahhhHHHHH

        }
    }
    
    public class Window //renamed from window because I guess everything is a control. maybe???this is hard.
    {
        Vector2 Position;
        public void Draw(Matrix Scale)
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using BaseEngine;

using BEPUphysics;
using BEPUphysics.CollisionShapes;
using BEPUphysics.CollisionShapes.ConvexShapes;
using BEPUphysics.Entities;
using BEPUphysics.Entities.Prefabs;
using BVector3 = BEPUutilities.Vector3;
using BMatrix = BEPUutilities.Matrix;
using ConversionHelper;
using BU = BEPUutilities;


namespace ConceptualGame
{
    class BepuExperimentGame : BaseEngine.CoreGame
    {
        //physics
        Box PlaneBody;
        Entity CubeBody;
        BoxShape CubeShape;
        ConeShape ConeShape;
        TransformableShape DebugShape;
        //TransformableShape ConeShape;
        BoxShape ReplacementShape;
        //TransformableShape CubeShape;
        Space bSpace;

        //models 
        Model Plane;
        Model Cube;

        //
        BVector3 Gravity;
        Vector3 cubeScale;
        int cubeSize;
        Matrix View;
        Matrix Projection;
        BMatrix CubeStart;
        BU.Matrix3x3 ShapeTransform;

        //game 
        KeyboardState oldKState;
        KeyboardState newKState;

        public BepuExperimentGame()
        {
            //TransformableShape xTransformable = new TransformableShape(CubeShape, BEPUutilities.Matrix3x3.Identity);
            
        }
        
        protected override void Initialize()
        {
            bSpace = new Space();
            Gravity = new BVector3(0, 0, -9.81f);
            BU.Matrix3x3 xMatrix = BU.Matrix3x3.Identity;
            BU.Matrix3x3 sMatrix = BU.Matrix3x3.CreateScale(1, 2, 3);
            BU.Matrix3x3 scaleCompare = BU.Matrix3x3.CreateScale(3, 2, 4);
            BU.Matrix3x3 rotCompare = BU.Matrix3x3.CreateFromAxisAngle(BVector3.Normalize(new BVector3(1, 4, 2)), MathHelper.ToRadians(45));
            BU.Matrix3x3 originalMatrix = BU.Matrix3x3.CreateScale(3, 2, 4) * BU.Matrix3x3.CreateFromAxisAngle(BVector3.Normalize( new BVector3(1, 4, 2)),
                MathHelper.ToRadians(45));
            BU.Matrix3x3 decomposedScale = new BU.Matrix3x3(
                originalMatrix.Right.Length(),0,0,
                0,originalMatrix.Up.Length(),0,
                0,0,originalMatrix.Backward.Length());

            BVector3 xHat = BVector3.Normalize( originalMatrix.Right);
            BVector3 yHat = BVector3.Normalize( originalMatrix.Up);
            BVector3 zHat = BVector3.Normalize( originalMatrix.Backward);


            BU.Matrix3x3 decomposedRotation = new BU.Matrix3x3(
                xHat.X, xHat.Y, xHat.Z,
                yHat.X, yHat.Y, yHat.Z,
                zHat.X, zHat.Y, zHat.Z);
            if(scaleCompare != decomposedScale)
            {
                int fail = 1;
            }
            if(rotCompare != decomposedRotation)
            {
                int fail = 1;
            }

            int pass = 1;


            base.Initialize();
        }
        protected override void LoadContent()
        {
            cubeScale = Vector3.One;
            cubeSize = 2;

            float distance = 10;
            float startingrotation = MathHelper.ToRadians(10);
            Vector3 CameraPosition = Vector3.Transform(
                new Vector3(0, distance, 0),
                Matrix.CreateRotationX(startingrotation));
            float aspect = (float)graphics.PreferredBackBufferWidth / graphics.PreferredBackBufferHeight;
            float fov = MathHelper.ToRadians(30);
            View = Matrix.CreateLookAt(CameraPosition, Vector3.Zero, Vector3.UnitZ);
            Projection = Matrix.CreatePerspectiveFieldOfView(fov, aspect, 1, 100);

            Plane = Content.Load<Model>("Plane");
            Cube = Content.Load<Model>("Cube");

            #region shapePositions
            CubeStart = BMatrix.CreateTranslation(new BEPUutilities.Vector3(0, 0, 2));
            ShapeTransform = BU.Matrix3x3.Identity;
            #endregion
            #region Shapes

            //CubeShape = new TransformableShape(new BoxShape(2, 2, 2), BEPUutilities.Matrix3x3.Identity);
            CubeShape = new BoxShape(2, 2, 2);
            //ConeShape = new TransformableShape(
            //  new ConeShape(2, 1), BEPUutilities.Matrix3x3.CreateFromAxisAngle(BVector3.UnitY, MathHelper.ToRadians(180)));
            ConeShape = new ConeShape(2, 1);
            //ReplacementShape = new BoxShape(1, 1, 1);
            DebugShape = new TransformableShape(CubeShape, ShapeTransform);
            #endregion

            CubeBody = new Entity(DebugShape);
            CubeBody.WorldTransform = CubeStart;
            CubeBody.Mass = .1f;
            CubeBody.Gravity = Gravity;
            PlaneBody = new Box(BEPUutilities.Vector3.Zero, 8, 8, .01f);

            bSpace.Add(CubeBody);
            bSpace.Add(PlaneBody);

            base.LoadContent();
        }
        int speed = 2;
        float absoluteEpsilon = .0001f;
        BVector3 ScaleActually = new BVector3(1, 1, 1);
        BVector3 ScaleFactor = new BVector3(.1f, .1f, .1f);
        BU.Matrix3x3 scaleRate = BU.Matrix3x3.CreateScale(1, 1, 1);
        BU.Matrix3x3 maxScale = BU.Matrix3x3.CreateScale(1, 1, 1);
        BU.Matrix3x3 minScale = BU.Matrix3x3.CreateScale(.1f, .1f, .1f);
        protected override void Update(GameTime gameTime)
        {
            oldKState = newKState;
            newKState = Keyboard.GetState();
            
            if (newKState.IsKeyDown(Keys.Space) && (Math.Abs(DebugShape.Transform.Forward.Length() - .1f) >= absoluteEpsilon))//DebugShape.Transform.Forward.Length() > .1f)//&& cubeScale.X > .1f)
            {

                //DebugShape.Transform *= (float)(.5f * (float)gameTime.ElapsedGameTime.TotalSeconds);
                //ScaleActually -= ScaleFactor * (float)gameTime.ElapsedGameTime.TotalSeconds * speed;
                //DebugShape.Transform = BUs.Matrix3x3.CreateScale(ref ScaleActually);
                DebugShape.Transform -= minScale * (float)gameTime.ElapsedGameTime.TotalSeconds * speed;
                //scaleRate *= (float)gameTime.ElapsedGameTime.TotalSeconds;
                //cubeScale -= Vector3.One * .1f * (float)gameTime.ElapsedGameTime.TotalSeconds * speed;
                //AdjustCubeSize();
            }
            if (newKState.IsKeyUp(Keys.Space) && (Math.Abs(DebugShape.Transform.Forward.Length() - 1f) >= absoluteEpsilon) && newKState.IsKeyUp(Keys.K))//cubeScale.X < 1 && newKState.IsKeyUp(Keys.K))
            {
                //
                
                //DebugShape.Transform *= (float)(2 * (float)gameTime.ElapsedGameTime.TotalSeconds);
                //ScaleActually += ScaleFactor * (float)gameTime.ElapsedGameTime.TotalSeconds * speed;
                //DebugShape.Transform = BUs.Matrix3x3.CreateScale(ref ScaleActually);
                DebugShape.Transform += minScale * (float)gameTime.ElapsedGameTime.TotalSeconds * speed;
                //cubeScale += Vector3.One * .1f * (float)gameTime.ElapsedGameTime.TotalSeconds * speed;
                //AdjustCubeSize();
            }
            if (newKState.IsKeyDown(Keys.K) && oldKState.IsKeyUp(Keys.K))
            {
                DebugShape.Transform = BU.Matrix3x3.CreateScale(.1f);
                //cubeScale = new Vector3(.1f, .1f, .1f);
                //AdjustCubeSize();
            }
            if(newKState.IsKeyUp(Keys.K) && oldKState.IsKeyDown(Keys.K))
            {
                DebugShape.Transform = BU.Matrix3x3.CreateScale(1);
                //cubeScale = new Vector3(1, 1, 1);
                //AdjustCubeSize();
            }
            if(newKState.IsKeyDown(Keys.L) && oldKState.IsKeyUp(Keys.L ))
            {
                DebugShape.Shape = ConeShape;
                DebugShape.Transform = BU.Matrix3x3.CreateFromAxisAngle(BVector3.UnitY, MathHelper.ToRadians(179));
                //CubeShape = ReplacementShape;
            }
            if(newKState.IsKeyUp(Keys.L) && oldKState.IsKeyDown(Keys.L))
            {                
                DebugShape.Shape = CubeShape;
                DebugShape.Transform = BU.Matrix3x3.Identity;
            }
            bSpace.Update((float)gameTime.ElapsedGameTime.TotalSeconds);
            base.Update(gameTime);
        }
        void AdjustCubeSize()
        {
            CubeShape.Width = cubeScale.X * cubeSize;
            CubeShape.Height = cubeScale.Y * cubeSize;
            CubeShape.Length = cubeScale.Z * cubeSize;
        }
        void CubeInidiate()
        {
            CubeShape = new BoxShape(
                    cubeSize * cubeScale.X,
                    cubeSize * cubeScale.Y,
                    cubeSize * cubeScale.Z);
        }

        Color backgroundColor = Color.DeepPink;
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(backgroundColor);
            DrawModel(Cube,
                //Matrix.CreateScale(cubeScale) * Convert(CubeBody.WorldTransform),
                MathConverter.Convert(DebugShape.Transform) * MathConverter.Convert(CubeBody.WorldTransform),
                View,
                Projection);
            DrawModel(Plane,
                //Convert(PlaneBody.WorldTransform),
                MathConverter.Convert(PlaneBody.WorldTransform),
                View,
                Projection);


            base.Draw(gameTime);
        }
        void DrawModel(Model m,Matrix World, Matrix View, Matrix Projection)
        {
            foreach(ModelMesh mesh in m.Meshes)
            {
                foreach(BasicEffect effect in mesh.Effects)
                {
                    effect.World = World;
                    effect.View = View;
                    effect.Projection = Projection;
                    effect.EnableDefaultLighting();
                }
                mesh.Draw();
            }
        }

        BMatrix Convert(Matrix m)
        {
            return new BMatrix(
                m.M11, m.M12, m.M13, m.M14,
                m.M21, m.M22, m.M23, m.M24,
                m.M31, m.M32, m.M33, m.M34,
                m.M41, m.M42, m.M43, m.M44);
        }
        Matrix Convert(BMatrix m)
        {
            return new Matrix(
                m.M11, m.M12, m.M13, m.M14,
                m.M21, m.M22, m.M23, m.M24,
                m.M31, m.M32, m.M33, m.M34,
                m.M41, m.M42, m.M43, m.M44);
        }
    }
}


﻿using System;
using System.Dynamic;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseEngine;
using BaseEngine.Rendering;
using BaseEngine.Components;
using BaseEngine.BepuPhysics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using Microsoft.CSharp;

namespace ConceptualGame
{
    
    public struct Asset
    {
        public string Name;
        public string Path;
        public AssetTag Tag;
        public Asset(string name,string path, AssetTag tag)
        {
            Name = name;
            Path = path;
            Tag = tag;
        }
    }
    public class AssetModule
    {
        ContentManager Content { get; set; }
        List<Asset> Assets = new List<Asset>();//this could probably be an array
        Dictionary<string, Asset> AssetCollection = new Dictionary<string, Asset>();//sooo. 
        //so the content manager stores everything already by default it does the library functionality I already wanted.
        //so the module is defining a specified library
        //so.
        //we can init. 
        public AssetModule(ContentManager content)
        {
            Content = content;
        }
        public void InitLoad()
        {
            foreach (Asset a in AssetCollection.Values)
            {
                a.Tag.Load(a.Path, Content);//this should load in all the types
            }
        }
        public dynamic this[string name]
        {
            get
            {
                if (AssetCollection.ContainsKey(name))
                {
                    return AssetCollection[name].Tag.Load(AssetCollection[name].Path, Content);
                }
                return null;
            }
        }
        public void Unload()
        {
            Content.Unload();
        }
        public void AddAsset(Asset asset)
        {
            //name should be contained in the thing too.
            AssetCollection.Add(asset.Name, asset);
        }

        //so we need a list of asset names
    }
    public interface AssetTag
    {
        dynamic Load(string name, ContentManager content);
    }
    public abstract class AssetTagC
    {
        public abstract object Load(string name);
    }
    public class ModelAsset : AssetTag
    {
        dynamic AssetTag.Load(string name, ContentManager content)
        {
            return content.Load<Model>(name);//lets. so.
        }
    }
    public class TextureAsset : AssetTag
    {
        Microsoft.Xna.Framework.Content.ContentManager manager;//what if this is just passed to the thing maybe? hmm.
        public Texture2D Load(string name)
        {
            return manager.Load<object>(name) as Texture2D;
        }
        //this doesn't work
        dynamic AssetTag.Load(string name, ContentManager content)
        {
            return content.Load<Texture2D>(name);
            //return Load(name);
        }
    }
    public class EffectAsset : AssetTag
    {
        dynamic AssetTag.Load(string name, ContentManager content)
        {
            return content.Load<Effect>(name);
        }
    }
    class GameState
    {
        //this is where things are complicated.

    }
    class PhongRenderingModuleBuild : CoreGame
    {
        //so I won't need all of these things below because I'm replacing them
        
        AssetModule Assets;

        //Model Ship;
        //Effect ShipEffect;
        //Texture2D ShipColor;
        //Texture2D ShipNormals;
        //Texture2D DecalColor;
        //Texture2D DecalNormals;

        PhongPipelineModules.DrawSceneModule mDrawModule;
        
        //everything else is lights and positions and such
        

        protected override void Initialize()
        {
            EngineServices = new ServiceManager();
            EngineServices.Renderer = new RenderingService(this);
            EngineServices.Renderer.primaryModule = PhongPipelineModules.PhongPipelinePrefab.PhongPrefab(this);

            EngineServices.physics = new BaseEngine.BepuPhysics.BepuPhysicsService(this);
            //EngineServices.physics.Module = new BepuPhysicsModule();
            //renderer.primaryModule = PhongPipelineModules.PhongPipelinePrefab.PhongPrefab(this);
            //renderer.primaryModule = PhongPipelineModules.PhongPipelinePrefab.DebugPrefab(this);
            base.Initialize();
        }
        bool loaded = false;
        protected override void LoadContent()
        {
            Assets = new AssetModule(new ContentManager(Content.ServiceProvider, Content.RootDirectory));
            Assets.AddAsset(
                new Asset(
                    "ShipModel",
                    @"DeferredModel\shipExport3",
                    new ModelAsset()));
            Assets.AddAsset(
                new Asset(
                    "ShipEffect",
                    @"ModelEffects\ShipModelEffect_DeferredPhong",
                    new EffectAsset()
               ));
            Assets.AddAsset(
                new Asset(
                    "ShipDiffuseTexture",
                    @"DeferredModel\ship1_c",
                    new TextureAsset()
                    ));
            Assets.AddAsset(
                new Asset(
                    "ShipNormalTexture",
                    @"DeferredModel/ship1_n",
                    new TextureAsset()
                    ));
            Assets.AddAsset(
                new Asset(
                    "DecalColor",
                    @"DeferredModel\DecalAlbedo",
                    new TextureAsset()
                    ));
            Assets.AddAsset(
                new Asset(
                    "DecalNormal",
                    @"DeferredModel\DecalNormal",
                    new TextureAsset()
                    ));

            BaseEngine.Rendering.GenericRenderPipeline xVal = (GenericRenderPipeline)EngineServices.renderer.primaryModule;
            foreach(IRenderingModule module in xVal.Modules)
            {
                if(module is PhongPipelineModules.DrawSceneModule)
                {
                    mDrawModule = (PhongPipelineModules.DrawSceneModule)module;
                }
            }
            //yaayy
            //Ship = Content.Load<Model>(@"DeferredModel\shipExport3");
            //ShipEffect = Content.Load<Effect>(@"ModelEffects\ShipModelEffect_DeferredPhong");
            //ShipColor = Content.Load<object>(@"DeferredModel\ship1_c") as Texture2D;
            //object xl = Content.Load<object>(@"DeferredModel\ship1_c");//okay. oookay.
            //ShipColor = xl as Texture2D;
            //ShipNormals = Content.Load<Texture2D>(@"DeferredModel/ship1_n");
            //DecalColor = Content.Load<Texture2D>(@"DeferredModel\DecalAlbedo");
            //DecalNormals = Content.Load<Texture2D>(@"DeferredModel\DecalNormal");
            gameScene = new Scene("actScene");
            //renderer.gameScene = gameScene;
            //physics.gameScene = gameScene;
            GenerateGameOjects();
            gameScene.Initial();
            //renderer.Load();
            loaded = true;
            base.LoadContent();
            EngineServices.renderer.Load(gameScene);
            mDrawModule.spriteBatch = spriteBatch;
        }
        protected override void UnloadContent()
        {
            base.UnloadContent();
        }
        protected override void Update(GameTime gameTime)
        {
            oldState = newState;
            newState = Keyboard.GetState();

            if(Press(Keys.Left))
            {
                mDrawModule.Decriment();
            }
            if(Press(Keys.Right))
            {
                mDrawModule.Incriment();
            }

            base.Update(gameTime);
        }
        KeyboardState oldState;
        KeyboardState newState;
        protected bool Press(Keys key)
        {
            if (oldState.IsKeyUp(key) && newState.IsKeyDown(key))
                return true;
            return false;
        }
        protected override void Draw(GameTime gameTime)
        {
            //if (loaded)
            {
                EngineServices.renderer.Draw(gameTime);
            }
            //GraphicsDevice.Clear(Color.Blue);
            //foreach(ModelMesh mesh in Ship.Meshes)

            //{
            //    foreach(var part in mesh.MeshParts)
            //    {
            //        part.Effect = ShipEffect;
            //        part.Effect.Parameters["World"].SetValue(shipTransform.GetParentedTransform());
            //        part.Effect.Parameters["View"].SetValue(camera.View);
            //        part.Effect.Parameters["Projection"].SetValue(camera.Projection);
                    
            //    }
            //    mesh.Draw();
            //}

            base.Draw(gameTime);
        }
        ICamera camera;
        TransformComponent shipTransform;
        protected void GenerateGameOjects()
        {
            GameObject ShipOject = new GameObject();
            ShipOject.AddComponent(
                new MeshComponent()
                {
                    mesh = Assets["ShipModel"]
                });
            ShipOject.AddComponent(
                new EffectComponent
                {
                    Effect = Assets["ShipEffect"]//ShipEffect
                });
            ShipOject.GetComponent<EffectComponent>().SetValue("Texture", Assets["ShipDiffuseTexture"]);
            ShipOject.GetComponent<EffectComponent>().SetValue("NormalMap", Assets["ShipNormalTexture"]);
            //add ship textures
            shipTransform = ShipOject.GetComponent<TransformComponent>();
            
            GameObject DecalObject = new GameObject();
            DecalObject.AddComponent(
                new DecalComponent()//components should have default constructors. //i'll get back to that.
                {
                    decalParams = new List<DecalTextureBinding>()
                    {
                        new DecalTextureBinding()
                        {
                            ParamName = "DecalColor",
                            Texture = Assets["DecalColor"]
                        },
                        new DecalTextureBinding()
                        {
                            ParamName = "NormalMap",
                            Texture = Assets["DecalNormal"]
                        }
                    }
                }                
            );            
            DecalObject.GetComponent<TransformComponent>().Position = new Vector3(1.4f, -1.4f, .2f);
            DecalObject.GetComponent<TransformComponent>().Scale =  new Vector3(.6f, .6f, .3f);

            GameObject DirectionalLight = new GameObject();
            DirectionalLight.AddComponent(
                new DirectionalLightComponent()
                {
                    Color = Color.White,
                });
            DirectionalLight.GetComponent<TransformComponent>().Rotation = Quaternion.CreateFromAxisAngle(Vector3.UnitY, MathHelper.ToRadians(45));

            GameObject PointLight = new GameObject();
            PointLight.AddComponent(
                new PointLightComponent()
                {
                    Color = Color.Red,
                    Intensity = 10,
                    Radius = 1           
                });
            shipTransform = PointLight.GetComponent<TransformComponent>();
            PointLight.GetComponent<TransformComponent>().Position = new Vector3(0, 1, 0);
            GameObject CameraObject = new GameObject();
            CameraObject.AddComponent(
                new Camera()
                {
                    target = ShipOject.GetComponent<TransformComponent>(),
                    perspective = true,
                    nearClipPlane = 1,
                    farClipPlane = 100,
                    aspect = GraphicsDevice.Viewport.AspectRatio,
                    fov = MathHelper.ToRadians(30),
                    Up = Vector3.UnitZ                   
                });
            CameraObject.GetComponent<TransformComponent>().Position = new Vector3(6, 6, 6);
            gameScene.AddGameObjects(ShipOject, DecalObject, DirectionalLight, PointLight, CameraObject);
            camera = CameraObject.GetComponent<Camera>();
        }
    }
    //okay
    //heres where things are going to suck
    //so this was in reference to writing the modules but i just copied the ones I already wrote because of course, really
}

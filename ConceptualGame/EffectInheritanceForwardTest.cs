﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using BaseEngine;
using BaseEngine.Components;
using BaseEngine.Rendering;
using BaseEngine.Extensions;
using System.Collections;

using RenderTargetLookup = BaseEngine.Rendering.RenderTargetManager.LookupContainer<Microsoft.Xna.Framework.Graphics.RenderTargetBinding>;
namespace ConceptualGame
{
    class EffectInheritanceForwardTest : Game
    {
        
        public SpriteBatch spriteBatch;
        public GraphicsDeviceManager graphics;

        public SpriteFont font;

        GameObject ShipObject = new GameObject() {Name = "Ship" };
        GameObject DecalObject = new GameObject() { Name = "Decal" };

        public Model Decal;
        public Texture2D DecalColor;
        public Texture2D DecalNormal;
        public Effect DecalEffect;

        public Model ship;
        public Texture2D shipTexture;
        public Texture2D shipNormal;
        public DirectionalLightSolo Sun;
        public Vector2 halfPixel;

        public Effect ShipEffect;
        public Effect shipForward;
        Effect ClearGBuffer;
        Effect DirectionalLightEffect;        
        EffectPass DirectionalLightPass;        
        Effect PointLightEffect;
        EffectPass PointLightPass;
        Effect FinalComposite;
        EffectPass FinalCompositePass;
        public Effect Constants;

        public TransformComponent transform; //= new TransformComponent();
        public TransformComponent decalTransform; //= new TransformComponent();
        public EditorCamera camera;

        Model PointSphere;

        RenderTarget2D colorRT;
        RenderTarget2D normalRT;
        RenderTarget2D depthRT;
        RenderTarget2D lightRT;
        RenderTarget2D finalRT;

        BlendState lightBlendState = new BlendState
        {
            AlphaSourceBlend = Blend.One,
            ColorSourceBlend = Blend.One,
            ColorDestinationBlend = Blend.One,
            AlphaDestinationBlend = Blend.One
        };

        public EffectInheritanceForwardTest()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }
        protected override void Initialize()
        {
            defaultState = GraphicsDevice.RasterizerState;
            defaultBlend = GraphicsDevice.BlendState;
            graphics.PreferredBackBufferWidth = graphics.PreferredBackBufferWidth * 2;
            graphics.PreferredBackBufferHeight = graphics.PreferredBackBufferHeight * 2;
            graphics.ApplyChanges();
            base.Initialize();
        }
        ShipVertex[] vertices;
        int[] indices;
        int backBufferWidth;
        int backBufferHeight;
        protected override void LoadContent()
        {
            transform = ShipObject.GetComponent<TransformComponent>();
            decalTransform = DecalObject.GetComponent<TransformComponent>();

            ShipObject.AddChild(DecalObject);
            
            spriteBatch = new SpriteBatch(GraphicsDevice);
            backBufferWidth = GraphicsDevice.PresentationParameters.BackBufferWidth;
            backBufferHeight = GraphicsDevice.PresentationParameters.BackBufferHeight;            

            Sun = new DirectionalLightSolo();
            Sun.Direction = Vector3.Normalize(new Vector3(1, 1, -1));            

            colorRT = new RenderTarget2D(GraphicsDevice, backBufferWidth, backBufferHeight, false, SurfaceFormat.Color, DepthFormat.Depth16);
            normalRT = new RenderTarget2D(GraphicsDevice, backBufferWidth, backBufferHeight, false, SurfaceFormat.Color, DepthFormat.Depth16);
            depthRT = new RenderTarget2D(GraphicsDevice, backBufferWidth, backBufferHeight, false, SurfaceFormat.Single, DepthFormat.Depth16);
            lightRT = new RenderTarget2D(GraphicsDevice, backBufferWidth, backBufferHeight, false, SurfaceFormat.Color, DepthFormat.Depth24Stencil8);
            finalRT = new RenderTarget2D(GraphicsDevice, backBufferWidth, backBufferHeight, false, SurfaceFormat.Color, DepthFormat.Depth16);

            halfPixel = new Vector2();
            halfPixel.X = 0.5f / (float)backBufferWidth;
            halfPixel.Y = 0.5f / (float)backBufferHeight;

            var x = sizeof(int);
            
            //transform.gameObject = new GameObject() { name = "Ship" };
            transform.Scale = new Vector3(1f, 1f, 1f);
            //transform.Rotation = Quaternion.Concatenate(Quaternion.CreateFromAxisAngle(Vector3.UnitX, MathHelper.ToRadians(90)), Quaternion.CreateFromAxisAngle(Vector3.UnitZ, MathHelper.ToRadians(180)));

            decalTransform.gameObject = new GameObject() { Name = "Decal"};
            //decalTransform.Position = new Vector3(.307f,-.306f,.033f);
            //decalTransform.Scale = new Vector3(.108f, .108f, .082f);
            //decalTransform.Position = new Vector3(14, -14, 1.5f);
            //decalTransform.Scale = new Vector3(3);
            decalTransform.Position = new Vector3(1.4f, -1.4f,.2f);
            decalTransform.Scale = new Vector3(.6f, .6f, .3f);

            //decalTransform.gameObject.SetParent(transform.gameObject);
            transform.gameObject.AddChild(decalTransform.gameObject);

            Decal = Content.Load<Model>(@"DeferredModel\DecalModel");
            DecalColor = Content.Load<Texture2D>(@"DeferredModel\DecalAlbedo");
            DecalNormal = Content.Load<Texture2D>(@"DeferredModel\DecalNormal");
            DecalEffect = Content.Load<Effect>(@"ModelEffects\TestDecal2");

            Constants = Content.Load<Effect>(@"ModelEffects\EngineConstants");
            //ship = Content.Load<Model>(@"DeferredModel\ship1Converted");
            ship = Content.Load<Model>(@"DeferredModel\shipExport3");
            PointSphere = Content.Load<Model>(@"PointSphereModel");
            shipTexture = Content.Load<Texture2D>(@"DeferredModel\ship1_c");
            shipNormal = Content.Load<Texture2D>(@"DeferredModel/ship1_n");
            ShipEffect = Content.Load<Effect>(@"ModelEffects\ShipModelEffect_DeferredPhong");
            shipForward = Content.Load<Effect>(@"ModelEffects\PhongForward");
            ClearGBuffer = Content.Load<Effect>(@"GBufferEffects\ClearGBufferPhong");
            DirectionalLightEffect = Content.Load<Effect>(@"GBufferEffects\DirectionalLight");
            DirectionalLightPass = DirectionalLightEffect.CurrentTechnique.Passes[0];
            DirectionalLightEffect.Parameters["halfPixel"].SetValue(halfPixel);
            PointLightEffect = Content.Load<Effect>(@"GBufferEffects\PointLight");
            PointLightPass = PointLightEffect.CurrentTechnique.Passes[0];
            PointLightEffect.Parameters["halfPixel"].SetValue(halfPixel);
            FinalComposite = Content.Load<Effect>(@"GBufferEffects\FinalColor");
            FinalCompositePass = FinalComposite.CurrentTechnique.Passes[0];
            FinalComposite.Parameters["halfPixel"].SetValue(halfPixel);

            font = Content.Load<SpriteFont>("File");
            ShipEffect.Parameters["Texture"].SetValue(shipTexture);
            ShipEffect.Parameters["NormalMap"].SetValue(shipNormal);
            shipForward.Parameters["Texture"].SetValue(shipTexture);
            //foreach (ModelMesh mesh in ship.Meshes)
            //{
            //    foreach(ModelMeshPart part in mesh.MeshParts)
            //    {
            //        vertices = new ShipVertex[part.VertexBuffer.VertexCount];
            //        indices = new int[part.IndexBuffer.IndexCount];
            //        part.VertexBuffer.GetData<ShipVertex>(vertices);
            //        part.IndexBuffer.GetData<int>(indices);
            //    }
            //}
            camera = new EditorCamera(GraphicsDevice, 10);
            camera.aspect = (float)graphics.PreferredBackBufferWidth / graphics.PreferredBackBufferHeight;
            camera.FOV = MathHelper.ToRadians(30);
            camera.Up = Vector3.UnitZ;
            camera.farClipPlane = 101;
            camera.nearClipPlane = .1f;
            base.LoadContent();
        }
        protected override void UnloadContent()
        {
            base.UnloadContent();
        }
        
        bool specular = true;
        bool RenderingStyle = true;
        KeyboardState oldstate;
        KeyboardState state;
        protected override void Update(GameTime gameTime)
        { 
            oldstate = state;
            state = Keyboard.GetState();

            if (oldstate.IsKeyUp(Keys.Space) && state.IsKeyDown(Keys.Space))
                RenderingStyle = !RenderingStyle;
            if(oldstate.IsKeyUp(Keys.Left) && state.IsKeyDown(Keys.Left))
            {
                if (targetDraw == 0)
                {
                    targetDraw = 4;
                }
                else
                {
                    targetDraw--;
                    targetDraw %= 5;
                }
            }
            if (oldstate.IsKeyUp(Keys.Right) && state.IsKeyDown(Keys.Right))
            {
                targetDraw++;
                targetDraw %= 5;
            }
            if (oldstate.IsKeyUp(Keys.D1) && state.IsKeyDown(Keys.D1))
            {
                targetDraw = 0;
            }
            if (oldstate.IsKeyUp(Keys.D2) && state.IsKeyDown(Keys.D2))
            {
                targetDraw = 1;
            }
            if (oldstate.IsKeyUp(Keys.D3) && state.IsKeyDown(Keys.D3))
            {
                targetDraw = 2;
            }
            if (oldstate.IsKeyUp(Keys.D4) && state.IsKeyDown(Keys.D4))
            {
                targetDraw = 3;
            }
            if(oldstate.IsKeyUp(Keys.D5) && state.IsKeyDown(Keys.D5))
            {
                targetDraw = 4;
            }
            if(oldstate.IsKeyUp(Keys.K) && state.IsKeyDown(Keys.K))
            {
                specular = !specular;
                FinalComposite.Parameters["specular"].SetValue(specular);
            }
            if (state.IsKeyUp(Keys.Q))
            {
                transform.Rotation = Quaternion.Concatenate(transform.Rotation, Quaternion.CreateFromAxisAngle(Vector3.UnitZ, (float)MathHelper.ToRadians(15) * (float)gameTime.ElapsedGameTime.TotalSeconds));
            }
            base.Update(gameTime);            
        }

        RasterizerState defaultState;
        BlendState defaultBlend;
        VertexPositionTexture[] verts =
            new VertexPositionTexture[4] {
                new VertexPositionTexture(new Vector3(-1,-1,0), new Vector2(0,1)),
                new VertexPositionTexture(new Vector3(-1,1,0), new Vector2(0,0)),
                new VertexPositionTexture(new Vector3(1,-1,0), new Vector2(1,1)),
                new VertexPositionTexture(new Vector3(1,1,0), new Vector2(1,0))};
        short[] ind = new short[6] { 0, 1, 2, 2, 1, 3 };

        VertexPositionTexture[] verts2 =
            new VertexPositionTexture[3] {
                new VertexPositionTexture(new Vector3(-1,-1,1), new Vector2(0,0)),
                new VertexPositionTexture(new Vector3(-1,1,1), new Vector2(0,1)),
                new VertexPositionTexture(new Vector3(1,-1,1), new Vector2(1,0)) };
                //new VertexPositionTexture(new Vector3(1,1,0), new Vector2(1,1))};
        short[] ind2 = new short[3] { 0, 1, 2};
        VertexPosition[] verts3 =
            new VertexPosition[2]
            {
                new VertexPosition(new Vector3(0,0,0)),
                new VertexPosition(new Vector3(0,0,0))
            };
        short[] ind3 = new short[2] { 0, 1 };

        protected override void Draw(GameTime gameTime)
        {
            if(RenderingStyle)
            {
                DrawDeferred(gameTime);
            }
            else
            {
                DrawForward(gameTime);
            }
            
            base.Draw(gameTime);
        }
        public void DrawForward(GameTime gameTime)
        {
            GraphicsDevice.RasterizerState = defaultState;
            GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            GraphicsDevice.Clear(Color.Black);
            shipForward.Parameters["WorldViewProjection"].SetValue(transform.GetParentedTransform() * camera.View * camera.Projection);
            foreach(ModelMesh mesh in ship.Meshes)
            {
                foreach(ModelMeshPart part in mesh.MeshParts)
                {
                    part.Effect = shipForward;
                }
                mesh.Draw();
            }
        }
        int targetDraw = 4;
        public void DrawDeferred(GameTime gameTime)
        {
            //GraphicsDevice.Clear(Color.DeepPink);
            //Constants.Parameters["WorldViewProjection"].SetValue(transform.AbsoluteTransform * camera.View * camera.Projection);

            GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;
            GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            GraphicsDevice.BlendState = BlendState.Opaque;
            //GraphicsDevice.SetRenderTargets(colorRT, normalRT, depthRT);
            
            
            GraphicsDevice.SetRenderTargets(colorRT, normalRT, depthRT);
            //ClearGBuffer.Techniques[0].Passes[0].Apply();
            //GraphicsDevice.DrawUserIndexedPrimitives<VertexPositionTexture>(
            //    PrimitiveType.TriangleList, verts, 0, 4, ind, 0, 2);

            
            //drawing
            ShipEffect.Parameters["World"].SetValue(transform.GetParentedTransform());
            ShipEffect.Parameters["View"].SetValue(camera.View);
            ShipEffect.Parameters["Projection"].SetValue(camera.Projection);
            //ShipEffect.CurrentTechnique.Passes[0].Apply();
            //GraphicsDevice.DrawUserIndexedPrimitives(
            //    PrimitiveType.TriangleList, vertices, 0, vertices.Length, indices, 0, indices.Length / 3);

            foreach (ModelMesh mesh in ship.Meshes)
            {
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    part.Effect = ShipEffect;
                    //part.Effect.Parameters["WorldViewProjection"].SetValue(transform.AbsoluteTransform * camera.View * camera.Projection);
                }
                mesh.Draw();
            }
            //so do decals HERE DECALS HERE
            //GraphicsDevice.SetRenderTargets(colorRT, normalRT);
            GraphicsDevice.DepthStencilState = DepthStencilState.DepthRead;
            GraphicsDevice.BlendState = BlendState.AlphaBlend;
            GraphicsDevice.BlendFactor = new Color(0, 0, 0, 1);
            //GraphicsDevice.BlendState = BlendState.AlphaBlend;
            //I think thats riiight. uhhh
            DecalEffect.Parameters["World"].SetValue(decalTransform.GetParentedTransform());
            DecalEffect.Parameters["View"].SetValue(camera.View);
            DecalEffect.Parameters["Projection"].SetValue(camera.Projection);
            DecalEffect.Parameters["InverseViewProjection"].SetValue(Matrix.Invert(camera.View * camera.Projection));
            DecalEffect.Parameters["InverseWorld"].SetValue(Matrix.Invert(decalTransform.GetParentedTransform()));

            DecalEffect.Parameters["depthMap"].SetValue(depthRT);
            DecalEffect.Parameters["DecalColor"].SetValue(DecalColor);
            DecalEffect.Parameters["NormalMap"].SetValue(DecalNormal);
            foreach(var mesh in Decal.Meshes)
            {
                foreach(var part in mesh.MeshParts)
                {
                    part.Effect = DecalEffect;
                }
                mesh.Draw();
            }

            GraphicsDevice.SetRenderTarget(lightRT);//then we set lights yayy
            GraphicsDevice.BlendState = lightBlendState;

            //DrawPointLight(new Vector3(15 * (float)Math.Sin(gameTime.TotalGameTime.TotalSeconds), 15 * (float)Math.Cos(gameTime.TotalGameTime.TotalSeconds), 4), Color.Red, 15, 4);

            DirectionalLightEffect.Parameters["lightDirection"].SetValue(Sun.Direction);
            DirectionalLightEffect.Parameters["lightColor"].SetValue(Sun.Color.ToVector3());
            DirectionalLightEffect.Parameters["cameraPosition"].SetValue(camera.Position);
            DirectionalLightEffect.Parameters["InvertViewProjection"].SetValue(Matrix.Invert(camera.View * camera.Projection));
            DirectionalLightEffect.Parameters["colorMap"].SetValue(colorRT);
            DirectionalLightEffect.Parameters["normalMap"].SetValue(normalRT);
            DirectionalLightEffect.Parameters["depthMap"].SetValue(depthRT);           
            DirectionalLightPass.Apply();
            GraphicsDevice.DrawUserIndexedPrimitives<VertexPositionTexture>(
               PrimitiveType.TriangleList, verts, 0, 4, ind, 0, 2);
            DrawPointLight(new Vector3(15 * (float)Math.Sin(gameTime.TotalGameTime.TotalSeconds), 15 * (float)Math.Cos(gameTime.TotalGameTime.TotalSeconds),4), Color.Red, 15, 4);
            //DrawPointLight(new Vector3(0, 0, 2), Color.Red, 5, 4);

            GraphicsDevice.SetRenderTarget(finalRT);
            GraphicsDevice.BlendState = BlendState.Opaque;
            FinalComposite.Parameters["colorMap"].SetValue(colorRT);
            FinalComposite.Parameters["lightMap"].SetValue(lightRT);
            FinalCompositePass.Apply();

            GraphicsDevice.DrawUserIndexedPrimitives<VertexPositionTexture>(
                PrimitiveType.TriangleList, verts, 0, 4, ind, 0, 2);

            

            GraphicsDevice.SetRenderTarget(null);
            //GraphicsDevice.Clear(Color.LightPink);
            spriteBatch.Begin();
          
            if(targetDraw == 0)
            {
                spriteBatch.Draw(colorRT, Vector2.Zero, Color.White);
                spriteBatch.DrawString(font, "Deferred: Color", Vector2.Zero, Color.White);
            }
            if (targetDraw == 1)
            {
                spriteBatch.Draw(normalRT, Vector2.Zero /* - new Vector2(backBufferWidth/4,0)*/, Color.White);
                spriteBatch.DrawString(font, "Deferred: Normal", Vector2.Zero, Color.White);
            }
            if(targetDraw == 2)
            {
                spriteBatch.Draw(depthRT, Vector2.Zero, Color.White);
                spriteBatch.DrawString(font, "Deferred: Depth", Vector2.Zero, Color.White);
            }
            if(targetDraw == 3)
            {
                spriteBatch.Draw(lightRT, Vector2.Zero, Color.White);
                spriteBatch.DrawString(font, "Deferred: Light", Vector2.Zero, Color.White);
            }
            if(targetDraw == 4)
            {
                spriteBatch.Draw(finalRT, Vector2.Zero, Color.White);
                spriteBatch.DrawString(font, "Deferred: Composite      Specular: " + specular,Vector2.Zero,Color.White);
            }
            spriteBatch.End();
            /**
            //foreach (ModelMesh mesh in ship.Meshes)
            //{
            //    foreach (BasicEffect effect in mesh.Effects)
            //    {
            //        effect.World = transform.AbsoluteTransform;
            //        effect.View = camera.View;
            //        effect.Projection = camera.Projection;
            //        effect.Texture = shipTexture;
            //    }
            //    mesh.Draw();
            //} */
        }
        public void DrawPointLight(Vector3 lightPosition, Color color, float lightRadius, float lightIntensity)
        {
            PointLightEffect.Parameters["colorMap"].SetValue(colorRT);
            PointLightEffect.Parameters["normalMap"].SetValue(normalRT);
            PointLightEffect.Parameters["depthMap"].SetValue(depthRT);

            Matrix mMatrix = Matrix.CreateScale(lightRadius) * Matrix.CreateTranslation(lightPosition);
            PointLightEffect.Parameters["World"].SetValue(mMatrix);
            PointLightEffect.Parameters["View"].SetValue(camera.View);
            PointLightEffect.Parameters["Projection"].SetValue(camera.Projection);

            PointLightEffect.Parameters["lightPosition"].SetValue(lightPosition);

            PointLightEffect.Parameters["lightRadius"].SetValue(lightRadius);
            PointLightEffect.Parameters["lightIntensity"].SetValue(lightIntensity);
            PointLightEffect.Parameters["Color"].SetValue(color.ToVector3());
            PointLightEffect.Parameters["cameraPosition"].SetValue(camera.Position);
            PointLightEffect.Parameters["InvertViewProjection"].SetValue(Matrix.Invert(camera.View * camera.Projection));

            float cameraToCenter = Vector3.Distance(camera.Position, lightPosition);
            if(cameraToCenter<lightRadius)
            {
                GraphicsDevice.RasterizerState = RasterizerState.CullClockwise;
            }
            else
            {
                GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;
            }

            PointLightPass.Apply();

            foreach(ModelMesh mesh in PointSphere.Meshes)
            {
                foreach(ModelMeshPart part in mesh.MeshParts)
                {
                    part.Effect = PointLightEffect;
                }
                mesh.Draw();
            }

            GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;

        }
    }
    public struct ShipVertex : IVertexType
    {
        public Vector3 Position;
        public Vector3 Normal;
        public Vector2 DiffTexCoord;
        public Vector2 SpecTexCoord;
        public Vector2 NormalTexCoord;

        public ShipVertex(Vector3 position, Vector3 normal, Vector2 diff, Vector2 spec, Vector2 norm)
        {
            Position = position;
            Normal = normal;
            DiffTexCoord = diff;
            SpecTexCoord = spec;
            NormalTexCoord = norm;
        }
        public static readonly VertexDeclaration VertexDeclaration = new VertexDeclaration(
            new VertexElement(0, VertexElementFormat.Vector3, VertexElementUsage.Position, 0),
            new VertexElement(12, VertexElementFormat.Vector3, VertexElementUsage.Normal, 0),
            new VertexElement(24, VertexElementFormat.Vector2, VertexElementUsage.TextureCoordinate, 0),
            new VertexElement(32, VertexElementFormat.Vector2, VertexElementUsage.TextureCoordinate, 1),
            new VertexElement(40, VertexElementFormat.Vector2, VertexElementUsage.TextureCoordinate, 2));
        VertexDeclaration IVertexType.VertexDeclaration
        {
            get => VertexDeclaration;
        }

    }
    public class DirectionalLightSolo
    {
        public Vector3 Direction { get; set; }
        public Color Color { get; set; }

        public DirectionalLightSolo()
        {
            Direction = -Vector3.UnitZ;
            Color = Color.White;
        }
    }
    
}

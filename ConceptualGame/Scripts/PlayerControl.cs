﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseEngine;
using BaseEngine.Components;
using BaseEngine.BepuPhysics;
using BaseEngine.Rendering;
using BaseEngine.Logic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using BVector2 = BEPUutilities.Vector2;

namespace ConceptualGame.Scripts
{
    class PlayerControl : Behavior
    {
        CharacterControllerComponent character;
        TransformComponent transform;
        Vector2 MovementDirection;
        float epsilon;
        float speed;
        public override void Initialize()
        {
            speed = 14f;
            epsilon = .1f;
            character = gameObject.GetComponent<CharacterControllerComponent>();
            //character.standingSpeed = 1600;
            
            transform = gameObject.GetComponent<TransformComponent>();
        }
        public override void Update(GameTime gameTime)
        {
            MovementDirection = Vector2.Zero;
            character.StandingSpeed = 8;
            if (ObsoleteInput.KeyDown(Keys.A))
                MovementDirection += new Vector2(-1, 0);
            if (ObsoleteInput.KeyDown(Keys.D))
                MovementDirection += new Vector2(1, 0);
            if (ObsoleteInput.KeyDown(Keys.W))
                MovementDirection += new Vector2(0, 1);
            if (ObsoleteInput.KeyDown(Keys.S))
                MovementDirection += new Vector2(0, -1);

            if (MovementDirection != Vector2.Zero)
                MovementDirection.Normalize();
            else
            {
                if (Math.Abs(transform.Position.X) > epsilon || Math.Abs(transform.Position.Y) > epsilon)
                {
                    MovementDirection = -Vector2.Normalize(new Vector2(transform.Position.X, transform.Position.Y));
                    character.StandingSpeed = 4;
                    character.DebugStop();
                }
            }

            //character.input.direction = MovementDirection * speed;
            character.Move(MovementDirection);
            
            if (ObsoleteInput.KeyPressed(Keys.Space))
                character.Jump();

            character.Teleport(
                new Vector3(
                    MathHelper.Clamp(transform.Position.X, -2.5f, 2.5f),
                    MathHelper.Clamp(transform.Position.Y, -2.5f, 2.5f),
                    transform.Position.Z));

            

            //if (transform.position.X > 2.5f)
            //    character.Teleport(new Vector3(2.5f, transform.position.Y, transform.position.Z));
            //if (transform.position.X < -2.5f)
            //    character.Teleport(new Vector3(-2.5f, transform.position.Y, transform.position.Z));
            //if (transform.position.Y > 2.5f)
            //    character.Teleport(new Vector3(transform.position.X, 2.5f, transform.position.Z));
            //if (transform.position.Y < -2.5f)
            //    character.Teleport(new Vector3(transform.position.X, -2.5f, transform.position.Z));
            //character.UpdateMotion();                        
        }
        
    }
}

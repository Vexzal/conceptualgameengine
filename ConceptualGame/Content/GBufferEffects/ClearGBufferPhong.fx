﻿#if OPENGL
	#define SV_POSITION POSITION
	#define VS_SHADERMODEL vs_3_0
	#define PS_SHADERMODEL ps_3_0
#else
	#define VS_SHADERMODEL vs_4_0_level_9_1
	#define PS_SHADERMODEL ps_4_0_level_9_1
#endif

Texture2D SpriteTexture;

sampler2D SpriteTextureSampler = sampler_state
{
	Texture = <SpriteTexture>;
};

struct VertexShaderInput
{
    float3 Position : SV_POSITION;
};
struct VertexShaderOutput
{
	float4 Position : SV_POSITION;	
};

struct PixelShaderOutput
{
    float4 Color : COLOR0;
    float4 Normal : COLOR1;
    float4 Depth : COLOR2;
};

VertexShaderOutput MainVS(VertexShaderInput input)
{
    VertexShaderOutput output;
    output.Position = float4(input.Position, 1);
    return output;
}

PixelShaderOutput MainPS(VertexShaderOutput input) : COLOR
{
    PixelShaderOutput output;

    output.Color = 1.0f;
    output.Color.a = 1.0f;
    
    output.Normal.rgb = 0.5f;
    output.Normal.a = 0.0f;
    
    output.Depth = float4(1.0f,1.0f,1.0f,1.0f);

    return output;
}

technique PhongClear
{
	pass P0
	{
        VertexShader = compile VS_SHADERMODEL MainVS();
		PixelShader = compile PS_SHADERMODEL MainPS();
	}
};
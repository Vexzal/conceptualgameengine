﻿#if OPENGL
	#define SV_POSITION POSITION
	#define VS_SHADERMODEL vs_3_0
	#define PS_SHADERMODEL ps_3_0
#else
	#define VS_SHADERMODEL vs_4_0_level_9_1
	#define PS_SHADERMODEL ps_4_0_level_9_1
#endif

matrix World;
matrix View;
matrix Projection;

float3 Color;
float3 cameraPosition;

matrix InvertViewProjection;
float3 lightPosition;
float lightRadius;

float lightIntensity = 1.0f;

texture colorMap;
texture normalMap;
texture depthMap;

sampler colorSampler = sampler_state
{
    Texture = (colorMap);
    AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = LINEAR;
    MinFilter = LINEAR;
    MipFilter = LINEAR;
};
sampler depthSampler = sampler_state
{
    Texture = (depthMap);
    AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = POINT;
    MinFilter = POINT;
    MipFilter = POINT;
};
sampler normalSampler = sampler_state
{
    Texture = (normalMap);
    AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = POINT;
    MinFilter = POINT;
    MipFilter = POINT;
};

struct VertexShaderInput
{
	float4 Position : POSITION0;	
};

struct VertexShaderOutput
{
	float4 Position : SV_POSITION;
    float4 ScreenPosition : TEXCOORD0;
};

VertexShaderOutput MainVS(in VertexShaderInput input)
{
	VertexShaderOutput output = (VertexShaderOutput)0;

    float4 wp = mul(input.Position, World);
    float vp = mul(wp, View);
    output.Position = mul(vp, Projection);
    //output.Position = mul(mul(mul(float4(input.Position), World), View), Projection);
    output.ScreenPosition = output.Position;

	return output;
}
float2 halfPixel;
float4 MainPS(VertexShaderOutput input) : COLOR
{
    input.ScreenPosition.xy /= input.ScreenPosition.w;
    float2 texCoord = 0.5 * (float2(input.ScreenPosition.x, -input.ScreenPosition.y) + 1);
    texCoord -= halfPixel;

    float4 normalData = tex2D(normalSampler, texCoord);
    float3 normal = 2.0f * normalData.xyz - 1.0f;

    float specularPower = normalData.a * 255;

    float specularIntensity = tex2D(colorSampler, texCoord).a;

    float depthVal = tex2D(depthSampler, texCoord).r;

    float4 position;
    position.xy = input.ScreenPosition.xy;
    position.z = depthVal;
    position.w = 1.0f;

    position = mul(position, IInvertViewProjection);
    position /= position.w;

    float3 lightVector = lightPosition - position;

    float attenuation = saturate(1.0f - length(lightVector) / lightRadius);
    lightVector = normalize(lightVector);


    float NdL = max(0, dot(normal, lightVector));
    float3 diffuseLight = NdL * Color.rgb;

    float3 reflectionVector = normalize(reflect(-lightVector, normal));

    float3 directionToCamera = normalize(cameraPosition - position);

    float specularLight = specularIntensity * pow(saturate(dot(reflectionVector, directionToCamera)), specularPower);

    return attenuation * lightIntensity * float4(diffuseLight.rgb, specularLight);
	
}

technique BasicColorDrawing
{
	pass P0
	{
		VertexShader = compile VS_SHADERMODEL MainVS();
		PixelShader = compile PS_SHADERMODEL MainPS();
	}
};
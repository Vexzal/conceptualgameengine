﻿#if OPENGL
	#define SV_POSITION POSITION
	#define VS_SHADERMODEL vs_3_0
	#define PS_SHADERMODEL ps_3_0
#else
	#define VS_SHADERMODEL vs_4_0_level_9_1
	#define PS_SHADERMODEL ps_4_0_level_9_1
#endif

#include "../EngineConstants.fx"

float3 lightDirection;
float3 lightColor;
//float3 cameraPosition;

//float4x4 InvertViewProjection;

texture colorMap;
texture normalMap;
texture depthMap;

sampler colorSampler = sampler_state
{
    Texture = (colorMap);
    AddressU = Clamp;
    AddressV = Clamp;
    MagFilter = Linear;
    MinFilter = Linear;
    MipFilter = Linear;
};

sampler depthSampler = sampler_state
{
    Texture = (depthMap);
    AddressU = Clamp;
    AddressV = Clamp;
    MagFilter = Point;
    MinFilter = Point;
    MipFilter = Point;
};

sampler normalSampler = sampler_state
{
    Texture = (normalMap);
    AddressU = Clamp;
    AddressV = Clamp;
    MagFilter = Point;
    MinFilter = Point;
    MipFilter = Point;
};

struct VertexShaderInput
{
	float4 Position : POSITION0;
    float2 TexCoord : TEXCOORD0;
};

struct VertexShaderOutput
{
	float4 Position : SV_POSITION;
    float2 TexCoord : TEXCOORD0;
};

float2 halfPixel;
VertexShaderOutput MainVS(in VertexShaderInput input)
{
	VertexShaderOutput output = (VertexShaderOutput)0;

    output.Position = float4(input.Position.xyz, 1);
    //output.Position = input.Position;
    output.TexCoord = input.TexCoord - halfPixel;

	return output;
}

float4 MainPS(VertexShaderOutput input) : COLOR
{
    //diffuse range
    float4 normalData = tex2D(normalSampler, input.TexCoord);
    normalData.a = 1;    

    float3 normal = 2.0f * normalData.xyz - 1.0f;

    //specular range

    float specularPower = normalData.a * 255;

    float specularIntensity = tex2D(colorSampler, input.TexCoord).a;

    float depthVal = tex2D(depthSampler, input.TexCoord).r;

    //screen space position
    float4 position;
    position.x = input.TexCoord.x * 2.0f - 1.0f;
    position.y = input.TexCoord.y * 2.0f - 1.0f;
    position.z = depthVal;
    position.w = 1.0f;

    position = mul(position, InverseViewProjection);
    position /= position.w;

    float3 lightVctor = -normalize(lightDirection);

    float NdL = max(0, dot(normal, lightVctor));
    float3 diffuseLight = NdL * lightColor.rgb;

    float3 reflectionVector = normalize(reflect(lightVctor, normal));
    
    float3 directionToCamera = normalize(cameraPosition - position);
    float specularLight = specularIntensity * pow(saturate(dot(reflectionVector, directionToCamera)), specularPower);

    
    return float4(diffuseLight.rgb, specularLight);
}

technique BasicColorDrawing
{
	pass P0
	{
		VertexShader = compile VS_SHADERMODEL MainVS();
		PixelShader = compile PS_SHADERMODEL MainPS();
	}
};
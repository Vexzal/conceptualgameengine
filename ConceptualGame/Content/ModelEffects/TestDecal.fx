﻿#if OPENGL
	#define SV_POSITION POSITION
	#define VS_SHADERMODEL vs_3_0
	#define PS_SHADERMODEL ps_3_0
#else
	#define VS_SHADERMODEL vs_4_0_level_9_1
	#define PS_SHADERMODEL ps_4_0_level_9_1
#endif



uniform matrix World;
uniform matrix View;
uniform matrix Projection;
uniform matrix InverseWorld;
uniform matrix InverseView;


uniform float2 Resolution;
uniform float2 HalfPixel;
uniform float FarClip;

uniform float3 DecalDirection;
uniform float DecalDirectionClip;
uniform float AngleFadeWdith = 0.17;


uniform float4 DecalColor;

uniform texture Depth : GBUFFERDEPTH;
sampler depthSampler = sampler_state
{
    Texture = (Depth);
    MinFilter = Point;
    MipFilter = Point;
    MagFilter = Point;
    AddressU = Clamp;
    AddressV = Clamp;
};


texture Albedo;
texture Normal;

sampler diffuseSampler = sampler_state
{
    Texture = (Albedo);
    MagFilter = Anisotropic;
    MinFilter = Anisotropic;
    MipFilter = Linear;
    AddressU = Clamp;
    AddressV = Clamp;
};
sampler normalSampler = sampler_state
{
    Texture = (Normal);
    MagFilter = LINEAR;
    MinFilter = LINEAR;
    MipFilter = LINEAR;
    AddressU = Wrap;
    AddressV = Wrap;
};

struct VertexShaderInput
{
	float4 Position : POSITION0;
    
};

//struct VertexShaderOutput
//{
//	float4 Position : SV_POSITION;
//    half2 uv : TEXCOORD0;
//    float4 screenUV : TECOORD1;
//    float3 ray : TEXCOORD2;
//    half3 orientation : TEXCOORD3;
//    half3 orientationX : TEXCOORD4;
//    half3 orientationZ : TEXCOORD5;
//};
struct VertexShaderOutput
{
    float4 Position : POSITION0;
    float Depth : TEXCOORD0;
    float4 PositionCS : TEXCOORD1;
    float4 PositionVS : TEXCOORD2;
};

struct PixelShaderOutput
{
    float4 Color : COLOR0;
    float4 Normal : COLOR1;
};

float CalculateDepth(float4 viewPosition, float farClip)
{
    return -viewPosition.z / farClip;
}
float ZTest(float zBufferDepth, float pixelDepth)
{
    clip(zBufferDepth - pixelDepth);
}
float3 ReconstructWorldPosition(float3 viewRay, float depth, float4x4 InverseView)
{
    float3 viewPosition = viewRay * depth;

    return mul(float4(viewPosition, 1), InverseView).xyz;
}
float3 CalculatePixelWP(in float4 positionCS, in float4 positionVS, in float depth, uniform bool zTest)
{
    float2 screenPos = positionCS.xy / positionCS.w;
    float2 texCoord = float2(
    (1 + screenPos.x) / 2 + (0.5 / Resolution.x),
    (1 - screenPos.y) / 2 + (0.5 / Resolution.y)
    );
    float4 sampledDepth = tex2D(depthSampler, texCoord);
    
    //float3 frustrumRay = positionVS.xyz * (FarClip / -positionVS.z);
    
    if(zTest)
        ZTest(sampledDepth, depth);

    float3 frustrumRay = positionVS.xyz * (FarClip / -positionVS.z);
    return ReconstructWorldPosition(frustrumRay, sampledDepth.x, InverseView);
}

float3  ClipPixelNormal(float3 worldPosition, out float3 tangent, out float3 binormal, out float alpha)
{
    float3 ddxWp = ddx(worldPosition);
    float3 ddyWp = ddy(worldPosition);
    float3 normal = normalize(cross(ddyWp, ddxWp));

    float angle = acos(dot(-normal, DecalDirection));

    float difference = DecalDirectionClip - angle;
    clip(difference);

    alpha = saturate(difference / AngleFadeWdith);

    binormal = normalize(ddxWp);
    tangent = normalize(ddyWp);

    return normal;
}
float2 CalculateDecalTexCoord(float3 worldPosition)
{
    float4 objectPosition = mul(float4(worldPosition, 1), InverseWorld);
    clip(0.5 - abs(objectPosition.xyz));

    return objectPosition.xz + 0.5f;
}

float4 DiffuseValue(float2 decalTexCoord, float alpha)
{
    float4 texReturn = tex2D(diffuseSampler, decalTexCoord);
    texReturn.a = alpha;
    return texReturn;
}

float4 NormalValue(float2 decalTexCoord, float3 pixelNormal,float3 pixelTangent, float3 pixelBinormal)
{
    float4 normalSample = tex2D(normalSampler, decalTexCoord);
    float3 normal = normalize(normalSample.xyz * 2 - 1);
    
    float3x3 tangentToView;
    tangentToView[0] = mul(pixelTangent, View);
    tangentToView[1] = mul(pixelBinormal, View);
    tangentToView[2] = mul(normal, View);

    normal = mul(normal, tangentToView);

    return normal;
}

float4 ComputeScreenPos(float4 position)
{
    float4 output = position * .5f;
    output.xy = float2(output.x, output.y * ProjectionParams.x) + output.w;
    output.zw - position.zw;
    return output;
}

VertexShaderOutput MainVS(in VertexShaderInput input)
{
    VertexShaderOutput output = (VertexShaderOutput)0;
    float4 viewPosition = mul(mul(input.Position, World), View);

    output.Positon = mul(viewPosition, Projection);
    output.Depth = CalculateDepth(viewPosition, FarClip);
    output.PositionCS = output.Position;
    output.PositionVS = viewPosition;
}
PixelShaderOutput MainPS(in VertexShaderOutput input)
{
    PixelShaderOutput output = (PixelShaderOutput) 0;
    
    float3 worldPosition = CalculatePixelWP(input.PositionCS, input.PositionVS, input.Depth, false);
    float2 decalTexCoord = CalculateDecalTexCoord(worldPosition);

    float3 pixelTangent; 
    float3 pixelBinaormal;
    float alpha;
    float3 pixelNormal = ClipPixelNormal(worldPosition, pixelTangent, pixelBinaormal, alpha);
    output.Color = DiffuseValue(decalTexCoord, alpha);
    output.Normal = NormalValue(decalTexCoord, pixelNormal, pixelTangent, pixelBinaormal);

}

/**
//VertexShaderOutput MainVS(in VertexShaderInput input)
//{
//	VertexShaderOutput output = (VertexShaderOutput)0;

//	//output.Position = mul(input.Position, WorldViewProjection);
//	//output.Color = input.Color;
//    float4 wP = mul(input.Position, World);
//    float4 vP = mul(wP, View);
//    output.Position = mul(vP, Projection);
//    output.uv = input.Position.xz + .5f;
//    output.screenUV = CComputeScreenPos(output.Position);
//    output.ray = mul(mul(float4(input.Position.xyz, 1), World), View).xyz * float3(-1, -1, 1);
//    output.orientation = mul((float3x3) World, float3(0, 1, 0));
//    output.orientationX = mul((float3x3) World, float3(1, 0, 0));
//    output.orientationZ = mul((float3x3) World, float3(0, 0, 1));
//	return output;
//}

//PixelShaderOutput MainPS(VertexShaderOutput input) : COLOR
//{
//    PixelShaderOutput output = (PixelShaderOutput) 0;
//    input.ray = input.ray * (ProjectionParams.z / input.ray.z);
//    float2 uv = input.screenUV.xy / input.screenUV.w;
//    float depth = 
    
//    return output;
//}
*/

technique BasicColorDrawing
{
	pass P0
	{
		VertexShader = compile VS_SHADERMODEL MainVS();
		PixelShader = compile PS_SHADERMODEL MainPS();
	}
};
﻿#if OPENGL
	#define SV_POSITION POSITION
	#define VS_SHADERMODEL vs_3_0
	#define PS_SHADERMODEL ps_3_0
#else
	#define VS_SHADERMODEL vs_4_0_level_9_1
	#define PS_SHADERMODEL ps_4_0_level_9_1
#endif

#include "../EngineConstants.fx"

//matrix World;
//matrix View;
//matrix Projection;
//matrix InverseViewProjection;
//matrix InverseWorld;

texture depthMap;
texture DecalColor;
texture NormalMap;

sampler depthSampler = sampler_state
{

    Texture = (depthMap);
    AddressU = Clamp;
    AddressV = Clamp;
    MagFilter = Linear;
    MinFilter = Linear;
    MipFilter = Linear;
};

sampler colorSampler = sampler_state
{
    Texture = (DecalColor);
    AddressU = Clamp;
    AddressV = Clamp;
    MagFilter = Linear;
    MinFilter = Linear;
    MipFilter = Linear;
};
sampler normalSampler = sampler_state
{
    Texture = (NormalMap);
    AddressU = Clamp;
    AddressV = Clamp;
    MagFilter = Linear;
    MinFilter = Linear;
    MipFilter = Linear;
};

struct VertexShaderInput
{
	float4 Position : POSITION0;
	
};

struct VertexShaderOutput
{
	float4 Position : SV_POSITION;
    float4 ScreenPosition : TEXCOORD0;
	
};
struct PixelShaderOutput
{
    float4 Color : COLOR0;
    float4 Normal : COLOR1;
};

VertexShaderOutput MainVS(in VertexShaderInput input)
{
	VertexShaderOutput output = (VertexShaderOutput)0;
    
    output.Position = mul(mul(mul(input.Position, World), View), Projection);
    output.ScreenPosition = output.Position;

	return output;
}
float2 halfPixel;
PixelShaderOutput MainPS(VertexShaderOutput input) : COLOR
{
    PixelShaderOutput output = (PixelShaderOutput) 0;
    
    input.ScreenPosition.xy /= input.ScreenPosition.w;
    float2 depthUV = 0.5 * (float2(input.ScreenPosition.x, -input.ScreenPosition.y) + 1);
    depthUV -= halfPixel;

    float depthValue = tex2D(depthSampler, depthUV).r;

    float4 worldPositionFromPixel;
    worldPositionFromPixel.xy = input.ScreenPosition.xy;
    worldPositionFromPixel.z = depthValue;
    worldPositionFromPixel.w = 1;

    worldPositionFromPixel = mul
    (worldPositionFromPixel, InverseViewProjection);
    worldPositionFromPixel /= worldPositionFromPixel.w;

    float4 objectPosition = mul(worldPositionFromPixel, InverseWorld);
    clip(0.5 - abs(objectPosition.xyz));
    float2 decalTexCoord = objectPosition.xy + 0.5f;
    output.Color = tex2D(colorSampler, decalTexCoord);
    clip(output.Color.a - .01);
    float3 ddxWp = ddx(worldPositionFromPixel);
    float3 ddyWp = ddy(worldPositionFromPixel);

    float3 normal = normalize(cross(ddyWp, ddxWp));
    float3 binormal = normalize(ddxWp);
    float3 tangent = normalize(ddyWp);

    float3x3 tangentToView;
    tangentToView[0] = mul(tangent, View);
    tangentToView[1] = mul(binormal, View);
    tangentToView[2] = mul(-normal, View);

    float4 normalSample = tex2D(normalSampler, decalTexCoord);

    normalSample = normalSample * 2 - 1.0f;


    //output.Normal = float4(0, 0, 1, 0);
    //output.Normal = float4(mul(normalSample.xyz, tangentToView), 1);
    normalSample = float4(mul(normalSample.xyz, tangentToView), 1);
    output.Normal = .5f * (normalSample + 1);

    return output;
}

technique BasicColorDrawing
{
	pass P0
	{
		VertexShader = compile VS_SHADERMODEL MainVS();
		PixelShader = compile PS_SHADERMODEL MainPS();
	}
};
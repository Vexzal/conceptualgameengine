﻿#if OPENGL
#define SV_POSITION POSITION
#define VS_SHADERMODEL vs_3_0
#define PS_SHADERMODEL ps_3_0
#else
#define VS_SHADERMODEL vs_4_0_level_9_1
#define PS_SHADERMODEL ps_4_0_level_9_1
#endif



float delteTime;
matrix World;
matrix View;
matrix Projection;
matrix WorldViewProjection : register(VS_SHADERMODEL, c0);

struct VSIn
{
    float4 Position : POSITION0;
};

struct VSOut
{
    float4 Position : POSITION0;
};

VSOut VS(in VSIn input)
{
    VSOut output;
    output.Position = mul(mul(mul(mul(input.Position, World), View), Projection), WorldViewProjection);
    output.Position.w = delteTime;
    return output;
}
float4 PS(VSOut input) : COLOR
{
    return float4(0, 0, 0, 0);
}

technique ConstantsTechnique
{
    pass P0
    {
        VertexShader = compile VS_SHADERMODEL VS();
        PixelShader = compile PS_SHADERMODEL PS();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace ConceptualGame
{
    delegate void GuiCache(WindowState state); 
    class WindowState
    {
        public string Name { get; set; }
        public Vector2 Position;
        public Vector2 Size;
    }
    interface ICanvasChild
    {
        Rectangle bounds { get; set; }
        bool MouseUpdate(MouseState mouse);
        
    }
    public class xCanvas : ICanvasChild
    {
        public Rectangle bounds { get; set; }
        List<ICanvasChild> children;
        public bool MouseUpdate(MouseState state)
        {
            var ret = bounds.Contains(state.Position);

            if(ret)
            {
                foreach(var child in children)
                {
                    MouseUpdate(state);
                }
                if (Hover != null)
                {
                    Hover.Invoke();
                }

                if(state.RightButton == ButtonState.Pressed)
                {
                    //hmm this doesn't mesh well with my input system.
                }
            }



            return ret;

        }
        delegate void mouseStateDelegate();
        event mouseStateDelegate Hover;
        event mouseStateDelegate Click;
    }
    //alright so a.
    class MemoryGUI
    {

        GraphicsDevice GraphicsDevice;
        //I want a font collection, and not just a single spritefont instance, so I can actually multi task on those
        //gui instance
        static MemoryGUI I;
        WindowState activeWindowState;
        
        Dictionary<string, WindowState> windows = new Dictionary<string, WindowState>();
        Queue<GuiCache> beforeWindowCommands = new Queue<GuiCache>();
        Stack<WindowState> activeWindowStack = new Stack<WindowState>();
        string ActiveWindow { get; set; }
        static void Before()
        {
            
        }
        static void After()
        {

        }
        public static void SetWindowPos(Vector2 pos)
        {
            if (I.activeWindowState != null)
                I.activeWindowState.Position = pos;
        }
        public static void SetWindowSize(Vector2 size)
        {
            if(I.activeWindowState !=null)
                I.activeWindowState.Size = size;
        }
        public static void End()
        {
            I.activeWindowState = null;//instead of null lets just, lets just have an active bool
            //wait no there is like subwindows and escaping and HECK.
            //heck this system doesn't work with that either.
        }
        public static void BeginWindow(string name)
        {
            //need to add tagging logic. 
            I.ActiveWindow = name;
            if (!I.windows.ContainsKey(name))
                I.windows.Add(name, new WindowState());
            //do I want to copy the immediate mode system because I want.
            //I do want rendering and logic to be uncoupled.
            I.activeWindowState = I.windows[name];
            
            //set windows name
            I.activeWindowState.Name = name.Split('#')[0];
        }


    }
}

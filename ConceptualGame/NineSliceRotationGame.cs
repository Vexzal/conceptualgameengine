﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using BaseEngine;
using BaseEngine.Extensions;


namespace ConceptualGame
{
    public static class TextExtends
    {

    }
    class CustomBatcher
    {
        private const int InitialBatchSize = 256;
        private const int MaxBatchSize = short.MaxValue / 6;
        private const int InitialVertexArraySize = 256;
        private CustomSpriteBatch.CustomItem[] _batchItemList;
        private int _batchItemCount;
        private readonly GraphicsDevice _device;
        private short[] _index;
        private VertexPositionColorTexture[] _vertexArray;
        EffectParameter spriteTransformParameter;
        public CustomBatcher(GraphicsDevice device, EffectParameter spriteParams, int capacity = 0)
        {
            _device = device;
            if (capacity <= 0)
                capacity = InitialBatchSize;
            else
                capacity = (capacity + 63) & (~63);
            _batchItemList = new CustomSpriteBatch.CustomItem[capacity];
            _batchItemCount = 0;

            for (int i = 0; i < capacity; i++)
                _batchItemList[i] = new CustomSpriteBatch.CustomItem();
            spriteTransformParameter = spriteParams;
            //spriteTransformParameter.SetValue(Matrix.Identity);
            EnsureArrayCapacity(capacity);
        }
        public CustomSpriteBatch.CustomItem CreateBatchItem()
        {
            if(_batchItemCount >= _batchItemList.Length)
            {
                var oldSize = _batchItemList.Length;
                var newSize = oldSize + oldSize / 2;
                newSize = (newSize * 63) & (~63);
                Array.Resize(ref _batchItemList, newSize);
                for (int i = oldSize; i < newSize; i++)
                    _batchItemList[i] = new CustomSpriteBatch.CustomItem();
                EnsureArrayCapacity(Math.Min(newSize, MaxBatchSize));
            }
            var item = _batchItemList[_batchItemCount++];
            return item;
        }
        public unsafe void EnsureArrayCapacity(int numBatchItems)
        {
            int neededCapacity = 6 * numBatchItems;
            if(_index != null && neededCapacity <= _index.Length)
            {
                return;
            }
            short[] newIndex = new short[6 * numBatchItems];
            int start = 0;
            if(_index != null)
            {
                _index.CopyTo(newIndex, 0);
                start = _index.Length / 6;
            }
            fixed(short* indexFixedPtr = newIndex)
            {
                var indexPtr = indexFixedPtr + (start * 6);
                for(var i = start; i < numBatchItems; i++,indexPtr += 6)
                {
                    *(indexPtr + 0) = (short)(i * 4);
                    *(indexPtr + 1) = (short)(i * 4 + 1);
                    *(indexPtr + 2) = (short)(i * 4 + 2);
                    *(indexPtr + 3) = (short)(i * 4 + 1);
                    *(indexPtr + 4) = (short)(i * 4 + 3);
                    *(indexPtr + 5) = (short)(i * 4 + 2);
                }
            }
            _index = newIndex;
            _vertexArray = new VertexPositionColorTexture[4 * numBatchItems];
        }
        public unsafe void DrawBatch(SpriteSortMode sortMode, Effect effect)
        {
            if (effect != null && effect.IsDisposed)
                throw new ObjectDisposedException("effect");
            if (_batchItemCount == 0)
                return;
            switch(sortMode)
            {
                case SpriteSortMode.Texture:
                case SpriteSortMode.FrontToBack:
                case SpriteSortMode.BackToFront:
                    Array.Sort(_batchItemList, 0, _batchItemCount);
                    break;
            }

            int batchIndex = 0;
            int batchCount = _batchItemCount;

            //unchecked
            //{
            //    _device._graphicsMetrics._spriteCount += batchCount;
            //}
            while(batchCount > 0)
            {
                var startIndex = 0;
                var index = 0;
                Texture2D tex = null;
                int numBatchesToProcess = batchCount;
                if(numBatchesToProcess > MaxBatchSize)
                {
                    numBatchesToProcess = MaxBatchSize;
                }
                fixed(VertexPositionColorTexture* vertexArrayFixedPtr = _vertexArray)
                {
                    var vertexArrayPtr = vertexArrayFixedPtr;

                    for(int i = 0;i<numBatchesToProcess;i++,batchIndex++,index+=4,vertexArrayPtr+=4)
                    {
                        CustomSpriteBatch.CustomItem item = _batchItemList[batchIndex];
                        var shouldFlush = !ReferenceEquals(item.Texture, tex);
                        if(shouldFlush)
                        {
                            FlushVertexArray(startIndex, index, effect, tex);
                            //flush array
                            tex = item.Texture;
                            startIndex = index = 0;
                            vertexArrayPtr = vertexArrayFixedPtr;
                            _device.Textures[0] = tex;
                        }
                        *(vertexArrayPtr + 0) = item.vertexTL;
                        *(vertexArrayPtr + 1) = item.vertexTR;
                        *(vertexArrayPtr + 2) = item.vertexBL;
                        *(vertexArrayPtr + 3) = item.vertexBR;
                        item.Texture = null;
                    }

                }
                //flush remaining data.
                FlushVertexArray(startIndex, index, effect, tex);
                batchCount -= numBatchesToProcess;
            }
            _batchItemCount = 0;
        }
        private void FlushVertexArray(int start, int end, Effect effect, Texture texture)
        {
            if (start == end)
                return;
            var vertexCount = end - start;
            if(effect!=null)
            {
                var passes = effect.CurrentTechnique.Passes;
                foreach (var pass in passes)
                {
                    pass.Apply();
                    _device.Textures[0] = texture;
                    _device.DrawUserIndexedPrimitives(PrimitiveType.TriangleList,
                        _vertexArray,
                        0,
                        vertexCount,
                        _index,
                        0,
                        (vertexCount / 4) * 2,
                        VertexPositionColorTexture.VertexDeclaration
                        );
                }
            }
            else
            {
                _device.Textures[0] = texture;
                _device.DrawUserIndexedPrimitives(
                    PrimitiveType.TriangleList,
                    _vertexArray,
                    0,
                    vertexCount,
                    _index,
                    0,
                    (vertexCount / 4) * 2,
                    VertexPositionColorTexture.VertexDeclaration);
            }
        }
    }
    class CustomSpriteBatch 
    {
        public class CustomItem : IComparable<CustomItem>
        { 
            
            public Texture2D Texture;
            public float SortKey;
            public Matrix Transform;//if these are just going to be quads I could just...do transforms on em?
            //though really it should be skews not, 
            //okay no right now spritebatch . begin can set a lot of internal data and some of that I want rendered in a pretty specific way.
            //wait spritebatch already renders to quads so it keeps the depth buffer. god damnit.
            public VertexPositionColorTexture vertexTL;
            public VertexPositionColorTexture vertexTR;
            public VertexPositionColorTexture vertexBL;
            public VertexPositionColorTexture vertexBR;
            public CustomItem()
            {
                var s = DepthStencilState.Default;
                
                vertexTL = new VertexPositionColorTexture();
                vertexTR = new VertexPositionColorTexture();
                vertexBL = new VertexPositionColorTexture();
                vertexBR = new VertexPositionColorTexture();

            }
            public void Set(float x, float y, float width, float height,Color color, Vector2 texCoordTL,Vector2 texCoordBR,float depth )
            {
                //alright so.
                vertexTL.Position.X = x;
                vertexTL.Position.Y = y;
                vertexTL.Position.Z = depth;
                vertexTL.Color = color;
                vertexTL.TextureCoordinate.X = texCoordTL.X;
                vertexTL.TextureCoordinate.Y = texCoordTL.Y;

                vertexTR.Position.X = x + width ;
                vertexTR.Position.Y = y;
                vertexTR.Position.Z = depth;
                vertexTR.Color = color;
                vertexTR.TextureCoordinate.X = texCoordBR.X;
                vertexTR.TextureCoordinate.Y = texCoordTL.Y;

                vertexBL.Position.X = x;
                vertexBL.Position.Y = y + height;
                vertexBL.Position.Z = depth;
                vertexBL.Color = color;
                vertexBL.TextureCoordinate.X = texCoordTL.X;
                vertexBL.TextureCoordinate.Y = texCoordBR.Y;

                vertexBR.Position.X = x + width; 
                vertexBR.Position.Y = y + height;
                vertexBR.Position.Z = depth;
                vertexBR.Color = color;
                vertexBR.TextureCoordinate.X = texCoordBR.X;
                vertexBR.TextureCoordinate.Y = texCoordBR.Y;
                
            }
            
            public int CompareTo(CustomItem other)
            {
                return SortKey.CompareTo(other.SortKey);
            }

            
        }
        //alright so we got an.
        //we got to generate verticies with a texuture and what not
        
        readonly CustomBatcher _batcher;
        bool isBegun;
        //there is a bsatcher here but I don't know what that is or why I want it different so nope.
        GraphicsDevice GraphicsDevice;
        SpriteSortMode _sortMode;
        BlendState _blendState;
        SamplerState _samplerState;
        DepthStencilState _depthStencilState;
        RasterizerState _rasterizerState;
        Effect _effect;
        EffectParameter spriteTransform;
        Microsoft.Xna.Framework.Graphics.SpriteEffect _spriteEffect;
        readonly EffectPass _spritePass;
        Matrix _projction;
        SpriteArguments ItemArguments;
        DrawBehaviour Behaviour;

        Vector2 TexCoordTL = Vector2.Zero;
        Vector2 TexCoordBR = Vector2.One;

        public CustomSpriteBatch(GraphicsDevice graphicsDevice) : this(graphicsDevice,0)
        {

        }
        public CustomSpriteBatch(GraphicsDevice graphicsDevice, int capacity) : base()
        {
            //if (graphicsDevice == null)
            this.GraphicsDevice = graphicsDevice;
            _spriteEffect = new SpriteEffect(graphicsDevice);
            _spritePass = _spriteEffect.CurrentTechnique.Passes[0];
            spriteTransform = _spriteEffect.Parameters["MatrixTransform"];
            //batcher don't care
            isBegun = false;
            ItemArguments = new SpriteArguments()
            {
                Color = Color.Black,
                Depth = 0,
                DestinationRectangle = new Rectangle(0, 0, 0, 0),
                SourceRectangle = new Rectangle(0, 0, 0, 0),
                effects = SpriteEffects.None,
                Origin = Vector2.Zero,
                Rotation = 0,
                Texture = null
            };
            Behaviour = (SpriteArguments a) =>
            {
                //fill this in
                //CustomItem ret = new CustomItem();
                CustomItem ret = _batcher.CreateBatchItem();
                if(a.SourceRectangle.HasValue)
                {
                    var srcRect = a.SourceRectangle.GetValueOrDefault();
                    TexCoordTL.X = srcRect.X * a.Texture.TexelWidth();
                    TexCoordTL.Y = srcRect.Y * a.Texture.TexelHeight();
                    TexCoordBR.X = (srcRect.X + srcRect.Width) * a.Texture.TexelWidth();
                    TexCoordBR.Y = (srcRect.Y + srcRect.Height) * a.Texture.TexelHeight();
                }
                else
                {
                    TexCoordTL = Vector2.Zero;
                    TexCoordBR = Vector2.One;
                }

                ret.Set(a.DestinationRectangle.X,
                    a.DestinationRectangle.Y,
                    a.DestinationRectangle.Width,
                    a.DestinationRectangle.Height,
                    a.Color,
                    TexCoordTL,
                    TexCoordBR,
                    a.Depth);

                ret.Texture = a.Texture;
                ret.Transform = a.Transform;
                //sort key
                ret.SortKey = a.Depth;   

                return ret;
            };
            _batcher = new CustomBatcher(graphicsDevice,spriteTransform, capacity);
            //all I want is, custom draw behaviour right?
            //so why not just do that then.
            //*bangs head* I  don't know how I'd extend that because transform is at item level.
        }

        //setup for drawing. 
        //guess we should just copy whats already there
        //except transform. I want that to be handled by the sprite.
        public void Draw()
        {
            //set up arguments

            //add to list of items
            var x = Behaviour(ItemArguments);
        }
        SpriteArguments x;
        public void Draw(Texture texture,Rectangle destinationRectangle, Rectangle? sourceRectangle, Color color, float depth, Matrix transform )
        {
            ItemArguments.Texture = (Texture2D)texture;
            ItemArguments.Depth = depth;
            ItemArguments.DestinationRectangle = destinationRectangle;
            ItemArguments.SourceRectangle = sourceRectangle;
            ItemArguments.Color = color;
            ItemArguments.Transform = transform;

            Behaviour(ItemArguments);
             //= Behaviour(ItemArguments);
        }
        public void Before(
            SpriteSortMode sortMode = SpriteSortMode.Deferred,
            BlendState blendState = null,
            SamplerState samplerState = null,
            DepthStencilState depthStencilState = null,
            RasterizerState rasterizerState = null,
            Effect effect = null,
            Matrix? transformMatrix = null)
        {
            if (isBegun)
                throw new InvalidOperationException("After not called since last Before");
            _sortMode = sortMode;
            _blendState = blendState ?? BlendState.AlphaBlend;
            _samplerState = samplerState ?? SamplerState.LinearClamp;
            _depthStencilState = depthStencilState ?? DepthStencilState.None;
            _rasterizerState = rasterizerState ?? RasterizerState.CullNone;
            _effect = effect;
            //_spriteEffect.TransformMatrix = transformMatrix;
            //spriteTransform.SetValue(transformMatrix ?? Matrix.Identity);

            isBegun = true;
        }
        //actually draw the batch here.
        public void After()
        {
            if (!isBegun)
                throw new InvalidOperationException("Before Not Called BeforeAfter");

            //do drawing.


            isBegun = false;
            if (_sortMode != SpriteSortMode.Immediate)
                Setup();
            _batcher.DrawBatch(_sortMode, _effect);
        }
        //alright so. SO
        void Setup()
        {
            var gd = GraphicsDevice;
            gd.BlendState = _blendState;
            gd.DepthStencilState = _depthStencilState;
            gd.RasterizerState = _rasterizerState;
            gd.SamplerStates[0] = _samplerState;


            Matrix.CreateOrthographicOffCenter(0, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height, 0, 0, -1, out _projction);
            spriteTransform.SetValue(_projction);

            _spritePass.Apply();
        }

        public delegate CustomItem DrawBehaviour(SpriteArguments arguments);
    }
    public struct SpriteArguments
    {
        //public Vector2 Position;
        //public Vector2 Size;
        //public Color Color;
        //public Texture Texture;//no. so what I actually want
        public Rectangle? SourceRectangle;
        public Rectangle DestinationRectangle;
        public Texture2D Texture;
        public Color Color;
        public float Rotation;
        public Vector2 Origin;
        public SpriteEffects effects;
        public float Depth;
        public Matrix Transform;
    }
    class NineSliceRotationGame : CoreGame
    {
        //so
        CustomSpriteBatch csBatch;
        Texture2D Box;
        Texture2D Box2;
        Rectangle[] sourceRectangles;
        Rectangle[] destinationRectangles;
        SpriteFont Font;
        Rectangle BoxDestination;
        protected override void LoadContent() 
        {
            csBatch = new CustomSpriteBatch(GraphicsDevice);
            Box = Content.Load<Texture2D>("WhiteBoxUI");
            //Box2 = Content.Load<Texture2D>("DemoUiFrame");
            Box2 = Content.Load<Texture2D>("UIRoundWindow");
            Font = Content.Load<SpriteFont>("file");
            base.LoadContent();
            //sourceRectangles = CreatePatches(Box.Bounds);
            //sourceRectangles = CreatePatches(Box2.Bounds,Box2.Width/8 * 7,Box2.Width/8 * 7,Box2.Height/8 * 7,Box2.Height/8 * 7);
            sourceRectangles = CreatePatches(Box2.Bounds, 96, 96, 96, 96);
            BoxDestination = new Rectangle(0, 0, 400, 50);
            //destinationRectangles = CreatePatches(new Rectangle(0, 0, 400, 50));
            //destinationRectangles = CreatePatches(BoxDestination,BoxDestination.Width/8 * 7,BoxDestination.Width/8 *7,BoxDestination.Height/8*7,BoxDestination.Height/8 *7);
            destinationRectangles = CreatePatches(BoxDestination, 25, 25, 25, 25);
        }
        protected override void Update(GameTime gameTime)
        {
            if(Keyboard.GetState().IsKeyDown(Keys.Right))
            {
                BoxDestination.Width++;
                //destinationRectangles = CreatePatches(BoxDestination);
                //destinationRectangles = CreatePatches(BoxDestination, BoxDestination.Width / 8 * 7, BoxDestination.Width / 8 * 7, BoxDestination.Height / 8 * 7, BoxDestination.Height / 8 * 7);
                destinationRectangles = CreatePatches(BoxDestination, 25, 25, 25, 25);
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Left))
            {
                BoxDestination.Width--;
                if (BoxDestination.Width < 50)
                    BoxDestination.Width = 50;
                //destinationRectangles = CreatePatches(BoxDestination);
                //destinationRectangles = CreatePatches(BoxDestination, BoxDestination.Width / 8 * 7, BoxDestination.Width / 8 * 7, BoxDestination.Height / 8 * 7, BoxDestination.Height / 8 * 7);
                destinationRectangles = CreatePatches(BoxDestination, 25, 25, 25, 25);
            }
            if(Keyboard.GetState().IsKeyDown(Keys.Down))
            {
                BoxDestination.Height++;
                destinationRectangles = CreatePatches(BoxDestination, 25, 25, 25, 25);              
            }
            if(Keyboard.GetState().IsKeyDown(Keys.Up))
            {

                BoxDestination.Height--;
                if (BoxDestination.Height < 50)
                    BoxDestination.Height = 50;
                destinationRectangles = CreatePatches(BoxDestination,25,25,25,25);
            }
        }
        protected override void Draw(GameTime gameTime)
        {
            //GraphicsDevice.Clear(Color.Pink);
            GraphicsDevice.Clear(Color.SkyBlue);

            for (int i = 0; i < sourceRectangles.Length; i++)
            {
                //DrawRectangles(destinationRectangles[i], sourceRectangles[i], Box2, gameTime, new Vector3(50, 50, 1),Color.White);
                //DrawRectangles(destinationRectangles[i], sourceRectangles[i], Box2, gameTime, new Vector3(50, 125, .75f),Color.White);
                //DrawRectangles(destinationRectangles[i], sourceRectangles[i], Box2, gameTime, new Vector3(50, 200, .5f),Color.White);
                //DrawRectangles(destinationRectangles[i], sourceRectangles[i], Box2, gameTime, new Vector3(50, 275, 0),Color.White);
                
            }
            DrawDemoShape(new Rectangle(Point.Zero, new Point(200, 200)), 30);
            DrawDemoShape2(new Rectangle(new Point(170, 170), new Point(30, 30)), 30);
            DrawText(/*"ALLCOME LURIM IPSUM"*/"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque ultrices sed lorem nec bibendum. Suspendisse potenti. Vestibulum volutpat justo ut tellus convallis aliquam ac non velit. Mauris ornare massa finibus blandit eleifend. Morbi ac purus sed mauris sollicitudin rhoncus non non tortor. Donec massa dolor, dapibus ac nibh at, maximus fringilla elit. Praesent efficitur tincidunt felis ac dapibus. Sed ante dolor, ornare molestie porta quis, ullamcorper in ipsum. Proin nec luctus sem. ", Font, BoxDestination, gameTime);


            //DrawRectangles(destinationRectangles[0], sourceRectangles[0], Box2, gameTime);

            //csBatch.Before();
            //for (int i = 0; i < sourceRectangles.Length; i++)
            //{
            //    csBatch.Draw(Box, destinationRectangles[i], sourceRectangles[i], Color.White, 1, Matrix.Identity);
            //}
            //csBatch.After();
            //float width = Box2.Width;// * (1 / Box2.Width);
            //float height = Box2.Height;// * (1 / Box2.Height);
            //VertexPositionColorTexture[] list = new VertexPositionColorTexture[]
            //{
            //    new VertexPositionColorTexture(
            //        new Vector3(Vector2.Zero,0),
            //        Color.White,
            //        Vector2.Zero),
            //    new VertexPositionColorTexture(
            //        new Vector3(0,height,0),Color.White,new Vector2(0,1)),
            //    new VertexPositionColorTexture(
            //        new Vector3(width,0,0),Color.White,new Vector2(1,0)),
            //    new VertexPositionColorTexture(
            //        new Vector3(width,height,0),Color.White,new Vector2(1,1))
            //};

            //int[] indicies = new int[] { 0, 1, 2, 2, 1, 3 };
            ////SpriteEffect effect = new SpriteEffect(GraphicsDevice);
            //Effect effect = Content.Load<Effect>("BatchEffect2");
            //Vector3 OriginOff = new Vector3(Box2.Width / 2, Box2.Height / 2, 0);
            //Matrix m = Matrix.CreateOrthographicOffCenter(0, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height, 0, 0, -1);
            //Matrix mOff = Matrix.CreateTranslation(OriginOff);
            //Matrix mTrans = Matrix.CreateTranslation(new Vector3(100, 100,0));
            //Matrix m2 = Matrix.Invert(mOff) * Matrix.CreateRotationZ((float)gameTime.TotalGameTime.TotalSeconds) * mOff * mTrans; 
            //effect.Parameters["MatrixTransform"].SetValue( m2* m);
            //effect.Parameters["SpriteTexture"].SetValue(Box2);
            //GraphicsDevice.Textures[0] = Box2;
            //GraphicsDevice.RasterizerState = RasterizerState.CullNone;
            //effect.CurrentTechnique.Passes[0].Apply();
            //GraphicsDevice.DrawUserIndexedPrimitives(
            //    PrimitiveType.TriangleList,
            //    list,
            //    0,
            //    4,
            //    indicies,
            //    0,
            //    2,
            //    VertexPositionColorTexture.VertexDeclaration);

            Matrix centerOffset = Matrix.CreateTranslation(new Vector3(-graphics.PreferredBackBufferWidth/2, -graphics.PreferredBackBufferHeight/2, 0));
            spriteBatch.Begin(transformMatrix: centerOffset *  Matrix.CreateRotationZ(MathHelper.ToRadians((float)gameTime.TotalGameTime.TotalSeconds)) * Matrix.Invert(centerOffset));
            //spriteBatch.Draw(Box2, Vector2.Zero, Color.White);
            //spriteBatch.Draw(Box, new Rectangle(0, 0, 200, 200), new Rectangle(0, 0, Box.Width, Box.Height), Color.White);
            //spriteBatch.Draw(Box, new Rectangle(100, 100, 200, 200), new Rectangle(0, 0, Box.Width, Box.Height), Color.Green, MathHelper.ToRadians(45), new Vector2(2, 2), SpriteEffects.None, 1);
            //for (int i = 0; i < sourceRectangles.Length; i++)
            //{
            //    Rectangle destination = destinationRectangles[i];
            //    destination.Location -= (destination.Location.ToVector2() * new Vector2(.5f, .5f)).ToPoint();
            //    spriteBatch.Draw(Box, destination, sourceRectangles[i], Color.White, MathHelper.ToRadians((float)gameTime.TotalGameTime.TotalSeconds), new Vector2(2, 2), SpriteEffects.None, 1);
            //}
            var Origin = new Vector2(50, 50);
            //var d0 = destinationRectangles[0];
            float TexelWidth = (float)1f / Box.Width;
            float TexelHeight = (float)1f / Box.Height;
            Vector2[] offsets = { new Vector2(100, 100), Vector2.Zero, Vector2.Zero, Vector2.Zero, Vector2.Zero, Vector2.Zero, Vector2.Zero, Vector2.Zero, Vector2.Zero };
            //for (int i = 0; i < sourceRectangles.Length; i++)
            //{
            //    var weightFactorX = (float)destinationRectangles[i].Width * TexelWidth;
            //    var weightFactorY = (float)destinationRectangles[i].Height * TexelHeight; 
            //    var dO = Origin;
            //    //dO.X = dO.X / weightFactorX;
            //    dO.X = dO.X / (float)destinationRectangles[i].Width;
            //    dO.X = dO.X / (float)sourceRectangles[i].Width;
            //    //dO.Y = dO.Y / weightFactorY;
            //    dO.Y = dO.Y / (float)destinationRectangles[i].Height;
            //    dO.Y = dO.Y / (float)destinationRectangles[i].Width;
            //    var d0x = dO;
            //    d0x.X = d0x.X * (float)destinationRectangles[i].Width * (float)sourceRectangles[i].Width;
            //    d0x.Y = d0x.Y * (float)destinationRectangles[i].Height * (float)sourceRectangles[i].Height;
            //    //we have an order of operations problem.
            //    //because x is multiplied by width first and THEN by texel width. so.
            //    var xOff = new Vector2(offsets[i].X / (float)destinationRectangles[i].Width / (float)sourceRectangles[i].Width,offsets[i].Y/destinationRectangles[i].Height/ sourceRectangles[i].Height);
            //    //dO += offsets[i] ;
            //    dO += xOff;
            //    var R = destinationRectangles[i];
            //    R.Location -= offsets[i].ToPoint();
            //    //spriteBatch.Draw(Box, destinationRectangles[i], sourceRectangles[i], Color.White, 0, d0, SpriteEffects.None, 1);
            //    spriteBatch.Draw(Box, R, sourceRectangles[i], Color.White, MathHelper.ToRadians((float)gameTime.TotalGameTime.TotalSeconds), dO, SpriteEffects.None, 1);
            //}
            for (int i = 0; i < sourceRectangles.Length; i++)
            {
                //spriteBatch.Draw(Box, destinationRectangles[i], sourceRectangles[i], Color.White);
            }
            //spriteBatch.Draw(Box2, Vector2.Zero, Color.White);
            //d0.Location + new Point(4, 4);//this is wrong
            //d0.Location += new Point(100, 100);
            //spriteBatch.Draw(Box,d0, sourceRectangles[0], Color.White, MathHelper.ToRadians(0), new Vector2(100,100), SpriteEffects.None, 1);
            //spriteBatch.Draw(Box, destinationRectangles[1], sourceRectangles[1], Color.White,0, new Vector2(2, 2), SpriteEffects.None, 1);
            //spriteBatch.Draw(Box, destinationRectangles[2], sourceRectangles[2], Color.White,0, new Vector2(2, 2), SpriteEffects.None, 1);
            //spriteBatch.Draw(Box, destinationRectangles[3], sourceRectangles[3], Color.White, 0, new Vector2(2, 2), SpriteEffects.None, 1);
            var d4 = destinationRectangles[4];
            //=d4.Location -= new Point(d4.Width / 2, d4.Height / 2) - new Point(30,30)/* - new Point(d0.Width / 2, d0.Height / 2)*/;
            //d4.Location += new Point(100, 100);
            //d4.Location -= new Point(13, 13);
            //d4.Location = Point.Zero;
            //d4.Location -= new Point(25 * (d4.Width/4),25 * (d4.Height/4));
            //d4.Location -= new Point(25, 25);
            //d4.Location = new Point(0,0);
            //d4.Location = new Point(-277, -277);
            //var d4O = Origin *
            //new Vector2(
            //(float)d4.Width * (1f / (float)Box.Width),
            //(float)d4.Height * (1f / (float)Box.Height));
            
            //d4.Location += Origin.ToPoint();
            //spriteBatch.Draw(Box, d4, sourceRectangles[4], Color.White,/* MathHelper.ToRadians((float)gameTime.TotalGameTime.TotalSeconds)*/MathHelper.ToRadians((float)gameTime.TotalGameTime.TotalSeconds),d4O, SpriteEffects.None, 1);
            //var d42 = destinationRectangles[4];
            //spriteBatch.Draw(Box, d42, sourceRectangles[4], Color.White, 0, new Vector2(25, 25), SpriteEffects.None, 1);

            //spriteBatch.Draw(Box, destinationRectangles[5], sourceRectangles[5], Color.White, 0, new Vector2(2, 2), SpriteEffects.None, 1);
            //spriteBatch.Draw(Box, destinationRectangles[6], sourceRectangles[6], Color.White, 0, new Vector2(2, 2), SpriteEffects.None, 1);
            //spriteBatch.Draw(Box, destinationRectangles[7], sourceRectangles[7], Color.White,0, new Vector2(2, 2), SpriteEffects.None, 1);
            //spriteBatch.Draw(Box, destinationRectangles[8], sourceRectangles[8], Color.White,0, new Vector2(2, 2), SpriteEffects.None, 1);
           // spriteBatch.Draw(Box, new Vector2(50, 50), Color.Green);
            //spriteBatch.Draw(Box, new Rectangle(50, 50, 170, 170), new Rectangle(0, 0, 4, 4), Color.Green);
            spriteBatch.End();
             
            base.Draw(gameTime);
        }
        //int leftPadding =2 , rightPadding = 2, topPadding = 2, bottomPadding = 2;
        private Rectangle[] CreatePatches(Rectangle rectangle, int leftPadding, int rightPadding, int topPadding, int bottomPadding)
        {
            var x = rectangle.X;
            var y = rectangle.Y;
            var w = rectangle.Width;
            var h = rectangle.Height;
            var middleWidth = w - leftPadding - rightPadding;
            var middleHeight = h - topPadding - bottomPadding;
            var bottomY = y + h - bottomPadding;
            var rightX = x + w - rightPadding;
            var leftX = x + leftPadding;
            var topY = y + topPadding;
            var patches = new[]
            {
        new Rectangle(x,      y,        leftPadding,  topPadding),      // top left
        new Rectangle(leftX,  y,        middleWidth,  topPadding),      // top middle
        new Rectangle(rightX, y,        rightPadding, topPadding),      // top right
        new Rectangle(x,      topY,     leftPadding,  middleHeight),    // left middle
        new Rectangle(leftX,  topY,     middleWidth,  middleHeight),    // middle
        new Rectangle(rightX, topY,     rightPadding, middleHeight),    // right middle
        new Rectangle(x,      bottomY,  leftPadding,  bottomPadding),   // bottom left
        new Rectangle(leftX,  bottomY,  middleWidth,  bottomPadding),   // bottom middle
        new Rectangle(rightX, bottomY,  rightPadding, bottomPadding)    // bottom right
    };
            return patches;
        }

        VertexPositionColorTexture[] Vertex = new VertexPositionColorTexture[4]
        {
            new VertexPositionColorTexture(Vector3.Zero,Color.White,Vector2.Zero),
            new VertexPositionColorTexture(Vector3.Zero,Color.White,Vector2.Zero),
            new VertexPositionColorTexture(Vector3.Zero,Color.White,Vector2.Zero),
            new VertexPositionColorTexture(Vector3.Zero,Color.White,Vector2.Zero)
        };
        VertexPositionColorTexture vTL = new VertexPositionColorTexture(Vector3.Zero, Color.White, Vector2.Zero);
        VertexPositionColorTexture vTR = new VertexPositionColorTexture(Vector3.Zero, Color.White, Vector2.Zero);
        VertexPositionColorTexture vBL = new VertexPositionColorTexture(Vector3.Zero, Color.White, Vector2.Zero);
        VertexPositionColorTexture vBR = new VertexPositionColorTexture(Vector3.Zero, Color.White, Vector2.Zero);
        Vector2 texCoordTL = new Vector2();
        Vector2 texCoordBR = new Vector2();
        short[] index = new short[] { 0, 1, 2, 1, 3, 2 };
        CustomBatcher x;
        public unsafe void DrawText(string text, SpriteFont font, Rectangle Limit,GameTime gameTime)
        {
            //clip text firs
            var paddedLimit = Limit;
            paddedLimit.Width -= 40;
            paddedLimit.Height -= 30;
            
            //alright lets setup translation ight with
            var textClip = font.MeasureString(text);
            var placementHeight = paddedLimit.Height / 2 - textClip.Y / 2 + 15;
            float widthClip = textClip.X;
            float heightCLip = textClip.Y;
            if (textClip.X > paddedLimit.Width)
                widthClip = paddedLimit.Width;
            if (textClip.Y > paddedLimit.Height)
                heightCLip = paddedLimit.Height;
            //wait just. okay so we're. we're going to do the thing //the thing was draw, the text using draw rectangles. and I guess OH. OH SHIT.
            //okay if thats all I'm doing I can do the culling in that loop so
            //lets pesudo code it and go to bed
            //rectangle destination
            //for loop
            //  check if destination width will exceed bounds
            //  if so constrain destination
            //  draw texture source position etc                
            //  add bounds to destination position.
            //  //break if out of bounds.
            //end
            //this doesn't do rotation well right now I'll. I'll work on that.
            var Glyphs = font.GetGlyphs();
            Rectangle destination = new Rectangle(0, 0, 0, 0);
            destination.Height = font.LineSpacing;
            for( int i =0;i<text.Length; i++)
            {
                //destination.Height = font.LineSpacing;
                
                char c = text[i];
                //destination.Height -= Glyphs[c].Cropping.Y;
                
                destination.Y = Glyphs[c].Cropping.Y;
                destination.X += (int)(font.Spacing + Glyphs[c].LeftSideBearing);
                //destination.Width = (int)Glyphs[c].Width;
                var source = Glyphs[c].BoundsInTexture;
                destination.Height = source.Height;
                destination.Width = source.Width;
                if ((destination.X + destination.Width) > widthClip)
                {
                    destination.Width = (int)widthClip - destination.X;
                    i = text.Length;//only problem is this is stretching instead of cropping. Hmm 
                    //okay so the problem is source controls cropping so.
                    source.Width -= (int)Glyphs[c].Width - destination.Width;
                }
                //should I just use pointers? I mean I guess
                DrawRectangles(destination, source, font.Texture, gameTime, new Vector3(50, 50, 0)+ new Vector3(15,placementHeight,0),Color.Purple);
                destination.X += destination.Width + (int)Glyphs[c].RightSideBearing;
            }

        }
        struct quickStruct
        {
            public VertexPositionColorTexture[] verticies;
        }
        //VertexPositionColorTexture[] verts;
        quickStruct ret = new quickStruct();
        int[] index2;
        Effect ImEffect;
        public void DrawDemoShape(Rectangle rect,float margin)
        {
            ret.verticies = new VertexPositionColorTexture[76];

            ret.verticies[0] = new VertexPositionColorTexture(new Vector3(0.4756137f * margin + rect.X, 0.3774018f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[1] = new VertexPositionColorTexture(new Vector3(0.4521645f * margin + rect.X, 0.384515f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[2] = new VertexPositionColorTexture(new Vector3(0.4999999f * margin + rect.X, 0.4999999f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[3] = new VertexPositionColorTexture(new Vector3(0.4305537f * margin + rect.X, 0.3960662f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[4] = new VertexPositionColorTexture(new Vector3(0.4116116f * margin + rect.X, 0.4116116f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[5] = new VertexPositionColorTexture(new Vector3(0.3960662f * margin + rect.X, 0.4305536f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[6] = new VertexPositionColorTexture(new Vector3(0.384515f * margin + rect.X, 0.4521645f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[7] = new VertexPositionColorTexture(new Vector3(0.3774018f * margin + rect.X, 0.4756136f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[8] = new VertexPositionColorTexture(new Vector3(0.3749999f * margin + rect.X, 0.4999999f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[9] = new VertexPositionColorTexture(new Vector3(0.125f * margin + rect.X + rect.Width - margin, 0.4999998f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[10] = new VertexPositionColorTexture(new Vector3(0.1225981f * margin + rect.X + rect.Width - margin, 0.4756135f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[11] = new VertexPositionColorTexture(new Vector3(-1.192093E-07f * margin + rect.X + rect.Width - margin, 0.4999999f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[12] = new VertexPositionColorTexture(new Vector3(0.1154848f * margin + rect.X + rect.Width - margin, 0.4521644f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[13] = new VertexPositionColorTexture(new Vector3(0.1039336f * margin + rect.X + rect.Width - margin, 0.4305535f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[14] = new VertexPositionColorTexture(new Vector3(0.0883882f * margin + rect.X + rect.Width - margin, 0.4116114f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[15] = new VertexPositionColorTexture(new Vector3(0.06944609f * margin + rect.X + rect.Width - margin, 0.3960661f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[16] = new VertexPositionColorTexture(new Vector3(0.04783511f * margin + rect.X + rect.Width - margin, 0.3845149f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[17] = new VertexPositionColorTexture(new Vector3(0.02438605f * margin + rect.X + rect.Width - margin, 0.3774017f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[18] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X + rect.Width - margin, 0.3749999f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[19] = new VertexPositionColorTexture(new Vector3(0.4999999f * margin + rect.X, 0.3749999f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[20] = new VertexPositionColorTexture(new Vector3(0.09754443f * margin + rect.X + rect.Width - margin, 0.009607226f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[21] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X + rect.Width - margin, 0f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[22] = new VertexPositionColorTexture(new Vector3(0.191341f * margin + rect.X + rect.Width - margin, 0.03805995f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[23] = new VertexPositionColorTexture(new Vector3(0.2777846f * margin + rect.X + rect.Width - margin, 0.08426481f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[24] = new VertexPositionColorTexture(new Vector3(0.3535529f * margin + rect.X + rect.Width - margin, 0.1464462f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[25] = new VertexPositionColorTexture(new Vector3(0.4157345f * margin + rect.X + rect.Width - margin, 0.2222144f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[26] = new VertexPositionColorTexture(new Vector3(0.4619396f * margin + rect.X + rect.Width - margin, 0.3086578f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[27] = new VertexPositionColorTexture(new Vector3(0.4903926f * margin + rect.X + rect.Width - margin, 0.4024543f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[28] = new VertexPositionColorTexture(new Vector3(0.5f * margin + rect.X + rect.Width - margin, 0.4999995f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[29] = new VertexPositionColorTexture(new Vector3(0.009607375f * margin + rect.X, 0.4024548f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[30] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X, 0.5f * margin + rect.Y, 0), new Color(0.4588235f, 0.4588235f, 0.4588235f, 1f), Vector2.Zero);
            ret.verticies[31] = new VertexPositionColorTexture(new Vector3(0.03806025f * margin + rect.X, 0.3086583f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[32] = new VertexPositionColorTexture(new Vector3(0.08426517f * margin + rect.X, 0.2222149f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[33] = new VertexPositionColorTexture(new Vector3(0.1464466f * margin + rect.X, 0.1464466f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[34] = new VertexPositionColorTexture(new Vector3(0.2222149f * margin + rect.X, 0.0842652f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[35] = new VertexPositionColorTexture(new Vector3(0.3086583f * margin + rect.X, 0.03806025f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[36] = new VertexPositionColorTexture(new Vector3(0.4024549f * margin + rect.X, 0.009607375f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[37] = new VertexPositionColorTexture(new Vector3(0.5f * margin + rect.X, 0f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[38] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X, 0f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[39] = new VertexPositionColorTexture(new Vector3(0.3749999f * margin + rect.X, -1.192093E-07f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[40] = new VertexPositionColorTexture(new Vector3(0.4999999f * margin + rect.X, -1.192093E-07f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[41] = new VertexPositionColorTexture(new Vector3(0.5f * margin + rect.X + rect.Width - margin, -4.768372E-07f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[42] = new VertexPositionColorTexture(new Vector3(0.125f * margin + rect.X + rect.Width - margin, -2.384186E-07f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[43] = new VertexPositionColorTexture(new Vector3(-1.192093E-07f * margin + rect.X + rect.Width - margin, -1.192093E-07f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[44] = new VertexPositionColorTexture(new Vector3(0.3774018f * margin + rect.X, 0.02438617f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[45] = new VertexPositionColorTexture(new Vector3(0.384515f * margin + rect.X, 0.04783535f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[46] = new VertexPositionColorTexture(new Vector3(0.3960662f * margin + rect.X, 0.06944621f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[47] = new VertexPositionColorTexture(new Vector3(0.4116116f * margin + rect.X, 0.0883882f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[48] = new VertexPositionColorTexture(new Vector3(0.4305537f * margin + rect.X, 0.1039336f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[49] = new VertexPositionColorTexture(new Vector3(0.4521645f * margin + rect.X, 0.1154848f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[50] = new VertexPositionColorTexture(new Vector3(0.4756137f * margin + rect.X, 0.1225981f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[51] = new VertexPositionColorTexture(new Vector3(0.5f * margin + rect.X, 0.1249999f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[52] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X + rect.Width - margin, 0.1249999f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[53] = new VertexPositionColorTexture(new Vector3(0.02438629f * margin + rect.X + rect.Width - margin, 0.1225981f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[54] = new VertexPositionColorTexture(new Vector3(0.04783535f * margin + rect.X + rect.Width - margin, 0.1154848f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[55] = new VertexPositionColorTexture(new Vector3(0.06944633f * margin + rect.X + rect.Width - margin, 0.1039336f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[56] = new VertexPositionColorTexture(new Vector3(0.08838832f * margin + rect.X + rect.Width - margin, 0.0883882f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[57] = new VertexPositionColorTexture(new Vector3(0.1039337f * margin + rect.X + rect.Width - margin, 0.06944609f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[58] = new VertexPositionColorTexture(new Vector3(0.115485f * margin + rect.X + rect.Width - margin, 0.04783523f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[59] = new VertexPositionColorTexture(new Vector3(0.1225982f * margin + rect.X + rect.Width - margin, 0.02438617f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[60] = new VertexPositionColorTexture(new Vector3(0.4903927f * margin + rect.X + rect.Width - margin, 0.09754467f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[61] = new VertexPositionColorTexture(new Vector3(0.4619399f * margin + rect.X + rect.Width - margin, 0.1913414f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[62] = new VertexPositionColorTexture(new Vector3(0.415735f * margin + rect.X + rect.Width - margin, 0.2777848f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[63] = new VertexPositionColorTexture(new Vector3(0.3535537f * margin + rect.X + rect.Width - margin, 0.3535532f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[64] = new VertexPositionColorTexture(new Vector3(0.2777853f * margin + rect.X + rect.Width - margin, 0.4157346f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[65] = new VertexPositionColorTexture(new Vector3(0.1913419f * margin + rect.X + rect.Width - margin, 0.4619397f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[66] = new VertexPositionColorTexture(new Vector3(0.09754539f * margin + rect.X + rect.Width - margin, 0.4903926f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[67] = new VertexPositionColorTexture(new Vector3(2.384186E-07f * margin + rect.X + rect.Width - margin, 0.5f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[68] = new VertexPositionColorTexture(new Vector3(0.402455f * margin + rect.X, 0.4903927f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[69] = new VertexPositionColorTexture(new Vector3(0.5000002f * margin + rect.X, 0.5f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[70] = new VertexPositionColorTexture(new Vector3(0.3086584f * margin + rect.X, 0.4619398f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[71] = new VertexPositionColorTexture(new Vector3(0.2222149f * margin + rect.X, 0.4157348f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[72] = new VertexPositionColorTexture(new Vector3(0.1464466f * margin + rect.X, 0.3535534f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[73] = new VertexPositionColorTexture(new Vector3(0.08426517f * margin + rect.X, 0.2777851f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[74] = new VertexPositionColorTexture(new Vector3(0.03806022f * margin + rect.X, 0.1913416f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[75] = new VertexPositionColorTexture(new Vector3(0.009607345f * margin + rect.X, 0.09754515f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            index2 = new int[342] { 0, 1, 2, 1, 3, 2, 3, 4, 2, 4, 5, 2, 5, 6, 2, 6, 7, 2, 7, 8, 2, 9, 10, 11, 10, 12, 11, 12, 13, 11, 13, 14, 11, 14, 15, 11, 15, 16, 11, 16, 17, 11, 17, 18, 11, 19, 0, 2, 20, 21, 18, 20, 18, 17, 22, 20, 17, 22, 17, 16, 23, 22, 16, 23, 16, 15, 24, 23, 15, 24, 15, 14, 25, 24, 14, 25, 14, 13, 26, 25, 13, 26, 13, 12, 27, 26, 12, 27, 12, 10, 28, 27, 10, 28, 10, 9, 29, 30, 8, 29, 8, 7, 31, 29, 7, 31, 7, 6, 32, 31, 6, 32, 6, 5, 33, 32, 5, 33, 5, 4, 34, 33, 4, 34, 4, 3, 35, 34, 3, 35, 3, 1, 36, 35, 1, 36, 1, 0, 37, 36, 0, 37, 0, 19, 30, 38, 39, 30, 39, 8, 8, 39, 40, 8, 40, 2, 41, 28, 9, 41, 9, 42, 42, 9, 11, 42, 11, 43, 21, 37, 19, 21, 19, 18, 18, 19, 2, 18, 2, 11, 40, 43, 11, 40, 11, 2, 39, 44, 40, 44, 45, 40, 45, 46, 40, 46, 47, 40, 47, 48, 40, 48, 49, 40, 49, 50, 40, 50, 51, 40, 52, 53, 43, 53, 54, 43, 54, 55, 43, 55, 56, 43, 56, 57, 43, 57, 58, 43, 58, 59, 43, 59, 42, 43, 60, 41, 42, 60, 42, 59, 61, 60, 59, 61, 59, 58, 62, 61, 58, 62, 58, 57, 63, 62, 57, 63, 57, 56, 64, 63, 56, 64, 56, 55, 65, 64, 55, 65, 55, 54, 66, 65, 54, 66, 54, 53, 67, 66, 53, 67, 53, 52, 68, 69, 51, 68, 51, 50, 70, 68, 50, 70, 50, 49, 71, 70, 49, 71, 49, 48, 72, 71, 48, 72, 48, 47, 73, 72, 47, 73, 47, 46, 74, 73, 46, 74, 46, 45, 75, 74, 45, 75, 45, 44, 38, 75, 44, 38, 44, 39, 69, 67, 52, 69, 52, 51, 51, 52, 43, 51, 43, 40, };

            ImEffect = Content.Load<Effect>("ImEffect");
            ImEffect.Parameters["MatrixTransform"].SetValue(Matrix.CreateTranslation(new Vector3(50, 50, 0)) *Matrix.CreateOrthographicOffCenter(0, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height, 0, 0, -1));
            ImEffect.Parameters["Color"].SetValue(Color.Pink.ToVector4());
            //effect.Parameters["SpriteTexture"].SetValue(texure);
            //GraphicsDevice.Textures[0] = texure;
            //GraphicsDevice.RasterizerState = RasterizerState.CullNone;
            GraphicsDevice.RasterizerState = RasterizerState.CullNone;
            ImEffect.CurrentTechnique.Passes[0].Apply();
            GraphicsDevice.DrawUserIndexedPrimitives(
                PrimitiveType.TriangleList,
                ret.verticies,
                0,
                ret.verticies.Length,
                index2,
                0,
                index2.Length/3,
                VertexPositionColorTexture.VertexDeclaration);

        }
        public void DrawDemoShape2(Rectangle rect, float margin)
        {
            ret.verticies = new VertexPositionColorTexture[18];

            ret.verticies[0] = new VertexPositionColorTexture(new Vector3(0.4903927f * margin + rect.X, 0.09754467f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[1] = new VertexPositionColorTexture(new Vector3(0.5f * margin + rect.X, -4.768372E-07f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[2] = new VertexPositionColorTexture(new Vector3(0.125f * margin + rect.X, -2.384186E-07f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[3] = new VertexPositionColorTexture(new Vector3(0.1225982f * margin + rect.X, 0.02438617f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[4] = new VertexPositionColorTexture(new Vector3(0.4619399f * margin + rect.X, 0.1913414f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[5] = new VertexPositionColorTexture(new Vector3(0.115485f * margin + rect.X, 0.04783523f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[6] = new VertexPositionColorTexture(new Vector3(0.415735f * margin + rect.X, 0.2777848f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[7] = new VertexPositionColorTexture(new Vector3(0.1039337f * margin + rect.X, 0.06944609f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[8] = new VertexPositionColorTexture(new Vector3(0.3535537f * margin + rect.X, 0.3535532f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[9] = new VertexPositionColorTexture(new Vector3(0.08838832f * margin + rect.X, 0.0883882f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[10] = new VertexPositionColorTexture(new Vector3(0.2777853f * margin + rect.X, 0.4157346f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[11] = new VertexPositionColorTexture(new Vector3(0.06944633f * margin + rect.X, 0.1039336f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[12] = new VertexPositionColorTexture(new Vector3(0.1913419f * margin + rect.X, 0.4619397f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[13] = new VertexPositionColorTexture(new Vector3(0.04783535f * margin + rect.X, 0.1154848f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[14] = new VertexPositionColorTexture(new Vector3(0.09754539f * margin + rect.X, 0.4903926f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[15] = new VertexPositionColorTexture(new Vector3(0.02438629f * margin + rect.X, 0.1225981f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[16] = new VertexPositionColorTexture(new Vector3(2.384186E-07f * margin + rect.X, 0.5f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[17] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X, 0.1249999f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            index2 = new int[48] { 0, 1, 2, 0, 2, 3, 4, 0, 3, 4, 3, 5, 6, 4, 5, 6, 5, 7, 8, 6, 7, 8, 7, 9, 10, 8, 9, 10, 9, 11, 12, 10, 11, 12, 11, 13, 14, 12, 13, 14, 13, 15, 16, 14, 15, 16, 15, 17, };

            ImEffect = Content.Load<Effect>("ImEffect");
            ImEffect.Parameters["MatrixTransform"].SetValue(Matrix.CreateTranslation(new Vector3(50, 50, 0)) * Matrix.CreateOrthographicOffCenter(0, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height, 0, 0, -1));
            ImEffect.Parameters["Color"].SetValue(Color.Blue.ToVector4());
            //effect.Parameters["SpriteTexture"].SetValue(texure);
            //GraphicsDevice.Textures[0] = texure;
            //GraphicsDevice.RasterizerState = RasterizerState.CullNone;
            GraphicsDevice.RasterizerState = RasterizerState.CullNone;
            ImEffect.CurrentTechnique.Passes[0].Apply();
            GraphicsDevice.DrawUserIndexedPrimitives(
                PrimitiveType.TriangleList,
                ret.verticies,
                0,
                ret.verticies.Length,
                index2,
                0,
                index2.Length / 3,
                VertexPositionColorTexture.VertexDeclaration);
        }

        public unsafe void DrawRectangles(Rectangle destination, Rectangle source,Texture2D texure, GameTime gameTime,Vector3 translate,Color color )
        {

            texCoordTL.X = source.X * texure.TexelWidth();
            texCoordTL.Y = source.Y * texure.TexelHeight();
            texCoordBR.X = (source.X + source.Width) * texure.TexelWidth();
            texCoordBR.Y = (source.Y + source.Height) * texure.TexelHeight();

            vTL.Position.X = destination.X;
            vTL.Position.Y = destination.Y;
            vTL.Position.Z = 0;
            vTL.Color = color;
            vTL.TextureCoordinate.X = texCoordTL.X;
            vTL.TextureCoordinate.Y = texCoordTL.Y;

            vTR.Position.X = destination.X + destination.Width;
            vTR.Position.Y = destination.Y;
            vTR.Position.Z = 0;
            vTR.Color = color;
            vTR.TextureCoordinate.X = texCoordBR.X;
            vTR.TextureCoordinate.Y = texCoordTL.Y;

            vBL.Position.X = destination.X;
            vBL.Position.Y = destination.Y + destination.Height; 
            vBL.Position.Z = 0;
            vBL.Color = color;
            vBL.TextureCoordinate.X = texCoordTL.X;
            vBL.TextureCoordinate.Y = texCoordBR.Y;

            vBR.Position.X = destination.X + destination.Width;
            vBR.Position.Y = destination.Y + destination.Height;
            vBR.Position.Z = 0;
            vBR.Color = color;
            vBR.TextureCoordinate.X = texCoordBR.X;
            vBR.TextureCoordinate.Y = texCoordBR.Y;

            fixed (VertexPositionColorTexture* vertexArrayFixedPtr = Vertex)
            {
                var vAPtr = vertexArrayFixedPtr;
                *(vAPtr + 0) = vTL;
                *(vAPtr + 1) = vTR;
                *(vAPtr + 2) = vBL;
                *(vAPtr + 3) = vBR;
            }

            Effect effect = Content.Load<Effect>("BatchEffect2");
            //Vector3 OriginOff = new Vector3(Box2.Width / 2, Box2.Height / 2, 0);
            //Vector3 OriginOff = new Vector3(200, 25, 0);
            Vector3 OriginOff = Vector3.Zero;
            Matrix m = Matrix.CreateOrthographicOffCenter(0, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height, 0, 0, -1);
            Matrix mOff = Matrix.CreateTranslation(OriginOff);
            Matrix mTrans = Matrix.CreateTranslation(translate );
            Matrix m2 = Matrix.Invert(mOff) * Matrix.CreateRotationZ(MathHelper.ToRadians(15)) * mOff * mTrans;
            effect.Parameters["MatrixTransform"].SetValue(m2 * m);
            effect.Parameters["SpriteTexture"].SetValue(texure);
            GraphicsDevice.Textures[0] = texure;
            //GraphicsDevice.RasterizerState = RasterizerState.CullNone;
            GraphicsDevice.RasterizerState = RasterizerState.CullNone;
            effect.CurrentTechnique.Passes[0].Apply();
            GraphicsDevice.DrawUserIndexedPrimitives(
                PrimitiveType.TriangleList,
                Vertex,
                0,
                4,
                index,
                0,
                2,
                VertexPositionColorTexture.VertexDeclaration);
        }

    }
}

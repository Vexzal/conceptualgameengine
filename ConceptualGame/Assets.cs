﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using BEPUphysics.CollisionShapes;
using BEPUphysics.CollisionShapes.ConvexShapes;
namespace ConceptualGame
{
    class Assets
    {
        public Model Cube;
        public Model Plane;
        public Model Torus;
        public Model Ship;
        public Texture2D shipDiffuse;
        public Effect shipEffect;
        public BoxShape FloorShape;

        public Assets()
        {

        }
        public void Load(ContentManager Content)
        {
            Cube  = Content.Load<Model>("Cube");
            Plane = Content.Load<Model>("Plane");
            Torus = Content.Load<Model>("Torus");
            Ship = Content.Load<Model>(@"DeferredModel\ship1Converted");
            shipDiffuse = Content.Load<Texture2D>(@"DeferredModel\ship1_c");
            shipEffect = Content.Load<Effect>(@"ModelEffects\ShipModelEffect_DeferredPhong");
            shipEffect.Parameters["Texture"].SetValue(shipDiffuse);
            FloorShape = new BoxShape(8, 8, .1f);
        }
    }
}

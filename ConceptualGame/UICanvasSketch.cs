﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ConceptualGame
{
    static class MemoryIMGUI
    {
        static CanvasComponent canvas;//should hold state info
        static Widget ActingWidget;
        public static void SetCanvas(CanvasComponent Canvas)
        {
            canvas = Canvas;
        }
        //I want lots of stuff so, lets see
        public static void BeginWindow(string id)
        {
            if(canvas != null)
            {
                
                ActingWidget = canvas.TryGetWindow(id);
            }

            throw new NotImplementedException();
        }
        public static void EndWindow()
        {
            ActingWidget = null;
            throw new NotImplementedException();
        }
        public static void SetPosition(Vector2 position)
        {
            
        }
        public static void SetSize(Vector2 size)
        {
            throw new NotImplementedException();
        }

    }
    //what was I even thinking.
    //eughhhh
    public  class CanvasComponent : BaseEngine.Components.Component, IUIUpdatable
    {
        public Vector2 Position { get; set; }
        public Vector2 Scale { get; set; }
        public void Update(IUIUpdatable u)
        {
            //?
        }
        public Widget TryGetWindow(string id)
        {
            if (Windows.ContainsKey(id))
                return Windows[id];
            else
            {
                Windows.Add(id, new Widget());
                return Windows[id];
            }
        }
        Dictionary<string, Widget> Windows;//no roots? I guess?//or all roots??? hmmmmm
        List<Widget> RootWidgets;
        TransformComponent2D Transform;
        Microsoft.Xna.Framework.Graphics.Texture2D outputImage; //this is the texture you get when the canvas component is rendered
        public Dictionary<string,Texture2D> TextureLibrary;//this isn't going to be enough we need.

        public static Rectangle[] CreatePatches(Rectangle rectangle,Vector4 margins)
        {
            int leftPadding = (int)margins.X;
            int rightPadding = (int)margins.Y;
            int topPadding = (int)margins.Z;
            int bottomPadding = (int)margins.W;
            var x = rectangle.X;
            var y = rectangle.Y;
            var w = rectangle.Width;
            var h = rectangle.Height;
            var middleWidth = w - leftPadding - rightPadding;
            var middleHeight = h - topPadding - bottomPadding;
            var bottomY = y + h - bottomPadding;
            var rightX = x + w - rightPadding;
            var leftX = x + leftPadding;
            var topY = y + topPadding;
            var patches = new[]
            {
        new Rectangle(x,      y,        leftPadding,  topPadding),      // top left
        new Rectangle(leftX,  y,        middleWidth,  topPadding),      // top middle
        new Rectangle(rightX, y,        rightPadding, topPadding),      // top right
        new Rectangle(x,      topY,     leftPadding,  middleHeight),    // left middle
        new Rectangle(leftX,  topY,     middleWidth,  middleHeight),    // middle
        new Rectangle(rightX, topY,     rightPadding, middleHeight),    // right middle
        new Rectangle(x,      bottomY,  leftPadding,  bottomPadding),   // bottom left
        new Rectangle(leftX,  bottomY,  middleWidth,  bottomPadding),   // bottom middle
        new Rectangle(rightX, bottomY,  rightPadding, bottomPadding)    // bottom right
    };
            return patches;
        }
    }
    //treat ui elements like models or textures or other assets.
    //something you build and load but is stored on its own.
    //class Widget //default class for widgets
    //{

    //    public CanvasComponent Canvas;
    //    public Widget Parent;
    //    public List<Widget> Children;
    //    //I need transform info like widgh height anchors etc
    //    //public TransformComponent2D Transform;//this is probably the wrong way to manage this.

    //}
    unsafe class PointerGuiTest
    {
        float* value;

        public void SetValue(ref float val)
        {
            fixed(float* v = &val)
            {
                value = v;
            }
                
        }
    }
    //so the biggest problem with this is a text field
    //which I'll figure out nooow I guess?
    unsafe class PointerVectorTest
    {
        Vector3* value;

        public void SetValue(ref Vector3 val)
        {
            fixed(Vector3* v = &val)
            {
                value = v;
            }
        }
    }
    unsafe struct StringPointerTest
    {
        char* text;
        public void SetValue(ref string val)
        {
            fixed(char* v = val)
            {
                text = v; 
            }
        }
    }
    class Buttonoldem : Widget
    {
        bool pressed;
        string textureKey;
        string Text = "";
        public Texture2D Texture
        {
            get => Canvas.TextureLibrary[textureKey];
        }

        //this is just a button, I guess.
    }

    public class CanvasSkinningLibrary
    {
        public Dictionary<string, Texture2D> Textures;
    }
    public class UIScritch
    {
        //lets just try to write down all the properties I think I need for this
        int x;
        int y;
        int width;//width of element
        int height;//heiht of element
        int minWidth;
        int minHeiht;
        int maxWidth;
        int maxHeight;
        
        
        //alright hold on
        UIScritch parentElement;

        void GettingActualWidthHeight()
        {

        }

        Vector2 AbsoluteTopCorner; //offset
        Vector2 AbsoluteBotCorner; //offset
        Vector2 RelativeTopCorner; // anchor
        Vector2 RelativeBotCorner; //Anchor


    }
    public interface IUIUpdatable
    {

        Vector2 Position { get; set; }
        Vector2 Scale { get; set; }
        void Update(IUIUpdatable u);
    }
    public class Widget : IUIUpdatable
    {
        /// <summary>
        /// Owning Canvas
        /// </summary>
        public CanvasComponent Canvas;
        int minHeight;
        int minWidth;
        Widget parent;
        List<Widget> children = new List<Widget>();
        /// <summary>
        /// absolute xy offset from top anchor
        /// </summary>
        Vector2 AbsoluteTopCorner;
        /// <summary>
        /// absolute xy offset from bot anchor
        /// </summary>
        Vector2 AbsoluteBotCorner;
        /// <summary>
        /// top anchor range 0 - 1
        /// </summary>
        Vector2 RelativeTopCorner;
        /// <summary>
        /// bot anchor range 0 - 1
        /// </summary>
        Vector2 RelativeBotCorner;

        Vector2 IUIUpdatable.Position { get; set; }
        Vector2 IUIUpdatable.Scale { get; set; }
        public void Update(IUIUpdatable u)
        {
            IUIUpdatable val = this;
            val.Position = u.Position * RelativeTopCorner + AbsoluteTopCorner;
            val.Scale = (u.Scale * RelativeBotCorner + AbsoluteBotCorner) - val.Position;
            BaseElement.Width = (int)MathHelper.Max( val.Scale.X,minWidth);
            BaseElement.Height = (int)MathHelper.Max( val.Scale.Y,minWidth) ;
            BaseElement.X = (int)val.Position.X;
            BaseElement.Y = (int)val.Position.Y;
            //rebuild margin rectangles.
            if (Margins.Length() > 0)
            {
                destinationRectangles = CanvasComponent.CreatePatches(BaseElement, Margins);
            }
            else
                destinationRectangles = new Rectangle[1] { BaseElement };

        }

        Rectangle BaseElement;
        Rectangle[] destinationRectangles;
        Vector4 Margins;
        //we need interact events

        public event UIInteractDelegate Clicked;
        public event UIInteractDelegate Left;
        public event UIInteractDelegate Entered;
        public event UIInteractDelegate Hover;

    }

      public delegate bool UIInteractDelegate();

    public class Button : Widget
    {
        //so we also have.
    }
    //okay I want to see if you can add a property to an event check that when you get home.

    public class CursedContent
    {
        float value;
        string prop;
        object Tag;
        public void AddBinding(string property,object obj)
        {
            prop = property;
            Tag = obj;
        }

        public void AddFloat(float f)
        {            
            var value = Tag.GetType().GetProperty(prop).GetValue(Tag);
            Tag.GetType().GetProperty(prop).SetValue(Tag, ((float)value + f));
        }
    }
    class FloatInputWidget : Widget
    {
        //float value
        PropertyInfo Property;
        object Owner;

        public void SetBinding(PropertyInfo prop,object obj)
        {
            Property = prop;
            Owner = obj;
        }

        float getValue()
        {
            if (Owner != null)
            {
                return (float)Property.GetValue(Owner);
            }
            throw new Exception();
        }

    }

    public class FloatInput : Widget
    {
        //so we need
        float value;
        //alright then what
        //we need a text input
        //also this is wrong thats not how widgets work
        //there is a window than stuff in the window.
        //I guess thats what the texture atlas is for.
        //alright then. what.
        Action<float> GetAction;
        Action<float> SetAction;
        //we need two actions, right
        IntPtr ptr;
        public unsafe void SetProperty(ref float value)
        {
            //getAction = c => c = value;
            //alriht cool this doesn't work that sucks.
            //I'll look into using pointer logic later but thats gonna be funky.
            //especially since I'm going to try to access data outside the unsafe scope
            fixed (float* pointer = &value)
            {
                ptr = (IntPtr)pointer;
            }

        }
        public void SetProperty(Action<float> getAction, Action<float> setAction)
        {
            GetAction = getAction;
            SetAction = setAction;
        }

        
    }
    public class LampMap
    {
        public Vector3 loop { get; set; }
    }
    public delegate Vector3 divulge();
    public class Settle
    {
        public LampMap map;
        public event divulge Exqamp;

        public void Start()
        {
            map = new LampMap();
            //Exqamp += mapf//oh right //so no;
        }
    }
}

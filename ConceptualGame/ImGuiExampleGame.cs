﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseEngine;
using BaseEngine.Components;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
namespace ConceptualGame
{
    class ImGuiExampleGame : CoreGame
    {
        ImmediateModeScratch contextComponent;
        Effect effect;
        MouseState oldState;
        MouseState newState;


        List<VertexPositionColorTexture> verticies = new List<VertexPositionColorTexture>();
        List<int> indicies = new List<int>();
        VertexBuffer vBuffer;
        IndexBuffer iBuffer;

        protected override void Initialize()
        {
            contextComponent = new ImmediateModeScratch(this);
            this.Window.IsBorderless = true;
            this.Window.Position = new Point(0, 0);
            this.graphics.PreferredBackBufferHeight = GraphicsDevice.Adapter.CurrentDisplayMode.Height-40;/*GraphicsDevice.Adapter.CurrentDisplayMode.Height;*/
            this.graphics.PreferredBackBufferWidth = GraphicsDevice.Adapter.CurrentDisplayMode.Width;
            this.IsMouseVisible = true;
            this.graphics.IsFullScreen = false;
            this.graphics.ApplyChanges();
            base.Initialize();
        }

        [Flags]
        enum xFlags
        {
            empty = 0,
            state1 = 1<<0,
            state2 = 1<<1,
        }

        protected override void LoadContent()
        {

             ButtonState calc = (ButtonState)(xFlags.state1);
            ButtonState xTell = ButtonState.Pressed;

            int i = (int)ButtonState.Pressed;

            contextComponent.font = Content.Load<SpriteFont>("File");
            contextComponent.Style = new StyleGuide();
            contextComponent.Style.AddStyle(MemoryConstants.WindowID, new SomeStyleClass()
            {
                margin = 15,
                Inactive = delegate(float margin,Rectangle rect, out VertexPositionColorTexture[] verticies, out string s)
                {
                    verticies = new VertexPositionColorTexture[76];
                    s = "RoundedWindowStandard";
                    #region MeshDefinition
                    verticies[0] = new VertexPositionColorTexture(new Vector3(0.4756137f * margin + rect.X, 0.3774018f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[1] = new VertexPositionColorTexture(new Vector3(0.4521645f * margin + rect.X, 0.384515f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[2] = new VertexPositionColorTexture(new Vector3(0.4999999f * margin + rect.X, 0.4999999f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[3] = new VertexPositionColorTexture(new Vector3(0.4305537f * margin + rect.X, 0.3960662f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[4] = new VertexPositionColorTexture(new Vector3(0.4116116f * margin + rect.X, 0.4116116f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[5] = new VertexPositionColorTexture(new Vector3(0.3960662f * margin + rect.X, 0.4305536f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[6] = new VertexPositionColorTexture(new Vector3(0.384515f * margin + rect.X, 0.4521645f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[7] = new VertexPositionColorTexture(new Vector3(0.3774018f * margin + rect.X, 0.4756136f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[8] = new VertexPositionColorTexture(new Vector3(0.3749999f * margin + rect.X, 0.4999999f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[9] = new VertexPositionColorTexture(new Vector3(0.125f * margin + rect.X + rect.Width - margin, 0.4999998f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[10] = new VertexPositionColorTexture(new Vector3(0.1225981f * margin + rect.X + rect.Width - margin, 0.4756135f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[11] = new VertexPositionColorTexture(new Vector3(-1.192093E-07f * margin + rect.X + rect.Width - margin, 0.4999999f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[12] = new VertexPositionColorTexture(new Vector3(0.1154848f * margin + rect.X + rect.Width - margin, 0.4521644f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[13] = new VertexPositionColorTexture(new Vector3(0.1039336f * margin + rect.X + rect.Width - margin, 0.4305535f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[14] = new VertexPositionColorTexture(new Vector3(0.0883882f * margin + rect.X + rect.Width - margin, 0.4116114f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[15] = new VertexPositionColorTexture(new Vector3(0.06944609f * margin + rect.X + rect.Width - margin, 0.3960661f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[16] = new VertexPositionColorTexture(new Vector3(0.04783511f * margin + rect.X + rect.Width - margin, 0.3845149f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[17] = new VertexPositionColorTexture(new Vector3(0.02438605f * margin + rect.X + rect.Width - margin, 0.3774017f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[18] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X + rect.Width - margin, 0.3749999f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[19] = new VertexPositionColorTexture(new Vector3(0.4999999f * margin + rect.X, 0.3749999f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[20] = new VertexPositionColorTexture(new Vector3(0.09754443f * margin + rect.X + rect.Width - margin, 0.009607226f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[21] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X + rect.Width - margin, 0f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[22] = new VertexPositionColorTexture(new Vector3(0.191341f * margin + rect.X + rect.Width - margin, 0.03805995f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[23] = new VertexPositionColorTexture(new Vector3(0.2777846f * margin + rect.X + rect.Width - margin, 0.08426481f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[24] = new VertexPositionColorTexture(new Vector3(0.3535529f * margin + rect.X + rect.Width - margin, 0.1464462f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[25] = new VertexPositionColorTexture(new Vector3(0.4157345f * margin + rect.X + rect.Width - margin, 0.2222144f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[26] = new VertexPositionColorTexture(new Vector3(0.4619396f * margin + rect.X + rect.Width - margin, 0.3086578f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[27] = new VertexPositionColorTexture(new Vector3(0.4903926f * margin + rect.X + rect.Width - margin, 0.4024543f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[28] = new VertexPositionColorTexture(new Vector3(0.5f * margin + rect.X + rect.Width - margin, 0.4999995f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[29] = new VertexPositionColorTexture(new Vector3(0.009607375f * margin + rect.X, 0.4024548f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[30] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X, 0.5f * margin + rect.Y, 0), new Color(0.4588235f, 0.4588235f, 0.4588235f, 1f), Vector2.Zero);
                    verticies[31] = new VertexPositionColorTexture(new Vector3(0.03806025f * margin + rect.X, 0.3086583f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[32] = new VertexPositionColorTexture(new Vector3(0.08426517f * margin + rect.X, 0.2222149f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[33] = new VertexPositionColorTexture(new Vector3(0.1464466f * margin + rect.X, 0.1464466f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[34] = new VertexPositionColorTexture(new Vector3(0.2222149f * margin + rect.X, 0.0842652f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[35] = new VertexPositionColorTexture(new Vector3(0.3086583f * margin + rect.X, 0.03806025f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[36] = new VertexPositionColorTexture(new Vector3(0.4024549f * margin + rect.X, 0.009607375f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[37] = new VertexPositionColorTexture(new Vector3(0.5f * margin + rect.X, 0f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[38] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X, 0f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[39] = new VertexPositionColorTexture(new Vector3(0.3749999f * margin + rect.X, -1.192093E-07f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[40] = new VertexPositionColorTexture(new Vector3(0.4999999f * margin + rect.X, -1.192093E-07f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[41] = new VertexPositionColorTexture(new Vector3(0.5f * margin + rect.X + rect.Width - margin, -4.768372E-07f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[42] = new VertexPositionColorTexture(new Vector3(0.125f * margin + rect.X + rect.Width - margin, -2.384186E-07f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[43] = new VertexPositionColorTexture(new Vector3(-1.192093E-07f * margin + rect.X + rect.Width - margin, -1.192093E-07f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[44] = new VertexPositionColorTexture(new Vector3(0.3774018f * margin + rect.X, 0.02438617f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[45] = new VertexPositionColorTexture(new Vector3(0.384515f * margin + rect.X, 0.04783535f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[46] = new VertexPositionColorTexture(new Vector3(0.3960662f * margin + rect.X, 0.06944621f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[47] = new VertexPositionColorTexture(new Vector3(0.4116116f * margin + rect.X, 0.0883882f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[48] = new VertexPositionColorTexture(new Vector3(0.4305537f * margin + rect.X, 0.1039336f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[49] = new VertexPositionColorTexture(new Vector3(0.4521645f * margin + rect.X, 0.1154848f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[50] = new VertexPositionColorTexture(new Vector3(0.4756137f * margin + rect.X, 0.1225981f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[51] = new VertexPositionColorTexture(new Vector3(0.5f * margin + rect.X, 0.1249999f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[52] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X + rect.Width - margin, 0.1249999f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[53] = new VertexPositionColorTexture(new Vector3(0.02438629f * margin + rect.X + rect.Width - margin, 0.1225981f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[54] = new VertexPositionColorTexture(new Vector3(0.04783535f * margin + rect.X + rect.Width - margin, 0.1154848f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[55] = new VertexPositionColorTexture(new Vector3(0.06944633f * margin + rect.X + rect.Width - margin, 0.1039336f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[56] = new VertexPositionColorTexture(new Vector3(0.08838832f * margin + rect.X + rect.Width - margin, 0.0883882f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[57] = new VertexPositionColorTexture(new Vector3(0.1039337f * margin + rect.X + rect.Width - margin, 0.06944609f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[58] = new VertexPositionColorTexture(new Vector3(0.115485f * margin + rect.X + rect.Width - margin, 0.04783523f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[59] = new VertexPositionColorTexture(new Vector3(0.1225982f * margin + rect.X + rect.Width - margin, 0.02438617f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[60] = new VertexPositionColorTexture(new Vector3(0.4903927f * margin + rect.X + rect.Width - margin, 0.09754467f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[61] = new VertexPositionColorTexture(new Vector3(0.4619399f * margin + rect.X + rect.Width - margin, 0.1913414f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[62] = new VertexPositionColorTexture(new Vector3(0.415735f * margin + rect.X + rect.Width - margin, 0.2777848f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[63] = new VertexPositionColorTexture(new Vector3(0.3535537f * margin + rect.X + rect.Width - margin, 0.3535532f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[64] = new VertexPositionColorTexture(new Vector3(0.2777853f * margin + rect.X + rect.Width - margin, 0.4157346f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[65] = new VertexPositionColorTexture(new Vector3(0.1913419f * margin + rect.X + rect.Width - margin, 0.4619397f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[66] = new VertexPositionColorTexture(new Vector3(0.09754539f * margin + rect.X + rect.Width - margin, 0.4903926f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[67] = new VertexPositionColorTexture(new Vector3(2.384186E-07f * margin + rect.X + rect.Width - margin, 0.5f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[68] = new VertexPositionColorTexture(new Vector3(0.402455f * margin + rect.X, 0.4903927f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[69] = new VertexPositionColorTexture(new Vector3(0.5000002f * margin + rect.X, 0.5f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[70] = new VertexPositionColorTexture(new Vector3(0.3086584f * margin + rect.X, 0.4619398f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[71] = new VertexPositionColorTexture(new Vector3(0.2222149f * margin + rect.X, 0.4157348f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[72] = new VertexPositionColorTexture(new Vector3(0.1464466f * margin + rect.X, 0.3535534f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[73] = new VertexPositionColorTexture(new Vector3(0.08426517f * margin + rect.X, 0.2777851f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[74] = new VertexPositionColorTexture(new Vector3(0.03806022f * margin + rect.X, 0.1913416f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[75] = new VertexPositionColorTexture(new Vector3(0.009607345f * margin + rect.X, 0.09754515f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    #endregion 
                },
                Hot = delegate (float margin, Rectangle rect, out VertexPositionColorTexture[] verticies, out string s)
                {
                    verticies = new VertexPositionColorTexture[76];
                    s = "RoundedWindowStandard";
                    #region MeshDefinition
                    verticies[0] = new VertexPositionColorTexture(new Vector3(0.4756137f * margin + rect.X, 0.3774018f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[1] = new VertexPositionColorTexture(new Vector3(0.4521645f * margin + rect.X, 0.384515f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[2] = new VertexPositionColorTexture(new Vector3(0.4999999f * margin + rect.X, 0.4999999f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[3] = new VertexPositionColorTexture(new Vector3(0.4305537f * margin + rect.X, 0.3960662f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[4] = new VertexPositionColorTexture(new Vector3(0.4116116f * margin + rect.X, 0.4116116f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[5] = new VertexPositionColorTexture(new Vector3(0.3960662f * margin + rect.X, 0.4305536f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[6] = new VertexPositionColorTexture(new Vector3(0.384515f * margin + rect.X, 0.4521645f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[7] = new VertexPositionColorTexture(new Vector3(0.3774018f * margin + rect.X, 0.4756136f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[8] = new VertexPositionColorTexture(new Vector3(0.3749999f * margin + rect.X, 0.4999999f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[9] = new VertexPositionColorTexture(new Vector3(0.125f * margin + rect.X + rect.Width - margin, 0.4999998f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[10] = new VertexPositionColorTexture(new Vector3(0.1225981f * margin + rect.X + rect.Width - margin, 0.4756135f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[11] = new VertexPositionColorTexture(new Vector3(-1.192093E-07f * margin + rect.X + rect.Width - margin, 0.4999999f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[12] = new VertexPositionColorTexture(new Vector3(0.1154848f * margin + rect.X + rect.Width - margin, 0.4521644f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[13] = new VertexPositionColorTexture(new Vector3(0.1039336f * margin + rect.X + rect.Width - margin, 0.4305535f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[14] = new VertexPositionColorTexture(new Vector3(0.0883882f * margin + rect.X + rect.Width - margin, 0.4116114f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[15] = new VertexPositionColorTexture(new Vector3(0.06944609f * margin + rect.X + rect.Width - margin, 0.3960661f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[16] = new VertexPositionColorTexture(new Vector3(0.04783511f * margin + rect.X + rect.Width - margin, 0.3845149f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[17] = new VertexPositionColorTexture(new Vector3(0.02438605f * margin + rect.X + rect.Width - margin, 0.3774017f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[18] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X + rect.Width - margin, 0.3749999f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[19] = new VertexPositionColorTexture(new Vector3(0.4999999f * margin + rect.X, 0.3749999f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[20] = new VertexPositionColorTexture(new Vector3(0.09754443f * margin + rect.X + rect.Width - margin, 0.009607226f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[21] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X + rect.Width - margin, 0f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[22] = new VertexPositionColorTexture(new Vector3(0.191341f * margin + rect.X + rect.Width - margin, 0.03805995f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[23] = new VertexPositionColorTexture(new Vector3(0.2777846f * margin + rect.X + rect.Width - margin, 0.08426481f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[24] = new VertexPositionColorTexture(new Vector3(0.3535529f * margin + rect.X + rect.Width - margin, 0.1464462f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[25] = new VertexPositionColorTexture(new Vector3(0.4157345f * margin + rect.X + rect.Width - margin, 0.2222144f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[26] = new VertexPositionColorTexture(new Vector3(0.4619396f * margin + rect.X + rect.Width - margin, 0.3086578f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[27] = new VertexPositionColorTexture(new Vector3(0.4903926f * margin + rect.X + rect.Width - margin, 0.4024543f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[28] = new VertexPositionColorTexture(new Vector3(0.5f * margin + rect.X + rect.Width - margin, 0.4999995f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[29] = new VertexPositionColorTexture(new Vector3(0.009607375f * margin + rect.X, 0.4024548f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[30] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X, 0.5f * margin + rect.Y, 0), new Color(0.4588235f, 0.4588235f, 0.4588235f, 1f), Vector2.Zero);
                    verticies[31] = new VertexPositionColorTexture(new Vector3(0.03806025f * margin + rect.X, 0.3086583f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[32] = new VertexPositionColorTexture(new Vector3(0.08426517f * margin + rect.X, 0.2222149f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[33] = new VertexPositionColorTexture(new Vector3(0.1464466f * margin + rect.X, 0.1464466f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[34] = new VertexPositionColorTexture(new Vector3(0.2222149f * margin + rect.X, 0.0842652f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[35] = new VertexPositionColorTexture(new Vector3(0.3086583f * margin + rect.X, 0.03806025f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[36] = new VertexPositionColorTexture(new Vector3(0.4024549f * margin + rect.X, 0.009607375f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[37] = new VertexPositionColorTexture(new Vector3(0.5f * margin + rect.X, 0f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[38] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X, 0f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[39] = new VertexPositionColorTexture(new Vector3(0.3749999f * margin + rect.X, -1.192093E-07f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[40] = new VertexPositionColorTexture(new Vector3(0.4999999f * margin + rect.X, -1.192093E-07f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[41] = new VertexPositionColorTexture(new Vector3(0.5f * margin + rect.X + rect.Width - margin, -4.768372E-07f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[42] = new VertexPositionColorTexture(new Vector3(0.125f * margin + rect.X + rect.Width - margin, -2.384186E-07f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[43] = new VertexPositionColorTexture(new Vector3(-1.192093E-07f * margin + rect.X + rect.Width - margin, -1.192093E-07f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[44] = new VertexPositionColorTexture(new Vector3(0.3774018f * margin + rect.X, 0.02438617f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[45] = new VertexPositionColorTexture(new Vector3(0.384515f * margin + rect.X, 0.04783535f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[46] = new VertexPositionColorTexture(new Vector3(0.3960662f * margin + rect.X, 0.06944621f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[47] = new VertexPositionColorTexture(new Vector3(0.4116116f * margin + rect.X, 0.0883882f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[48] = new VertexPositionColorTexture(new Vector3(0.4305537f * margin + rect.X, 0.1039336f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[49] = new VertexPositionColorTexture(new Vector3(0.4521645f * margin + rect.X, 0.1154848f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[50] = new VertexPositionColorTexture(new Vector3(0.4756137f * margin + rect.X, 0.1225981f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[51] = new VertexPositionColorTexture(new Vector3(0.5f * margin + rect.X, 0.1249999f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[52] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X + rect.Width - margin, 0.1249999f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[53] = new VertexPositionColorTexture(new Vector3(0.02438629f * margin + rect.X + rect.Width - margin, 0.1225981f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[54] = new VertexPositionColorTexture(new Vector3(0.04783535f * margin + rect.X + rect.Width - margin, 0.1154848f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[55] = new VertexPositionColorTexture(new Vector3(0.06944633f * margin + rect.X + rect.Width - margin, 0.1039336f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[56] = new VertexPositionColorTexture(new Vector3(0.08838832f * margin + rect.X + rect.Width - margin, 0.0883882f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[57] = new VertexPositionColorTexture(new Vector3(0.1039337f * margin + rect.X + rect.Width - margin, 0.06944609f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[58] = new VertexPositionColorTexture(new Vector3(0.115485f * margin + rect.X + rect.Width - margin, 0.04783523f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[59] = new VertexPositionColorTexture(new Vector3(0.1225982f * margin + rect.X + rect.Width - margin, 0.02438617f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[60] = new VertexPositionColorTexture(new Vector3(0.4903927f * margin + rect.X + rect.Width - margin, 0.09754467f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[61] = new VertexPositionColorTexture(new Vector3(0.4619399f * margin + rect.X + rect.Width - margin, 0.1913414f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[62] = new VertexPositionColorTexture(new Vector3(0.415735f * margin + rect.X + rect.Width - margin, 0.2777848f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[63] = new VertexPositionColorTexture(new Vector3(0.3535537f * margin + rect.X + rect.Width - margin, 0.3535532f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[64] = new VertexPositionColorTexture(new Vector3(0.2777853f * margin + rect.X + rect.Width - margin, 0.4157346f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[65] = new VertexPositionColorTexture(new Vector3(0.1913419f * margin + rect.X + rect.Width - margin, 0.4619397f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[66] = new VertexPositionColorTexture(new Vector3(0.09754539f * margin + rect.X + rect.Width - margin, 0.4903926f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[67] = new VertexPositionColorTexture(new Vector3(2.384186E-07f * margin + rect.X + rect.Width - margin, 0.5f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[68] = new VertexPositionColorTexture(new Vector3(0.402455f * margin + rect.X, 0.4903927f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[69] = new VertexPositionColorTexture(new Vector3(0.5000002f * margin + rect.X, 0.5f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[70] = new VertexPositionColorTexture(new Vector3(0.3086584f * margin + rect.X, 0.4619398f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[71] = new VertexPositionColorTexture(new Vector3(0.2222149f * margin + rect.X, 0.4157348f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[72] = new VertexPositionColorTexture(new Vector3(0.1464466f * margin + rect.X, 0.3535534f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[73] = new VertexPositionColorTexture(new Vector3(0.08426517f * margin + rect.X, 0.2777851f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[74] = new VertexPositionColorTexture(new Vector3(0.03806022f * margin + rect.X, 0.1913416f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[75] = new VertexPositionColorTexture(new Vector3(0.009607345f * margin + rect.X, 0.09754515f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    #endregion
                },
                Clicked = delegate (float margin, Rectangle rect, out VertexPositionColorTexture[] verticies, out string s)
                {
                    verticies = new VertexPositionColorTexture[76];
                    s = "RoundedWindowStandard";
                    #region MeshDefinition
                    verticies[0] = new VertexPositionColorTexture(new Vector3(0.4756137f * margin + rect.X, 0.3774018f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[1] = new VertexPositionColorTexture(new Vector3(0.4521645f * margin + rect.X, 0.384515f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[2] = new VertexPositionColorTexture(new Vector3(0.4999999f * margin + rect.X, 0.4999999f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[3] = new VertexPositionColorTexture(new Vector3(0.4305537f * margin + rect.X, 0.3960662f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[4] = new VertexPositionColorTexture(new Vector3(0.4116116f * margin + rect.X, 0.4116116f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[5] = new VertexPositionColorTexture(new Vector3(0.3960662f * margin + rect.X, 0.4305536f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[6] = new VertexPositionColorTexture(new Vector3(0.384515f * margin + rect.X, 0.4521645f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[7] = new VertexPositionColorTexture(new Vector3(0.3774018f * margin + rect.X, 0.4756136f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[8] = new VertexPositionColorTexture(new Vector3(0.3749999f * margin + rect.X, 0.4999999f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[9] = new VertexPositionColorTexture(new Vector3(0.125f * margin + rect.X + rect.Width - margin, 0.4999998f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[10] = new VertexPositionColorTexture(new Vector3(0.1225981f * margin + rect.X + rect.Width - margin, 0.4756135f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[11] = new VertexPositionColorTexture(new Vector3(-1.192093E-07f * margin + rect.X + rect.Width - margin, 0.4999999f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[12] = new VertexPositionColorTexture(new Vector3(0.1154848f * margin + rect.X + rect.Width - margin, 0.4521644f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[13] = new VertexPositionColorTexture(new Vector3(0.1039336f * margin + rect.X + rect.Width - margin, 0.4305535f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[14] = new VertexPositionColorTexture(new Vector3(0.0883882f * margin + rect.X + rect.Width - margin, 0.4116114f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[15] = new VertexPositionColorTexture(new Vector3(0.06944609f * margin + rect.X + rect.Width - margin, 0.3960661f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[16] = new VertexPositionColorTexture(new Vector3(0.04783511f * margin + rect.X + rect.Width - margin, 0.3845149f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[17] = new VertexPositionColorTexture(new Vector3(0.02438605f * margin + rect.X + rect.Width - margin, 0.3774017f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[18] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X + rect.Width - margin, 0.3749999f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[19] = new VertexPositionColorTexture(new Vector3(0.4999999f * margin + rect.X, 0.3749999f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[20] = new VertexPositionColorTexture(new Vector3(0.09754443f * margin + rect.X + rect.Width - margin, 0.009607226f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[21] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X + rect.Width - margin, 0f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[22] = new VertexPositionColorTexture(new Vector3(0.191341f * margin + rect.X + rect.Width - margin, 0.03805995f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[23] = new VertexPositionColorTexture(new Vector3(0.2777846f * margin + rect.X + rect.Width - margin, 0.08426481f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[24] = new VertexPositionColorTexture(new Vector3(0.3535529f * margin + rect.X + rect.Width - margin, 0.1464462f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[25] = new VertexPositionColorTexture(new Vector3(0.4157345f * margin + rect.X + rect.Width - margin, 0.2222144f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[26] = new VertexPositionColorTexture(new Vector3(0.4619396f * margin + rect.X + rect.Width - margin, 0.3086578f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[27] = new VertexPositionColorTexture(new Vector3(0.4903926f * margin + rect.X + rect.Width - margin, 0.4024543f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[28] = new VertexPositionColorTexture(new Vector3(0.5f * margin + rect.X + rect.Width - margin, 0.4999995f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[29] = new VertexPositionColorTexture(new Vector3(0.009607375f * margin + rect.X, 0.4024548f * margin + rect.Y, 0), new Color(0.9960784f, 0.9960784f, 0.9960784f, 1f), Vector2.Zero);
                    verticies[30] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X, 0.5f * margin + rect.Y, 0), new Color(0.9960784f, 0.9960784f, 0.9960784f, 1f), Vector2.Zero);
                    verticies[31] = new VertexPositionColorTexture(new Vector3(0.03806025f * margin + rect.X, 0.3086583f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[32] = new VertexPositionColorTexture(new Vector3(0.08426517f * margin + rect.X, 0.2222149f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[33] = new VertexPositionColorTexture(new Vector3(0.1464466f * margin + rect.X, 0.1464466f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[34] = new VertexPositionColorTexture(new Vector3(0.2222149f * margin + rect.X, 0.0842652f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[35] = new VertexPositionColorTexture(new Vector3(0.3086583f * margin + rect.X, 0.03806025f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[36] = new VertexPositionColorTexture(new Vector3(0.4024549f * margin + rect.X, 0.009607375f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[37] = new VertexPositionColorTexture(new Vector3(0.5f * margin + rect.X, 0f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[38] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X, 0f * margin + rect.Y + rect.Height - margin, 0), new Color(0.9882353f, 0.9882353f, 0.9882353f, 1f), Vector2.Zero);
                    verticies[39] = new VertexPositionColorTexture(new Vector3(0.3749999f * margin + rect.X, -1.192093E-07f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[40] = new VertexPositionColorTexture(new Vector3(0.4999999f * margin + rect.X, -1.192093E-07f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[41] = new VertexPositionColorTexture(new Vector3(0.5f * margin + rect.X + rect.Width - margin, -4.768372E-07f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[42] = new VertexPositionColorTexture(new Vector3(0.125f * margin + rect.X + rect.Width - margin, -2.384186E-07f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[43] = new VertexPositionColorTexture(new Vector3(-1.192093E-07f * margin + rect.X + rect.Width - margin, -1.192093E-07f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[44] = new VertexPositionColorTexture(new Vector3(0.3774018f * margin + rect.X, 0.02438617f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[45] = new VertexPositionColorTexture(new Vector3(0.384515f * margin + rect.X, 0.04783535f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[46] = new VertexPositionColorTexture(new Vector3(0.3960662f * margin + rect.X, 0.06944621f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[47] = new VertexPositionColorTexture(new Vector3(0.4116116f * margin + rect.X, 0.0883882f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[48] = new VertexPositionColorTexture(new Vector3(0.4305537f * margin + rect.X, 0.1039336f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[49] = new VertexPositionColorTexture(new Vector3(0.4521645f * margin + rect.X, 0.1154848f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[50] = new VertexPositionColorTexture(new Vector3(0.4756137f * margin + rect.X, 0.1225981f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[51] = new VertexPositionColorTexture(new Vector3(0.5f * margin + rect.X, 0.1249999f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[52] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X + rect.Width - margin, 0.1249999f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[53] = new VertexPositionColorTexture(new Vector3(0.02438629f * margin + rect.X + rect.Width - margin, 0.1225981f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[54] = new VertexPositionColorTexture(new Vector3(0.04783535f * margin + rect.X + rect.Width - margin, 0.1154848f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[55] = new VertexPositionColorTexture(new Vector3(0.06944633f * margin + rect.X + rect.Width - margin, 0.1039336f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[56] = new VertexPositionColorTexture(new Vector3(0.08838832f * margin + rect.X + rect.Width - margin, 0.0883882f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[57] = new VertexPositionColorTexture(new Vector3(0.1039337f * margin + rect.X + rect.Width - margin, 0.06944609f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[58] = new VertexPositionColorTexture(new Vector3(0.115485f * margin + rect.X + rect.Width - margin, 0.04783523f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[59] = new VertexPositionColorTexture(new Vector3(0.1225982f * margin + rect.X + rect.Width - margin, 0.02438617f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[60] = new VertexPositionColorTexture(new Vector3(0.4903927f * margin + rect.X + rect.Width - margin, 0.09754467f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[61] = new VertexPositionColorTexture(new Vector3(0.4619399f * margin + rect.X + rect.Width - margin, 0.1913414f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[62] = new VertexPositionColorTexture(new Vector3(0.415735f * margin + rect.X + rect.Width - margin, 0.2777848f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[63] = new VertexPositionColorTexture(new Vector3(0.3535537f * margin + rect.X + rect.Width - margin, 0.3535532f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[64] = new VertexPositionColorTexture(new Vector3(0.2777853f * margin + rect.X + rect.Width - margin, 0.4157346f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[65] = new VertexPositionColorTexture(new Vector3(0.1913419f * margin + rect.X + rect.Width - margin, 0.4619397f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[66] = new VertexPositionColorTexture(new Vector3(0.09754539f * margin + rect.X + rect.Width - margin, 0.4903926f * margin + rect.Y + rect.Height - margin, 0), new Color(0.9960784f, 0.9960784f, 0.9960784f, 1f), Vector2.Zero);
                    verticies[67] = new VertexPositionColorTexture(new Vector3(2.384186E-07f * margin + rect.X + rect.Width - margin, 0.5f * margin + rect.Y + rect.Height - margin, 0), new Color(0.9960784f, 0.9960784f, 0.9960784f, 1f), Vector2.Zero);
                    verticies[68] = new VertexPositionColorTexture(new Vector3(0.402455f * margin + rect.X, 0.4903927f * margin + rect.Y + rect.Height - margin, 0), new Color(0.9960784f, 0.9960784f, 0.9960784f, 1f), Vector2.Zero);
                    verticies[69] = new VertexPositionColorTexture(new Vector3(0.5000002f * margin + rect.X, 0.5f * margin + rect.Y + rect.Height - margin, 0), new Color(0.9960784f, 0.9960784f, 0.9960784f, 1f), Vector2.Zero);
                    verticies[70] = new VertexPositionColorTexture(new Vector3(0.3086584f * margin + rect.X, 0.4619398f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[71] = new VertexPositionColorTexture(new Vector3(0.2222149f * margin + rect.X, 0.4157348f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[72] = new VertexPositionColorTexture(new Vector3(0.1464466f * margin + rect.X, 0.3535534f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[73] = new VertexPositionColorTexture(new Vector3(0.08426517f * margin + rect.X, 0.2777851f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[74] = new VertexPositionColorTexture(new Vector3(0.03806022f * margin + rect.X, 0.1913416f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[75] = new VertexPositionColorTexture(new Vector3(0.009607345f * margin + rect.X, 0.09754515f * margin + rect.Y + rect.Height - margin, 0), new Color(0.9960784f, 0.9960784f, 0.9960784f, 1f), Vector2.Zero);
                    #endregion
                },
                Indicies = delegate(out int[] indicies)
                {
                    indicies = new int[342] { 0, 1, 2, 1, 3, 2, 3, 4, 2, 4, 5, 2, 5, 6, 2, 6, 7, 2, 7, 8, 2, 9, 10, 11, 10, 12, 11, 12, 13, 11, 13, 14, 11, 14, 15, 11, 15, 16, 11, 16, 17, 11, 17, 18, 11, 19, 0, 2, 20, 21, 18, 20, 18, 17, 22, 20, 17, 22, 17, 16, 23, 22, 16, 23, 16, 15, 24, 23, 15, 24, 15, 14, 25, 24, 14, 25, 14, 13, 26, 25, 13, 26, 13, 12, 27, 26, 12, 27, 12, 10, 28, 27, 10, 28, 10, 9, 29, 30, 8, 29, 8, 7, 31, 29, 7, 31, 7, 6, 32, 31, 6, 32, 6, 5, 33, 32, 5, 33, 5, 4, 34, 33, 4, 34, 4, 3, 35, 34, 3, 35, 3, 1, 36, 35, 1, 36, 1, 0, 37, 36, 0, 37, 0, 19, 30, 38, 39, 30, 39, 8, 8, 39, 40, 8, 40, 2, 41, 28, 9, 41, 9, 42, 42, 9, 11, 42, 11, 43, 21, 37, 19, 21, 19, 18, 18, 19, 2, 18, 2, 11, 40, 43, 11, 40, 11, 2, 39, 44, 40, 44, 45, 40, 45, 46, 40, 46, 47, 40, 47, 48, 40, 48, 49, 40, 49, 50, 40, 50, 51, 40, 52, 53, 43, 53, 54, 43, 54, 55, 43, 55, 56, 43, 56, 57, 43, 57, 58, 43, 58, 59, 43, 59, 42, 43, 60, 41, 42, 60, 42, 59, 61, 60, 59, 61, 59, 58, 62, 61, 58, 62, 58, 57, 63, 62, 57, 63, 57, 56, 64, 63, 56, 64, 56, 55, 65, 64, 55, 65, 55, 54, 66, 65, 54, 66, 54, 53, 67, 66, 53, 67, 53, 52, 68, 69, 51, 68, 51, 50, 70, 68, 50, 70, 50, 49, 71, 70, 49, 71, 49, 48, 72, 71, 48, 72, 48, 47, 73, 72, 47, 73, 47, 46, 74, 73, 46, 74, 46, 45, 75, 74, 45, 75, 45, 44, 38, 75, 44, 38, 44, 39, 69, 67, 52, 69, 52, 51, 51, 52, 43, 51, 43, 40, };
                }
            });
            contextComponent.Style.AddStyle(MemoryConstants.ButtonID, new SomeStyleClass()
            {
                margin = 10,
                Alignment = Alignment.CenterAlinged,
                Inactive = delegate (float margin, Rectangle rect, out VertexPositionColorTexture[] verticies, out string s)
               {
                   verticies = new VertexPositionColorTexture[76];
                   s = "RoundedWindowStandard";
                    #region MeshDefinition
                    verticies[0] = new VertexPositionColorTexture(new Vector3(0.4756137f * margin + rect.X, 0.3774018f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                   verticies[1] = new VertexPositionColorTexture(new Vector3(0.4521645f * margin + rect.X, 0.384515f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                   verticies[2] = new VertexPositionColorTexture(new Vector3(0.4999999f * margin + rect.X, 0.4999999f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                   verticies[3] = new VertexPositionColorTexture(new Vector3(0.4305537f * margin + rect.X, 0.3960662f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                   verticies[4] = new VertexPositionColorTexture(new Vector3(0.4116116f * margin + rect.X, 0.4116116f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                   verticies[5] = new VertexPositionColorTexture(new Vector3(0.3960662f * margin + rect.X, 0.4305536f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                   verticies[6] = new VertexPositionColorTexture(new Vector3(0.384515f * margin + rect.X, 0.4521645f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                   verticies[7] = new VertexPositionColorTexture(new Vector3(0.3774018f * margin + rect.X, 0.4756136f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                   verticies[8] = new VertexPositionColorTexture(new Vector3(0.3749999f * margin + rect.X, 0.4999999f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                   verticies[9] = new VertexPositionColorTexture(new Vector3(0.125f * margin + rect.X + rect.Width - margin, 0.4999998f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                   verticies[10] = new VertexPositionColorTexture(new Vector3(0.1225981f * margin + rect.X + rect.Width - margin, 0.4756135f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                   verticies[11] = new VertexPositionColorTexture(new Vector3(-1.192093E-07f * margin + rect.X + rect.Width - margin, 0.4999999f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                   verticies[12] = new VertexPositionColorTexture(new Vector3(0.1154848f * margin + rect.X + rect.Width - margin, 0.4521644f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                   verticies[13] = new VertexPositionColorTexture(new Vector3(0.1039336f * margin + rect.X + rect.Width - margin, 0.4305535f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                   verticies[14] = new VertexPositionColorTexture(new Vector3(0.0883882f * margin + rect.X + rect.Width - margin, 0.4116114f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                   verticies[15] = new VertexPositionColorTexture(new Vector3(0.06944609f * margin + rect.X + rect.Width - margin, 0.3960661f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                   verticies[16] = new VertexPositionColorTexture(new Vector3(0.04783511f * margin + rect.X + rect.Width - margin, 0.3845149f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                   verticies[17] = new VertexPositionColorTexture(new Vector3(0.02438605f * margin + rect.X + rect.Width - margin, 0.3774017f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                   verticies[18] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X + rect.Width - margin, 0.3749999f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                   verticies[19] = new VertexPositionColorTexture(new Vector3(0.4999999f * margin + rect.X, 0.3749999f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                   verticies[20] = new VertexPositionColorTexture(new Vector3(0.09754443f * margin + rect.X + rect.Width - margin, 0.009607226f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                   verticies[21] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X + rect.Width - margin, 0f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                   verticies[22] = new VertexPositionColorTexture(new Vector3(0.191341f * margin + rect.X + rect.Width - margin, 0.03805995f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                   verticies[23] = new VertexPositionColorTexture(new Vector3(0.2777846f * margin + rect.X + rect.Width - margin, 0.08426481f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                   verticies[24] = new VertexPositionColorTexture(new Vector3(0.3535529f * margin + rect.X + rect.Width - margin, 0.1464462f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                   verticies[25] = new VertexPositionColorTexture(new Vector3(0.4157345f * margin + rect.X + rect.Width - margin, 0.2222144f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                   verticies[26] = new VertexPositionColorTexture(new Vector3(0.4619396f * margin + rect.X + rect.Width - margin, 0.3086578f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                   verticies[27] = new VertexPositionColorTexture(new Vector3(0.4903926f * margin + rect.X + rect.Width - margin, 0.4024543f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                   verticies[28] = new VertexPositionColorTexture(new Vector3(0.5f * margin + rect.X + rect.Width - margin, 0.4999995f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                   verticies[29] = new VertexPositionColorTexture(new Vector3(0.009607375f * margin + rect.X, 0.4024548f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                   verticies[30] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X, 0.5f * margin + rect.Y, 0), new Color(0.4588235f, 0.4588235f, 0.4588235f, 1f), Vector2.Zero);
                   verticies[31] = new VertexPositionColorTexture(new Vector3(0.03806025f * margin + rect.X, 0.3086583f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                   verticies[32] = new VertexPositionColorTexture(new Vector3(0.08426517f * margin + rect.X, 0.2222149f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                   verticies[33] = new VertexPositionColorTexture(new Vector3(0.1464466f * margin + rect.X, 0.1464466f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                   verticies[34] = new VertexPositionColorTexture(new Vector3(0.2222149f * margin + rect.X, 0.0842652f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                   verticies[35] = new VertexPositionColorTexture(new Vector3(0.3086583f * margin + rect.X, 0.03806025f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                   verticies[36] = new VertexPositionColorTexture(new Vector3(0.4024549f * margin + rect.X, 0.009607375f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                   verticies[37] = new VertexPositionColorTexture(new Vector3(0.5f * margin + rect.X, 0f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                   verticies[38] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X, 0f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                   verticies[39] = new VertexPositionColorTexture(new Vector3(0.3749999f * margin + rect.X, -1.192093E-07f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                   verticies[40] = new VertexPositionColorTexture(new Vector3(0.4999999f * margin + rect.X, -1.192093E-07f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                   verticies[41] = new VertexPositionColorTexture(new Vector3(0.5f * margin + rect.X + rect.Width - margin, -4.768372E-07f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                   verticies[42] = new VertexPositionColorTexture(new Vector3(0.125f * margin + rect.X + rect.Width - margin, -2.384186E-07f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                   verticies[43] = new VertexPositionColorTexture(new Vector3(-1.192093E-07f * margin + rect.X + rect.Width - margin, -1.192093E-07f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                   verticies[44] = new VertexPositionColorTexture(new Vector3(0.3774018f * margin + rect.X, 0.02438617f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                   verticies[45] = new VertexPositionColorTexture(new Vector3(0.384515f * margin + rect.X, 0.04783535f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                   verticies[46] = new VertexPositionColorTexture(new Vector3(0.3960662f * margin + rect.X, 0.06944621f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                   verticies[47] = new VertexPositionColorTexture(new Vector3(0.4116116f * margin + rect.X, 0.0883882f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                   verticies[48] = new VertexPositionColorTexture(new Vector3(0.4305537f * margin + rect.X, 0.1039336f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                   verticies[49] = new VertexPositionColorTexture(new Vector3(0.4521645f * margin + rect.X, 0.1154848f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                   verticies[50] = new VertexPositionColorTexture(new Vector3(0.4756137f * margin + rect.X, 0.1225981f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                   verticies[51] = new VertexPositionColorTexture(new Vector3(0.5f * margin + rect.X, 0.1249999f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                   verticies[52] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X + rect.Width - margin, 0.1249999f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                   verticies[53] = new VertexPositionColorTexture(new Vector3(0.02438629f * margin + rect.X + rect.Width - margin, 0.1225981f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                   verticies[54] = new VertexPositionColorTexture(new Vector3(0.04783535f * margin + rect.X + rect.Width - margin, 0.1154848f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                   verticies[55] = new VertexPositionColorTexture(new Vector3(0.06944633f * margin + rect.X + rect.Width - margin, 0.1039336f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                   verticies[56] = new VertexPositionColorTexture(new Vector3(0.08838832f * margin + rect.X + rect.Width - margin, 0.0883882f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                   verticies[57] = new VertexPositionColorTexture(new Vector3(0.1039337f * margin + rect.X + rect.Width - margin, 0.06944609f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                   verticies[58] = new VertexPositionColorTexture(new Vector3(0.115485f * margin + rect.X + rect.Width - margin, 0.04783523f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                   verticies[59] = new VertexPositionColorTexture(new Vector3(0.1225982f * margin + rect.X + rect.Width - margin, 0.02438617f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                   verticies[60] = new VertexPositionColorTexture(new Vector3(0.4903927f * margin + rect.X + rect.Width - margin, 0.09754467f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                   verticies[61] = new VertexPositionColorTexture(new Vector3(0.4619399f * margin + rect.X + rect.Width - margin, 0.1913414f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                   verticies[62] = new VertexPositionColorTexture(new Vector3(0.415735f * margin + rect.X + rect.Width - margin, 0.2777848f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                   verticies[63] = new VertexPositionColorTexture(new Vector3(0.3535537f * margin + rect.X + rect.Width - margin, 0.3535532f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                   verticies[64] = new VertexPositionColorTexture(new Vector3(0.2777853f * margin + rect.X + rect.Width - margin, 0.4157346f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                   verticies[65] = new VertexPositionColorTexture(new Vector3(0.1913419f * margin + rect.X + rect.Width - margin, 0.4619397f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                   verticies[66] = new VertexPositionColorTexture(new Vector3(0.09754539f * margin + rect.X + rect.Width - margin, 0.4903926f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                   verticies[67] = new VertexPositionColorTexture(new Vector3(2.384186E-07f * margin + rect.X + rect.Width - margin, 0.5f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                   verticies[68] = new VertexPositionColorTexture(new Vector3(0.402455f * margin + rect.X, 0.4903927f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                   verticies[69] = new VertexPositionColorTexture(new Vector3(0.5000002f * margin + rect.X, 0.5f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                   verticies[70] = new VertexPositionColorTexture(new Vector3(0.3086584f * margin + rect.X, 0.4619398f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                   verticies[71] = new VertexPositionColorTexture(new Vector3(0.2222149f * margin + rect.X, 0.4157348f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                   verticies[72] = new VertexPositionColorTexture(new Vector3(0.1464466f * margin + rect.X, 0.3535534f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                   verticies[73] = new VertexPositionColorTexture(new Vector3(0.08426517f * margin + rect.X, 0.2777851f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                   verticies[74] = new VertexPositionColorTexture(new Vector3(0.03806022f * margin + rect.X, 0.1913416f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                   verticies[75] = new VertexPositionColorTexture(new Vector3(0.009607345f * margin + rect.X, 0.09754515f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    #endregion
                },
                Hot = delegate (float margin, Rectangle rect, out VertexPositionColorTexture[] verticies, out string s)
                {
                    verticies = new VertexPositionColorTexture[76];
                    s = "RoundedWindowStandard";
                    #region MeshDefinition
                    verticies[0] = new VertexPositionColorTexture(new Vector3(0.4756137f * margin + rect.X, 0.3774018f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[1] = new VertexPositionColorTexture(new Vector3(0.4521645f * margin + rect.X, 0.384515f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[2] = new VertexPositionColorTexture(new Vector3(0.4999999f * margin + rect.X, 0.4999999f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[3] = new VertexPositionColorTexture(new Vector3(0.4305537f * margin + rect.X, 0.3960662f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[4] = new VertexPositionColorTexture(new Vector3(0.4116116f * margin + rect.X, 0.4116116f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[5] = new VertexPositionColorTexture(new Vector3(0.3960662f * margin + rect.X, 0.4305536f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[6] = new VertexPositionColorTexture(new Vector3(0.384515f * margin + rect.X, 0.4521645f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[7] = new VertexPositionColorTexture(new Vector3(0.3774018f * margin + rect.X, 0.4756136f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[8] = new VertexPositionColorTexture(new Vector3(0.3749999f * margin + rect.X, 0.4999999f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[9] = new VertexPositionColorTexture(new Vector3(0.125f * margin + rect.X + rect.Width - margin, 0.4999998f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[10] = new VertexPositionColorTexture(new Vector3(0.1225981f * margin + rect.X + rect.Width - margin, 0.4756135f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[11] = new VertexPositionColorTexture(new Vector3(-1.192093E-07f * margin + rect.X + rect.Width - margin, 0.4999999f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[12] = new VertexPositionColorTexture(new Vector3(0.1154848f * margin + rect.X + rect.Width - margin, 0.4521644f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[13] = new VertexPositionColorTexture(new Vector3(0.1039336f * margin + rect.X + rect.Width - margin, 0.4305535f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[14] = new VertexPositionColorTexture(new Vector3(0.0883882f * margin + rect.X + rect.Width - margin, 0.4116114f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[15] = new VertexPositionColorTexture(new Vector3(0.06944609f * margin + rect.X + rect.Width - margin, 0.3960661f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[16] = new VertexPositionColorTexture(new Vector3(0.04783511f * margin + rect.X + rect.Width - margin, 0.3845149f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[17] = new VertexPositionColorTexture(new Vector3(0.02438605f * margin + rect.X + rect.Width - margin, 0.3774017f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[18] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X + rect.Width - margin, 0.3749999f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[19] = new VertexPositionColorTexture(new Vector3(0.4999999f * margin + rect.X, 0.3749999f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[20] = new VertexPositionColorTexture(new Vector3(0.09754443f * margin + rect.X + rect.Width - margin, 0.009607226f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[21] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X + rect.Width - margin, 0f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[22] = new VertexPositionColorTexture(new Vector3(0.191341f * margin + rect.X + rect.Width - margin, 0.03805995f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[23] = new VertexPositionColorTexture(new Vector3(0.2777846f * margin + rect.X + rect.Width - margin, 0.08426481f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[24] = new VertexPositionColorTexture(new Vector3(0.3535529f * margin + rect.X + rect.Width - margin, 0.1464462f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[25] = new VertexPositionColorTexture(new Vector3(0.4157345f * margin + rect.X + rect.Width - margin, 0.2222144f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[26] = new VertexPositionColorTexture(new Vector3(0.4619396f * margin + rect.X + rect.Width - margin, 0.3086578f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[27] = new VertexPositionColorTexture(new Vector3(0.4903926f * margin + rect.X + rect.Width - margin, 0.4024543f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[28] = new VertexPositionColorTexture(new Vector3(0.5f * margin + rect.X + rect.Width - margin, 0.4999995f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[29] = new VertexPositionColorTexture(new Vector3(0.009607375f * margin + rect.X, 0.4024548f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[30] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X, 0.5f * margin + rect.Y, 0), new Color(0.4588235f, 0.4588235f, 0.4588235f, 1f), Vector2.Zero);
                    verticies[31] = new VertexPositionColorTexture(new Vector3(0.03806025f * margin + rect.X, 0.3086583f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[32] = new VertexPositionColorTexture(new Vector3(0.08426517f * margin + rect.X, 0.2222149f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[33] = new VertexPositionColorTexture(new Vector3(0.1464466f * margin + rect.X, 0.1464466f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[34] = new VertexPositionColorTexture(new Vector3(0.2222149f * margin + rect.X, 0.0842652f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[35] = new VertexPositionColorTexture(new Vector3(0.3086583f * margin + rect.X, 0.03806025f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[36] = new VertexPositionColorTexture(new Vector3(0.4024549f * margin + rect.X, 0.009607375f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[37] = new VertexPositionColorTexture(new Vector3(0.5f * margin + rect.X, 0f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[38] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X, 0f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[39] = new VertexPositionColorTexture(new Vector3(0.3749999f * margin + rect.X, -1.192093E-07f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[40] = new VertexPositionColorTexture(new Vector3(0.4999999f * margin + rect.X, -1.192093E-07f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[41] = new VertexPositionColorTexture(new Vector3(0.5f * margin + rect.X + rect.Width - margin, -4.768372E-07f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[42] = new VertexPositionColorTexture(new Vector3(0.125f * margin + rect.X + rect.Width - margin, -2.384186E-07f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[43] = new VertexPositionColorTexture(new Vector3(-1.192093E-07f * margin + rect.X + rect.Width - margin, -1.192093E-07f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[44] = new VertexPositionColorTexture(new Vector3(0.3774018f * margin + rect.X, 0.02438617f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[45] = new VertexPositionColorTexture(new Vector3(0.384515f * margin + rect.X, 0.04783535f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[46] = new VertexPositionColorTexture(new Vector3(0.3960662f * margin + rect.X, 0.06944621f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[47] = new VertexPositionColorTexture(new Vector3(0.4116116f * margin + rect.X, 0.0883882f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[48] = new VertexPositionColorTexture(new Vector3(0.4305537f * margin + rect.X, 0.1039336f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[49] = new VertexPositionColorTexture(new Vector3(0.4521645f * margin + rect.X, 0.1154848f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[50] = new VertexPositionColorTexture(new Vector3(0.4756137f * margin + rect.X, 0.1225981f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[51] = new VertexPositionColorTexture(new Vector3(0.5f * margin + rect.X, 0.1249999f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[52] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X + rect.Width - margin, 0.1249999f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[53] = new VertexPositionColorTexture(new Vector3(0.02438629f * margin + rect.X + rect.Width - margin, 0.1225981f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[54] = new VertexPositionColorTexture(new Vector3(0.04783535f * margin + rect.X + rect.Width - margin, 0.1154848f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[55] = new VertexPositionColorTexture(new Vector3(0.06944633f * margin + rect.X + rect.Width - margin, 0.1039336f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[56] = new VertexPositionColorTexture(new Vector3(0.08838832f * margin + rect.X + rect.Width - margin, 0.0883882f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[57] = new VertexPositionColorTexture(new Vector3(0.1039337f * margin + rect.X + rect.Width - margin, 0.06944609f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[58] = new VertexPositionColorTexture(new Vector3(0.115485f * margin + rect.X + rect.Width - margin, 0.04783523f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[59] = new VertexPositionColorTexture(new Vector3(0.1225982f * margin + rect.X + rect.Width - margin, 0.02438617f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[60] = new VertexPositionColorTexture(new Vector3(0.4903927f * margin + rect.X + rect.Width - margin, 0.09754467f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[61] = new VertexPositionColorTexture(new Vector3(0.4619399f * margin + rect.X + rect.Width - margin, 0.1913414f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[62] = new VertexPositionColorTexture(new Vector3(0.415735f * margin + rect.X + rect.Width - margin, 0.2777848f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[63] = new VertexPositionColorTexture(new Vector3(0.3535537f * margin + rect.X + rect.Width - margin, 0.3535532f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[64] = new VertexPositionColorTexture(new Vector3(0.2777853f * margin + rect.X + rect.Width - margin, 0.4157346f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[65] = new VertexPositionColorTexture(new Vector3(0.1913419f * margin + rect.X + rect.Width - margin, 0.4619397f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[66] = new VertexPositionColorTexture(new Vector3(0.09754539f * margin + rect.X + rect.Width - margin, 0.4903926f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[67] = new VertexPositionColorTexture(new Vector3(2.384186E-07f * margin + rect.X + rect.Width - margin, 0.5f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[68] = new VertexPositionColorTexture(new Vector3(0.402455f * margin + rect.X, 0.4903927f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[69] = new VertexPositionColorTexture(new Vector3(0.5000002f * margin + rect.X, 0.5f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[70] = new VertexPositionColorTexture(new Vector3(0.3086584f * margin + rect.X, 0.4619398f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[71] = new VertexPositionColorTexture(new Vector3(0.2222149f * margin + rect.X, 0.4157348f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[72] = new VertexPositionColorTexture(new Vector3(0.1464466f * margin + rect.X, 0.3535534f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[73] = new VertexPositionColorTexture(new Vector3(0.08426517f * margin + rect.X, 0.2777851f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[74] = new VertexPositionColorTexture(new Vector3(0.03806022f * margin + rect.X, 0.1913416f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[75] = new VertexPositionColorTexture(new Vector3(0.009607345f * margin + rect.X, 0.09754515f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    #endregion
                },
                Clicked = delegate (float margin, Rectangle rect, out VertexPositionColorTexture[] verticies, out string s)
                {
                    verticies = new VertexPositionColorTexture[76];
                    s = "RoundedWindowStandard";
                    #region MeshDefinition
                    verticies[0] = new VertexPositionColorTexture(new Vector3(0.4756137f * margin + rect.X, 0.3774018f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[1] = new VertexPositionColorTexture(new Vector3(0.4521645f * margin + rect.X, 0.384515f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[2] = new VertexPositionColorTexture(new Vector3(0.4999999f * margin + rect.X, 0.4999999f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[3] = new VertexPositionColorTexture(new Vector3(0.4305537f * margin + rect.X, 0.3960662f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[4] = new VertexPositionColorTexture(new Vector3(0.4116116f * margin + rect.X, 0.4116116f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[5] = new VertexPositionColorTexture(new Vector3(0.3960662f * margin + rect.X, 0.4305536f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[6] = new VertexPositionColorTexture(new Vector3(0.384515f * margin + rect.X, 0.4521645f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[7] = new VertexPositionColorTexture(new Vector3(0.3774018f * margin + rect.X, 0.4756136f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[8] = new VertexPositionColorTexture(new Vector3(0.3749999f * margin + rect.X, 0.4999999f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[9] = new VertexPositionColorTexture(new Vector3(0.125f * margin + rect.X + rect.Width - margin, 0.4999998f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[10] = new VertexPositionColorTexture(new Vector3(0.1225981f * margin + rect.X + rect.Width - margin, 0.4756135f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[11] = new VertexPositionColorTexture(new Vector3(-1.192093E-07f * margin + rect.X + rect.Width - margin, 0.4999999f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[12] = new VertexPositionColorTexture(new Vector3(0.1154848f * margin + rect.X + rect.Width - margin, 0.4521644f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[13] = new VertexPositionColorTexture(new Vector3(0.1039336f * margin + rect.X + rect.Width - margin, 0.4305535f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[14] = new VertexPositionColorTexture(new Vector3(0.0883882f * margin + rect.X + rect.Width - margin, 0.4116114f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[15] = new VertexPositionColorTexture(new Vector3(0.06944609f * margin + rect.X + rect.Width - margin, 0.3960661f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[16] = new VertexPositionColorTexture(new Vector3(0.04783511f * margin + rect.X + rect.Width - margin, 0.3845149f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[17] = new VertexPositionColorTexture(new Vector3(0.02438605f * margin + rect.X + rect.Width - margin, 0.3774017f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[18] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X + rect.Width - margin, 0.3749999f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[19] = new VertexPositionColorTexture(new Vector3(0.4999999f * margin + rect.X, 0.3749999f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[20] = new VertexPositionColorTexture(new Vector3(0.09754443f * margin + rect.X + rect.Width - margin, 0.009607226f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[21] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X + rect.Width - margin, 0f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[22] = new VertexPositionColorTexture(new Vector3(0.191341f * margin + rect.X + rect.Width - margin, 0.03805995f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[23] = new VertexPositionColorTexture(new Vector3(0.2777846f * margin + rect.X + rect.Width - margin, 0.08426481f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[24] = new VertexPositionColorTexture(new Vector3(0.3535529f * margin + rect.X + rect.Width - margin, 0.1464462f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[25] = new VertexPositionColorTexture(new Vector3(0.4157345f * margin + rect.X + rect.Width - margin, 0.2222144f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[26] = new VertexPositionColorTexture(new Vector3(0.4619396f * margin + rect.X + rect.Width - margin, 0.3086578f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[27] = new VertexPositionColorTexture(new Vector3(0.4903926f * margin + rect.X + rect.Width - margin, 0.4024543f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[28] = new VertexPositionColorTexture(new Vector3(0.5f * margin + rect.X + rect.Width - margin, 0.4999995f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[29] = new VertexPositionColorTexture(new Vector3(0.009607375f * margin + rect.X, 0.4024548f * margin + rect.Y, 0), new Color(0.9960784f, 0.9960784f, 0.9960784f, 1f), Vector2.Zero);
                    verticies[30] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X, 0.5f * margin + rect.Y, 0), new Color(0.9960784f, 0.9960784f, 0.9960784f, 1f), Vector2.Zero);
                    verticies[31] = new VertexPositionColorTexture(new Vector3(0.03806025f * margin + rect.X, 0.3086583f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[32] = new VertexPositionColorTexture(new Vector3(0.08426517f * margin + rect.X, 0.2222149f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[33] = new VertexPositionColorTexture(new Vector3(0.1464466f * margin + rect.X, 0.1464466f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[34] = new VertexPositionColorTexture(new Vector3(0.2222149f * margin + rect.X, 0.0842652f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[35] = new VertexPositionColorTexture(new Vector3(0.3086583f * margin + rect.X, 0.03806025f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[36] = new VertexPositionColorTexture(new Vector3(0.4024549f * margin + rect.X, 0.009607375f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[37] = new VertexPositionColorTexture(new Vector3(0.5f * margin + rect.X, 0f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[38] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X, 0f * margin + rect.Y + rect.Height - margin, 0), new Color(0.9882353f, 0.9882353f, 0.9882353f, 1f), Vector2.Zero);
                    verticies[39] = new VertexPositionColorTexture(new Vector3(0.3749999f * margin + rect.X, -1.192093E-07f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[40] = new VertexPositionColorTexture(new Vector3(0.4999999f * margin + rect.X, -1.192093E-07f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[41] = new VertexPositionColorTexture(new Vector3(0.5f * margin + rect.X + rect.Width - margin, -4.768372E-07f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[42] = new VertexPositionColorTexture(new Vector3(0.125f * margin + rect.X + rect.Width - margin, -2.384186E-07f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[43] = new VertexPositionColorTexture(new Vector3(-1.192093E-07f * margin + rect.X + rect.Width - margin, -1.192093E-07f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[44] = new VertexPositionColorTexture(new Vector3(0.3774018f * margin + rect.X, 0.02438617f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[45] = new VertexPositionColorTexture(new Vector3(0.384515f * margin + rect.X, 0.04783535f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[46] = new VertexPositionColorTexture(new Vector3(0.3960662f * margin + rect.X, 0.06944621f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[47] = new VertexPositionColorTexture(new Vector3(0.4116116f * margin + rect.X, 0.0883882f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[48] = new VertexPositionColorTexture(new Vector3(0.4305537f * margin + rect.X, 0.1039336f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[49] = new VertexPositionColorTexture(new Vector3(0.4521645f * margin + rect.X, 0.1154848f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[50] = new VertexPositionColorTexture(new Vector3(0.4756137f * margin + rect.X, 0.1225981f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[51] = new VertexPositionColorTexture(new Vector3(0.5f * margin + rect.X, 0.1249999f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[52] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X + rect.Width - margin, 0.1249999f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[53] = new VertexPositionColorTexture(new Vector3(0.02438629f * margin + rect.X + rect.Width - margin, 0.1225981f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[54] = new VertexPositionColorTexture(new Vector3(0.04783535f * margin + rect.X + rect.Width - margin, 0.1154848f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[55] = new VertexPositionColorTexture(new Vector3(0.06944633f * margin + rect.X + rect.Width - margin, 0.1039336f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[56] = new VertexPositionColorTexture(new Vector3(0.08838832f * margin + rect.X + rect.Width - margin, 0.0883882f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[57] = new VertexPositionColorTexture(new Vector3(0.1039337f * margin + rect.X + rect.Width - margin, 0.06944609f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[58] = new VertexPositionColorTexture(new Vector3(0.115485f * margin + rect.X + rect.Width - margin, 0.04783523f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[59] = new VertexPositionColorTexture(new Vector3(0.1225982f * margin + rect.X + rect.Width - margin, 0.02438617f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
                    verticies[60] = new VertexPositionColorTexture(new Vector3(0.4903927f * margin + rect.X + rect.Width - margin, 0.09754467f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[61] = new VertexPositionColorTexture(new Vector3(0.4619399f * margin + rect.X + rect.Width - margin, 0.1913414f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[62] = new VertexPositionColorTexture(new Vector3(0.415735f * margin + rect.X + rect.Width - margin, 0.2777848f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[63] = new VertexPositionColorTexture(new Vector3(0.3535537f * margin + rect.X + rect.Width - margin, 0.3535532f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[64] = new VertexPositionColorTexture(new Vector3(0.2777853f * margin + rect.X + rect.Width - margin, 0.4157346f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[65] = new VertexPositionColorTexture(new Vector3(0.1913419f * margin + rect.X + rect.Width - margin, 0.4619397f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[66] = new VertexPositionColorTexture(new Vector3(0.09754539f * margin + rect.X + rect.Width - margin, 0.4903926f * margin + rect.Y + rect.Height - margin, 0), new Color(0.9960784f, 0.9960784f, 0.9960784f, 1f), Vector2.Zero);
                    verticies[67] = new VertexPositionColorTexture(new Vector3(2.384186E-07f * margin + rect.X + rect.Width - margin, 0.5f * margin + rect.Y + rect.Height - margin, 0), new Color(0.9960784f, 0.9960784f, 0.9960784f, 1f), Vector2.Zero);
                    verticies[68] = new VertexPositionColorTexture(new Vector3(0.402455f * margin + rect.X, 0.4903927f * margin + rect.Y + rect.Height - margin, 0), new Color(0.9960784f, 0.9960784f, 0.9960784f, 1f), Vector2.Zero);
                    verticies[69] = new VertexPositionColorTexture(new Vector3(0.5000002f * margin + rect.X, 0.5f * margin + rect.Y + rect.Height - margin, 0), new Color(0.9960784f, 0.9960784f, 0.9960784f, 1f), Vector2.Zero);
                    verticies[70] = new VertexPositionColorTexture(new Vector3(0.3086584f * margin + rect.X, 0.4619398f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[71] = new VertexPositionColorTexture(new Vector3(0.2222149f * margin + rect.X, 0.4157348f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[72] = new VertexPositionColorTexture(new Vector3(0.1464466f * margin + rect.X, 0.3535534f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[73] = new VertexPositionColorTexture(new Vector3(0.08426517f * margin + rect.X, 0.2777851f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[74] = new VertexPositionColorTexture(new Vector3(0.03806022f * margin + rect.X, 0.1913416f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
                    verticies[75] = new VertexPositionColorTexture(new Vector3(0.009607345f * margin + rect.X, 0.09754515f * margin + rect.Y + rect.Height - margin, 0), new Color(0.9960784f, 0.9960784f, 0.9960784f, 1f), Vector2.Zero);
                    #endregion
                },
                Indicies = delegate (out int[] indicies)
                {
                    indicies = new int[342] { 0, 1, 2, 1, 3, 2, 3, 4, 2, 4, 5, 2, 5, 6, 2, 6, 7, 2, 7, 8, 2, 9, 10, 11, 10, 12, 11, 12, 13, 11, 13, 14, 11, 14, 15, 11, 15, 16, 11, 16, 17, 11, 17, 18, 11, 19, 0, 2, 20, 21, 18, 20, 18, 17, 22, 20, 17, 22, 17, 16, 23, 22, 16, 23, 16, 15, 24, 23, 15, 24, 15, 14, 25, 24, 14, 25, 14, 13, 26, 25, 13, 26, 13, 12, 27, 26, 12, 27, 12, 10, 28, 27, 10, 28, 10, 9, 29, 30, 8, 29, 8, 7, 31, 29, 7, 31, 7, 6, 32, 31, 6, 32, 6, 5, 33, 32, 5, 33, 5, 4, 34, 33, 4, 34, 4, 3, 35, 34, 3, 35, 3, 1, 36, 35, 1, 36, 1, 0, 37, 36, 0, 37, 0, 19, 30, 38, 39, 30, 39, 8, 8, 39, 40, 8, 40, 2, 41, 28, 9, 41, 9, 42, 42, 9, 11, 42, 11, 43, 21, 37, 19, 21, 19, 18, 18, 19, 2, 18, 2, 11, 40, 43, 11, 40, 11, 2, 39, 44, 40, 44, 45, 40, 45, 46, 40, 46, 47, 40, 47, 48, 40, 48, 49, 40, 49, 50, 40, 50, 51, 40, 52, 53, 43, 53, 54, 43, 54, 55, 43, 55, 56, 43, 56, 57, 43, 57, 58, 43, 58, 59, 43, 59, 42, 43, 60, 41, 42, 60, 42, 59, 61, 60, 59, 61, 59, 58, 62, 61, 58, 62, 58, 57, 63, 62, 57, 63, 57, 56, 64, 63, 56, 64, 56, 55, 65, 64, 55, 65, 55, 54, 66, 65, 54, 66, 54, 53, 67, 66, 53, 67, 53, 52, 68, 69, 51, 68, 51, 50, 70, 68, 50, 70, 50, 49, 71, 70, 49, 71, 49, 48, 72, 71, 48, 72, 48, 47, 73, 72, 47, 73, 47, 46, 74, 73, 46, 74, 46, 45, 75, 74, 45, 75, 45, 44, 38, 75, 44, 38, 44, 39, 69, 67, 52, 69, 52, 51, 51, 52, 43, 51, 43, 40, };
                }
            });
            effect = Content.Load<Effect>("ImEffect");
            //effect.Parameters["MatrixTransform"].SetValue(Matrix.CreateOrthographic(graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight, 0, 100));
            effect.Parameters["MatrixTransform"].SetValue(Matrix.CreateOrthographicOffCenter(0, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height, 0, -10, 100));
            iBuffer = new IndexBuffer(GraphicsDevice, IndexElementSize.ThirtyTwoBits, 342*3, BufferUsage.None);
            vBuffer = new VertexBuffer(GraphicsDevice, VertexPositionColorTexture.VertexDeclaration, 76*3, BufferUsage.None);
            gameTarget = new RenderTarget2D(GraphicsDevice, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height, false, SurfaceFormat.Color, DepthFormat.None);
            contextComponent.SetTarget(gameTarget);
            base.LoadContent();
        }
        public bool Window2 = false;
        RenderTarget2D gameTarget;
        protected override void Update(GameTime gameTime)
        {
            oldState = newState;
            newState = Mouse.GetState();

            contextComponent.MousePos = newState.Position;
            contextComponent.clickHappened = (newState.LeftButton == ButtonState.Pressed && oldState.LeftButton == ButtonState.Released);

            if(newState.LeftButton == ButtonState.Pressed && oldState.LeftButton == ButtonState.Released)
            {
                var wait = 0;
            }
            
            effect.Parameters["MatrixTransform"].SetValue(Matrix.CreateOrthographicOffCenter(0, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height, 0, 0,-1));
            contextComponent.Update(gameTime);
            contextComponent.SetCursor("Mouse", newState.Position, newState.LeftButton);
            MemoryGui.Context = contextComponent;

            //MemoryGui.Window("WindowName", new Point(20, 20), new Point(400, 400));
            //if(newState.LeftButton == ButtonState.Pressed)
            //{
            //    MemoryGui.Button("sOMEdEBUG");
            //}
            MemoryGui.BeginCanvas();
            if (MemoryGui.Button("Lamp"))
            {
                var stop = 0;
                Exit();
            }
            if(MemoryGui.Button("Active Calibration"))
            {
                Window2 = !Window2;
            }
            MemoryGui.Button("Active Calibration");
            MemoryGui.Button("Active Calibration");
            MemoryGui.Button("Active Calibration");
            MemoryGui.Button("Active Calibration");

            if(Window2)
            {
                MemoryGui.Window("Window Two",new Point (200,200),new Point(300,300));
            }
           // MemoryGui.Window("WindowName");
            //MemoryGui.Button("Active Calibration");
            //if (MemoryGui.Button("Some New Button"))
            //{
            //    Exit();
            //}
            //if (MemoryGui.Button("Button", new Point(400, 40), new Point(120, 100)))
            //{
            //    Exit();
            //}

            //if (MemoryGui.Button("CLamp", new Point(40, 40), new Point(120, 100)))
            //{
            //    Exit();
            //}
            //if (MemoryGui.Button("Lamp", new Point(40, 160), new Point(120, 100)))
            //{
            //    Exit();
            //}
            //if (MemoryGui.Button("mapping", new Point(32, 100), new Point(120, 100)))
            //{
            //    Exit();
            //}
            //if (MemoryGui.Button("mipping", new Point(22, 300), new Point(50, 50)))
            //{
            //    Exit();
            //}
            contextComponent.LateUpdate(gameTime);

            //float wait = 0;
        }
        protected override void Draw(GameTime gameTime)
        {

            

            //vBuffer.SetData(verticies.ToArray());
            //iBuffer.SetData(indicies.ToArray());

            RasterizerState rState = new RasterizerState()
            {
                CullMode = CullMode.None,
                DepthBias = 0,
                FillMode = FillMode.Solid,
                MultiSampleAntiAlias = false,
                ScissorTestEnable = true,
                SlopeScaleDepthBias = 0
            };
            GraphicsDevice.Clear(Color.Pink);
            GraphicsDevice.BlendFactor = Color.White;
            GraphicsDevice.BlendState = BlendState.NonPremultiplied;
            GraphicsDevice.RasterizerState = rState;
            GraphicsDevice.DepthStencilState = DepthStencilState.DepthRead;

            //GraphicsDevice.RasterizerState = RasterizerState.CullNone;
            contextComponent.BeforeDraw(GraphicsDevice);
            GraphicsDevice.SetVertexBuffer(contextComponent.vBuffer);
            GraphicsDevice.Indices = contextComponent.iBuffer;
            var beforeClip = GraphicsDevice.ScissorRectangle;

            if(contextComponent.drawCommands.Count > 11)
            {
                //throw new Exception("Improperly Cleared Buffer");
            }

            foreach (var command in contextComponent.drawCommands)
            {
                //GraphicsDevice.ScissorRectangle = command.clippingRect;
                effect.Parameters["Color"].SetValue(command.color.ToVector4());
                //foreach (var pass in effect.CurrentTechnique.Passes)
                //{
                //    pass.Apply();
                //}
                if(command.texture != null)
                {
                    effect.Parameters["SpriteTexture"].SetValue(command.texture);
                    effect.CurrentTechnique.Passes["HasTexture"].Apply();
                }
                else
                {
                    effect.CurrentTechnique.Passes["P0"].Apply();
                }

                //GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList
                //, command.vertexOffset,
                //command.indexStart,
                //command.primitiveCount);//cool.
                //oh effect right. do that at lunch
                GraphicsDevice.DrawUserIndexedPrimitives(PrimitiveType.TriangleList, contextComponent.verticies, command.vertexOffset, command.vertexCount, contextComponent.indicies, command.indexOffset, command.primitiveCount);


#pragma warning disable CS0618
                //GraphicsDevice.DrawIndexedPrimitives(
                //    primitiveType: PrimitiveType.TriangleList,
                //    baseVertex: command.vertexOffset,
                //    minVertexIndex: 0,
                //    numVertices: command.vertexCount,
                //    startIndex: command.indexOffset,
                //    primitiveCount: command.primitiveCount);
#pragma warning restore CS0618

            }
            GraphicsDevice.ScissorRectangle = beforeClip;
                       contextComponent.AfterDraw();
            //spriteBatch.Begin();
            //spriteBatch.Draw(contextComponent.font.Texture, Vector2.Zero, Color.White);
            //spriteBatch.End();

            VertexPositionColorTexture[] verticies = new VertexPositionColorTexture[8];
            int[] indicies = new int[6] {0,1,2,2,1,3 };

            var glyph = contextComponent.font.GetGlyphs()['c'];

            float TexelWidth = (float)1 / contextComponent.font.Texture.Width;
            float TexelHeight = (float)1 / contextComponent.font.Texture.Height;

            Vector2 TopPosition = new Vector2(glyph.Cropping.X,glyph.Cropping.Y);
            Vector2 BotPosition = new Vector2(TopPosition.X + glyph.BoundsInTexture.Width, TopPosition.Y + glyph.BoundsInTexture.Height);
            Vector2 TopUV = new Vector2(glyph.BoundsInTexture.X * TexelWidth , glyph.BoundsInTexture.Y * TexelHeight);
            Vector2 BotUV = new Vector2((glyph.BoundsInTexture.X + glyph.BoundsInTexture.Width) * TexelWidth, (glyph.BoundsInTexture.Y + glyph.BoundsInTexture.Height) * TexelHeight);

            verticies[0] = new VertexPositionColorTexture(new Vector3(TopPosition.X, TopPosition.Y, 0), Color.Green, new Vector2(TopUV.X, TopUV.Y));
            verticies[1] = new VertexPositionColorTexture(new Vector3(TopPosition.X, BotPosition.Y, 0), Color.Green, new Vector2(TopUV.X, BotUV.Y));
            verticies[2] = new VertexPositionColorTexture(new Vector3(BotPosition.X, TopPosition.Y, 0), Color.Green, new Vector2(BotUV.X, TopUV.Y));
            verticies[3] = new VertexPositionColorTexture(new Vector3(BotPosition.X, BotPosition.Y, 0), Color.Green, new Vector2(BotUV.X, BotUV.Y));

            verticies[4] = new VertexPositionColorTexture(new Vector3(TopPosition.X + 30, TopPosition.Y, 0), Color.Green, new Vector2(TopUV.X, TopUV.Y));
            verticies[5] = new VertexPositionColorTexture(new Vector3(TopPosition.X + 30, BotPosition.Y, 0), Color.Green, new Vector2(TopUV.X, BotUV.Y));
            verticies[6] = new VertexPositionColorTexture(new Vector3(BotPosition.X + 30, TopPosition.Y, 0), Color.Green, new Vector2(BotUV.X, TopUV.Y));
            verticies[7] = new VertexPositionColorTexture(new Vector3(BotPosition.X + 30, BotPosition.Y, 0), Color.Green, new Vector2(BotUV.X, BotUV.Y));
            effect.Parameters["SpriteTexture"].SetValue(contextComponent.font.Texture);
            effect.CurrentTechnique.Passes["HasTexture"].Apply();

            GraphicsDevice.DrawUserIndexedPrimitives(PrimitiveType.TriangleList, verticies, 0, 4, indicies, 0, 2);
            GraphicsDevice.DrawUserIndexedPrimitives(PrimitiveType.TriangleList, verticies, 4, 4, indicies, 0, 2);



            //okay so. what the fcuck
            DrawingCollection c = StyleBuilder.BuildRoundedFrame(0,"",new Rectangle(0,0,200,200),30);
            //effect.CurrentTechnique.Passes["P0"].Apply();
            //GraphicsDevice.DrawUserIndexedPrimitives(PrimitiveType.TriangleList, c.verticies, 0, c.verticies.Length, c.indicies, 0, c.indicies.Length / 3);


            //GraphicsDevice.SetVertexBuffer(vBuffer);
            //GraphicsDevice.Indices = iBuffer;
            //GraphicsDevice.RasterizerState = rState;
            //GraphicsDevice.BlendFactor = Color.White;
            //GraphicsDevice.BlendState = BlendState.NonPremultiplied;
            //GraphicsDevice.RasterizerState = rState;
            //GraphicsDevice.DepthStencilState = DepthStencilState.DepthRead;

            //verticies.Clear();
            //indicies.Clear();
            //var data = StyleBuilder.BuildRoundedFrame(0, "lamp", new Rectangle(0, 0, 400, 400), 30);
            //verticies.AddRange(data.verticies);
            //indicies.AddRange(data.indicies);

            //data = StyleBuilder.BuildRoundedFrame(0, "clamp", new Rectangle(new Point(20, 20), new Point(120, 100)),30);
            //verticies.AddRange(data.verticies);
            //indicies.AddRange(data.indicies);

            //data = StyleBuilder.BuildRoundedFrame(0, "lLamp", new Rectangle(new Point(20, 200), new Point(100, 120)), 30);
            //verticies.AddRange(data.verticies);
            //indicies.AddRange(data.indicies);

            //effect.Parameters["Color"].SetValue(Color.White.ToVector4());
            //foreach(var pass in effect.CurrentTechnique.Passes)
            //{
            //    pass.Apply();
            //}
            //GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList,0, 0, 114);
            //GraphicsDevice.DrawUserIndexedPrimitives(PrimitiveType.TriangleList,verticies.ToArray(), 0, 76, indicies.ToArray(), 0, 114);
            //GraphicsDevice.DrawUserIndexedPrimitives(PrimitiveType.TriangleList, verticies.ToArray(), 76, 76, indicies.ToArray(), 342, 114);


        }
    }

}

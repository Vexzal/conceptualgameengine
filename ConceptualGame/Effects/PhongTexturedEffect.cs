﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using BaseEngine.Components;
namespace ConceptualGame.Effects
{
    class PhongTexturedEffect : Component
    {
        TransformComponent transform;
        //need camera
        public Effect phongEffect; //the thing were here for
        Camera camera;
        //VV the texture variables
        EffectParameter _World;
        EffectParameter _WorldViewProjection;
        EffectParameter _specularIntensity;
        EffectParameter _specularPower;
        EffectParameter _Texture;
        
        public Texture2D Texture
        {
            set { _Texture.SetValue(value); }
        }
        public float SpecularIntensity
        {
            set { _specularIntensity.SetValue(value); }
        }
        public float SpecularPower
        {
            set { _specularPower.SetValue(value); }
        }
        public Matrix World
        {
            set { _World.SetValue(value); }
        }
        public Matrix WorldViewProjection
        {
            set { _WorldViewProjection.SetValue(value); }
        }
        
        //constructor
        public PhongTexturedEffect()
        {
            
        }
        public override void Initialize()
        {
            transform = gameObject.GetComponent<TransformComponent>();
            _World = phongEffect.Parameters["World"];
            _WorldViewProjection = phongEffect.Parameters["WorldViewProjection"];
            _specularIntensity = phongEffect.Parameters["specularIntensity"];
            _specularPower = phongEffect.Parameters["specularPower"];
            _Texture = phongEffect.Parameters["Texture"];
        }

        public void Apply()
        {
            World = transform.GetParentedTransform();
            WorldViewProjection = transform.GetParentedTransform() * camera.View * camera.Projection;
            foreach(EffectPass pass in phongEffect.CurrentTechnique.Passes)
            {
                pass.Apply();
            }
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using BaseEngine;
using BaseEngine.Rendering;
using BaseEngine.Components;
using BaseEngine.BepuPhysics;
namespace ConceptualGame
{
    class EditorRenderingModule : IRenderingModule
    {
        public short drawPriority { get; set; }
        Scene gameScene;
        public ICamera Camera { get; set; }
        public GraphicsDevice graphics { get; set; }
        public RenderTargetManager TargetManager { get; set; }
        public List<IModelMesh> meshes = new List<IModelMesh>();
        List<IModelMesh> frameMesh = new List<IModelMesh>();

        public void Draw(GameTime gameTime)
        {
            
            meshes.Clear();
            frameMesh.Clear();

            foreach (GameObject go in gameScene.gameObjects)
            {
                foreach (Component c in go.components.Values)
                {
                    if (c is IModelMesh)
                    {
                        meshes.Add((IModelMesh)c);
                        break;
                    }
                    if(c is ICamera)
                    {
                        Camera = (ICamera)c;
                    }
                }
            }

            BoundingFrustum cameraFrustrum = Camera.CameraFrustrum;

            foreach (var m in meshes)
            {
                foreach (ModelMesh mesh in m.DrawModel.Meshes)
                {
                    if (cameraFrustrum.Intersects(mesh.BoundingSphere))
                    {
                        frameMesh.Add(m);
                        break;
                    }
                }
            }
            foreach (var m in frameMesh)
            {
                foreach (ModelMesh mesh in m.DrawModel.Meshes)
                {
                    foreach (Effect e in mesh.Effects)
                    {
                        var bE = e as BasicEffect;
                        bE.EnableDefaultLighting();
                        bE.World = m.WorldTransform;
                        bE.View = Camera.View;
                        bE.Projection = Camera.Projection;
                    }
                    mesh.Draw();
                }

            }
        }

        public void Load(Scene gameScene)
        {
            this.gameScene = gameScene;
            //do I need to do this when all meshes should be added by the editor.
        }
    }
    public class XClass
    {
        public static T A<T>(Microsoft.Xna.Framework.Content.ContentManager content,string asset)
        {
            return content.Load<T>(asset); 
        }
    }
    
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : CoreGame
    {
        #region notes
        //I'd have here a big list of assets
        //models
        //shaders
        //textures
        //audio
        //animations

        //logical components
        //objects for specific things
        //other stuff
        //physics setup

        //physics world

        //rendering
        /** so the idea is you add every model 
         *  and it's effect and relevant effect properties
         *  to a list that the renderer steps through and renders.
         *  
         **/
        //physics
        /**
         *  like rendering but not like rendering.
         *  uhhhh
         *  yeah?
         *  alright so we have a collection of physics items that are added to the world
         *  that at least is straight forward enough
         **/
        //

        #endregion

        //Scene gameScene;
        Assets _assets;
        //RenderingModule renderer;
        //PhysicsModule bepuPhysics;
        
        public Game1() :
            base()
        {
            
            MeshComponent mmEsh = new MeshComponent();
            //mmEsh.mesh = XClass.A<Model>(base.Content, "");
            
        }

        

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            //graphics.PreferredBackBufferHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
            //graphics.PreferredBackBufferWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            //graphics.IsFullScreen = true;
            // TODO: Add your initialization logic here
            gameScene = new Scene("someName");
            EngineServices.Renderer = new RenderingService(this);
            EngineServices.Renderer.primaryModule = new EditorRenderingModule();
            EngineServices.Physics = new BepuPhysicsService(this);
            _assets = new Assets();
            graphics.ApplyChanges();
            
            base.Initialize();

        }        

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            // TODO: use this.Content to load your game content here
            //base.renderer.pipeline = new PhongDeferredRendering(this);
            _assets.Load(Content);
            LoadScene();
            gameScene.Initial();
            //gameScene.Init();
            //LoadShipScene();
            //Services.renderer.gameScene = gameScene;
            //Services.physics.gameScene = gameScene;
            base.gameScene = gameScene;
            Matrix[] array = new Matrix[3] { Matrix.Identity, Matrix.Identity, Matrix.Identity };
            SkinnedEffect xeffect = new SkinnedEffect(GraphicsDevice);
            EngineServices.Renderer.Load(gameScene);
            EngineServices.Physics.Load(gameScene);
            base.LoadContent();
        }
        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }
        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();
            //input
            BaseEngine.ObsoleteInput.Update();
            //logic
            LogicUpdate?.Invoke(gameTime);
            //physics
 
            
            // TODO: Add your update logic here
            //update logical components and render info
            base.Update(gameTime);
        }
        public delegate void Updatable(GameTime gameTime);
        public event Updatable LogicUpdate;
        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CadetBlue);
            //Services.renderer.Draw(gameTime);
            // TODO: Add your drawing code here
            
            //do all drawing

            base.Draw(gameTime);
        }
        void LoadShipScene()
        {
            gameScene = new Scene("ShipScene");
            #region Camera
            GameObject Camera = new GameObject();
            Camera.Name = "Camera";
            Camera.GetComponent<TransformComponent>().Position = ((Vector3.Normalize(new Vector3(0, -2, 1))) * 15);
            Camera cameraComponent = new Camera();
            cameraComponent.Pipeline = new PhongDeferredRendering(this);
            cameraComponent.Up = Vector3.UnitZ;
            cameraComponent.target = new TransformComponent();
            cameraComponent.aspect = (float)graphics.PreferredBackBufferWidth / graphics.PreferredBackBufferHeight;
            cameraComponent.fov = MathHelper.ToRadians(30);
            cameraComponent.nearClipPlane = 1;
            cameraComponent.farClipPlane = 100;
            cameraComponent.Enambled = true;            
            Camera.AddComponent(cameraComponent);
            #endregion
            #region ship
            GameObject Ship = new GameObject();
            Ship.Name = "Ship";
            MeshComponent ShipModel = new MeshComponent();
            ShipModel.mesh = _assets.Ship;
            Ship.AddComponent(ShipModel);
            EffectComponent ShipEffect = new EffectComponent();
            ShipEffect.Effect = _assets.shipEffect;
            Ship.AddComponent(ShipEffect);
            #endregion

            gameScene.AddGameObject(Camera);
            gameScene.AddGameObject(Ship);
            

            gameScene.Initial();
        }
        void LoadScene()
        {
            gameScene = new Scene("SomeScene");

            #region Camera
            GameObject Camera = new GameObject();
            Camera.GetComponent<TransformComponent>().Position = ((Vector3.Normalize(new Vector3(0, -2, 1))) * 15);
            Camera cameraComponent = new Camera();

            cameraComponent.Up = Vector3.UnitZ;
            cameraComponent.target = new TransformComponent();            
            cameraComponent.aspect = (float)graphics.PreferredBackBufferWidth / graphics.PreferredBackBufferHeight;
            cameraComponent.fov = MathHelper.ToRadians(30);
            cameraComponent.nearClipPlane = 1;
            cameraComponent.farClipPlane = 100;
            cameraComponent.Enambled = true;
            gameScene.InitializeScene += cameraComponent.Initialize;
            Camera.AddComponent(cameraComponent);

            #endregion
            #region Player
            GameObject CubeObject = new GameObject();
            CubeObject.Name = "Cube";
            CubeObject.GetComponent<TransformComponent>().Position = new Vector3(0, 0, 5);

            MeshComponent CubeMesh = new MeshComponent();
            CubeMesh.mesh = _assets.Cube;
            gameScene.InitializeScene += CubeMesh.Initialize;            
            CubeObject.AddComponent(CubeMesh);

            CharacterControllerComponent characterController = new CharacterControllerComponent()
            {
                height = 2                 
            };
            gameScene.InitializeScene += characterController.Initialize;
            CubeObject.AddComponent(characterController);

            Scripts.PlayerControl playerScript = new Scripts.PlayerControl();
            gameScene.InitializeScene += playerScript.Initialize;
            LogicUpdate += playerScript.Update;
            CubeObject.AddComponent(playerScript);

            #endregion
            #region floor

            GameObject PlaneObject = new GameObject();
            PlaneObject.Name = "Plane";

            MeshComponent PlaneMesh = new MeshComponent();
            PlaneMesh.mesh = _assets.Plane;
            PlaneObject.AddComponent(PlaneMesh);
            gameScene.InitializeScene += PlaneMesh.Initialize;

            EntityComponent FloorEntity = new EntityComponent()
            {
                shape = _assets.FloorShape,
                dynamic = false,
                mass = 0,
                Offset = Matrix.CreateTranslation(0, 0, _assets.FloorShape.Length / 2)
            };
            gameScene.InitializeScene += FloorEntity.Initialize;
            PlaneObject.AddComponent(FloorEntity);

            #endregion
            gameScene.AddGameObject(Camera);
            gameScene.AddGameObject(CubeObject);
            gameScene.AddGameObject(PlaneObject);


        }
    }
}

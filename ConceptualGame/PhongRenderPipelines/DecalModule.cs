﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using BaseEngine;
using BaseEngine.Components;
using BaseEngine.Rendering;
using BaseEngine.Extensions;

namespace BaseEngine.Components
{
    public class DecalComponent : Component, IDecal
    {
        //actually this should contain some reference to the pass that gets applied on the effect.
        //for if its normal only or such
        public int DecalTechniqueIndex { get; set; }
        public List<DecalTextureBinding> DecalParams { get => decalParams; }
        TransformComponent transform;
        public Matrix World { get => transform.GetParentedTransform(); }

        public List<DecalTextureBinding> decalParams = new List<DecalTextureBinding>();
        public override void Initialize()
        {
            transform = gameObject.GetComponent<TransformComponent>();
        }
    }
    public struct DecalTextureBinding
    {
        //oh all of this sucks
        //default values can be, varius things, so.
        //eughhh.
        //I haaate it.
        //public bool HasTexture;//Iii don't need this//lets assume texures are present. its weird for them not to be I guess
        bool isColorValue;
        //public object DefaultValue;//so. The idea here is I can set this as whatever right???
        //okay no you need to pass a proper type to the hecking, thing.
        //well so by default its either a whole color or a float, right
        //it could be a float 2 or vector3 map but you can meaningfully clamp those I guess
        public float DefaultFloat;
        public Color DefaultColor;
        public Texture2D Texture;
        public string ParamName;
    }
}
namespace BaseEngine.Rendering
{

    interface IDecal
    {
        int DecalTechniqueIndex { get; }
        List<DecalTextureBinding> DecalParams { get; }
        Matrix World { get;  }
    }
}

namespace ConceptualGame.PhongPipelineModules
{
    //this whole file is cursed and should be burned.
    

    class DecalModule : IRenderingModule, IRenderTargetDependant
    {
        public short drawPriority { get; set; }
        public ICamera Camera { get; set; }
        public RenderTargetManager TargetManager {get;set;}
        public GraphicsDevice graphics { get; set; }
        BasicEffect debugEffect;
        public Effect DecalEffect { get; set; }
        public Model DecalModel { get; set; }
        List<IDecal> decals = new List<IDecal>();
        public DecalModule()
        {
            drawPriority = 2;            
        }

        public void Load(Scene scene)
        {
            TargetManager.UpdateBindings += BindTargets;
            decals.Clear();
            foreach(GameObject go in scene.gameObjects)
            {
                foreach(Component c in go.components.Values)
                {
                    if(c is IDecal)
                    {
                        decals.Add((IDecal)c);
                    }
                }
            }
            //load decal listvvvvvvvvvvvvvvv 
        }
        public void Draw(GameTime gameTime)
        {
            graphics.DepthStencilState = DepthStencilState.DepthRead;
            graphics.BlendState = BlendState.AlphaBlend;
            //do decal effects

            //decal map binding // actually no that goes in the full binding thing// lets just replace the full binding thing with the whole rendertargetmanager and grab whats needed because dancing around it s U C K S

            //so actually do decal engine constants
            //then do pass application
            //now draw
            DecalEffect.Parameters[EngineConstants.ViewConstant].SetValue(Camera.View);
            DecalEffect.Parameters[EngineConstants.ProjectionConstant].SetValue(Camera.Projection);
            DecalEffect.Parameters[EngineConstants.InverseVP].SetValue(Matrix.Invert(Camera.View * Camera.Projection));
            


            foreach(var decal in decals)
            {
                foreach(ModelMesh mesh in DecalModel.Meshes)
                {
                    foreach(ModelMeshPart e in mesh.MeshParts)
                    {
                        foreach(DecalTextureBinding binding in decal.DecalParams)
                        {
                            DecalEffect.Parameters[binding.ParamName].SetValue(binding.Texture);
                        }
                        DecalEffect.Parameters[EngineConstants.WorldConstnat].SetValue(decal.World);
                        DecalEffect.Parameters[EngineConstants.InverseWorld].SetValue(Matrix.Invert(decal.World));
                        e.Effect = DecalEffect;
                        e.Effect.CurrentTechnique = DecalEffect.Techniques[decal.DecalTechniqueIndex];   //no.
                        //no each modle needs its own decal effect
                        
                    }
                    mesh.Draw();
                }
            }
        }

        public void BindTargets(RenderTargetManager.LookupContainer<RenderTargetBinding> bindings)
        {
                DecalEffect.Parameters["depthMap"].SetValue(bindings[NameConstants.GBDepth].RenderTarget);
            
        }
    }
}

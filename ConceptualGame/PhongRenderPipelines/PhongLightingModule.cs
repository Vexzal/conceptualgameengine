﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseEngine;
using BaseEngine.Components;
using BaseEngine.Rendering;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ConceptualGame.PhongPipelineModules
{
    class PhongLightingModule : IRenderingModule, IRenderTargetDependant
    {
        public short drawPriority { get; set; }

        //these should just share the rt manager.
        public ICamera Camera { get; set; }
        public GraphicsDevice graphics { get; set; }
       
        public RenderTargetManager TargetManager { get; set; }

        public Effect DirectionalLightEffect;
        List<IDirectionalLight> DirectionalLights = new List<IDirectionalLight>();
        public Model PointSphere;
        public Effect PointLightEffect;
        List<IPointLight> PointLights = new List<IPointLight>();

        BlendState lightBlendState = new BlendState
        {
            AlphaSourceBlend = Blend.One,
            ColorSourceBlend = Blend.One,
            ColorDestinationBlend = Blend.One,
            AlphaDestinationBlend = Blend.One
        };

        public PhongLightingModule()
        {
            drawPriority = 3;
        }
        
        public void Load(Scene gameScene)
        {
            TargetManager.UpdateBindings += BindTargets;
            foreach(GameObject go in gameScene.gameObjects)
            {
                foreach(Component c in go.components.Values)
                {
                    if(c is IPointLight)
                    {
                        PointLights.Add((IPointLight)c);
                        break;
                    }
                    if(c is IDirectionalLight)
                    {
                        DirectionalLights.Add((IDirectionalLight)c);
                        break;
                    }
                }
                
            }
        }
        public void DrawDirectionalLight()
        {
            foreach(IDirectionalLight dLight in DirectionalLights)
            {
                DirectionalLightEffect.Parameters["lightDirection"].SetValue(dLight.Direction);
                DirectionalLightEffect.Parameters["lightColor"].SetValue(dLight.Color.ToVector3());
                ConstantsBinder.BindConstants(DirectionalLightEffect.Parameters, Camera, Matrix.Identity, 0);
                //apply effect
                foreach(EffectPass pass in DirectionalLightEffect.CurrentTechnique.Passes)
                {
                    pass.Apply();
                }
                Quad.Draw(graphics);

                //draw fullscreen quad;

            }
        }
        public void DrawPointLight3()
        {
            foreach (var point in PointLights)
            {
                Matrix mMatrix = Matrix.CreateScale(point.Radius) * Matrix.CreateTranslation(point.Position);
                //PointLightEffect.Parameters["World"].SetValue(mMatrix);
                //PointLightEffect.Parameters["View"].SetValue(Camera.View);
                //PointLightEffect.Parameters["Projection"].SetValue(Camera.Projection);

                //PointLightEffect.Parameters["cameraPosition"].SetValue(Camera.Position);
                //PointLightEffect.Parameters["InvertViewProjection"].SetValue(Matrix.Invert(Camera.View * Camera.Projection));
                ConstantsBinder.BindConstants(PointLightEffect.Parameters, Camera, mMatrix, 0);

                PointLightEffect.Parameters["Color"].SetValue(point.Color.ToVector3());
                PointLightEffect.Parameters["lightPosition"].SetValue(point.Position);
                PointLightEffect.Parameters["lightRadius"].SetValue(point.Radius);
                PointLightEffect.Parameters["lightIntensity"].SetValue(point.Intensity);

                float cameraToCenter = Vector3.Distance(Camera.Position, point.Position);
                if(cameraToCenter<point.Radius)
                {
                    graphics.RasterizerState = RasterizerState.CullClockwise;                    
                }
                else
                {
                    graphics.RasterizerState = RasterizerState.CullCounterClockwise;
                }

                foreach(ModelMesh mesh in PointSphere.Meshes)
                {
                    foreach(ModelMeshPart part in mesh.MeshParts)
                    {
                        part.Effect = PointLightEffect;
                    }
                    mesh.Draw();
                }

                graphics.RasterizerState = RasterizerState.CullCounterClockwise;
            }
        }
        public void DrawPointLight()
        {
            //already set color maps in bind targets should of anyway
            foreach(IPointLight pointLight in PointLights)
            {
                ConstantsBinder.BindConstants(PointLightEffect.Parameters, Camera, pointLight.World, 0);//thisis a solid case to change, setting
                //rendundant setting of time and camera data
                //I guess the main example does this too but its a neat little optimization (neat as in tidy not as in cool)
                //(its not cool :( )

                //OH RIGHT I HAVE TO ACTUALLY WRITE THE LIGHT SPECIFIC CODE HECK
                //I might later come back and just write a, a engine binding
                //for lighting models, since they're pretty basic
                //and like the base component only has so much so.
                //but also I don't want to add strictness until I know its okay
                PointLightEffect.Parameters["Color"].SetValue(pointLight.Color.ToVector3());
                PointLightEffect.Parameters["lightPosition"].SetValue(pointLight.Position);
                PointLightEffect.Parameters["lightRadius"].SetValue(pointLight.Radius);
                PointLightEffect.Parameters["lightIntensity"].SetValue(pointLight.Intensity);

                float cameraToCenter = Vector3.Distance(Camera.Position, pointLight.Position);

                if(cameraToCenter<pointLight.Radius)
                {
                    graphics.RasterizerState = RasterizerState.CullClockwise;
                }
                else
                {
                    graphics.RasterizerState = RasterizerState.CullCounterClockwise;
                }
                //foreach(var pass in PointLightEffect.CurrentTechnique.Passes)
                //{
                //    pass.Apply();
                //}

                foreach(ModelMesh mesh in PointSphere.Meshes)
                {
                    foreach(ModelMeshPart part in mesh.MeshParts)
                    {
                        part.Effect = PointLightEffect;//this should be redundant. both of theses should be redundant.
                        //but its all cpu side so I guess, its not too bad? as long as point lights don't get absurdly high

                    }
                    mesh.Draw();
                }
                graphics.RasterizerState = RasterizerState.CullCounterClockwise;
            }
        }
        public void DrawPointLight2()
        {
            foreach(var light in PointLights)
            {
                DrawPointLight(light.Position, light.Color, light.Radius, light.Intensity);
            }
        }
        public void DrawPointLight(Vector3 lightPosition, Color color, float lightRadius, float lightIntensity)
        {
            //PointLightEffect.Parameters["colorMap"].SetValue(colorRT);
            //PointLightEffect.Parameters["normalMap"].SetValue(normalRT);
            //PointLightEffect.Parameters["depthMap"].SetValue(depthRT);

            Matrix mMatrix = Matrix.CreateScale(lightRadius) * Matrix.CreateTranslation(lightPosition);
            PointLightEffect.Parameters["World"].SetValue(mMatrix);
            PointLightEffect.Parameters["View"].SetValue(Camera.View);
            PointLightEffect.Parameters["Projection"].SetValue(Camera.Projection);

            PointLightEffect.Parameters["lightPosition"].SetValue(lightPosition);

            PointLightEffect.Parameters["lightRadius"].SetValue(lightRadius);
            PointLightEffect.Parameters["lightIntensity"].SetValue(lightIntensity);
            PointLightEffect.Parameters["Color"].SetValue(color.ToVector3());
            PointLightEffect.Parameters["cameraPosition"].SetValue(Camera.Position);
            PointLightEffect.Parameters["InvertViewProjection"].SetValue(Matrix.Invert(Camera.View * Camera.Projection));

            float cameraToCenter = Vector3.Distance(Camera.Position, lightPosition);
            if (cameraToCenter < lightRadius)
            {
                graphics.RasterizerState = RasterizerState.CullClockwise;
            }
            else
            {
                graphics.RasterizerState = RasterizerState.CullCounterClockwise;
            }

            //PointLightPass.Apply();

            foreach (ModelMesh mesh in PointSphere.Meshes)
            {
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    part.Effect = PointLightEffect;
                }
                mesh.Draw();
            }

            graphics.RasterizerState = RasterizerState.CullCounterClockwise;

        }

        public void Draw(GameTime gameTime)
        {
            graphics.SetRenderTarget((RenderTarget2D)TargetManager.NRTLookup[NameConstants.LightMap].RenderTarget);
            graphics.BlendState = lightBlendState;
            DrawDirectionalLight();            
            DrawPointLight();
            //DrawPointLight2();
            //DrawPointLight3();
        }
        public void BindTargets(RenderTargetManager.LookupContainer<RenderTargetBinding> bindings)//I guess I can drop the argument, since I'm just using the whole manager now
        {
            //mm do I
            //yeah, yeah
            PointLightEffect.Parameters["colorMap"].SetValue(bindings[NameConstants.GBColor].RenderTarget);
            PointLightEffect.Parameters["normalMap"].SetValue(bindings[NameConstants.GBNormal].RenderTarget);
            PointLightEffect.Parameters["depthMap"].SetValue(bindings[NameConstants.GBDepth].RenderTarget);
            DirectionalLightEffect.Parameters["colorMap"].SetValue(bindings[NameConstants.GBColor].RenderTarget);
            DirectionalLightEffect.Parameters["normalMap"].SetValue(bindings[NameConstants.GBNormal].RenderTarget);
            DirectionalLightEffect.Parameters["depthMap"].SetValue(bindings[NameConstants.GBDepth].RenderTarget);
        }
    }
}

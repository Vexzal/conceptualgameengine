﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using BaseEngine;
using BaseEngine.Rendering;

namespace ConceptualGame.PhongPipelineModules
{
    class CompositeModule : IRenderingModule, IRenderTargetDependant
    {
        public short drawPriority { get; set; }

        public ICamera Camera { get; set; }
        public GraphicsDevice graphics { get; set; }
        public RenderTargetManager TargetManager { get; set; }

        public Effect CompositeEffect { get; set; }

        public void BindTargets(RenderTargetManager.LookupContainer<RenderTargetBinding> bindings)
        {
            CompositeEffect.Parameters["colorMap"].SetValue(bindings[NameConstants.GBColor].RenderTarget);
            CompositeEffect.Parameters["lightMap"].SetValue(bindings[NameConstants.LightMap].RenderTarget);
        }

        public void Draw(GameTime gameTime)
        {
            graphics.SetRenderTarget((RenderTarget2D)TargetManager.NRTLookup[NameConstants.CompositeMap].RenderTarget);
            graphics.BlendState = BlendState.Opaque;
            foreach(var pass in CompositeEffect.CurrentTechnique.Passes)
            {
                pass.Apply();
            }
            Quad.Draw(graphics);


        }

        public void Load(Scene gameScene)
        {
            TargetManager.UpdateBindings += BindTargets;
            drawPriority = 4;
            //throw new NotImplementedException();
        }
    }
}

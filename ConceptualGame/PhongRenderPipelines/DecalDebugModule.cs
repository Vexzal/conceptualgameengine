﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseEngine;
using BaseEngine.Rendering;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ConceptualGame.PhongPipelineModules
{
    public class DecalDebugModule : IRenderingModule
    {
        public short drawPriority { get; set; }

        public ICamera Camera { get; set; }
        public GraphicsDevice graphics { get; set; }
        public RenderTargetManager TargetManager { get; set; }

        public void Draw(GameTime gameTime)
        {
            throw new NotImplementedException();
        }

        public void Load(Scene gameScene)
        {
            throw new NotImplementedException();
        }
    }
}

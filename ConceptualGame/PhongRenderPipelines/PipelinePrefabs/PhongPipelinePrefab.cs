﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
//using BaseEngine.Rendering.Prefabs.PhongPipeline;
using BaseEngine;
using BaseEngine.Rendering;


namespace ConceptualGame.PhongPipelineModules
{
    class PhongPipelinePrefab
    {
        //okay all of this should move into conceptual game itself and then
        //then we'll build a thing in the editor.
        public static IRenderingModule PhongPrefab(CoreGame game)
        {
            var backBufferWidth = game.GraphicsDevice.PresentationParameters.BackBufferWidth;
            var backBufferHeight = game.GraphicsDevice.PresentationParameters.BackBufferHeight;

            GBufferMap gMap = new GBufferMap();
            gMap.Name = "GBuffer";
            gMap.AddMap(NameConstants.GBColor, new RenderTarget2D(game.GraphicsDevice, backBufferWidth, backBufferHeight, false, SurfaceFormat.Color, DepthFormat.Depth16), new List<string>() { "Color", "Specular Intensity" });
            gMap.AddMap(NameConstants.GBNormal, new RenderTarget2D(game.GraphicsDevice, backBufferWidth, backBufferHeight, false, SurfaceFormat.Color, DepthFormat.Depth16), new List<string>() { "Normal", "Specular Power" });
            gMap.AddMap(NameConstants.GBDepth, new RenderTarget2D(game.GraphicsDevice, backBufferWidth, backBufferHeight, false, SurfaceFormat.Single, DepthFormat.Depth16), new List<string>() { "Depth" });

            List<NamedRenderTarget> soloTargets = new List<NamedRenderTarget>();
            soloTargets.Add(new NamedRenderTarget(NameConstants.LightMap, new RenderTargetBinding(new RenderTarget2D(game.GraphicsDevice, backBufferWidth, backBufferHeight, false, SurfaceFormat.Color, DepthFormat.Depth24Stencil8))));

            soloTargets.Add(new NamedRenderTarget(NameConstants.CompositeMap, new RenderTarget2D(game.GraphicsDevice, backBufferWidth, backBufferHeight, false, SurfaceFormat.Color, DepthFormat.Depth16)));
            

            RenderTargetManager Manager = new RenderTargetManager(gMap,soloTargets);

            GenericRenderPipeline PhongPipeline = new GenericRenderPipeline();
            PhongPipeline.graphics = game.GraphicsDevice;
            PhongPipeline.TargetManager = Manager;
            //YOU"VE IMPLEMENTED NO LOADING CODE
            //gbuffer
            PhongGBufferModule gModule = new PhongGBufferModule();
            
            //model effects should be verified for out put I guess
            //but that goes in loading

            //decals
            DecalModule pDecal = new DecalModule();
            pDecal.DecalEffect = game.Content.Load<Effect>(@"ModelEffects/TestDecal2");
            pDecal.DecalModel = game.Content.Load<Model>(@"DeferredModel\DecalModel");
                        
            //lighting
            PhongLightingModule lModule = new PhongLightingModule();
            lModule.DirectionalLightEffect = game.Content.Load<Effect>(@"GbufferEffects/DirectionalLight");
            lModule.PointLightEffect = game.Content.Load<Effect>(@"GBufferEffects/PointLight");
            lModule.PointSphere = game.Content.Load<Model>("PointSphere");
            

            //composite
            CompositeModule cModule = new CompositeModule();
            cModule.CompositeEffect = game.Content.Load<Effect>(@"GBufferEffects/FinalColor");
            DrawSceneModule dsModule = new DrawSceneModule();
            dsModule.Font = game.Content.Load<SpriteFont>("File");
            dsModule.spriteBatch = game.spriteBatch;


            PhongPipeline.Add(gModule, pDecal, lModule, cModule, dsModule);
            //PhongPipeline.Sort();

            return PhongPipeline;
        }
        public static IRenderingModule DebugPrefab(CoreGame game)
        {
            var backBufferWidth = game.GraphicsDevice.PresentationParameters.BackBufferWidth;
            var backBufferHeight = game.GraphicsDevice.PresentationParameters.BackBufferHeight;

            GBufferMap gMap = new GBufferMap();
            gMap.Name = "GBuffer";
            gMap.AddMap(NameConstants.GBColor, new RenderTarget2D(game.GraphicsDevice, backBufferWidth, backBufferHeight, false, SurfaceFormat.Color, DepthFormat.Depth16), new List<string>() { "Color", "Specular Intensity" });
            gMap.AddMap(NameConstants.GBNormal, new RenderTarget2D(game.GraphicsDevice, backBufferWidth, backBufferHeight, false, SurfaceFormat.Color, DepthFormat.Depth16), new List<string>() { "Normal", "Specular Power" });
            gMap.AddMap(NameConstants.GBDepth, new RenderTarget2D(game.GraphicsDevice, backBufferWidth, backBufferHeight, false, SurfaceFormat.Single, DepthFormat.Depth16), new List<string>() { "Depth" });

            List<NamedRenderTarget> soloTargets = new List<NamedRenderTarget>();
            soloTargets.Add(new NamedRenderTarget(NameConstants.LightMap, new RenderTargetBinding(new RenderTarget2D(game.GraphicsDevice, backBufferWidth, backBufferHeight, false, SurfaceFormat.Color, DepthFormat.Depth24Stencil8))));

            soloTargets.Add(new NamedRenderTarget(NameConstants.CompositeMap, new RenderTarget2D(game.GraphicsDevice, backBufferWidth, backBufferHeight, false, SurfaceFormat.Color, DepthFormat.Depth16)));


            RenderTargetManager Manager = new RenderTargetManager(gMap, soloTargets);
            GenericRenderPipeline pipeline = new GenericRenderPipeline();
            pipeline.graphics = game.GraphicsDevice;
            pipeline.TargetManager = Manager;

            PhongGBufferModule gModule = new PhongGBufferModule();
            DrawSceneModule dModule = new DrawSceneModule();
            dModule.Font = game.Content.Load<SpriteFont>("File");

            pipeline.Add(gModule, dModule);
            return pipeline;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BaseEngine;
using BaseEngine.Components;
using BaseEngine.Rendering;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ConceptualGame.PhongPipelineModules
{
    public static class EngineConstants
    {
        public const string WorldConstnat = "World";
        public const string ViewConstant = "View";
        public const string ProjectionConstant = "Projection";
        public const string InverseWorld = "InverseWorld";
        public const string InverseView = "InverseView";
        public const string InverseProjection = "InverseProjection";
        public const string InverseVP = "InverseViewProjection";
        public const string InverseWV = "InverseWorldView";
        public const string InverseWVP = "InverseWorldViewProjection";
        public const string CameraPosition = "cameraPosition";
        public const string DeltaTick = "deltaTime";
    }
    public static class NameConstants
    {
        public const string GBColor = "ColorMap";
        public const string GBNormal = "NormalMap";
        public const string GBDepth = "DepthMap";
        public const string LightMap = "LightMap";
        public const string CompositeMap = "CompositeMap";

        public const string GBufferBind = "GBuffer";
    }
    public static class ConstantsBinder
    {
        //all of these are constant engine values that 
        //consider splitting this for other generic binds like animation
        //or have an updater in animation that makes sure the frames are updated apropriately/
        public static void BindConstants(EffectParameterCollection parameters, ICamera Camera, Matrix Transform, float deltaTime)
        {
            if(parameters[EngineConstants.WorldConstnat] != null)
            {
                parameters[EngineConstants.WorldConstnat].SetValue( Transform);
            }
            if (parameters[EngineConstants.ViewConstant] != null)
            {
                parameters[EngineConstants.ViewConstant].SetValue(Camera.View);
            }
            if (parameters[EngineConstants.ProjectionConstant] != null)
            {
                parameters[EngineConstants.ProjectionConstant].SetValue(Camera.Projection);
            }
            if (parameters[EngineConstants.InverseWorld] != null)
            {
                parameters[EngineConstants.InverseWorld].SetValue(Matrix.Invert(Transform));
            }
            if (parameters[EngineConstants.InverseView] != null)
            {
                parameters[EngineConstants.InverseView].SetValue(Matrix.Invert(Camera.View));
            }
            if (parameters[EngineConstants.InverseProjection] != null)
            {
                parameters[EngineConstants.InverseProjection].SetValue(Matrix.Invert(Camera.Projection));
            }            
            if (parameters[EngineConstants.InverseVP] != null)
            {
                parameters[EngineConstants.InverseVP].SetValue(Matrix.Invert(Camera.View * Camera.Projection));
            }
            if (parameters[EngineConstants.InverseWV] != null)
            {
                parameters[EngineConstants.InverseWV].SetValue(Matrix.Invert(Transform * Camera.View));
            }
            if (parameters[EngineConstants.InverseWVP] != null)
            {
                parameters[EngineConstants.InverseWVP].SetValue(Matrix.Invert(Transform * Camera.View * Camera.Projection));
            }
            if (parameters[EngineConstants.DeltaTick] != null)
            {
                parameters[EngineConstants.DeltaTick].SetValue(deltaTime);
            }
            if (parameters[EngineConstants.CameraPosition] != null)
            {
                parameters[EngineConstants.CameraPosition].SetValue(Camera.Position);
            }

        }
    }
    class PhongGBufferModule : IRenderingModule
    {
        public short drawPriority { get; set; }
        //so
        public ICamera Camera { get; set; }
        public GraphicsDevice graphics { get; set; }        
        public RenderTargetManager TargetManager { get; set; }

        //RenderTargetBinding[] gbuffer;//okay the current setup doesn't accomodate this well
        List<MeshComponent> Models = new List<MeshComponent>(); 

        public PhongGBufferModule()
        {
            drawPriority = 1;
        }

        public void Load(Scene scene)
        {
            foreach(GameObject go in scene.gameObjects)
            {
                foreach(Component c in go.components.Values)
                {
                    if(c is MeshComponent)//TAG FOR REPLACMENT
                    {
                        Models.Add((MeshComponent)c);
                    }
                }
            }
        }
        public void Draw(GameTime gameTime)
        {
            graphics.RasterizerState = RasterizerState.CullCounterClockwise;
            graphics.DepthStencilState = DepthStencilState.Default;
            graphics.BlendState = BlendState.Opaque;

            graphics.SetRenderTargets(TargetManager.MRTLookup[NameConstants.GBufferBind]);///yeaahHHHHHH
            //we need to set engine constants.
            
            //so what all do I need.
            //mm I need the engine constant values I need a constants effect file and a uh. Uhh.
            foreach(MeshComponent model in Models)
            {
                foreach(ModelMesh mesh in model.mesh.Meshes)
                {
                    foreach(ModelMeshPart part in mesh.MeshParts)
                    {
                        part.Effect = model.Effect.Effect;
                        ConstantsBinder.BindConstants(model.Effect.Effect.Parameters, Camera, model.Transform.GetParentedTransform(), (float)gameTime.ElapsedGameTime.TotalSeconds);
                    }
                    mesh.Draw();
                }
            }
        }
        
    }
}

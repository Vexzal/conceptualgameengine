﻿using System;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseEngine;
using BaseEngine.Components;
using BaseEngine.Logic;
using BaseEngine.Physics;

namespace EngineGameScene
{

    public static class SceneGenerator
    {
        public static Scene GenScene()
        {
            Scene returnScene = new Scene("MemoryClient");

            //alright you know what yeah lets start from a networking perspective.
            //so .
            //we have something that tracks client states right?
            //alright what does this game look like/
            //we have a central player that moves



            return returnScene;
        }
        //so some, like things.

        //lets have players as a prefab that we pull from
        public static GameObject PlayerPrefab()
        {
            GameObject ret = new GameObject("PlayerNameHere");

            return ret;
        }

        //so this is the, general prefab for a gameobject
        public static GameObject GeneralPrefab()
        {
            GameObject ret = new GameObject("someName");
            ret.AddComponent(new MeshComponent());
            ret.AddComponent(new EngineComponentScript());
            ret.AddComponent(new BaseEngine.Components.EntityComponent());//need some entity shape, relative to asset shape.
            //is this, correct?
            //alright so lets say I wanna add a camera.
            //we have the camera mesh, 
            //no collider really.
            //thats a thing you should have collision togglable on an object in the editor.
            //because you don't wanna just run into things.
            //and you don't want, just a bunch of things you can run into.
            //
            //seperately what does a like, bsp set look like or a, mesh modeler, animator, 
            //texture editor . or material editor .
            //I should really create a custom component.
            //okay so what does adding a component in run time look like I've never figured that. 


            //entity component for object selection and movement collision.
            //we need some point stored as an origin, that is drawn in the mesh renderer.
            //so when you're building an asset like a model asset or a bsp asset it builds down to, a normal model asset or a sound asset.
            //or a physics shape asset.
            //and then that can be loaded in as a 
            return ret;
        }

    }
    //
    class SomePlayerObject
    {
        //physical player
        //player logic
        //Permissions, what you can and can not edit, open, or create.
        //movement and collision within the editor space
        //managing local menu and local constructs, before synced to server.
        //

    }

    public class EngineComponentScript : LogicComponent
    {
        //CommandTree<GameObject> ObjectTree;//so a normal component is a engine tree.
    }



    //so we generate prefabs for objects to add.
    //so lets start with.




    class CommandTree<T> where T : new()
    {
        //alright what does this look like.
        //so lets go through what a session looks like and how that breaks down technically.
        //you load up or create a new scene.
        //then you can open some menu for adding objects we'll go through what all those are later.
        //so at basic you're adding some prefab game object. 
        //so a scene has commands for moving adding and removing objects.
        //then objects have states for adding removing components.
        //I guess components would have their own trees that determine state.
        //actually hmm the tree would be part of the base object that is compiled down into a game scene.
        //remember I'm not just directly building a scene I'm making a collection of things that are compiled into a game scene.
        //I'm modifing the game scene of the engine with game objects but those aren't one to one with the final compiled game.
        //allllright okay this is good.
        List<Command<T>> CommandNodes = new List<Command<T>>();

        public T GetOutputState()
        {
            return new T();
        }


    }

    //what does making my blocks look like
    //this is a lot easier than nodes because its just a visual version of the thing. 
    //so we have, scope blocks. 
    class Block
    {
        protected List<Type> AcceptableBlocks;
    }



    class ScopeBlock { }

    class NamespaceBlock : Block
    {
        public NamespaceBlock()
        {
            AcceptableBlocks = new List<Type>() { typeof(ClassBlock), typeof(InterfaceBlock), typeof(StructBlock) };
        }
    }

    class ClassBlock : Block
    {
        public ClassBlock()
        {
            AcceptableBlocks = new List<Type>() { typeof(FunctionBlock) };
        }
    }

    class InterfaceBlock : Block { }

    class StructBlock : Block { }

    class AbstractClassBlock : Block { }




    class FunctionBlock : Block { }//hmm
                                   //so I don't like this.
                                   //alright so a function isn't, quite a scope block?
                                   //well alright so blocks have, children of some kind but more importantly types they can 
    
    class OperatorBlock : Block
    {
        //hmm.//alright so creating function operations is gonna be a seperate thing, probably.
        //because scoping and actual operations are very different things.
        //so a function block defines a signature that you can prop into a node chain.
        //I think thats just how I want to do it.
        //hmm. and you can easily spit out the script text. because all any of this is doing is
        //generating script text.
        //because how do you just chain blocks together like I get you can just. chain em up I guess.
        //
        //so you can reference the. I don't know what this line was.
        //so is it just a list of blocks in order?
        //that then get generated into a script. or generates a script into a chain of blocks.
        //
        
    }


    //blocks don't need to contain script code directly, the collection of them can be group parsed into a whole thing.
    //how do I manage variable scope...
    //well I guess every blocks got a list of variables in scope that can be scooped and collected.
    class VariableBlock : Block
    {
        public VariableBlock()
        {
            AcceptableBlocks = new List<Type>(0);
        }
        //so.
        //what gets me here is I want
        //so I'm curious if I need seperate things for properties and fields?
        //properties are generally better but there are cases where they don't work and i just, don't remember where
    }
    //so a class node would look like
    /**
     * [
     * |
     * o property(set)
     * |
     * o property(set)
     * |
     * //alright this doesn't work because how do you do function inputs??
     * //ha. alright so property nodes are like this where there is get sets on them.
     * */
    //lets figure out nodes.

    //so a node is a pretty simple thing. you need


    class Node
    {

    }
    //so the idea is you have a base type passed through sockets.
    class InputSocket : Attribute { }
    class OutputSocket : Attribute { }

    class Socket<T>
    {
        public string Name { get; set; }
        public T Value { get; set; }//hmm. so what this is is getting value from the owning node, right?
                                    //this is the thing thats actually tripping me up what do socket connections actually look like.
        public Socket(string Name)
        {

        }
        public T GetValue()
        {
            return Value;
        }
    }
    class Tree { }

    class CodeAddNode : Node
    {

        Socket<string> InA;
        Socket<string> InB;
        Socket<string> Output;

        public CodeAddNode()
        {
            InA = new Socket<string>("Left");
            InB = new Socket<string>("Right");

        }

        void Operate()
        {
            Output.Value = InA.Value + "+" + InB.Value;
        }

    }
    //so how this works. 
    //so. 
    public abstract class Command<T>//some base command
    {
        //so some data
        //what this data effects. l
    }
    //lets create some stuff quick.
    class NodeTree
    {
        List<Node> nodes;
    }    
}

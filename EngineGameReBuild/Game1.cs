﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;


namespace EngineGame
{
    struct ClippingVector
    {
        public Vector3 Position;
        public Vector3 ABVector;
        public Vector3 ACVector;
    }
    class DemoEffectGame : Game
    {
        public GraphicsDeviceManager graphics;
        Model UIModel;
        Matrix ATransform;
        Matrix BTransform;
        Matrix Projection;
        Effect GUIEffect;

        ClippingVector ScreenClip;
        ClippingVector CanvasClip;
        float margin = 5;

        Vector2 ParentSize;
        Vector2 ChildSize;





        public DemoEffectGame()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

        }
        protected override void LoadContent()
        {
            UIModel = Content.Load<Model>("GUIModel");
            GUIEffect = Content.Load<Effect>("ImGuiEffect");
            base.LoadContent();

            ATransform = Matrix.CreateTranslation(50, 50, 0);//200 BY 200
            BTransform = Matrix.CreateTranslation(0, 100, 0);//100 by 100


            ScreenClip = new ClippingVector()
            {
                Position = new Vector3(0, 0, 0),
                ABVector = new Vector3(graphics.PreferredBackBufferWidth, 0, 0),
                ACVector = new Vector3(0, graphics.PreferredBackBufferHeight, 0),

            };

            Vector3 CanvasPosition = new Vector3(65, 65,0);
            Vector3 CanvasBVector = new Vector3(65 + 185, 65, 0);
            Vector3 CanvasCVector = new Vector3(65,65 + 185, 0);

            CanvasClip = new ClippingVector()
            {
                //Position = //canvas poistion

                //Position = new Vector3(55, 55, 0),
                //ABVector = new Vector3(195, 0, 0),
                //ACVector = new Vector3(0, 195, 0)
                Position = CanvasPosition,
                ABVector = CanvasBVector - CanvasPosition,
                ACVector = CanvasCVector - CanvasBVector 
                

            };//since we're not transforming the panel rotationally this is all I need for that.

            Projection = Matrix.CreateOrthographicOffCenter(0, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height, 0, 0, -1);
        }
        protected override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }
        //heck. wait no thats fine its drawing the model twice I'm not doing instancing yet.
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            GUIEffect.Parameters["Projection"].SetValue(Projection);
            GUIEffect.Parameters["Margin"].SetValue(15.0f);
            //so we set clipping stuff then draw model
            //alright SO
            GUIEffect.Parameters["ClippingPosition"].SetValue(ScreenClip.Position);
            GUIEffect.Parameters["ClippingA"].SetValue(ScreenClip.ABVector);
            GUIEffect.Parameters["ClippingB"].SetValue(ScreenClip.ACVector);
            GUIEffect.Parameters["Size"].SetValue(new Vector2(200, 200));
            GUIEffect.Parameters["Color"].SetValue(Color.DeepPink.ToVector4());
            DrawModel(UIModel, ATransform);
            GUIEffect.Parameters["ClippingPosition"].SetValue(CanvasClip.Position);
            GUIEffect.Parameters["ClippingA"].SetValue(CanvasClip.ABVector);
            GUIEffect.Parameters["ClippingB"].SetValue(CanvasClip.ACVector);
            GUIEffect.Parameters["Size"].SetValue(new Vector2(100, 100));
            GUIEffect.Parameters["Color"].SetValue(Color.Green.ToVector4());
            DrawModel(UIModel, BTransform);


            //get info on clipping space.
            GUIEffect.Parameters["Size"].SetValue(new Vector2(185, 185));
            GUIEffect.Parameters["Margin"].SetValue(1.0f);
            GUIEffect.Parameters["Color"].SetValue(Color.Gray.ToVector4());
            DrawModel(UIModel, Matrix.CreateTranslation(65, 65, 0));
            base.Draw(gameTime);
        }

        void DrawModel(Model m, Matrix World)
        {
            foreach (ModelMesh mesh in m.Meshes)
            {
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    part.Effect = GUIEffect;
                    part.Effect.Parameters["WorldTransform"].SetValue(World);

                }
                mesh.Draw();
            }
        }
    }
}
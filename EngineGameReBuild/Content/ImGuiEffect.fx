﻿#if OPENGL
	#define SV_POSITION POSITION
	#define VS_SHADERMODEL vs_3_0
	#define PS_SHADERMODEL ps_3_0
#else
	#define VS_SHADERMODEL vs_4_0_level_9_1
	#define PS_SHADERMODEL ps_4_0_level_9_1
#endif

matrix WorldViewProjection;

//lets talk about data compression here
//so if I have an array for data accessed through weights as indicies,
//I need to assign those indicies to each pass.
//thats the thing isn't it.
//if I'm not gonna bother generating unique verticies in memory whats the point.
//what I want is, instanced drawing for ui elements.


matrix WorldTransform;
matrix Projection;
//alright so we need to store, 

float3 ClippingPosition; //starting position. // should be transformed.

float3 ClippingA; // a minus top corner.
float3 ClippingB; //b minus top corner.

float2 Size;
float Margin;
float4 Color; 
texture Image;
//what is text data I need.
//

sampler GUISampler = sampler_state
{
    Texture = (Image);
    AddressU = clamp;
    AddressV = clamp;
    MagFilter = Linear;
    MinFilter = Linear;
    MipFilter = Linear;
};


struct VertexShaderInput
{
	float4 Position : POSITION0;
	float4 Color : COLOR0;
    float4 DataColor : COLOR1;//color containing only rg colors for corners
    //I don't need normal.
    //I should have uv in case.
    //float2 TexCoord : TEXCOORD0;
    //this should do it for now.
    
};

struct VertexShaderOutput
{
    float4 ReqPostion : SV_Position;
	float4 Position : COLOR0;
	float4 Color : COLOR1;
    
    float2 TexCoord : TEXCOORD0;
};

VertexShaderOutput MainVS(in VertexShaderInput input)
{
	VertexShaderOutput output = (VertexShaderOutput)0;
    //hmm. well okay this is pre alignment so its likely in world space as a flat object so whatever
            
    float2 PositionData = input.DataColor.xy;
    float4 correctSign = float4(input.Position.x, input.Position.y * -1, input.Position.z, input.Position.w);
    //float4 CorrectedPosition = input.Position + (float4(-2 * input.DataColor.x, -2 * input.DataColor.y, 0,0));
    //float4 scaledPosition = correctSign * Margin;//we can't do this first we gotta. //okay

    //float4 sizePosition = correctSign + float4(-100, 0, 0, 0);
    float4 correctedPosition = float4(correctSign.x - (2 * input.DataColor.x), correctSign.y - (2 * input.DataColor.y), correctSign.z, correctSign.w); //should be 0 or 1
    //float4 ajustedPosition = scaledPosition + (float4(Size.x , 0, 0, 0) * input.DataColor.x + float4(0, Size.y, 0, 0) * input.DataColor.y);
    float4 correctedScale = float4(correctedPosition.x * Margin, correctedPosition.y * Margin, correctedPosition.z, correctedPosition.w);
    
    float4 ajustedPosition = correctedScale + (float4(Size.x * input.DataColor.x, Size.y * input.DataColor.y, 0, 0));
    output.Position = mul(mul(ajustedPosition, WorldTransform), Projection);
    output.ReqPostion = output.Position;
	output.Color = input.Color;
    
    
	return output;
}

float4 ColorPS(VertexShaderOutput input) : COLOR
{
    //so dot values which is.
    float3 PositionClip = input.Position.xyz - ClippingPosition;
    float3 bPositionClip = input.Position.xyz - (ClippingB + ClippingPosition);
    float ADot = dot(ClippingA, ClippingA);
    float BDot = dot(ClippingB, ClippingB);
    float PositionDot = dot(ClippingA, PositionClip);
    float bPositionDot = dot(ClippingB, bPositionClip);
    //greater than zero, less than a dot and b dot.
    clip(PositionDot);
    clip(bPositionDot);
    clip(ADot - PositionDot);
    clip(BDot - PositionDot);
    //alright now that THATS out of the way lets do colors! yayyy
    //hmm. I don't think I want to use vertex color at all since its pretty static.
    //so.
    //hmm okay this is a problem.
    //how do  I set weight data when its order dependant.
    //heck.
    
    
    //alright so FUCK.
    //okay lets go ahead and func in a texture. wait shit so a texture pass and a non texture pass.
    //hmm.
    
    
    
    return Color;
}

technique BasicColorDrawing
{
	pass P0
	{
		VertexShader = compile VS_SHADERMODEL MainVS();
		PixelShader = compile PS_SHADERMODEL ColorPS();
	}
};
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseEngine;
using BaseEngine.Components;
using MemoryLeakEditor.ComponentTags;
using SConvert = MemoryLeakEditor.SystemConverter;

//namespace MemoryLeakEngine
//{
//    using static MemoryLeakEditor.MemoryLeakEngine;
//}
public static class MemoryLeakEngine
{
    public static CoreGame CoreGame { get; set; }
    public static Microsoft.Xna.Framework.Graphics.GraphicsDevice GraphicsDevice { get; set; }
    public static Microsoft.Xna.Framework.GraphicsDeviceManager graphics { get; set; }



    public static void SetTagTransform(TransformComponent t)
    {
        var tag = ((TransformTag)t.Tag);
        t.Position = SConvert.Convert(tag.position);
        t.Rotation = SConvert.Convert(tag.qrotation);
        t.Scale = SConvert.Convert(tag.scale);

    }
    public static void EngineSetParent(GameObject Parent, params GameObject[] children)
    {
        foreach (var go in children)
        {
            //ClearGameObject(go);
            Parent.AddChild(go);
            var payloadTransform = go.GetComponent<TransformComponent>();
            var payloadTag = (TransformTag)payloadTransform.Tag;
            payloadTag.SetWithParent(payloadTransform);


            //ClearGameObject(editorScene.gameObjects[payload_n]);
            //go.AddChild(editorScene.gameObjects[payload_n]);
            //var payloadTransform = editorScene.gameObjects[payload_n].GetComponent<TransformComponent>();
            //var payloadTag = (TransformTag)payloadTransform.Tag;
            //payloadTag.SetWithParent(payloadTransform);
        }
    }
}
namespace MemoryLeakEditor
{
    
    //public static class MemoryLeakEngine
    //{
    //    public static CoreGame CoreGame { get; set; }
    //    public static Microsoft.Xna.Framework.Graphics.GraphicsDevice GraphicsDevice { get; set; }
    //    public static Microsoft.Xna.Framework.GraphicsDeviceManager graphics { get; set; }



    //    public static void SetTagTransform(TransformComponent t)
    //    {
    //        var tag = ((TransformTag)t.Tag);
    //        t.Position = SConvert.Convert(tag.position);
    //        t.Rotation = SConvert.Convert(tag.qrotation);
    //        t.Scale = SConvert.Convert(tag.scale);

    //    }
    //    public static void EngineSetParent(GameObject Parent, params GameObject[] children)
    //    {
    //        foreach (var go in children)
    //        {
    //            //ClearGameObject(go);
    //            Parent.AddChild(go);
    //            var payloadTransform = go.GetComponent<TransformComponent>();
    //            var payloadTag = (TransformTag)payloadTransform.Tag;
    //            payloadTag.SetWithParent(payloadTransform);


    //            //ClearGameObject(editorScene.gameObjects[payload_n]);
    //            //go.AddChild(editorScene.gameObjects[payload_n]);
    //            //var payloadTransform = editorScene.gameObjects[payload_n].GetComponent<TransformComponent>();
    //            //var payloadTag = (TransformTag)payloadTransform.Tag;
    //            //payloadTag.SetWithParent(payloadTransform);
    //        }
    //    }
    //}
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseEngine;
using BaseEngine.Components;
using BaseEngine.Logic;
//using BaseEngine.Rendering;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using BEPUphysics;
using BEPUphysics.BroadPhaseEntries;
using BEPUphysics.CollisionRuleManagement;
using BRay = BEPUutilities.Ray;
using BEPUphysics.BroadPhaseEntries.MobileCollidables;
using ConversionHelper;
using S = MemoryLeakEditor.SystemConverter;
using Num = System.Numerics;
using BaseEngine.Input;
using MemoryLeakEditor.ComponentTags;
using static MemoryLeakEngine;
using static BaseEngine.Input.CommandBuilder;
using System.Collections;
using BaseEngine.Components;


//so I have the basic state data
//then the different commands that modify that state data in different ways

namespace MemoryLeakEditor.Scripts
{
    //I got this idea of like.
    //what if you start with the list of Key bindings.
    //what if thats your top level thing?
    //since they already have the dispatcher reference I don't need to go through that
    //so you can reference dispatcher for what it spits out event wise
    //but managing bindings is its own thing
    //the thing is I want to get lists of keys based on modifiers 
    //so you could for example list what buttons do what based on modifiers
    //just. just build that functionality if you want it god.
    

    [Flags]
    public enum EngineModifiers
    {
        ModifierA =1,
        ModifierB = 1<<1,
        ModifierC = 1<<2,
        ModifierD = 1<<3,
        ModifierE = 1<<4
    }
    public struct ModifierBinding : IKeyBinding
    {
        public string Device { get; set; }
        public object Key { get; set; }
        public double Scale { get; set; }
        public ICommandDispatcher Dispatcher { get; set; }
        public EngineModifiers Modifiers;

        public ModifierBinding( object key, string device, EngineModifiers modifier,ICommandDispatcher dispatcher)
        {
            Device = device;
            Key = key;
            Modifiers = modifier;
            Scale = 0;
            Dispatcher = dispatcher;

        }
        public ModifierBinding( object key,string device, double scale, EngineModifiers modifiers,ICommandDispatcher dispatcher)
        {
            Device = device;
            Key = key;
            Scale = scale;
            Modifiers = modifiers;
            Dispatcher = dispatcher;
        }
    }
    struct modifierPair
    {
        public EngineModifiers modifier;
        public CommandDispatcher command;
        public modifierPair(EngineModifiers m, CommandDispatcher c)
        {
            modifier = m;
            command = c;
        }
    }
    
    class CommandModifierCollection
    {
        //this doesn't work
        //commands are removable aren't they?
        //so.

        //so this is where things are hard?
        List<CommandDispatcher> commands = new List<CommandDispatcher>();//this is the core list
        //then we have modifier mappings.
        Dictionary<string, int> nameIndex = new Dictionary<string, int>();
        Dictionary<EngineModifiers, List<int>> modifierIndex = new Dictionary<EngineModifiers, List<int>>();
        //public List<CommandDispatcher> getCommandsForModifiers()
        //{
            
        //}
    }
    class ModifierMap : IMapping
    {
        public bool Active { get; set; }

        EngineModifiers keys;

        #region Modifier Functions
        void SetModA()
        {
            keys |= EngineModifiers.ModifierA;
        }
        void UnsetModA()
        {
            keys &= ~EngineModifiers.ModifierA;
        }
        void SetModB()
        {
            keys |= EngineModifiers.ModifierB;
        }
        void UnsetModB()
        {
            keys &= ~EngineModifiers.ModifierB;
        }
        void SetModC()
        {
            keys |= EngineModifiers.ModifierC;
        }
        void UnsetModC()
        {
            keys &= ~EngineModifiers.ModifierC;
        }
        void SetModD()
        {
            keys |= EngineModifiers.ModifierD;
        }
        void UnsetModD()
        {
            keys &= ~EngineModifiers.ModifierD;
        }
        void SetModE()
        {
            keys |= EngineModifiers.ModifierE;
        }
        void UnsetModE()
        {
            keys &= ~EngineModifiers.ModifierE;
        }
        #endregion
        //Dictionary<EngineModifiers, CommandDispatcher> modifiers = new Dictionary<EngineModifiers, CommandDispatcher>();
        //Dictionary<EngineModifiers, Dictionary<string, ICommand>> bindings = new Dictionary<EngineModifiers, Dictionary<string, ICommand>>();//hmm
        //Dictionary<string, ICommand> bindings = new Dictionary<string, ICommand>();

        //this is wrong
        //this forces one way to trigger a modifier
        //and I don't want that
        //Dictionary<EngineModifiers, ICommand> modifiers = new Dictionary<EngineModifiers, ICommand>();
        //using name we can bind several commands to a modifier
        // ORing several of the same bit shoudln't break things?
        Dictionary<string, ToggleCommandDispatcher> ModifierDispatchers = new Dictionary<string, ToggleCommandDispatcher>();
        List<IKeyBinding> Modifiers = new List<IKeyBinding>();

        Dictionary<string, ICommandDispatcher> commands = new Dictionary<string, ICommandDispatcher>();
        Dictionary<string, EngineModifiers> keyModifiers = new Dictionary<string, EngineModifiers>();
        List<IKeyBinding> Bindings = new List<IKeyBinding>();

        public ModifierMap()
        {
            keys = new EngineModifiers();
            Active = true;
            //bindings.Add((EngineModifiers)0, new Dictionary<string, ICommand>());

            ////
            ModifierDispatchers.Add(EngineModifiers.ModifierA.ToString(), new ToggleCommandDispatcher(EngineModifiers.ModifierA.ToString()));
            ModifierDispatchers[EngineModifiers.ModifierA.ToString()].Up += SetModA;
            ModifierDispatchers[EngineModifiers.ModifierA.ToString()].Down += UnsetModA;
            ////
            ModifierDispatchers.Add(EngineModifiers.ModifierB.ToString(), new ToggleCommandDispatcher(EngineModifiers.ModifierB.ToString()));
            ModifierDispatchers[EngineModifiers.ModifierB.ToString()].Up += SetModA;
            ModifierDispatchers[EngineModifiers.ModifierB.ToString()].Down += UnsetModA;
            ////
            ModifierDispatchers.Add(EngineModifiers.ModifierC.ToString(), new ToggleCommandDispatcher(EngineModifiers.ModifierC.ToString()));
            ModifierDispatchers[EngineModifiers.ModifierC.ToString()].Up += SetModA;
            ModifierDispatchers[EngineModifiers.ModifierC.ToString()].Down += UnsetModA;
            ////
            ModifierDispatchers.Add(EngineModifiers.ModifierD.ToString(), new ToggleCommandDispatcher(EngineModifiers.ModifierD.ToString()));
            ModifierDispatchers[EngineModifiers.ModifierD.ToString()].Up += SetModA;
            ModifierDispatchers[EngineModifiers.ModifierD.ToString()].Down += UnsetModA;
            ////
            ModifierDispatchers.Add(EngineModifiers.ModifierE.ToString(), new ToggleCommandDispatcher(EngineModifiers.ModifierE.ToString()));
            ModifierDispatchers[EngineModifiers.ModifierE.ToString()].Up += SetModA;
            ModifierDispatchers[EngineModifiers.ModifierE.ToString()].Down += UnsetModA;


        }
        public void SetModifier(EngineModifiers modifier, params object[] keys)
        {

            if (!ModifierDispatchers.ContainsKey(/*command.Name*/modifier.ToString()))
            {
                //modifiers.Add(command.Name, command);
                //return;

                return;
            }
            foreach (var key in keys)
            {
                string device = ValidateKey(key);
                if (key != "-1")
                {
                    Modifiers.Add(new KeyBinding(key, device, ModifierDispatchers[modifier.ToString()]));
                    //ModifierDispatchers[modifier.ToString()].AddBinding(new KeyBinding(key, device));
                }
            }



        }
        public ToggleCommandDispatcher GetToggle(string Name)
        {
            //foreach (var dictionary in commands.Values)
            {
                //if (dictionary.ContainsKey(Name))
                if (commands.ContainsKey(Name))
                {
                    if (commands[Name] is ToggleCommandDispatcher)
                        return (ToggleCommandDispatcher)commands[Name];
                }
            }
            throw new Exception("No Toggle Binding with name " + Name);
            return null;

        }
        public AxisCommandDispatcher GetAxis(string Name)
        {
            //if (bindings.ContainsKey(Name))
            //{
            //    if (bindings[Name] is AxisBinding)
            //        return (AxisBinding)bindings[Name];
            //}
            //return null;
            //foreach (var dictionary in bindings.Values)
            {
                //if (dictionary.ContainsKey(Name))
                if (commands.ContainsKey(Name))
                {
                    //if (dictionary[Name] is AxisBinding)
                    //return (AxisBinding)dictionary[Name];
                    if (commands[Name] is AxisCommandDispatcher)
                        return (AxisCommandDispatcher)commands[Name];

                }
            }

            throw new Exception("No Axis Binding with name " + Name);
            return null;
        }
        public void Update(ref Dictionary<string, InputDevice> devices)
        {
            //foreach(var value in ModifierDispatchers.Values)
            //{
            //    foreach (var binding in value.KeyBindings)
            //    {
            //        devices[binding.Device].UpdateBinding(binding);
            //    }
            //    { }
            //    //FINALLY
            //    //we've set the modifiers, lets do checking

            //}
            foreach (var key in Modifiers)
            {
                devices[key.Device].UpdateBinding(key);
            }
            //simple implmentation is
            //if(bindings.ContainsKey(keys))
            //{
            //    foreach(var binding in bindings[keys].Values)
            //    {
            //        devices[binding.Device].UpdateBinding(binding);
            //    }
            //}
            //foreach(var binding in commands.Values)
            //{
            //    if(keyModifiers[binding.Name] == keys)
            //    {
            //        foreach (ModifierBinding bind in binding.KeyBindings)
            //        {
            //            if(bind.Modifiers == keys)
            //                devices[bind.Device].UpdateBinding(bind);
            //        }
            //    }
            //}
            foreach (ModifierBinding binding in Bindings)
            {
                if (binding.Modifiers == keys)
                    devices[binding.Device].UpdateBinding(binding);
            }
            //there are more complicated alternatives
            //activating and excluding based on presence of variation
            //but this will work for now.


        }

        //public void AddCommand(CommandDispatcher binding)
        //{
        //    bindings[(EngineModifiers)0].Add(binding.Name, binding);
        //}    
        public void AddCommand(EngineModifiers modifiers, string name, params object[] keys)
        {
            if (!commands.ContainsKey(name))
            {
                commands.Add(name, new ToggleCommandDispatcher(name));
                //euuughhhh

                //I want to seperate event info from key info.
            }
            //hmm
            //oh I don't have to look for existing if .yeah just okay
            //so check if it doesn't exist generate it.
            //now thats done make sure its toggle for already existing
            //then we add bindings
            if (!(commands[name] is ToggleCommandDispatcher))
                return;



            foreach (var key in keys)
            {
                string device = ValidateKey(key);
                if (device != "-1")
                {
                    Bindings.Add(new ModifierBinding(key, device, modifiers, commands[name]));
                    //commands[name].AddBinding(new ModifierBinding(key, device,modifiers));
                }
            }
        }

        public void AddCommand(EngineModifiers modifiers, string name, double scale, params object[] keys)
        {
            if (!commands.ContainsKey(name))
            {
                commands.Add(name, new AxisCommandDispatcher(name));
            }
            if (!(commands[name] is AxisCommandDispatcher))
                return;
            foreach (var key in keys)
            {
                string device = ValidateKey(key);
                if (device != "-1")
                {
                    //this feels more like me
                    Bindings.Add(new ModifierBinding(key, device, scale, modifiers, commands[name]));
                    //commands[name].AddBinding(new ModifierBinding( key, device, scale,modifiers));
                }
            }
        }
        public void RemoveBinding(int i)
        {
            Bindings.Remove(Bindings[i]);
        }
        public void RemoveDispatcher(string name)
        {
            var BindingList = Bindings.Where(x => x.Dispatcher.Name == name);
            commands.Remove(name);
            foreach(var b in BindingList)
            {
                Bindings.Remove(b);
            }

        }
        //store bindings with hash code for finding it later
        

        //public void AddComand(EngineModifiers modifiers,ICommand command)
        //{
        //    if (!bindings.ContainsKey(modifiers))
        //        bindings.Add(modifiers, new Dictionary<string, ICommand>());
        //    bindings[modifiers].Add(command.Name, command);
        //}
    } 

    class ToolModeEditorScript : Behavior
    {
        //CoreGame game;//this will not work.
        //whajt is this
        public CollisionGroup SelectionGroup { get; set; }
        
        BEPUphysics.Space SceneSelectionSpace;//so need to pull thsi out into the general case but its probably fine here for now.
        BaseEngine.Rendering.EditorCamera sceneCamera;
        GraphicsDevice GraphicsDevice;
        
        [Flags] 
        public enum AxisState
        {
            None = 1 << 1,
            X = 1 << 2,
            Y = 1 << 3,
            Z = 1 << 4
        }
        public enum EditorOperations
        {
            Idle,
            Move,
            Rotate,
            Scale,
            Snapping
        }

        //these are all our fancy position data stuff

        Vector2 MousePosition;
        Vector2 MousePositionOld;

        Vector3 UnprojectedOld;
        Vector3 UnprojectedPosition;
        Vector3 UnprojectedDirection;

       // InputState SceneInputState;//replace this
        InputHandler Handler = new InputHandler();//shouldn't this be somewhere else.

        ModifierMap inputModifierMap;


        EditorOperations ActiveOperation;
        AxisState axisLock;
        Plane operatingPlane;
        Plane XYPlane = new Plane(Vector3.Zero, Vector3.UnitX, Vector3.UnitY);
        Plane YZPlane = new Plane(Vector3.Zero, Vector3.UnitY, Vector3.UnitZ);
        Plane ZXPlane = new Plane(Vector3.Zero, Vector3.UnitZ, Vector3.UnitX);
        Plane CameraPlane;

        //selection
        //GameObject activeObject = null;
        //multi selection
        GameObject leadObject = null;//I don't, blender uses this but I don't really need it.
        List<GameObject> selectedObjects = new List<GameObject>();
        List<TransformTag> originalTags = new List<TransformTag>();
        GameObject multiSelectParent;//didn't I obsolete this?
        //so we need.
        //so we need to add shift click selection mode
        //  which keeps track of the object selection list
        //then we need to backapply the parent object transforms when removing multi selection
        //that part, I don't know how to do that part with the current setup.
        //I guess just taking the transform tags, since
        //yeah just add the transform tags.


        //or we could just update axis locking 

        public override void Initialize()//this would go in whatever I call start.
        {
            Handler.AddDevice(new KeyboardInputDevice());
            Handler.AddDevice(new MouseInputDevice());//okay
            Handler.AddDevice(new JoyconInputDevice(JoyconIndex.JoyOne));
            Handler.AddDevice(new JoyconInputDevice(JoyconIndex.JoyTwo));

            //hold on so a thing I want is to be able to bind\
            //a single command to several dispatchers at once
            //and thats wwhat i had with the params object[] list
            //and I just lost that in the weeds
            //I also didn't do names, go fix names
            inputModifierMap = /*new ModifierMap();*/ (ModifierMap) gameObject.GetComponent<InputMapComponent>().Map;

            Handler.AddCommandMap(inputModifierMap);

            inputModifierMap.SetModifier(EngineModifiers.ModifierA, Keys.LeftShift,Keys.RightShift);
            inputModifierMap.SetModifier(EngineModifiers.ModifierB, Keys.LeftControl, Keys.RightControl);
            inputModifierMap.SetModifier(EngineModifiers.ModifierC, Keys.RightAlt, Keys.LeftAlt);


            ActiveOperation = EditorOperations.Idle;

            CameraPlane = new Plane(Vector3.Zero, sceneCamera.GetCameraX(), sceneCamera.GetCameraZ());


            //inputs
            string MouseInputBinding = "MouseUnprojectedMove";
            string MouseMiddleDragBinding = "MouseMiddleDrag";
            string MouseMiddleClickBinding = "MouseMiddleClick";
            string MouseScrollBinding = "MouseScroll";
            string MouseLeftClickBinding = "MouseLeftClick";
            string MouseRightClickBinding = "MouseRightClick";
            string ShiftSelect = "MouseShiftLeftClick";
            string PanCam = "PanCamera";
            string Focus = "Focus";

            string GPress = "GPressEvent";
            string AltGPress = "AltGPressEvent";

            string RPress = "RPressEvent";
            string AltRPress = "AltRPressEvent";

            string SPress = "SPressEvent";
            string AltSPress = "AltSPressEvent";


            string AltP = "AltP";//this is alll wrong but alright lets go!

            string XKey = "XKey";
            string YKey = "YKey";
            string ZKey = "ZKey";
            string XShiftKey = "XShift";
            string YShiftKey = "YShift";
            string ZShiftKey = "ZShift";


            //remember to add keyboardstate check in mouse when adding modifier checks
            //SceneInputState.AddBinding<MouseEventDispatcher>(new MouseBinding(new AxisBinding(MouseInputBinding)));
            
            //SceneInputState.AddBinding<MouseEventDispatcher>(new MouseBinding(new ButtonBinding(MouseMiddleDragBinding), new MouseButtonCombo(MouseButton.Middle, Modifiers.Null)));
            
            //SceneInputState.AddBinding<MouseEventDispatcher>(new MouseBinding(new ButtonBinding(MouseMiddleClickBinding), new MouseButtonCombo(MouseButton.Middle, Modifiers.Null)));
            inputModifierMap.AddCommand(0, "RotateCamera", MouseButton.Middle);
            //SceneInputState.AddBinding<MouseEventDispatcher>(new MouseBinding(new ButtonBinding(MouseLeftClickBinding), new MouseButtonCombo(MouseButton.Left, Modifiers.Null)));
            inputModifierMap.AddCommand(0, "Select", MouseButton.Left);
            //SceneInputState.AddBinding<MouseEventDispatcher>(new MouseBinding(new ButtonBinding(MouseRightClickBinding), new MouseButtonCombo(MouseButton.Right, Modifiers.Null)));
            inputModifierMap.AddCommand(0, "Cancle Operation", MouseButton.Right);
            //SceneInputState.AddBinding<MouseEventDispatcher>(new MouseBinding(new FloatBinding(MouseScrollBinding)));
            inputModifierMap.AddCommand(0, "Scroll", 1, MouseAux.MouseWheel);

            //SceneInputState.AddBinding<MouseEventDispatcher>(new MouseBinding(new ButtonBinding(ShiftSelect), new MouseButtonCombo(MouseButton.Left, Modifiers.Shift)));
            inputModifierMap.AddCommand(EngineModifiers.ModifierA, "Multi Select", MouseButton.Left);
            //SceneInputState.AddBinding<MouseEventDispatcher>(new MouseBinding(new ButtonBinding(PanCam), new MouseButtonCombo(MouseButton.Middle, Modifiers.Shift)));
            inputModifierMap.AddCommand(EngineModifiers.ModifierA, "PanCamera", MouseButton.Middle);

            //SceneInputState.AddBinding<KeyboardEventDispatcher>(new KeyboardBind(new ButtonBinding(Focus), new KeyboardKeyCombo(Keys.F, Modifiers.Null)));
            inputModifierMap.AddCommand(0, "Focus", Keys.F);
            //SceneInputState.AddBinding<KeyboardEventDispatcher>(new KeyboardBind(new ButtonBinding(GPress), new KeyboardKeyCombo(Keys.G, Modifiers.Null)));
            inputModifierMap.AddCommand(0, "Grab", Keys.G);
            //SceneInputState.AddBinding<KeyboardEventDispatcher>(new KeyboardBind(new ButtonBinding(AltGPress), new KeyboardKeyCombo(Keys.G, Modifiers.Alt)));
            inputModifierMap.AddCommand(EngineModifiers.ModifierC,"ClearGrab", Keys.G);




            //SceneInputState.AddBinding<KeyboardEventDispatcher>(new KeyboardBind(new ButtonBinding(RPress), new KeyboardKeyCombo(Keys.R, Modifiers.Null)));
            inputModifierMap.AddCommand(0, "Rotate", Keys.R);

            //SceneInputState.AddBinding<KeyboardEventDispatcher>(new KeyboardBind(new ButtonBinding(AltRPress), new KeyboardKeyCombo(Keys.R, Modifiers.Alt)));
            inputModifierMap.AddCommand(EngineModifiers.ModifierC, "ClearRotate", Keys.R);

            //SceneInputState.AddBinding<KeyboardEventDispatcher>(new KeyboardBind(new ButtonBinding(SPress), new KeyboardKeyCombo(Keys.S, Modifiers.Null)));
            inputModifierMap.AddCommand(0, "Scale", Keys.S);
            //SceneInputState.AddBinding<KeyboardEventDispatcher>(new KeyboardBind(new ButtonBinding(AltSPress), new KeyboardKeyCombo(Keys.S, Modifiers.Alt)));
            inputModifierMap.AddCommand(EngineModifiers.ModifierC, "ClearScale", Keys.S);
            //SceneInputState.AddBinding<KeyboardEventDispatcher>(new KeyboardBind(new ButtonBinding(AltP), new KeyboardKeyCombo(Keys.P, Modifiers.Alt)));
            inputModifierMap.AddCommand(EngineModifiers.ModifierC, "SetParent",
                Keys.P);
            //SceneInputState.AddBinding<KeyboardEventDispatcher>(new KeyboardBind(new ButtonBinding(XKey), new KeyboardKeyCombo(Keys.X, Modifiers.Null)));
            inputModifierMap.AddCommand(0, "AxisLockX", Keys.X);

            //SceneInputState.AddBinding<KeyboardEventDispatcher>(new KeyboardBind(new ButtonBinding(YKey), new KeyboardKeyCombo(Keys.Y, Modifiers.Null)));
            inputModifierMap.AddCommand(0, "AxisLockY", Keys.Y);
            //SceneInputState.AddBinding<KeyboardEventDispatcher>(new KeyboardBind(new ButtonBinding(ZKey), new KeyboardKeyCombo(Keys.Z, Modifiers.Null)));
            inputModifierMap.AddCommand(0, "AxisLockZ", Keys.Z);
            //SceneInputState.AddBinding<KeyboardEventDispatcher>(new KeyboardBind(new ButtonBinding(XShiftKey), new KeyboardKeyCombo(Keys.X, Modifiers.Shift)));
            inputModifierMap.AddCommand(EngineModifiers.ModifierA, "AxisExludeX", Keys.X);
            //SceneInputState.AddBinding<KeyboardEventDispatcher>(new KeyboardBind(new ButtonBinding(YShiftKey), new KeyboardKeyCombo(Keys.Y, Modifiers.Shift)));
            inputModifierMap.AddCommand(EngineModifiers.ModifierA, "AxisExludeY", Keys.Y);
            //SceneInputState.AddBinding<KeyboardEventDispatcher>(new KeyboardBind(new ButtonBinding(ZShiftKey), new KeyboardKeyCombo(Keys.Z, Modifiers.Shift)));
            inputModifierMap.AddCommand(EngineModifiers.ModifierA, "AxisExludeZ", Keys.Z);
            ////SceneInputState[MouseInputBinding]
            ////camera controls
            //SceneInputState.GetAxis(MouseInputBinding).Axis += MouseUpdateEvent;
            inputModifierMap.AddCommand(0, "MousePositionX", 1, MouseAux.MouseX);
            inputModifierMap.AddCommand(0, "MousePositionY", 1, MouseAux.MouseY);
            //SceneInputState.GetButton(MouseMiddleDragBinding).Down += MouseSwivelEvent;

            //refigure this

            //SceneInputState.GetButton(MouseMiddleClickBinding).Press += MouseSwivelInitial;

            //inputModifierMap.GetToggle("RotateCamera").Down += 

            //SceneInputState.GetFloat(MouseScrollBinding).FloatVal += Zoom;
            inputModifierMap.GetAxis("Scroll").Axis += Zoom;
            //SceneInputState.GetButton(MouseLeftClickBinding).Press += Select;
            inputModifierMap.GetToggle("Select").Down += Select;
            //SceneInputState.GetButton(ShiftSelect).Press += MultiSelect;
            inputModifierMap.GetToggle("Multi Select").Down += MultiSelect;
            //SceneInputState.GetButton(PanCam).Down += PanCamera;
            //SceneInputState.GetButton(PanCam).Press += PanInitialize;
            //SceneInputState.GetButton(Focus).Press += FocusCamera;
            inputModifierMap.GetToggle("Focus").Down += FocusCamera;
            //SceneInputState.GetAxis(MouseInputBinding).Axis += PanPositionValues;
            ////movement
            //SceneInputState.GetButton(GPress).Press += SetMoveState;
            inputModifierMap.GetToggle("Grab").Down += SetMoveState;
            //SceneInputState.GetAxis(MouseInputBinding).Axis += MoveOperation;
            //SceneInputState.GetButton(AltGPress).Press += ResetMove;
            inputModifierMap.GetToggle("ClearGrab").Down += ResetMove;
            ////rotation
            //SceneInputState.GetButton(RPress).Press += SetRotationState;
            inputModifierMap.GetToggle("Rotate").Down += SetRotationState;
            //SceneInputState.GetAxis(MouseInputBinding).Axis += RotationOperation;
            //SceneInputState.GetButton(AltRPress).Press += ResetRotation;
            inputModifierMap.GetToggle("ClearRotate").Down += ResetRotation;
            //SceneInputState.GetButton(MouseLeftClickBinding).Press += ClearRotDebugLine;
            //SceneInputState.GetButton(MouseRightClickBinding).Press += ClearRotDebugLine;
            ////Scale
            //SceneInputState.GetButton(SPress).Press += SetScaleState;
            inputModifierMap.GetToggle("Scale").Down += SetScaleState;
            //SceneInputState.GetAxis(MouseInputBinding).Axis += ScaleOperation;
            //SceneInputState.GetButton(AltSPress).Press += ResetScale;
            inputModifierMap.GetToggle("ClearScale").Down += ResetScale;
            ////operations
            //SceneInputState.GetButton(AltP).Press += SetParent;
            inputModifierMap.GetToggle("SetParent").Down += SetParent;
            ////general transform
            //SceneInputState.GetButton(MouseLeftClickBinding).Press += ConfirmTransform;
            //SceneInputState.GetButton(MouseRightClickBinding).Press += CancleTransform;
            


            ////axisLock
            //SceneInputState.GetButton(XKey).Press += LockX;
            inputModifierMap.GetToggle("AxisLockX").Down += LockX;
            //SceneInputState.GetButton(YKey).Press += LockY;
            inputModifierMap.GetToggle("AxisLockY").Down += LockY;
            //SceneInputState.GetButton(ZKey).Press += LockZ;
            inputModifierMap.GetToggle("AxisLockZ").Down += LockZ;
            //SceneInputState.GetButton(XShiftKey).Press += LockYZ;
            inputModifierMap.GetToggle("AxisExcludeX").Down += LockYZ;
            //SceneInputState.GetButton(YShiftKey).Press += LockZX;
            inputModifierMap.GetToggle("AxisExcludeY").Down += LockZX;
            //SceneInputState.GetButton(ZShiftKey).Press += LockXY;
            inputModifierMap.GetToggle("AxisExcludeZ").Down += LockXY;


        }
        public override void Update(GameTime gameTime)
        {
            //SceneInputState.Update();
            Handler.Update(gameTime);//this should be somewhere more abstract, in the game loop. or its own component
            var ActivePoint = UnprojectedPosition + UnprojectedDirection * sceneCamera.distance;
            if (ActiveOperation == EditorOperations.Move)
            {
                switch (axisLock)
                {
                    case AxisState.None:
                        editTag.position = S.Convert(ActivePoint - StartMove);
                        break;
                    case AxisState.X:
                        editTag.position = S.Convert(
                            PlaneAlignedPosition(XYPlane, ActivePoint) - PlaneAlignedPosition(XYPlane, StartMove));
                        editTag.position.Y = 0;
                        break;
                    case AxisState.Y:
                        editTag.position = S.Convert(
                            PlaneAlignedPosition(YZPlane, ActivePoint) - PlaneAlignedPosition(YZPlane, StartMove));
                        editTag.position.Z = 0;
                        break;
                    case AxisState.Z:
                        editTag.position = S.Convert(
                            PlaneAlignedPosition(ZXPlane, ActivePoint) - PlaneAlignedPosition(ZXPlane, StartMove));
                        editTag.position.X = 0;
                        break;
                    case (AxisState.X | AxisState.Y):
                        editTag.position = S.Convert(
                            PlaneAlignedPosition(XYPlane, ActivePoint) - PlaneAlignedPosition(XYPlane, StartMove));
                        break;
                    case (AxisState.Y | AxisState.Z):
                        editTag.position = S.Convert(
                            PlaneAlignedPosition(YZPlane, ActivePoint) - PlaneAlignedPosition(YZPlane, StartMove));
                        break;
                    case (AxisState.Z | AxisState.X):
                        editTag.position = S.Convert(
                            PlaneAlignedPosition(ZXPlane, ActivePoint) - PlaneAlignedPosition(ZXPlane, StartMove));
                        break;
                }
                editTag.scale = System.Numerics.Vector3.Zero;
                //activeObject.GetComponent<TransformComponent>().Tag = originalTag + editTag;
                foreach (GameObject go in selectedObjects)
                {
                    go.GetComponent<TransformComponent>().Tag = originalTags[selectedObjects.IndexOf(go)] + editTag;
                    SetTagTransform(go.GetComponent<TransformComponent>());
                }
            }
            if (ActiveOperation == EditorOperations.Rotate)
            {
                if (RotSingleAxis)
                {
                    //debugLine.Clear();

                    //debugLine.AddLine(CenterUnprojected, UnprojectedPosition, Color.Blue);

                    //var sceneCameraPosition = sceneCamera.GetCameraPosition();
                    var sceneCameraTrans = sceneCamera.FocusPoint;
                    var zeroedUnprojected = UnprojectedPosition - (UnprojectedDirection * sceneCamera.distance);
                    //var rotActive = UnprojectedPosition - sceneCamera.GetCameraPosition();
                    var rotActive = Vector3.Normalize(UnprojectedPosition - CenterUnprojected);
                    //var DotAffirmation = Vector3.Dot(Vector3.Normalize(RotStart), Vector3.Normalize( rotActive));
                    //float activeAngle = (float)Math.Acos(Vector3.Dot(Vector3.Normalize(RotStart), Vector3.Normalize(rotActive)));

                    float dot = Vector3.Dot(RotStart, rotActive);
                    //float det = new Matrix(Vector4.Zero, Vector4.Zero,Vector4.Zero,Vector4.Zero).Determinant();//alright we need a matrix from, our values lets go
                    float det =
                        new Matrix(new Vector4(RotStart, 0),
                                   new Vector4(rotActive, 0),
                                   new Vector4(RotNormal, 0),
                                   new Vector4(0, 0, 0, 1)).Determinant();

                    //float activeAngle = (float)Math.Acos(Vector3.Dot(RotStart, rotActive));
                    float activeAngle = -(float)Math.Atan2(det, dot);
                    switch (axisLock)
                    {
                        case AxisState.X:
                            editTag.qrotation = S.Convert(Quaternion.CreateFromAxisAngle(Vector3.UnitX, activeAngle));
                            break;
                        case AxisState.Y:
                            editTag.qrotation = S.Convert(Quaternion.CreateFromAxisAngle(Vector3.UnitY, activeAngle));
                            break;
                        case AxisState.Z:
                            editTag.qrotation = S.Convert(Quaternion.CreateFromAxisAngle(Vector3.UnitZ, activeAngle));
                            break;
                        default:
                            editTag.qrotation = S.Convert(Quaternion.CreateFromAxisAngle(sceneCamera.GetCameraZ(), activeAngle));
                            break;
                    }
                }
                else
                {
                    var delta = MousePosition - MousePositionOld;
                    TwoRot += delta;
                    //I think this is it.
                    editTag.qrotation = S.Convert(Quaternion.Concatenate(Quaternion.CreateFromAxisAngle(sceneCamera.GetCameraY(), TwoRot.X * .01f), Quaternion.CreateFromAxisAngle(sceneCamera.GetCameraX(), TwoRot.Y * .01f)));
                }
                editTag.scale = Num.Vector3.Zero;
                foreach (GameObject go in selectedObjects)
                {
                    go.GetComponent<TransformComponent>().Tag = originalTags[selectedObjects.IndexOf(go)] + editTag;
                    SetTagTransform(go.GetComponent<TransformComponent>());
                }
                //activeObject.GetComponent<TransformComponent>().Tag = originalTag + editTag;

            }
            if (ActiveOperation == EditorOperations.Scale)
            {
                //float ScaleFactor = 8f;
                float startDistance = (ScaleStart - CenterUnprojected).Length();
                float activeDistance = ((UnprojectedPosition + (UnprojectedDirection * sceneCamera.distance)) - CenterUnprojected).Length();
                // float scaleValue = (UnprojectedPosition - ScaleStart).Length();
                float scaleValue = activeDistance - startDistance;
                //scaleValue *= ScaleFactor;
                switch (axisLock)
                {
                    case AxisState.X:
                        editTag.scale = new Num.Vector3(scaleValue, 0, 0);
                        break;
                    case AxisState.Y:
                        editTag.scale = new Num.Vector3(0, scaleValue, 0);
                        break;
                    case AxisState.Z:
                        editTag.scale = new Num.Vector3(0, 0, scaleValue);
                        break;
                    case (AxisState.X | AxisState.Y):
                        editTag.scale = new Num.Vector3(scaleValue, scaleValue, 0);
                        break;
                    case (AxisState.Y | AxisState.Z):
                        editTag.scale = new Num.Vector3(0, scaleValue, scaleValue);
                        break;
                    case (AxisState.Z | AxisState.X):
                        editTag.scale = new Num.Vector3(scaleValue, 0, scaleValue);
                        break;
                    case AxisState.None:
                        editTag.scale = new Num.Vector3(scaleValue, scaleValue, scaleValue);
                        break;
                }

                //activeObject.GetComponent<TransformComponent>().Tag = originalTag + editTag;
                foreach (GameObject go in selectedObjects)
                {
                    go.GetComponent<TransformComponent>().Tag = originalTags[selectedObjects.IndexOf(go)] + editTag;
                    SetTagTransform(go.GetComponent<TransformComponent>());
                }
            }
            base.Update(gameTime);
        }

        #region AxisLock

        void LockX()
        {
            if (ActiveOperation != EditorOperations.Idle)
                axisLock = AxisState.X;
        }
        void LockY()
        {
            if (ActiveOperation != EditorOperations.Idle)
                axisLock = AxisState.Y;
        }
        void LockZ()
        {
            if (ActiveOperation != EditorOperations.Idle)
                axisLock = AxisState.Z;
        }
        void LockXY()
        {
            if (ActiveOperation != EditorOperations.Idle)
                axisLock = AxisState.X | AxisState.Y;
        }
        void LockYZ()
        {
            if (ActiveOperation != EditorOperations.Idle)
                axisLock = AxisState.Y | AxisState.Z;
        }
        void LockZX()
        {
            if (ActiveOperation != EditorOperations.Idle)
                axisLock = AxisState.Z | AxisState.X;
        }

        #endregion
        //lets start with selection and move
        TransformTag originalTag;
        TransformTag editTag;

        


        #region Move

        Vector3 StartMove;

        public void SetMoveState()
        {
            if (ActiveOperation == EditorOperations.Idle)
            {
                //if(activeObject != null)
                if (selectedObjects.Count != 0)
                {
                    ActiveOperation = EditorOperations.Move;
                    //var activeTag = (TransformTag)activeObject.GetComponent<TransformComponent>().Tag;

                    //originalTag = new TransformTag();
                    //originalTag.qrotation = activeTag.qrotation;
                    //originalTag.position = activeTag.position;
                    //originalTag.scale = activeTag.scale;
                    originalTags = new List<TransformTag>();
                    foreach (GameObject go in selectedObjects)
                    {
                        originalTags.Add(((TransformTag)go.GetComponent<TransformComponent>().Tag).Clone());
                    }

                    editTag = new TransformTag();
                    StartMove = UnprojectedPosition + UnprojectedDirection * sceneCamera.distance;//yeah like this
                }
            }
        }
        //do I do, two plane algins or do I just get the
        //the unprojected position at the cameras distance
        //hmm
        //oh I can just get that during the move start
        //so now that thats aligned already we can just use the active plane
        public void MoveOperation(Vector2 v)
        {
            var ActivePoint = UnprojectedPosition + UnprojectedDirection * sceneCamera.distance;
            if (ActiveOperation == EditorOperations.Move)
            {
                switch (axisLock)
                {
                    case AxisState.None:
                        editTag.position = S.Convert(ActivePoint - StartMove);
                        break;
                    case AxisState.X:
                        editTag.position = S.Convert(
                            PlaneAlignedPosition(XYPlane, ActivePoint) - PlaneAlignedPosition(XYPlane, StartMove));
                        editTag.position.Y = 0;
                        break;
                    case AxisState.Y:
                        editTag.position = S.Convert(
                            PlaneAlignedPosition(YZPlane, ActivePoint) - PlaneAlignedPosition(YZPlane, StartMove));
                        editTag.position.Z = 0;
                        break;
                    case AxisState.Z:
                        editTag.position = S.Convert(
                            PlaneAlignedPosition(ZXPlane, ActivePoint) - PlaneAlignedPosition(ZXPlane, StartMove));
                        editTag.position.X = 0;
                        break;
                    case (AxisState.X | AxisState.Y):
                        editTag.position = S.Convert(
                            PlaneAlignedPosition(XYPlane, ActivePoint) - PlaneAlignedPosition(XYPlane, StartMove));
                        break;
                    case (AxisState.Y | AxisState.Z):
                        editTag.position = S.Convert(
                            PlaneAlignedPosition(YZPlane, ActivePoint) - PlaneAlignedPosition(YZPlane, StartMove));
                        break;
                    case (AxisState.Z | AxisState.X):
                        editTag.position = S.Convert(
                            PlaneAlignedPosition(ZXPlane, ActivePoint) - PlaneAlignedPosition(ZXPlane, StartMove));
                        break;
                }
                editTag.scale = System.Numerics.Vector3.Zero;
                //activeObject.GetComponent<TransformComponent>().Tag = originalTag + editTag;
                foreach (GameObject go in selectedObjects)
                {
                    go.GetComponent<TransformComponent>().Tag = originalTags[selectedObjects.IndexOf(go)] + editTag;
                    SetTagTransform(go.GetComponent<TransformComponent>());
                }
            }
        }
        public Vector3 PlaneAlignedPosition(Plane plane, Vector3 position)
        {
            Vector3 ret = Vector3.Zero;
            var cameraforward = sceneCamera.GetCameraY();
            Ray rayUp = new Ray(position, UnprojectedDirection);//new Ray(position, plane.Normal);
            Ray rayDown = new Ray(position, -UnprojectedDirection); //new Ray(position, -plane.Normal);

            float? intersectUp = rayUp.Intersects(plane);
            float? intersectDown = rayDown.Intersects(plane);

            //check for perfect align edge case
            if (intersectUp == null && intersectDown == null)
            {
                rayUp = new Ray(position, plane.Normal);
                rayDown = new Ray(position, -plane.Normal);

                intersectUp = rayUp.Intersects(plane);
                intersectDown = rayDown.Intersects(plane);
                return position + (plane.Normal * ((intersectUp != null) ? (float)intersectUp : -(float)intersectDown));
            }

            return position + (UnprojectedDirection * ((intersectUp != null) ? (float)intersectUp : -(float)intersectDown));//this should do it
        }
        public void ResetMove()
        {
            //if(activeObject!=null)
            //{
            //    ((TransformTag)(activeObject.GetComponent<TransformComponent>().Tag)).position = Num.Vector3.Zero;
            //}
            foreach (var go in selectedObjects)
            {
                ((TransformTag)(go.GetComponent<TransformComponent>().Tag)).position = Num.Vector3.Zero;
                SetTagTransform(go.GetComponent<TransformComponent>());
            }
        }
        #endregion
        #region Rotation

        Vector3 RotStart;
        Vector3 RotNormal;
        Vector3 CenterUnprojected;
        Vector2 TwoRot;
        bool RotSingleAxis; //operations are either single axis or dual axis, single axis is defualt and as such true
        public void SetRotationState()
        {
            if (ActiveOperation == EditorOperations.Rotate)
            {
                RotSingleAxis = !RotSingleAxis;//flip operation
            }
            if (ActiveOperation == EditorOperations.Idle)
            {
                //if(activeObject != null)
                if (selectedObjects.Count != 0)
                {
                    ActiveOperation = EditorOperations.Rotate;
                    //var activeTag = (TransformTag)activeObject.GetComponent<TransformComponent>().Tag;
                    //originalTag = new TransformTag();
                    //originalTag.qrotation = activeTag.qrotation;
                    //originalTag.position = activeTag.position;
                    //originalTag.scale = activeTag.scale;
                    originalTags = new List<TransformTag>();
                    foreach (GameObject go in selectedObjects)
                    {
                        originalTags.Add(((TransformTag)go.GetComponent<TransformComponent>().Tag).Clone());
                    }

                    editTag = new TransformTag();
                    //RotStart = UnprojectedPosition;


                    //RotStart = UnprojectedPosition - sceneCamera.GetCameraPosition();
                    CenterUnprojected = GraphicsDevice.Viewport.Unproject(new Vector3(graphics.PreferredBackBufferWidth / 2, graphics.PreferredBackBufferHeight / 2, 0), sceneCamera.Projection, sceneCamera.View, Matrix.Identity);

                    RotNormal = Vector3.Normalize(GraphicsDevice.Viewport.Unproject(new Vector3(graphics.PreferredBackBufferWidth / 2, graphics.PreferredBackBufferHeight, 1), sceneCamera.Projection, sceneCamera.View, Matrix.Identity));

                    RotStart = Vector3.Normalize(UnprojectedPosition - CenterUnprojected);

                    RotSingleAxis = true;
                    TwoRot = Vector2.Zero;

                }
            }

        }
        public void RotationOperation(Vector2 v)
        {
            //so the general idea is. 
            //we get the angle by taking the angle of the initial position using dot product and arccos
            //and then get the difference of the current angle with arccos
            //or we use. we use the initial direction to get the first dot product, that should work well    
            if (ActiveOperation == EditorOperations.Rotate)
            {
                if (RotSingleAxis)
                {
                    //debugLine.Clear();

                    //debugLine.AddLine(CenterUnprojected, UnprojectedPosition, Color.Blue);

                    //var sceneCameraPosition = sceneCamera.GetCameraPosition();
                    var sceneCameraTrans = sceneCamera.FocusPoint;
                    var zeroedUnprojected = UnprojectedPosition - (UnprojectedDirection * sceneCamera.distance);
                    //var rotActive = UnprojectedPosition - sceneCamera.GetCameraPosition();
                    var rotActive = Vector3.Normalize(UnprojectedPosition - CenterUnprojected);
                    //var DotAffirmation = Vector3.Dot(Vector3.Normalize(RotStart), Vector3.Normalize( rotActive));
                    //float activeAngle = (float)Math.Acos(Vector3.Dot(Vector3.Normalize(RotStart), Vector3.Normalize(rotActive)));

                    float dot = Vector3.Dot(RotStart, rotActive);
                    //float det = new Matrix(Vector4.Zero, Vector4.Zero,Vector4.Zero,Vector4.Zero).Determinant();//alright we need a matrix from, our values lets go
                    float det =
                        new Matrix(new Vector4(RotStart, 0),
                                   new Vector4(rotActive, 0),
                                   new Vector4(RotNormal, 0),
                                   new Vector4(0, 0, 0, 1)).Determinant();

                    //float activeAngle = (float)Math.Acos(Vector3.Dot(RotStart, rotActive));
                    float activeAngle = -(float)Math.Atan2(det, dot);
                    switch (axisLock)
                    {
                        case AxisState.X:
                            editTag.qrotation = S.Convert(Quaternion.CreateFromAxisAngle(Vector3.UnitX, activeAngle));
                            break;
                        case AxisState.Y:
                            editTag.qrotation = S.Convert(Quaternion.CreateFromAxisAngle(Vector3.UnitY, activeAngle));
                            break;
                        case AxisState.Z:
                            editTag.qrotation = S.Convert(Quaternion.CreateFromAxisAngle(Vector3.UnitZ, activeAngle));
                            break;
                        default:
                            editTag.qrotation = S.Convert(Quaternion.CreateFromAxisAngle(sceneCamera.GetCameraZ(), activeAngle));
                            break;
                    }
                }
                else
                {
                    var delta = MousePosition - MousePositionOld;
                    TwoRot += delta;
                    //I think this is it.
                    editTag.qrotation = S.Convert(Quaternion.Concatenate(Quaternion.CreateFromAxisAngle(sceneCamera.GetCameraY(), TwoRot.X * .01f), Quaternion.CreateFromAxisAngle(sceneCamera.GetCameraX(), TwoRot.Y * .01f)));
                }
                editTag.scale = Num.Vector3.Zero;
                foreach (GameObject go in selectedObjects)
                {
                    go.GetComponent<TransformComponent>().Tag = originalTags[selectedObjects.IndexOf(go)] + editTag;
                    SetTagTransform(go.GetComponent<TransformComponent>());
                }
                //activeObject.GetComponent<TransformComponent>().Tag = originalTag + editTag;

            }

        }
        public void ClearRotDebugLine()
        {
            if (ActiveOperation == EditorOperations.Rotate)
            {
                //debugLine.Clear();
            }
        }
        public void ResetRotation()
        {
            //if(activeObject!=null)
            //{
            //    ((TransformTag)activeObject.GetComponent<TransformComponent>().Tag).qrotation = S.Convert(Quaternion.Identity);
            //}
            foreach (var go in selectedObjects)
            {
                ((TransformTag)(go.GetComponent<TransformComponent>().Tag)).position = Num.Vector3.Zero;
                SetTagTransform(go.GetComponent<TransformComponent>());
            }
        }
        #endregion
        #region Scale
        Vector3 ScaleStart;//lets move this to 0 position, see if that helps


        public void SetScaleState()
        {
            if (ActiveOperation == EditorOperations.Idle)
            {
                //if(activeObject != null)
                if (selectedObjects.Count != 0)
                {
                    ActiveOperation = EditorOperations.Scale;
                    //var activeTag = (TransformTag)activeObject.GetComponent<TransformComponent>().Tag;

                    //originalTag = new TransformTag();
                    //originalTag.position = activeTag.position;
                    //originalTag.qrotation = activeTag.qrotation;
                    //originalTag.scale = activeTag.scale;
                    originalTags = new List<TransformTag>();
                    foreach (GameObject go in selectedObjects)
                    {
                        originalTags.Add(((TransformTag)go.GetComponent<TransformComponent>().Tag).Clone());
                    }

                    editTag = new TransformTag();
                    ScaleStart = UnprojectedPosition + UnprojectedDirection * sceneCamera.distance;

                    CenterUnprojected = GraphicsDevice.Viewport.Unproject(new Vector3(graphics.PreferredBackBufferWidth / 2, graphics.PreferredBackBufferHeight / 2, 0), sceneCamera.Projection, sceneCamera.View, Matrix.Identity);
                    var centerNormal = Vector3.Normalize(GraphicsDevice.Viewport.Unproject(new Vector3(graphics.PreferredBackBufferWidth / 2, graphics.PreferredBackBufferHeight / 2, 1), sceneCamera.Projection, sceneCamera.View, Matrix.Identity) - CenterUnprojected);
                    CenterUnprojected = CenterUnprojected + centerNormal * sceneCamera.distance;
                }
            }
        }
        public void ScaleOperation(Vector2 v)
        {
            if (ActiveOperation == EditorOperations.Scale)
            {
                //float ScaleFactor = 8f;
                float startDistance = (ScaleStart - CenterUnprojected).Length();
                float activeDistance = ((UnprojectedPosition + (UnprojectedDirection * sceneCamera.distance)) - CenterUnprojected).Length();
                // float scaleValue = (UnprojectedPosition - ScaleStart).Length();
                float scaleValue = activeDistance - startDistance;
                //scaleValue *= ScaleFactor;
                switch (axisLock)
                {
                    case AxisState.X:
                        editTag.scale = new Num.Vector3(scaleValue, 0, 0);
                        break;
                    case AxisState.Y:
                        editTag.scale = new Num.Vector3(0, scaleValue, 0);
                        break;
                    case AxisState.Z:
                        editTag.scale = new Num.Vector3(0, 0, scaleValue);
                        break;
                    case (AxisState.X | AxisState.Y):
                        editTag.scale = new Num.Vector3(scaleValue, scaleValue, 0);
                        break;
                    case (AxisState.Y | AxisState.Z):
                        editTag.scale = new Num.Vector3(0, scaleValue, scaleValue);
                        break;
                    case (AxisState.Z | AxisState.X):
                        editTag.scale = new Num.Vector3(scaleValue, 0, scaleValue);
                        break;
                    case AxisState.None:
                        editTag.scale = new Num.Vector3(scaleValue, scaleValue, scaleValue);
                        break;
                }

                //activeObject.GetComponent<TransformComponent>().Tag = originalTag + editTag;
                foreach (GameObject go in selectedObjects)
                {
                    go.GetComponent<TransformComponent>().Tag = originalTags[selectedObjects.IndexOf(go)] + editTag;
                    SetTagTransform(go.GetComponent<TransformComponent>());
                }
            }
        }
        public void ResetScale()
        {
            //if(activeObject != null)
            //{
            //    ((TransformTag)activeObject.GetComponent<TransformComponent>().Tag).scale = Num.Vector3.One;
            //}
            foreach (var go in selectedObjects)
            {
                ((TransformTag)go.GetComponent<TransformComponent>().Tag).scale = Num.Vector3.One;
            }
        }

        #endregion



        void SetParent()
        {
            if (ActiveOperation == EditorOperations.Idle)
            {
                if (selectedObjects.Count > 1)
                {
                    var activeObject = selectedObjects[0];
                    List<GameObject> children = selectedObjects.GetRange(1, selectedObjects.Count - 1);

                    EngineSetParent(activeObject, children.ToArray());
                }
            }
        }
        //okay so all of this is just, logic scripting.
        #region General Mouse
        public void CancleTransform()
        {
            if (ActiveOperation == EditorOperations.Move || ActiveOperation == EditorOperations.Rotate || ActiveOperation == EditorOperations.Scale)
            {
                ActiveOperation = EditorOperations.Idle;
                //activeObject.GetComponent<TransformComponent>().Tag = originalTag;
                foreach (GameObject go in selectedObjects)
                {
                    go.GetComponent<TransformComponent>().Tag = originalTags[selectedObjects.IndexOf(go)];
                }
                axisLock = AxisState.None;
            }
        }
        public void ConfirmTransform()
        {
            if (ActiveOperation == EditorOperations.Move || ActiveOperation == EditorOperations.Rotate || ActiveOperation == EditorOperations.Scale)
            {
                ActiveOperation = EditorOperations.Idle;
                axisLock = AxisState.None;
            }
        }
        public void MouseUpdateEvent(Vector2 v)
        {
            UnprojectedOld = UnprojectedPosition;
            UnprojectedPosition = GraphicsDevice.Viewport.Unproject(new Vector3(v.X, v.Y, 0), sceneCamera.Projection, sceneCamera.View, Matrix.Identity);
            var UnprojectedUnit = GraphicsDevice.Viewport.Unproject(new Vector3(v.X, v.Y, 1), sceneCamera.Projection, sceneCamera.View, Matrix.Identity);
            UnprojectedDirection = Vector3.Normalize(UnprojectedUnit - UnprojectedPosition);
            //mmm yeah?
            MousePositionOld = MousePosition;
            MousePosition = v;

        }
        public void Select()
        {
            if (ActiveOperation == EditorOperations.Idle)
            {
                //BRay selectRay = new BRay(
                //    MathConverter.Convert(UnprojectedPosition),
                //    MathConverter.Convert(UnprojectedDirection));
                RayCastResult rayResult = SelectionRay();
                //RayCastResult rayResult;
                //SceneSelectionSpace.RayCast(selectRay, out rayResult);

                if (rayResult.HitObject != null)
                {
                    selectedObjects = new List<GameObject>();
                    selectedObjects.Add((GameObject)((EntityCollidable)rayResult.HitObject).Entity.Tag);
                    //activeObject = selectedObjects[0];
                }
            }
            //I have to update, the system for modifier keys
            //hmm
        }
        public void MultiSelect()
        {
            if (ActiveOperation == EditorOperations.Idle)
            {
                ////so we need to create a new multitrnasform object
                //multiSelectParent = new GameObject();
                ////by defualt it creates a transform component yeah? and a transform tag?
                //multiSelectParent.GetComponent<TransformComponent>().Tag = new TransformTag();
                ////so we don't set this yet we need to know first
                //if (activeObject != null)
                //{
                //    multiSelectParent.AddChild(activeObject);//this doesn't work.this only works if the parent isn't set. 
                //    //okay so parenting breaks down with this so what if we just.
                //    //just perform this operation on all the selected objects
                //    //so none of this parenting stuff or clear transform just a list of gameobjects and an edittag.//Also original tag for the sake of, the whole point.so lets go
                //}
                //so we need an active object because of course. 
                //

                //BRay selectRay = new BRay(
                //MathConverter.Convert(UnprojectedPosition),
                //MathConverter.Convert(UnprojectedDirection));
                //RayCastResult rayResult;
                //SceneSelectionSpace.RayCast(selectRay, out rayResult);
                RayCastResult rayResult = SelectionRay();

                if (rayResult.HitObject != null)
                {
                    var hitObject = (GameObject)((EntityCollidable)rayResult.HitObject).Entity.Tag;

                    if (selectedObjects.Contains(hitObject))
                    {
                        selectedObjects.Remove(hitObject);
                        selectedObjects.Insert(0, hitObject);
                    }
                    else
                    {
                        selectedObjects.Insert(0, hitObject);
                    }
                    //selectedObjects.Add();


                    //activeObject = selectedObjects[0];
                }


                //cool so we need to make sure 
                //now we need to//
                //we don't have to actually set the active object until 

            }
        }
        RayCastResult SelectionRay()
        {
            BRay selectRay = new BRay(
                MathConverter.Convert(UnprojectedPosition),
                MathConverter.Convert(UnprojectedDirection));
            RayCastResult rayResult;
            SceneSelectionSpace.RayCast(selectRay,(BroadPhaseEntry entry)=>(entry.CollisionRules.Group == SelectionGroup), out rayResult);
            //ray casting needs, exceptions hmm.
            return rayResult;
        }
        //I guess this is obsolete huh
        Vector2 MouseTurntableStart;
        private void MouseSwivelInitial()
        {
            MouseTurntableStart = MousePosition;
        }
        private void MouseSwivelEvent()
        {
            //var delta = UnprojectedPosition - UnprojectedOld;
            if (ActiveOperation == EditorOperations.Idle)
            {
                var delta = MousePosition - MousePositionOld;
                sceneCamera.zAngel -= delta.X;
                sceneCamera.xAngel -= delta.Y;
                //sceneCamera.UpdateProjection();
                sceneCamera.UpdatePivot();
            }
            //so the only problem with this is out of boundsing the program but thats fine.
        }
        #endregion;
        double oldScrollvalue = 0;
        double ScrollValue = 0;
        double scrollSensitivity = .01;
        private void Zoom(double v)
        {

            oldScrollvalue = ScrollValue;
            ScrollValue = v;
            if (ActiveOperation == EditorOperations.Idle)
            {
                if (ScrollValue != oldScrollvalue)
                {
                    sceneCamera.distance -= (float)((ScrollValue - oldScrollvalue) * scrollSensitivity);
                    sceneCamera.UpdatePivot();
                }
            }
        }

        Matrix panView = Matrix.Identity;
        Matrix panProjection = Matrix.Identity;

        private void PanInitialize()
        {
            panView = sceneCamera.View;
            panProjection = sceneCamera.Projection;
        }
        Vector3 UnprojectedPan;
        Vector3 UnprojectedPanOld;
        private void PanPositionValues(Vector2 v)
        {
            UnprojectedPanOld = UnprojectedPan;
            UnprojectedPan = GraphicsDevice.Viewport.Unproject(new Vector3(v.X, v.Y, sceneCamera.distance), panProjection, panView, Matrix.Identity);

        }
        private void PanCamera()
        {
            if (ActiveOperation == EditorOperations.Idle)
            {

                var delta = UnprojectedPosition - UnprojectedOld;
                //var delta = UnprojectedPan - UnprojectedPanOld;
                sceneCamera.FocusPoint += delta;
            }
        }
        private void FocusCamera()
        {
            if (ActiveOperation == EditorOperations.Idle)
            {
                if (selectedObjects.Count > 0)
                {
                    var Transform = selectedObjects[0].GetComponent<TransformComponent>();
                    sceneCamera.FocusPoint = Transform.Position;
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using VertexData = MemoryLeakEditor.EnginePrimitives.GeometryCache.VertexData;

using Vertex = MemoryLeakEditor.EnginePrimitives.GeometryCache.Vertex;
using Edge = MemoryLeakEditor.EnginePrimitives.GeometryCache.Edge;
using Face = MemoryLeakEditor.EnginePrimitives.GeometryCache.Face;
using HalfEdge = MemoryLeakEditor.EnginePrimitives.GeometryCache.HalfEdge;

namespace MemoryLeakEditor.EnginePrimitives
{
    class CubePrimitive : DrawablePrimitive<VertexPositionNormalColor>
    {
        
        //public int VertexCount;
        //public int PrimitiveCount;
        //public int startingIndex;
        //public VertexPositionNormalColor[] vertices = new VertexPositionNormalColor[8];
        //public int[] indexBuffer;
        private List<short> indexList = new List<short>();

        private Color _color;
        public Color Color
        {
            get { return _color; }
            set
            {
                _color = value;
                for(int i = 0;i < vertices.Length;i++)
                {
                    vertices[i].Color = value;
                }
            }
        }

        public CubePrimitive(float x, float y, float z, Color color)
        {
            vertexCount = 8;
            
            primitiveCount = 12;

            primitiveType = PrimitiveType.TriangleList;

            var halfX = x / 2;
            var halfY = y / 2;
            var halfZ = z / 2;

            _color = color;
            vertices = new VertexPositionNormalColor[vertexCount];
            

            vertices[0] = new VertexPositionNormalColor(new Vector3(halfX, -halfY, halfZ), Vector3.Normalize(new Vector3(halfX, -halfY, halfZ)), color);
            vertices[1] = new VertexPositionNormalColor(new Vector3(-halfX, -halfY, halfZ), Vector3.Normalize(new Vector3(-halfX, -halfY, halfZ)), color);
            vertices[2] = new VertexPositionNormalColor(new Vector3(halfX, halfY, halfZ), Vector3.Normalize(new Vector3(halfX, halfY, halfZ)), color);
            vertices[3] = new VertexPositionNormalColor(new Vector3(-halfX, halfY, halfZ), Vector3.Normalize(new Vector3(-halfX, halfY, halfZ)), color);

            vertices[4] = new VertexPositionNormalColor(new Vector3(halfX, -halfY, -halfZ), Vector3.Normalize(new Vector3(halfX, -halfY, -halfZ)), color);
            vertices[5] = new VertexPositionNormalColor(new Vector3(-halfX, -halfY, -halfZ), Vector3.Normalize(new Vector3(-halfX, -halfY, -halfZ)), color);
            vertices[6] = new VertexPositionNormalColor(new Vector3(halfX, halfY, -halfZ), Vector3.Normalize(new Vector3(halfX, halfY, -halfZ)), color);
            vertices[7] = new VertexPositionNormalColor(new Vector3(-halfX, halfY, -halfZ), Vector3.Normalize(new Vector3(-halfX, halfY, -halfZ)), color);
            //TOp
            AddFace(0, 1, 2);
            AddFace(2, 1, 3);
            //Back
            //AddFace(0, 1, 4);
            AddFace(4, 1, 0);
            //AddFace(4, 1, 5);
            AddFace(5, 1, 4);
            //Left
            AddFace(3, 1, 7);
            AddFace(7, 1, 5);
            //front
            AddFace(2, 3, 6);
            AddFace(6, 3, 7);
            //Right
            AddFace(0, 2, 4);
            //AddFace(4, 2, 0);
            AddFace(4, 2, 6);
            //AddFace(6, 2, 4);
            //Bottom
            AddFace(5, 4, 6);
            AddFace(5, 6, 7);

            indices = indexList.ToArray();
        }
        public override void Initialize(GraphicsDevice graphicsDevice)
        {
            effect = new BasicEffect(graphicsDevice);
            effect.VertexColorEnabled = true;
            effect.EnableDefaultLighting();
            
        }
        
        public void AddFace(short i1, short i2, short i3)
        {
            indexList.Add(i1);
            indexList.Add(i2);
            indexList.Add(i3);
            
        }


    }
    public struct VertexPositionNormalColor : IVertexType
    {
        public Vector3 Position;
        public Vector3 Normal;
        public Color Color;

        public VertexPositionNormalColor(Vector3 position, Vector3 normal, Color color)
        {
            Position = position;
            Normal = normal;
            Color = color;
        }

        public static readonly VertexDeclaration VertexDeclaration = new VertexDeclaration(
            new VertexElement(0, VertexElementFormat.Vector3, VertexElementUsage.Position, 0),
            new VertexElement(12, VertexElementFormat.Vector3, VertexElementUsage.Normal, 0),
            new VertexElement(24, VertexElementFormat.Color, VertexElementUsage.Color, 0)
            );

        VertexDeclaration IVertexType.VertexDeclaration
        {
            get { return VertexDeclaration; }
        }
    }
    public struct VertexPositionNormalColorTexture : IVertexType
    {
        public Vector3 Position;
        public Vector3 Normal;
        public Color Color;
        public Vector2 TextureCoordinates;

        public VertexPositionNormalColorTexture(Vector3 position, Vector3 normal, Color color, Vector2 texturecoordinates)
        {
            Position = position;
            Normal = normal;
            Color = color;
            TextureCoordinates = texturecoordinates;
        }

        public static readonly VertexDeclaration VertexDeclaration = new VertexDeclaration(
            new VertexElement(0, VertexElementFormat.Vector3, VertexElementUsage.Position, 0),
            new VertexElement(12, VertexElementFormat.Vector3, VertexElementUsage.Normal, 0),
            new VertexElement(24, VertexElementFormat.Color, VertexElementUsage.Color, 0),
            new VertexElement(28, VertexElementFormat.Vector2, VertexElementUsage.TextureCoordinate, 0)
            );
        VertexDeclaration IVertexType.VertexDeclaration
        {
            get { return VertexDeclaration; }
        }

    }
    class Triangle : DrawablePrimitive<VertexPositionNormalColorTexture>
    {
        public override void Initialize(GraphicsDevice graphicsDevice)
        {
            effect = new BasicEffect(graphicsDevice);
            effect.VertexColorEnabled = true;
            effect.EnableDefaultLighting();
        }
    }
        

    public class CubePrimitive2
    {
        public static Model PrimitiveModel(CubePrimitive2 prim,GraphicsDevice graphics)
        {
            return prim.GenerateModel(graphics);
        }

        List<VertexPositionNormalColor> verticies = new List<VertexPositionNormalColor>();
        List<List<ushort>> polygons = new List<List<ushort>>();
        GeometryCache editableMesh = new GeometryCache();
        public CubePrimitive2(float x, float y, float z, Color color)
        {
            var halfX = x / 2;
            var halfY = y / 2;
            var halfZ = z / 2;

            verticies.Add( new VertexPositionNormalColor(new Vector3(halfX, -halfY, halfZ), Vector3.Normalize(new Vector3(halfX, -halfY, halfZ)), color));
            verticies.Add(  new VertexPositionNormalColor(new Vector3(-halfX, -halfY, halfZ), Vector3.Normalize(new Vector3(-halfX, -halfY, halfZ)), color));
            verticies.Add( new VertexPositionNormalColor(new Vector3(halfX, halfY, halfZ), Vector3.Normalize(new Vector3(halfX, halfY, halfZ)), color));
            verticies.Add( new VertexPositionNormalColor(new Vector3(-halfX, halfY, halfZ), Vector3.Normalize(new Vector3(-halfX, halfY, halfZ)), color));

            verticies.Add( new VertexPositionNormalColor(new Vector3(halfX, -halfY, -halfZ), Vector3.Normalize(new Vector3(halfX, -halfY, -halfZ)), color));
            verticies.Add( new VertexPositionNormalColor(new Vector3(-halfX, -halfY, -halfZ), Vector3.Normalize(new Vector3(-halfX, -halfY, -halfZ)), color));
            verticies.Add(new VertexPositionNormalColor(new Vector3(halfX, halfY, -halfZ), Vector3.Normalize(new Vector3(halfX, halfY, -halfZ)), color));
            verticies.Add( new VertexPositionNormalColor(new Vector3(-halfX, halfY, -halfZ), Vector3.Normalize(new Vector3(-halfX, halfY, -halfZ)), color));


            polygons.Add(new List<ushort>() { 0, 1, 3, 2 });
            polygons.Add(new List<ushort>() { 0, 2, 6, 4 });
            polygons.Add(new List<ushort>() { 2, 3, 7, 6 });
            polygons.Add(new List<ushort>() { 1, 5, 7, 3 });
            polygons.Add(new List<ushort>() { 1, 0, 4, 5 });
            //polygons.Add(new List<ushort>() { 5, 7, 6, 4 });
            polygons.Add(new List<ushort>() { 4, 6, 7, 5});
            Dictionary<VertexPositionNormalColor, ushort> VPToIndex = new Dictionary<VertexPositionNormalColor, ushort>();

            editableMesh = new GeometryCache();

            List<Vector3> VP = new List<Vector3>();
            List<List<ushort>> P = new List<List<ushort>>();
            GeometryCache.VertexDataBinding bindings = new GeometryCache.VertexDataBinding();

            foreach (var v in verticies)
            {
                if (!(VP.Contains(v.Position)))
                {
                    VP.Add(v.Position);
                    VPToIndex.Add(v, (ushort)VP.IndexOf(v.Position));
                }
                else
                {
                    VPToIndex.Add(v, (ushort)VP.IndexOf(v.Position));
                }
            }
            foreach (List<ushort> Face in polygons)
            {
                List<ushort> generatedPolygon = new List<ushort>();
                foreach (ushort FIndex in Face)
                {
                    VertexPositionNormalColor active = verticies[FIndex];
                    ushort iIndex = VPToIndex[active];
                    generatedPolygon.Add(iIndex);
                    bindings.Add(VP[iIndex], generatedPolygon, new VertexData()
                    {
                        Color = active.Color,
                        Normal = active.Normal,
                        //UV = active.TextureCoordinates
                    });
                }
                P.Add(generatedPolygon);
                //yeaah?yeah
            }
            editableMesh.Generate(VP, P, bindings);
            editableMesh.Triangulate();
            editableMesh.PropogateFaceNormals();

        }

        Model GenerateModel(GraphicsDevice graphics)
        {
            //prep return model
            // Model ret;

            //prep buffers
            IndexBuffer index;// = //new IndexBuffer(GraphicsDevice,IndexElementSize.SixteenBits,0)

            VertexBuffer vertex;

            //pref model data lists
            List<VertexPositionNormalColorTexture> verticies = new List<VertexPositionNormalColorTexture>();
            List<short> indicies = new List<short>();


            //lets loop over all our half edges 
            //I guess I meant faces so each face
            foreach (Face f in editableMesh.Faces)
            {
                //go over each half edge
                HalfEdge hIter = f.HalfEdge;
                do
                {
                    //create temp verticy
                    VertexPositionNormalColorTexture edgeVert = new VertexPositionNormalColorTexture(
                      hIter.Vertex.Position, hIter.Data.Normal, hIter.Data.Color, hIter.Data.UV);
                    //VertexPositionNormalColor edgeVert = new VertexPositionNormalColor(hIter.Vertex.Position, hIter.Data.Normal, hIter.Data.Color);
                    //check if vertex exists
                    if (!verticies.Contains(edgeVert))
                    {
                        //if not add the vert to vertex list
                        verticies.Add(edgeVert);

                        //add index
                        indicies.Add((short)verticies.IndexOf(edgeVert));
                    }
                    else
                    {
                        //if exists just add indicie
                        indicies.Add((short)verticies.IndexOf(edgeVert));
                    }

                    hIter = hIter.next;
                } while (hIter != f.HalfEdge);
            }
            //okay so thats faces done lets create our buffers

            index = new IndexBuffer(graphics, typeof(short), indicies.Count, BufferUsage.None);
            index.SetData<short>(indicies.ToArray());
            vertex = new VertexBuffer(graphics, typeof(VertexPositionNormalColorTexture), verticies.Count, BufferUsage.None);
            vertex.SetData(verticies.ToArray());
            BasicEffect partEffect = new BasicEffect(graphics);
            partEffect.VertexColorEnabled = true;
            partEffect.EnableDefaultLighting();

            ModelMeshPart xPart = new ModelMeshPart();
            //{
            //    Effect = partEffect,
            //    IndexBuffer = index,
            //    VertexBuffer = vertex,
            //    NumVertices = verticies.Count,
            //    PrimitiveCount = indicies.Count / 3,
            //    StartIndex = 0,
            //    VertexOffset = 0
            //};

            xPart.IndexBuffer = index;
            xPart.VertexBuffer = vertex;
            xPart.NumVertices = verticies.Count;
            xPart.PrimitiveCount = indicies.Count / 3;
            xPart.StartIndex = 0;
            xPart.VertexOffset = 0;

            List<ModelMeshPart> mmeshPartList = new List<ModelMeshPart>()
                    {
                        xPart
                    };

            List<ModelMesh> mmeshList = new List<ModelMesh>()
                {
                    new ModelMesh(graphics,
                   mmeshPartList )
                };
            Model ret = new Model(graphics, new List<ModelBone>(), mmeshList

                );
            ret.Meshes[0].MeshParts[0].Effect = partEffect;
            return ret;


            //uh lets get all the verticies I guess

        }
    }
   
    public class SubdivDiamondPrimitive
    {
        public static Model PrimitiveModel(SubdivDiamondPrimitive primitive, GraphicsDevice graphics)
        {
            return primitive.GenerateModel(graphics);
        }

        List<VertexPositionNormalColorTexture> vertexList = new List<VertexPositionNormalColorTexture>();

        List<List<short>> Polygons = new List<System.Collections.Generic.List<short>>();

        
        GeometryCache editableMesh = new GeometryCache();
        

        public SubdivDiamondPrimitive(float radius, float divisions, Color color)
        {
            vertexList.Add(new VertexPositionNormalColorTexture(
                Vector3.UnitZ, Vector3.UnitZ, color, new Vector2(.5f, .5f)));
            vertexList.Add(new VertexPositionNormalColorTexture(
                Vector3.UnitX, Vector3.UnitX, color, new Vector2(0, .5f)));
            vertexList.Add(new VertexPositionNormalColorTexture(
                Vector3.UnitY, Vector3.UnitY, color, new Vector2(.5f, 0)));
            vertexList.Add(new VertexPositionNormalColorTexture(
                -Vector3.UnitX, -Vector3.UnitX, color, new Vector2(1, .5f)));
            vertexList.Add(new VertexPositionNormalColorTexture(
                -Vector3.UnitY, -Vector3.UnitY, color, new Vector2(.5f,1)));
            vertexList.Add(new VertexPositionNormalColorTexture(
                -Vector3.UnitZ, -Vector3.UnitZ, color, new Vector2(1,1)));
            vertexList.Add(new VertexPositionNormalColorTexture(
                -Vector3.UnitZ, -Vector3.UnitZ, color, new Vector2(0,1)));
            vertexList.Add(new VertexPositionNormalColorTexture(
                -Vector3.UnitZ, -Vector3.UnitZ, color, new Vector2(1, 0)));
            vertexList.Add(new VertexPositionNormalColorTexture(
                -Vector3.UnitZ, -Vector3.UnitZ, color, new Vector2(0, 0)));

            Polygons.Add(new List<short>() { 2, 1, 0 });
            Polygons.Add(new List<short>() { 3, 2, 0 });
            Polygons.Add(new List<short>() { 4, 3, 0 });
            Polygons.Add(new List<short>() { 1, 4, 0 });
            Polygons.Add(new List<short>() { 3, 4, 5 });
            Polygons.Add(new List<short>() { 4, 1, 6 });
            Polygons.Add(new List<short>() { 2, 3, 7 });
            Polygons.Add(new List<short>() { 1, 2, 8 });

            //now we do mesh generation


            Dictionary<VertexPositionNormalColorTexture, ushort> VPToIndex = new Dictionary<VertexPositionNormalColorTexture, ushort>();

            editableMesh = new GeometryCache();

            List<Vector3> VP = new List<Vector3>();
            List<List<ushort>> P = new List<List<ushort>>();
            GeometryCache.VertexDataBinding bindings = new GeometryCache.VertexDataBinding();

            foreach(var v in vertexList)
            {
                if(!(VP.Contains(v.Position)))
                {
                    VP.Add(v.Position);
                    VPToIndex.Add(v, (ushort)VP.IndexOf(v.Position));
                }
                else
                {
                    VPToIndex.Add(v, (ushort)VP.IndexOf(v.Position));
                }
            }
            foreach(List<short> Face in Polygons)
            {
                List<ushort> generatedPolygon = new List<ushort>();
                foreach(short FIndex in Face)
                {
                    VertexPositionNormalColorTexture active = vertexList[FIndex];
                    ushort iIndex = VPToIndex[active];
                    generatedPolygon.Add(iIndex);
                    bindings.Add(VP[iIndex], generatedPolygon, new VertexData()
                    {
                        Color = active.Color,
                        Normal = active.Normal,
                        UV = active.TextureCoordinates
                    });
                }
                P.Add(generatedPolygon);
                //yeaah?yeah
            }
            editableMesh.Generate(VP, P, bindings);
            editableMesh.PropogateDegree();
            for(int i = 0;i<divisions;i++)
            {
                editableMesh = editableMesh.LoopSubdivison();

            }
            editableMesh.PropogateDegree();
            editableMesh.Sphereize(radius);
            editableMesh.PropogateNormals();
            //editableMesh.PropogateFaceNormals();

            //okay I lost track so I need
            //a vertex list of just positions
            //we need a datalist
            //and we need faces
            //so for each face connected to a polygon it needs its own data
            //so lets generate vertex list
        }
        Model GenerateModel(GraphicsDevice graphics)
        {
            //prep return model
            // Model ret;

            //prep buffers
            IndexBuffer index;// = //new IndexBuffer(GraphicsDevice,IndexElementSize.SixteenBits,0)

            VertexBuffer vertex;

            //pref model data lists
            List<VertexPositionNormalColorTexture> verticies = new List<VertexPositionNormalColorTexture>();
            List<short> indicies = new List<short>();


            //lets loop over all our half edges 
            //I guess I meant faces so each face
            foreach (Face f in editableMesh.Faces)
            {
                //go over each half edge
                HalfEdge hIter = f.HalfEdge;
                do
                {
                    //create temp verticy
                    VertexPositionNormalColorTexture edgeVert = new VertexPositionNormalColorTexture(
                      hIter.Vertex.Position, hIter.Data.Normal, hIter.Data.Color, hIter.Data.UV);
                    //VertexPositionNormalColor edgeVert = new VertexPositionNormalColor(hIter.Vertex.Position, hIter.Data.Normal, hIter.Data.Color);
                    //check if vertex exists
                    if (!verticies.Contains(edgeVert))
                    {
                        //if not add the vert to vertex list
                        verticies.Add(edgeVert);

                        //add index
                        indicies.Add((short)verticies.IndexOf(edgeVert));
                    }
                    else
                    {
                        //if exists just add indicie
                        indicies.Add((short)verticies.IndexOf(edgeVert));
                    }

                    hIter = hIter.next;
                } while (hIter != f.HalfEdge);
            }
            //okay so thats faces done lets create our buffers

            index = new IndexBuffer(graphics, typeof(short), indicies.Count, BufferUsage.None);
            index.SetData<short>(indicies.ToArray());
            vertex = new VertexBuffer(graphics, typeof(VertexPositionNormalColorTexture), verticies.Count, BufferUsage.None);
            vertex.SetData(verticies.ToArray());
            BasicEffect partEffect = new BasicEffect(graphics);
            partEffect.VertexColorEnabled = true;
            partEffect.EnableDefaultLighting();

            ModelMeshPart xPart = new ModelMeshPart();
            //{
            //    Effect = partEffect,
            //    IndexBuffer = index,
            //    VertexBuffer = vertex,
            //    NumVertices = verticies.Count,
            //    PrimitiveCount = indicies.Count / 3,
            //    StartIndex = 0,
            //    VertexOffset = 0
            //};

            xPart.IndexBuffer = index;
            xPart.VertexBuffer = vertex;
            xPart.NumVertices = verticies.Count;
            xPart.PrimitiveCount = indicies.Count / 3;
            xPart.StartIndex = 0;
            xPart.VertexOffset = 0;

            List<ModelMeshPart> mmeshPartList = new List<ModelMeshPart>()
                    {
                        xPart
                    };

            List<ModelMesh> mmeshList = new List<ModelMesh>()
                {
                    new ModelMesh(graphics,
                   mmeshPartList )
                };
            Model ret = new Model(graphics, new List<ModelBone>(), mmeshList

                );
            ret.Meshes[0].MeshParts[0].Effect = partEffect;
            return ret;

        }


    }


}



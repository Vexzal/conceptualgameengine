﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
namespace MemoryLeakEditor.EnginePrimitives
{
    class Grid : DrawablePrimitive<VertexPositionColor>
    {        
        //public int[] indicies;
        //public VertexPositionColor[] verticies;

        //public int vertexCount;
        //public int primitiveCount;

        public Grid(int subdivisions, float size, Color color)
        {
            List<VertexPositionColor> vList = new List<VertexPositionColor>();
            List<short> iList = new List<short>();

            float halfSize = size / 2;
            float incriment = size / subdivisions;

            var xPosition = -halfSize;
            var yPosition = -halfSize;
            

            for(int subdiv = 0; subdiv <= subdivisions;subdiv++)
            {
                //x ways
                vList.Add(new VertexPositionColor(new Vector3(xPosition, -halfSize, 0),color));
                vList.Add(new VertexPositionColor(new Vector3(xPosition, halfSize, 0), color));
                //y ways
                vList.Add(new VertexPositionColor(new Vector3(-halfSize, yPosition, 0), color));
                vList.Add(new VertexPositionColor(new Vector3(halfSize, yPosition, 0), color));

                //indicies 
                iList.Add((short)(4 * subdiv));
                iList.Add((short)(4 * subdiv + 1));
                iList.Add((short)(4 * subdiv + 2));
                iList.Add((short)(4 * subdiv + 3));
                //incriment position
                xPosition += incriment;
                yPosition += incriment;
            }

            var xIndex = iList.Count;
            //xAxis
            vList.Add(new VertexPositionColor( new Vector3(-halfSize, 0, 0), Color.Red));
            vList.Add(new VertexPositionColor(new Vector3(halfSize, 0, 0), Color.Red));
            //yAxis
            vList.Add(new VertexPositionColor(new Vector3(0, -halfSize, 0), Color.Green));
            vList.Add(new VertexPositionColor(new Vector3(0, halfSize, 0), Color.Green));
            //zAxis
            vList.Add(new VertexPositionColor(new Vector3(0, 0, -halfSize), Color.Blue));
            vList.Add(new VertexPositionColor(new Vector3(0, 0, halfSize), Color.Blue));

            //indexset
            iList.Add((short)xIndex);
            iList.Add((short)(xIndex + 1));
            iList.Add((short)(xIndex + 2));
            iList.Add((short)(xIndex + 3));
            iList.Add((short)(xIndex + 4));
            iList.Add((short)(xIndex + 5));

            //convert to array
            vertices = vList.ToArray();
            indices = iList.ToArray();
            vList.Clear();
            iList.Clear();

            vertexCount = vertices.Length;
            primitiveCount = indices.Length / 2;
            primitiveType = PrimitiveType.LineList;
            /**
             * psedo loop
             * xlength spot;
             * yLength spot;
             * 
             * foreach subdivision
             *      //x
             *      Vert xLength(x:xLength ,y:-halfsize ,z:0)
             *      vert xEnd (x:xlength, y:halfsize, z:0)
             *      **/

        }
        public override void Initialize(GraphicsDevice graphicsDevice)
        {
            effect = new BasicEffect(graphicsDevice);
            effect.VertexColorEnabled = true;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MemoryLeakEditor.EnginePrimitives
{
    abstract class DrawablePrimitive<T> where T:IVertexType
    {
        //vertex data
        /// <summary>
        /// Tells the graphics device what type of primitive is being drawn
        /// </summary>
        public PrimitiveType primitiveType;
        /// <summary>
        /// list of vertices 
        /// </summary>
        public T[] vertices;
        /// <summary>
        /// list of indices
        /// </summary>
        public short[] indices;
        /// <summary>
        /// number of vertices
        /// </summary>
        public int vertexCount;
        /// <summary>
        /// list of primtitives, (triangles or lines)
        /// </summary>
        public int primitiveCount;
        #region Matrices
        private Matrix _oldMatrix;
        public Matrix OldMatrix
        {            
            get => _oldMatrix;
            set => _oldMatrix = value;
        }

        public Matrix World
        {
            get => effect.World;
            set => effect.World = value;
        }
        public Matrix View
        {
            get => effect.View;
            set => effect.View = value;
        }
        public Matrix Projection
        {
            get => effect.Projection;
            set => effect.Projection = value;
        }
        #endregion
        /// <summary>
        /// Effect used by the grpahic
        /// </summary>
        public BasicEffect effect;

        public void Apply()
        {
            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
                pass.Apply();
        }
        public abstract void Initialize(GraphicsDevice graphicsDevice);
    }

    //public struct MeshContainer<T> where T:IVertexType
    //{
    //    public T[] vertices;
    //    public short[] indices;
    //    public int vertexCount;
    //    public int primitiveCount;
    //    public PrimitiveType drawType;
        
    //    public MeshContainer(PrimitiveType type, T[] _vertices,short[] _indices)
    //    {
    //        vertices = _vertices;
    //        indices = _indices;
    //        drawType = type;
    //        vertexCount = vertices.Length;
    //        primitiveCount = 0;
    //        if (type == PrimitiveType.LineList || type == PrimitiveType.LineStrip)
    //            primitiveCount = indices.Length / 2;
    //        if (type == PrimitiveType.TriangleList || type == PrimitiveType.TriangleStrip)
    //            primitiveCount = indices.Length / 3;                        
    //    }


    //}
    
}

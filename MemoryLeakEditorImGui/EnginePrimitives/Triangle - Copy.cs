﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MemoryLeakEditor.EnginePrimitives.x
{
    //Okay lets replace this wer'e gonna do, shape info here for faces edges and verticies

    public class GeometryCache    //for index type 
    {
        //private static Face BoundaryFace = new Face()
        
        public class VertexData
        {
            //public Vector3 Position { get; set; }//god just move this back to, to vertex its so much easier to manage position changes with one reference
            public Vector3 Normal { get; set; }
            public Vector2 UV { get; set; }
            public Color Color { get; set; }
        }
        #region old Implement, removed
        //List<Vector3> Verticies;
        //List<Face> Faces;
        //class VertexData
        //{
        //    //??
        //}
        ////hold on none of this works.
        ////okay so how do you get.
        ////edges aren't, vector 3s they're indicies.So a face is also a set of indicies 
        //class Edge
        //{
        //    List<short> indicies = new List<short>(2);
        //    public Edge(short index_a, short index_b)
        //    {
        //        indicies.Add(index_a);
        //        indicies.Add(index_b);
        //    }
        //    public bool Contains(short index)
        //    {
        //        return indicies.Contains(index);
        //    }
        //}
        //class Face
        //{
        //    List<short> indicies = new List<short>(4);//prevent making more than a quad
        //    public Face

        //}
        #endregion
        VertexDeclaration xDecl = new VertexDeclaration(
            new VertexElement(0, VertexElementFormat.Vector3, VertexElementUsage.Position, 0),
            new VertexElement(sizeof(float) * 3, VertexElementFormat.Vector3, VertexElementUsage.Normal, 0),
            new VertexElement(sizeof(float) * 6, VertexElementFormat.Vector2, VertexElementUsage.TextureCoordinate, 0));
        public ushort AvailableIndex = 0;//increment with each vertex add
        public List<Vertex> Verticies = new List<Vertex>(sizeof(ushort));
        public List<Edge> Edges = new List<Edge>();
        public List<Face> Faces = new List<Face>();

        public List<HalfEdge> HalfEdges = new List<HalfEdge>();

        public GeometryCache()
        {

        }
        public GeometryCache(List<Vertex>V,List<Edge>E,List<Face>F)
        {

        }
        //lets try, the half edge model, even though I sort of want non manifold to be possible for later?
        public abstract class HalfEdgeElement
        {
            public HalfEdge HalfEdge { get; set; }
            //public Vertex Vertex { get; set; }
            //public Edge Edge { get; set; }
            //public Face Face { get; set; }
            public abstract BoundingBox bounds();
            public abstract Vector3 centroid();
            //axes?

            internal void GenerateMaxMin(Vector3 point, ref Vector3 min, ref Vector3 max)
            {
                if (point.X < min.X)
                    min.X = point.X;

                if (point.Y < min.Y)
                    min.X = point.Y;

                if (point.Z < min.Z)
                    min.X = point.Z;
                if (point.X > max.X)
                    min.X = point.X;

                if (point.Y > max.Y)
                    min.X = point.Y;

                if (point.Z < min.Z)
                    min.X = point.Z;
            }
        }
        public class HalfEdge //: HalfEdgeElement
        {
            //public static HalfEdge BoundaryHEdge = new HalfEdge() { isBoundary = true };
            //public static HalfEdge BoundaryEdge(HalfEdge twin,)

            /// <summary>
            /// next edge in the faces winding order
            /// </summary>
            public HalfEdge next { get; set; }
            /// <summary>
            /// previous edge in the winding order
            /// </summary>
            public HalfEdge previous { get; set; }
            
            /// <summary>
            /// adjacent half edge linked to the next polygon.
            /// </summary>
            public HalfEdge twin { get; set; }
            /// <summary>
            /// Starting Verticy of the Edge
            /// </summary>
            public Vertex Vertex { get; set; }
            /// <summary>
            /// Edge Associated with the half edge
            /// </summary>
            public Edge Edge { get; set; } 
            /// <summary>
            /// Face containing the half edge
            /// </summary>
            public Face Face { get; set; }
            //public override BoundingBox bounds()
            //{
            //    return Edge.bounds();
            //}
            //public override Vector3 centroid()
            //{
            //    return Edge.centroid();
            //}
            public VertexData Data { get; set; }
            public bool isBoundary { get; set; }
        }
        public class Vertex : HalfEdgeElement
        {
            private Vector3 boundsScale = new Vector3(.1f, .1f, .1f);
            //public Vector3 Position { get; private set; }
            //public ushort Index { get; private set; }
            //public HalfEdge hEdge { get; private set; }//immeditaely notice these should inheret some base class
            //public Vertex(Vector3 position,ushort index)
            //{
            //    this.Index = index;
            //    this.Position = position;
            //}
            public Vector3 Position;
            public Vertex(Vector3 pos)
            {
                Position = pos;
            }

            public override Vector3 centroid()
            {
                //return HalfEdge.Data.Position;
                return Position;
            }
            public override BoundingBox bounds()
            {
                //return new BoundingBox(Position - boundsScale, Position + boundsScale);
                return new BoundingBox(Position - boundsScale, Position + boundsScale);
            }
            
            //other vertex data, to build maybe?
        }
        public class Edge : HalfEdgeElement
        {            
           // public HalfEdge hEdge { get; private set; }
            //hmm what does edge information look like? hm
            public override BoundingBox bounds()
            {
                Vector3 min = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);
                Vector3 max = new Vector3(float.MinValue, float.MinValue, float.MinValue);

                GenerateMaxMin(HalfEdge.Vertex.Position, ref min, ref max);
                GenerateMaxMin(HalfEdge.twin.Vertex.Position, ref min, ref max);

                return new BoundingBox(min, max);
                //return new BoundingBox(hEdge.Vertex.Position, hEdge.twin.Vertex.Position);

            }
            public override Vector3 centroid()
            {
                Vector3 average;

                average = HalfEdge.Vertex.Position + HalfEdge.twin.Vertex.Position;
                return average / 2;
            }
        }
        public class Face : HalfEdgeElement
        {
            //public static Face BoundaryFace = new Face() { isBoundary = true
            //,
            //HalfEdge = HalfEdge.BoundaryHEdge,            
            //};
            public Vector3 Normal;
            public Plane FacePlane;
            public int Degree;
            
            //public HalfEdge hEdge { get; private set; }

            public override BoundingBox bounds()
            {
                Vector3 min = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);
                Vector3 max = new Vector3(float.MinValue, float.MinValue, float.MinValue);

                HalfEdge h = HalfEdge;
                do
                {
                    GenerateMaxMin(h.Vertex.Position, ref min, ref max);
                    h = h.next;

                } while (h != HalfEdge);

                return new BoundingBox(min, max);
            }
            public override Vector3 centroid()
            {
                Vector3 average = Vector3.Zero;
                int magnitude = 0;
                HalfEdge h = HalfEdge;
                do
                {
                    average += h.Vertex.Position;
                    magnitude += 0;
                    h = h.next;

                } while (h != HalfEdge);

                return average / magnitude;
            }
            public bool isBoundary { get; set; }
            //maybe a plane. for selection at least or like. 
            //like for the bsp csp css whatever thing. binary space partitioning constructive solid geometry
        }
        
        protected struct IndexPair
        {
            ushort A;
            ushort B;
            public IndexPair(ushort a, ushort b)
            {
                A = a;
                B = b;
            }
        }
        
        public class VertexDataBinding
        {
            public struct VertexPolygonPair
            {
                public Vector3 vertex;
                public List<ushort> polygon;

                public VertexPolygonPair(Vector3 V, List<ushort> P)
                {
                    vertex = V;
                    polygon = P;
                }
            }
            Dictionary<VertexPolygonPair, VertexData> vertexBinding = new Dictionary<VertexPolygonPair, VertexData>();

            public VertexData this[Vector3 v, List<ushort> p]
            {
                get { return vertexBinding[new VertexPolygonPair(v, p)]; }
            }

            public void Add(Vector3 V, List<ushort> P, VertexData data)
            {
                vertexBinding.Add(new VertexPolygonPair(V, P), data);
            }
        }
        public void Generate(List<Vector3> verticies, List<List<ushort>> polygons, VertexDataBinding bindings)
        {
            ushort counter = 0;
            foreach(Vector3 vector in verticies)
            {
                //Verticies.Add(new Vertex(vector, counter));
                Verticies.Add(new Vertex(vector));
                counter++;
            }

            List<IndexPair> indexPairs = new List<IndexPair>();
            Dictionary<IndexPair, HalfEdge> pairToHEdge = new Dictionary<IndexPair, HalfEdge>();


            //now we can construct, polgyons I think
            foreach(List<ushort> polygon in polygons)
            {
                Face face = new Face();
                List<HalfEdge> faceHalfEdges = new List<HalfEdge>();
                for (int i = 0; i < polygon.Count; i++)
                {
                    Edge e = new Edge();//the edge 
                    ushort vertA = polygon[i];
                    ushort vertB = polygon[(i + 1) % polygon.Count];
                    IndexPair AB = new IndexPair(vertA, vertB);
                    //this half edge already exists, edge has 3 faces
                    if(pairToHEdge.ContainsKey(AB))
                    {
                        return;
                    }
                    //from this we can construct an edge, particularly a half edge
                    //half edge would at least be easier
                    HalfEdge h = new HalfEdge()
                    {
                        Vertex = Verticies[vertA],
                        Edge = e,
                        Face = face,
                        Data = bindings[verticies[vertA], polygon]
                    };

                    Verticies[vertA].HalfEdge = h;
                    face.HalfEdge = h;

                    //okay so we need to check for existance of twin before this
                    IndexPair BA = new IndexPair(vertB, vertA);
                    if (!pairToHEdge.ContainsKey(BA))
                    {
                        
                        e.HalfEdge = h;
                        //this aint elegant but eh it works and shouldn't be too slow
                        //defining halfEdge

                        HalfEdge boundary = new HalfEdge()
                        {
                            Edge = e,
                            Vertex = Verticies[vertB],
                            Face = new Face() { isBoundary = true },
                            isBoundary = true,
                            twin = h,
                            //next = boundary,
                            //previus = boundary
                            //shouldn't need data?
                        };
                        boundary.next = boundary;
                        boundary.previous = boundary;
                        //define boundary edge twin
                        h.twin = boundary;
                        //h.twin = new HalfEdge()
                        //{
                        //    Edge = e,
                        //    Vertex = Verticies[vertB],
                        //    Face = Face.BoundaryFace,
                        //    isBoundary = true,
                        //    twin = h,                            
                        //};
                        //iif I define half Edge as the FaceBoundary HalfEdge...hmm
                        //set boundary next to self
                       // h.twin.next = h.twin;
                        
                        Edges.Add(e);//only add edges when its first generated
                    }
                    else // ContainsHalfEdgeCondition
                    {
                        var existingEdge = pairToHEdge[BA];
                        h.twin = existingEdge; //set twin
                        h.Edge = existingEdge.Edge; //unset boundary halfEdge //oh no this is setting existing edge
                        existingEdge.twin = h;//this is unsetting boundary hEdge
                        
                    }
                    pairToHEdge.Add(AB, h);
                    faceHalfEdges.Add(h);

                    HalfEdges.Add(h);
                }
                //set next
                for (int i = 0; i < faceHalfEdges.Count; i++)
                {

                    faceHalfEdges[i].next = faceHalfEdges[(i + 1) % faceHalfEdges.Count];
                }
                //set previous
                for(int i = faceHalfEdges.Count;i>0;i--)
                {
                    //this shouuuuld work?
                    faceHalfEdges[i % faceHalfEdges.Count].previous = faceHalfEdges[i - 1];
                }
               
                Faces.Add(face);
            }
        }
        public void PropogateFacePlanes()
        {
            foreach(Face f in Faces)
            {
                HalfEdge hIter = f.HalfEdge;

                var ab = hIter.Vertex.Position - hIter.next.Vertex.Position;
                var cb = hIter.next.next.Vertex.Position - hIter.next.Vertex.Position;

                ab.Normalize();
                cb.Normalize();
                f.Normal =  Vector3.Cross(ab, cb);

                //f.FacePlane = new Plane(hIter.Vertex.Position, hIter.previous.Vertex.Position, hIter.previous.previous.Vertex.Position);
            }
        }
        public void PropogateDegree()
        {
            foreach(Face face in Faces)
            {
                int xDegree = 0;
                HalfEdge hIter = face.HalfEdge;
                do
                {
                    xDegree++;
                    hIter = hIter.next;

                } while (hIter != face.HalfEdge & xDegree < 10);

                face.Degree = xDegree;

                if(xDegree>=10)
                {
                    throw new Exception("UnResolvedFaceLoop");
                }


            }
            
        }
        public void PropogateNormals()
        {
            PropogateFacePlanes();
            foreach(Vertex v in Verticies)
            {
                int xDegree = 0;
                Vector3 average = Vector3.Zero;
                HalfEdge hIter = v.HalfEdge;
                do
                {
                    //average += hIter.twin.Vertex.Position;
                    average += Vector3.Normalize(hIter.twin.Face.FacePlane.Normal);
                    hIter = hIter.twin.next;
                    xDegree++;

                } while (hIter != v.HalfEdge);

                //average = average / xDegree;
                do
                {
                    hIter.Data.Normal = Vector3.Normalize(average);
                    hIter = hIter.twin.next;
                } while (hIter != v.HalfEdge);
            }
        }
        public void PropogateFaceNormals()
        {
            PropogateFacePlanes();
            foreach(Face f in Faces)
            {
                HalfEdge hIter = f.HalfEdge;
                do
                {
                    //hIter.Data.Normal = Vector3.Normalize(f.FacePlane.Normal);
                    hIter.Data.Normal = Vector3.Normalize(f.Normal);
                    hIter = hIter.next;
                } while (hIter!=f.HalfEdge);
            }
        }

        List<Edge> SubdivisonEdges = new List<Edge>(); //AddHereForoperationsthatgenerate edges during subdiv

        public GeometryCache LoopSubdivison()
        {
            SubdivisonEdges = new List<Edge>();
            GeometryCache returnCache = new GeometryCache();
            //do subdiv here?

            List<Vertex> createdVerticies = new List<Vertex>();
           

            List<Edge> startEdges = new List<Edge>();
            //List<Vector3> newPositions = new List<Vector3>();
          
            startEdges.AddRange(Edges);

            foreach (Edge e in startEdges)
            {
                //newPositions.Add(e.centroid());
            }

            foreach (Edge e in startEdges)
            {
                createdVerticies.Add(SplitEdge(e));//,newPositions[startEdges.IndexOf(e)]));
                
            }
            List<Edge> flipEdges = new List<Edge>();
            foreach(Edge e in SubdivisonEdges)
            {
                //so conditions needs one vert in one vert out.
                //so SO god damn so we need to 
               // bool containsBoth = (createdVerticies.Contains(e.HalfEdge.Vertex) && createdVerticies.Contains(e.HalfEdge.twin.Vertex));
                //bool containsNeither = ((!createdVerticies.Contains(e.HalfEdge.Vertex)) && (!createdVerticies.Contains(e.HalfEdge.Vertex)));

                
                if(!(createdVerticies.Contains(e.HalfEdge.Vertex) && createdVerticies.Contains(e.HalfEdge.twin.Vertex)))
                {
                    flipEdges.Add(e);               
                    //EdgeFlip2(e);
                }
            }
            foreach(Edge e in flipEdges )
            {
                EdgeFlip2(e);
            }
            //this should do it?????
            return this;
        }
        public GeometryCache LoopSubdivisonNoFlip()
        {
            GeometryCache returnCache = new GeometryCache();
            //do subdiv here?

            List<Vertex> createdVerticies = new List<Vertex>();


            List<Edge> startEdges = new List<Edge>();
            startEdges.AddRange(Edges);
            

            foreach (Edge e in startEdges)
            {
                //createdVerticies.Add(SplitEdge(e));

            }

            foreach (Edge e in SubdivisonEdges)
            {
                //so conditions needs one vert in one vert out.
                //so SO god damn so we need to 
                bool containsBoth = (createdVerticies.Contains(e.HalfEdge.Vertex) && createdVerticies.Contains(e.HalfEdge.twin.Vertex));
                bool containsNeither = ((!createdVerticies.Contains(e.HalfEdge.Vertex)) && (!createdVerticies.Contains(e.HalfEdge.Vertex)));


                if ((!containsBoth) && (!containsNeither))
                {

                    //EdgeFlip2(e);
                }
            }

            //this should do it?????
            return this;
        }
        //I forgot to generate new edges from split to end. so, lets do that too
        public Vertex SplitEdge(Edge e)
        {
            //so we need to generate a new vertex, easy

            //we need to generate a new half edge with vertex data, harder

            //Vector3 newPosition = Vector3.Zero;

            //two half edges, one for each edge.
            HalfEdge A = e.HalfEdge;
            HalfEdge B = e.HalfEdge.twin;

            //define new mesh elements
            Vertex newVertex = new Vertex(e.centroid());

            Edge newEdge = new Edge();

            VertexData centerDataA = new VertexData()
            {
                //Position = e.centroid(),
                Color = Color.Lerp(A.Data.Color, A.next.Data.Color, .5f),
                UV = Vector2.Lerp(A.Data.UV, A.next.Data.UV, .5f),
                Normal = Vector3.Normalize(Vector3.Lerp(A.Data.Normal, A.next.Data.Normal, .5f))
            };
            VertexData centerDataB = new VertexData()
            {
                //Position = e.centroid(),
                Color = Color.Lerp(B.Data.Color, B.next.Data.Color, .5f),
                UV = Vector2.Lerp(B.Data.UV, B.next.Data.UV, .5f),
                Normal = Vector3.Normalize(Vector3.Lerp(B.Data.Normal, B.next.Data.Normal, .5f))
            };

            HalfEdge AA = new HalfEdge()//new halfEdge
            {
                Vertex = newVertex,
                Edge = newEdge,
                Face = A.Face,
                Data = centerDataA,
                previous = A,
                next = A.next
            };
            HalfEdge BB = new HalfEdge()
            {
                Vertex = newVertex,
                Edge = newEdge,
                Face = B.Face,
                Data = centerDataB,
                previous = B,
                next = B.next
            };
            //B.Edge = newEdge;
            //insert new half edge into existing half edge loop
            AA.next.previous = AA;
            AA.previous.next = AA;
            BB.next.previous = BB;
            BB.previous.next = BB;

            //finish instantiating new half edges
            //thsi is wrong.
            //the new half edges are oriented with their opposing existing edges, so
            //AA.twin = BB;
            //BB.twin = AA;
            //this should be it.
            AA.twin = B;
            AA.twin.Edge = AA.Edge;//don't need repeat on, no fuck it lets repeat it
            B.twin = AA;
            BB.twin = A;
            A.twin = BB;
            A.twin.Edge = A.Edge;

            //add half edge reference to new elements
            newVertex.HalfEdge = BB;
            newEdge.HalfEdge = BB;
            //add all of these to the lists
            Verticies.Add(newVertex);
            Edges.Add(newEdge);
            
            HalfEdges.Add(AA);
            HalfEdges.Add(BB);

            ////join edge for each face
            ////A
            //Edge jointEdgeA = new Edge();
            //Face existingFaceA = A.Face;
            //Face newFaceA = new Face();
            ////half edges for new edge
            //HalfEdge JAA = new HalfEdge();
            //HalfEdge JAB = new HalfEdge();

            ////B
            //Edge jointEdgeB = new Edge();
            //Face existingFaceB = B.Face;
            //Face newFaceB = new Face();

            //HalfEdge JBA = new HalfEdge();
            //HalfEdge JBB = new HalfEdge();

            //SplitEdgeSplitFace(AA);
            //SplitEdgeSplitFace(BB);

            SplitFace(AA);
            SplitFace(BB);
            return newVertex;
        }

        void SplitEdge2(Edge e)
        {
            //so you want the edges and their twins to , no you don't want them the same what.
            //fuck
            //why didn't this cause any problems on the first loop

        }

        void SplitEdgeSplitFace(HalfEdge hEdge)
        {
            Edge jointEdge = new Edge();
            Face existingFace = hEdge.Face;//face from existing half edge, set halfEdge as new half edge connected to original hedge
            Face newFace = new Face(); //face assigned to twin half edge

            //HalfEdge altEdge = hEdge.previus;

            HalfEdge A;
            HalfEdge B;//lets define our half edges

            //okay so SO no new verticies, but one new face and one new edge

            A = new HalfEdge()
            {
                Vertex = hEdge.next.next.Vertex,
                Edge = jointEdge,
                Face = existingFace,//existingFace,
                Data = hEdge.next.next.Data,
                next = hEdge,
                previous = hEdge.next,                
            };
            //^^^^^
            //THIS WORKS??
            //wait, what if its this that doesn't work
            //hEdge.previus = A;
            //hEdge.next.next = A;
            existingFace.HalfEdge = A;
            //THIS DOESN"T WORK
            //FIX IT?? SOMEHOW??
            B = new HalfEdge()
            {
                Vertex = hEdge.Vertex,
                Edge = jointEdge,
                Face = newFace,
                Data = A.Data,
                next = hEdge.next.next,
                previous = hEdge.previous
            };
            //okay no this works??? ahh???
            A.twin = B;
            B.twin = A;

            A.next.previous = A;
            A.previous.next = A;
            B.next.previous = B;
            B.previous.next = B;//uhhhh yeah? yeah

            existingFace.HalfEdge = A;
            newFace.HalfEdge = B;

            HalfEdge hIter = A;
            do
            {
                hIter.Face = newFace;
                hIter = hIter.next;
            } while (hIter != A);
            hIter = B;
            do
            {
                hIter.Face = existingFace;
                hIter = hIter.next;
            } while (hIter != B);

            jointEdge.HalfEdge = B; 

            Faces.Add(newFace);
            Edges.Add(jointEdge);
            HalfEdges.Add(A);
            HalfEdges.Add(B);
        }

        void SplitFace(HalfEdge hEdge)
        {
            Face existingFace = hEdge.Face;
            Face newFaceA = new Face();
            Face newFaceB = new Face();

            Edge jointEdge = new Edge();

            HalfEdge A;
            HalfEdge B;//lets define our half edges

            //okay so SO no new verticies, but one new face and one new edge

            A = new HalfEdge()
            {
                Vertex = hEdge.next.next.Vertex,
                Edge = jointEdge,
                Face = newFaceA,
                Data = hEdge.next.next.Data,
                next = hEdge,
                previous = hEdge.next,
            };
            
            B = new HalfEdge()
            {
                Vertex = hEdge.Vertex,
                Edge = jointEdge,
                Face = newFaceB,
                Data = A.Data,
                next = hEdge.next.next,
                previous = hEdge.previous
            };
            //okay no this works??? ahh???
            A.twin = B;
            B.twin = A;

            A.next.previous = A;
            A.previous.next = A;
            B.next.previous = B;
            B.previous.next = B;//uhhhh yeah? yeah

            newFaceA.HalfEdge = A;
            newFaceB.HalfEdge = B;

            HalfEdge hIter = A;
            do
            {
                hIter.Face = newFaceA;
                hIter = hIter.next;
            } while (hIter != A);
            hIter = B;
            do
            {
                hIter.Face = newFaceB;
                hIter = hIter.next;
            } while (hIter != B);

            //existingFace.HalfEdge = A;
            //newFace.HalfEdge = B;
            jointEdge.HalfEdge = B;

            Faces.Add(newFaceA);
            Faces.Add(newFaceB);
            Faces.Remove(existingFace);
            Edges.Add(jointEdge);
            SubdivisonEdges.Add(jointEdge);
            HalfEdges.Add(A);
            HalfEdges.Add(B);
        }

        void EdgeFlip(Edge e)
        {
            //so in turning an edge we
            //so we start with 
            HalfEdge A = e.HalfEdge;
            HalfEdge B = A.twin;

            Face FA = A.Face;
            Face FB = B.Face;

            HalfEdge ANext = A.next.next;
            HalfEdge APrevious = B.next;
            HalfEdge ASource = B.next.next;

            HalfEdge BNext = B.next.next;
            HalfEdge BPrevious = A.next;
            HalfEdge BSource = A.next.next;

            A.next = ANext;
            A.previous = APrevious;
            B.next = BNext;
            B.previous = BPrevious;//I have to set vertex and data info too, fuuuck

            A.next.previous = A;
            A.previous.next = A;
            B.next.previous = B;
            B.previous.next = B;

            A.Vertex = ASource.Vertex;
            A.Data = ASource.Data;
            B.Vertex = BSource.Vertex;
            B.Data = BSource.Data;

            //alright lets loop through and set new faces for all the edges just in case
            HalfEdge hIter = A;
            do
            {
                hIter.Face = FA;
                hIter = hIter.next;
            } while (hIter != A);
            hIter = B;
            do
            {
                hIter.Face = FB;
                hIter = hIter.next;
            } while (hIter != B);
            Console.WriteLine(" edge flip: " + e.HalfEdge.Vertex.Position);
            FA.HalfEdge = A;
            FB.HalfEdge = B;

            //we're gonna redefine these edges positions
        }
        public void EdgeFlip2(Edge e)
        {
            //ensure face on either end is associated with existing half edge
            //so when we loop through later
            //we have a solid point of reference
            HalfEdge A = e.HalfEdge;
            A.Face.HalfEdge = A;
            HalfEdge B = e.HalfEdge.twin;
            B.Face.HalfEdge = B;
            
            //define all new pointers
            HalfEdge AStarNext = A.next.next;
            HalfEdge AStarPRevious = A.twin.next;
            Vertex AStarVertex = A.twin.next.next.Vertex;
            VertexData AStarVertexData = A.twin.next.next.Data;

            HalfEdge BStarNext = B.next.next;
            HalfEdge BStarPrevious = B.twin.next;
            Vertex BStarVertex = B.twin.next.next.Vertex;
            VertexData BStarVertexData = B.twin.next.next.Data;

            //assigne all new pointers
            A.next = AStarNext;
            A.previous = AStarPRevious;
            B.next = BStarNext;
            B.previous = BStarPrevious;
            //this section will only resolve on tris
            //
            AStarPRevious.previous = AStarNext;
            AStarNext.next = AStarPRevious;
            BStarPrevious.previous = BStarNext;
            BStarNext.next = BStarPrevious;
            //to resolve on higher order polygons, 
            //create single looped polygon by looping references
            //then reinsert edges

            A.next.previous = A;
            A.previous.next = A;
            B.next.previous = A;
            B.previous.next = B;
            
            A.Vertex = AStarVertex;
            A.Data = AStarVertexData;
            B.Vertex = BStarVertex;
            B.Data = BStarVertexData;
            //propogate faces to half edges
            Face FA = A.Face;
            Face FB = B.Face;
            HalfEdge hIter = A;
            do
            {
                hIter.Face = FA;
                hIter = hIter.next;
            } while (hIter != A);
            hIter = B;
            do
            {
                hIter.Face = FB;
                hIter = hIter.next;
            } while (hIter != B);

            Console.WriteLine(Faces.IndexOf(A.Face));
            Console.WriteLine(Faces.IndexOf(B.Face));

        }

        void EdgeFlip3(Edge e)
        {
            //alright lets do this a fucking gain.
            //okay lets define our moving edges
            HalfEdge A = e.HalfEdge;
            HalfEdge B = e.HalfEdge.twin;
            //cool cool cool NOW lets define insert points
            HalfEdge AStarNext = A.next.next;
            HalfEdge AStarPrevious = A.twin.next;
            HalfEdge BStarNext = B.next.next;
            HalfEdge BStarPrevious = B.twin.next;
            //after we've defined our insert points lets close the loop

            //A.next.next.next = A.twin.next;
            //A.twin.previus = A.next.next;
            //equivalant to 
            AStarNext.next = AStarPrevious;
            AStarPrevious.previous = AStarNext;
            BStarNext.next = BStarPrevious;
            //oh so no this whole god damn algorithm only works on tris, mother fucker 


        
        }
        public void Sphereize(float radius)
        {
            Vector3 center = Vector3.Zero;

            foreach(Vertex v in Verticies)
            {
                //center += v.HalfEdge.Data.Position;
                center += v.Position;

            }
            center /= Verticies.Count;


            //now we just go back over every verticy find the, direction a vertex is from the center

            foreach(Vertex v in Verticies)
            {
                v.Position = Vector3.Normalize(v.Position - center) * radius;
            }//uhh, yeah thats it
        }

        //I don't now how to do this programatically
        //I guess actually, don't do this here
        //create external functions that take a geometry cache (rename that what is that)
        //and return the needed vertex type, with default input values oh yeah,
        //the big problem is data assurance and if you just pass default values and build from this your fine I guess?
        //public void BuildDrawable() - don't do this here
    }
    class SphereDiamondPrimitiveRef : DrawablePrimitive<VertexPositionNormalColorTexture>
    {

        private List<short> indexList = new List<short>();
        private List<VertexPositionNormalColorTexture> vertexList = new List<VertexPositionNormalColorTexture>();
        public SphereDiamondPrimitiveRef(float radius, int divisions, Color color)
        {
            vertexList.Add(new VertexPositionNormalColorTexture(Vector3.UnitZ, Vector3.UnitZ, color, new Vector2(.5f, .5f)));//0
            vertexList.Add(new VertexPositionNormalColorTexture(Vector3.UnitX, Vector3.UnitX, color, new Vector2(1, .5f)));//1
            vertexList.Add(new VertexPositionNormalColorTexture(Vector3.UnitY, Vector3.UnitY, color, new Vector2(.5f, 1)));//2
            vertexList.Add(new VertexPositionNormalColorTexture(-Vector3.UnitX, -Vector3.UnitX, color, new Vector2(0, .5f)));//3
            vertexList.Add(new VertexPositionNormalColorTexture(-Vector3.UnitY, -Vector3.UnitY, color, new Vector2(.5f, 0)));//4
            vertexList.Add(new VertexPositionNormalColorTexture(-Vector3.UnitZ, -Vector3.UnitZ, color, new Vector2(0, 0)));//5
            vertexList.Add(new VertexPositionNormalColorTexture(-Vector3.UnitZ, -Vector3.UnitZ, color, new Vector2(1, 0)));//6
            vertexList.Add(new VertexPositionNormalColorTexture(-Vector3.UnitZ, -Vector3.UnitZ, color, new Vector2(0, 1)));//7
            vertexList.Add(new VertexPositionNormalColorTexture(-Vector3.UnitZ, -Vector3.UnitZ, color, new Vector2(1, 1)));//8

            AddFace(2, 1, 0);//0
            AddFace(3, 2, 0);//1
            AddFace(4, 3, 0);//2
            AddFace(1, 4, 0);//3
            AddFace(3, 4, 5);//4
            AddFace(4, 1, 6);//5
            AddFace(2, 3, 7);//6
            AddFace(1, 2, 8);//7

            vertexCount = 9;
            primitiveCount = 8;
            primitiveType = PrimitiveType.TriangleList;
            vertices = vertexList.ToArray();
            indices = indexList.ToArray();

        }
        public override void Initialize(GraphicsDevice graphicsDevice)
        {
            effect = new BasicEffect(graphicsDevice);
            effect.VertexColorEnabled = true;
            effect.EnableDefaultLighting();
        }


        public void AddFace(short i1, short i2, short i3)
        {
            indexList.Add(i1);
            indexList.Add(i2);
            indexList.Add(i3);

        }
    }
}

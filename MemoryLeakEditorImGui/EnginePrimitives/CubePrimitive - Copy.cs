﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using VertexData = MemoryLeakEditor.EnginePrimitives.GeometryCache.VertexData;

using Vertex = MemoryLeakEditor.EnginePrimitives.GeometryCache.Vertex;
using Edge = MemoryLeakEditor.EnginePrimitives.GeometryCache.Edge;
using Face = MemoryLeakEditor.EnginePrimitives.GeometryCache.Face;
using HalfEdge = MemoryLeakEditor.EnginePrimitives.GeometryCache.HalfEdge;

namespace MemoryLeakEditor.EnginePrimitives.x
{
    class CubePrimitive : DrawablePrimitive<VertexPositionNormalColor>
    {
        
        //public int VertexCount;
        //public int PrimitiveCount;
        //public int startingIndex;
        //public VertexPositionNormalColor[] vertices = new VertexPositionNormalColor[8];
        //public int[] indexBuffer;
        private List<short> indexList = new List<short>();

        private Color _color;
        public Color Color
        {
            get { return _color; }
            set
            {
                _color = value;
                for(int i = 0;i < vertices.Length;i++)
                {
                    vertices[i].Color = value;
                }
            }
        }

        public CubePrimitive(float x, float y, float z, Color color)
        {
            vertexCount = 8;
            
            primitiveCount = 12;

            primitiveType = PrimitiveType.TriangleList;

            var halfX = x / 2;
            var halfY = y / 2;
            var halfZ = z / 2;

            _color = color;
            vertices = new VertexPositionNormalColor[vertexCount];
            

            vertices[0] = new VertexPositionNormalColor(new Vector3(halfX, -halfY, halfZ), Vector3.Normalize(new Vector3(halfX, -halfY, halfZ)), color);
            vertices[1] = new VertexPositionNormalColor(new Vector3(-halfX, -halfY, halfZ), Vector3.Normalize(new Vector3(-halfX, -halfY, halfZ)), color);
            vertices[2] = new VertexPositionNormalColor(new Vector3(halfX, halfY, halfZ), Vector3.Normalize(new Vector3(halfX, halfY, halfZ)), color);
            vertices[3] = new VertexPositionNormalColor(new Vector3(-halfX, halfY, halfZ), Vector3.Normalize(new Vector3(-halfX, halfY, halfZ)), color);

            vertices[4] = new VertexPositionNormalColor(new Vector3(halfX, -halfY, -halfZ), Vector3.Normalize(new Vector3(halfX, -halfY, -halfZ)), color);
            vertices[5] = new VertexPositionNormalColor(new Vector3(-halfX, -halfY, -halfZ), Vector3.Normalize(new Vector3(-halfX, -halfY, -halfZ)), color);
            vertices[6] = new VertexPositionNormalColor(new Vector3(halfX, halfY, -halfZ), Vector3.Normalize(new Vector3(halfX, halfY, -halfZ)), color);
            vertices[7] = new VertexPositionNormalColor(new Vector3(-halfX, halfY, -halfZ), Vector3.Normalize(new Vector3(-halfX, halfY, -halfZ)), color);
            //TOp
            AddFace(0, 1, 2);
            AddFace(2, 1, 3);
            //Back
            //AddFace(0, 1, 4);
            AddFace(4, 1, 0);
            //AddFace(4, 1, 5);
            AddFace(5, 1, 4);
            //Left
            AddFace(3, 1, 7);
            AddFace(7, 1, 5);
            //front
            AddFace(2, 3, 6);
            AddFace(6, 3, 7);
            //Right
            AddFace(0, 2, 4);
            //AddFace(4, 2, 0);
            AddFace(4, 2, 6);
            //AddFace(6, 2, 4);
            //Bottom
            AddFace(5, 4, 6);
            AddFace(5, 6, 7);

            indices = indexList.ToArray();
        }
        public override void Initialize(GraphicsDevice graphicsDevice)
        {
            effect = new BasicEffect(graphicsDevice);
            effect.VertexColorEnabled = true;
            effect.EnableDefaultLighting();
            
        }
        
        public void AddFace(short i1, short i2, short i3)
        {
            indexList.Add(i1);
            indexList.Add(i2);
            indexList.Add(i3);
            
        }


    }
    public struct VertexPositionNormalColor : IVertexType
    {
        public Vector3 Position;
        public Vector3 Normal;
        public Color Color;

        public VertexPositionNormalColor(Vector3 position, Vector3 normal, Color color)
        {
            Position = position;
            Normal = normal;
            Color = color;
        }

        public static readonly VertexDeclaration VertexDeclaration = new VertexDeclaration(
            new VertexElement(0, VertexElementFormat.Vector3, VertexElementUsage.Position, 0),
            new VertexElement(12, VertexElementFormat.Vector3, VertexElementUsage.Normal, 0),
            new VertexElement(24, VertexElementFormat.Color, VertexElementUsage.Color, 0)
            );

        VertexDeclaration IVertexType.VertexDeclaration
        {
            get { return VertexDeclaration; }
        }
    }
    public struct VertexPositionNormalColorTexture : IVertexType
    {
        public Vector3 Position;
        public Vector3 Normal;
        public Color Color;
        public Vector2 TextureCoordinates;

        public VertexPositionNormalColorTexture(Vector3 position, Vector3 normal, Color color, Vector2 texturecoordinates)
        {
            Position = position;
            Normal = normal;
            Color = color;
            TextureCoordinates = texturecoordinates;
        }

        public static readonly VertexDeclaration VertexDeclaration = new VertexDeclaration(
            new VertexElement(0, VertexElementFormat.Vector3, VertexElementUsage.Position, 0),
            new VertexElement(12, VertexElementFormat.Vector3, VertexElementUsage.Normal, 0),
            new VertexElement(24, VertexElementFormat.Color, VertexElementUsage.Color, 0),
            new VertexElement(28, VertexElementFormat.Vector2, VertexElementUsage.TextureCoordinate, 0)
            );
        VertexDeclaration IVertexType.VertexDeclaration
        {
            get { return VertexDeclaration; }
        }

    }
    class Triangle : DrawablePrimitive<VertexPositionNormalColorTexture>
    {
        public override void Initialize(GraphicsDevice graphicsDevice)
        {
            effect = new BasicEffect(graphicsDevice);
            effect.VertexColorEnabled = true;
            effect.EnableDefaultLighting();
        }
    }
        
    class SphereDiamondPrimitive : DrawablePrimitive<VertexPositionNormalColorTexture>
    {

        private List<short> indexList = new List<short>();
        private List<VertexPositionNormalColorTexture> vertexList = new List<VertexPositionNormalColorTexture>();
        public SphereDiamondPrimitive(float radius, int divisions,Color color)
        {
            vertexList.Add(new VertexPositionNormalColorTexture(Vector3.UnitZ, Vector3.UnitZ, color, new Vector2(.5f, .5f)));//0
            vertexList.Add(new VertexPositionNormalColorTexture(Vector3.UnitX, Vector3.UnitX, color, new Vector2(1, .5f)));//1
            vertexList.Add(new VertexPositionNormalColorTexture(Vector3.UnitY, Vector3.UnitY, color, new Vector2(.5f, 1)));//2
            vertexList.Add(new VertexPositionNormalColorTexture(-Vector3.UnitX, -Vector3.UnitX, color, new Vector2(0, .5f)));//3
            vertexList.Add(new VertexPositionNormalColorTexture(-Vector3.UnitY, -Vector3.UnitY, color, new Vector2(.5f, 0)));//4
            vertexList.Add(new VertexPositionNormalColorTexture(-Vector3.UnitZ, -Vector3.UnitZ, color, new Vector2(0, 0)));//5
            vertexList.Add(new VertexPositionNormalColorTexture(-Vector3.UnitZ, -Vector3.UnitZ, color, new Vector2(1, 0)));//6
            vertexList.Add(new VertexPositionNormalColorTexture(-Vector3.UnitZ, -Vector3.UnitZ, color, new Vector2(0, 1)));//7
            vertexList.Add(new VertexPositionNormalColorTexture(-Vector3.UnitZ, -Vector3.UnitZ, color, new Vector2(1, 1)));//8

            AddFace(2, 1, 0);
            AddFace(3, 2, 0);
            AddFace(4, 3, 0);
            AddFace(1, 4, 0);
            AddFace(3, 4, 5);
            AddFace(4, 1, 6);
            AddFace(2, 3, 7);
            AddFace(1, 2, 8);

            vertexCount = 9;
            primitiveCount = 8;
            primitiveType = PrimitiveType.TriangleList;
            vertices = vertexList.ToArray();
            indices = indexList.ToArray();

        }
        public override void Initialize(GraphicsDevice graphicsDevice)
        {
            effect = new BasicEffect(graphicsDevice);
            effect.VertexColorEnabled = true;
            effect.EnableDefaultLighting();
        }


        public void AddFace(short i1, short i2, short i3)
        {
            indexList.Add(i1);
            indexList.Add(i2);
            indexList.Add(i3);

        }
    }

    public class SphereDiamondPrimitive2 
    {

        public static Model PrimitiveModel(SphereDiamondPrimitive2 prim,GraphicsDevice graphics)
        {
            return prim.GenerateModel(graphics);
        }


        List<Vector3> VP = new List<Vector3>();//vertex positions
        List<List<ushort>> P = new List<List<ushort>>();//indicies collections, polygons
        GeometryCache.VertexDataBinding dataBindings = new GeometryCache.VertexDataBinding();

        GeometryCache generatedCache = new GeometryCache();

        public SphereDiamondPrimitive2(float radius, float divisions, Color color)
        {
            VP.Add(Vector3.UnitZ);  //0
            VP.Add(Vector3.UnitX);  //1
            VP.Add(Vector3.UnitY);  //2
            VP.Add(-Vector3.UnitX); //3
            VP.Add(-Vector3.UnitY); //4
            VP.Add(-Vector3.UnitZ); //5

            P.Add(new List<ushort>() { 2, 1, 0 });//0 Red
            P.Add(new List<ushort>() { 3, 2, 0 });//1 Green
            P.Add(new List<ushort>() { 4, 3, 0 });//2 Blue
            P.Add(new List<ushort>() { 1, 4, 0 });//3 White
            P.Add(new List<ushort>() { 3, 4, 5 });//4 Cyan
            P.Add(new List<ushort>() { 4, 1, 5 });//5 Magenta
            P.Add(new List<ushort>() { 2, 3, 5 });//6 Yellow
            P.Add(new List<ushort>() { 1, 2, 5 });//7 Black

            #region DataBindings
            #region Index0
            dataBindings.Add(VP[0], P[0], new VertexData()
            {
                //Position = VP[0],
                Normal = Vector3.UnitZ,
                UV = new Vector2(.5f, .5f),
                Color = color
            });
            dataBindings.Add(VP[0], P[1], new VertexData()
            {
                //Position = VP[0],
                Normal = Vector3.UnitZ,
                UV = new Vector2(.5f, .5f),
                Color = color
            });
            dataBindings.Add(VP[0], P[2], new VertexData()
            {
                //Position = VP[0],
                Normal = Vector3.UnitZ,
                UV = new Vector2(.5f, .5f),
                Color = color
            });
            dataBindings.Add(VP[0], P[3], new VertexData()
            {
                //Position = VP[0],
                Normal = Vector3.UnitZ,
                UV = new Vector2(.5f, .5f),
                Color = color
            });
            #endregion
            #region Index1
            dataBindings.Add(VP[1], P[0], new VertexData()
            {
                //Position = VP[1],
                Normal = Vector3.UnitX,
                UV = new Vector2(1, .5f),
                Color = color
            });
            dataBindings.Add(VP[1], P[3], new VertexData()
            {
                //Position = VP[1],
                Normal = Vector3.UnitX,
                UV = new Vector2(1, .5f),
                Color = color
            });
            dataBindings.Add(VP[1], P[5], new VertexData()
            {
                //Position = VP[1],
                Normal = Vector3.UnitX,
                UV = new Vector2(1, .5f),
                Color = color
            });
            dataBindings.Add(VP[1], P[7], new VertexData()
            {
                //Position = VP[1],
                Normal = Vector3.UnitX,
                UV = new Vector2(1, .5f),
                Color = color
            });
            #endregion
            #region Index2
            dataBindings.Add(VP[2], P[0], new VertexData()
            {
                //Position = VP[2],
                Normal = Vector3.UnitY,
                UV = new Vector2(.5f, 1),
                Color = color
            });
            dataBindings.Add(VP[2], P[1], new VertexData()
            {
                //Position = VP[2],
                Normal = Vector3.UnitY,
                UV = new Vector2(.5f, 1),
                Color = color
            });
            dataBindings.Add(VP[2], P[6], new VertexData()
            {
                //Position = VP[2],
                Normal = Vector3.UnitY,
                UV = new Vector2(.5f, 1),
                Color = color
            });
            dataBindings.Add(VP[2], P[7], new VertexData()
            {
                //Position = VP[2],
                Normal = Vector3.UnitY,
                UV = new Vector2(.5f, 1),
                Color = color
            });
            #endregion
            #region Index3
            dataBindings.Add(VP[3], P[1], new VertexData()
            {
                //Position = VP[3],
                Normal = -Vector3.UnitX,
                UV = new Vector2(0, .5f),
                Color = color
            });
            dataBindings.Add(VP[3], P[2], new VertexData()
            {
                //Position = VP[3],
                Normal = -Vector3.UnitX,
                UV = new Vector2(0, .5f),
                Color = color
            });
            dataBindings.Add(VP[3], P[4], new VertexData()
            {
                //Position = VP[3],
                Normal = -Vector3.UnitX,
                UV = new Vector2(0, .5f),
                Color = color
            });
            dataBindings.Add(VP[3], P[6], new VertexData()
            {
                //Position = VP[3],
                Normal = -Vector3.UnitX,
                UV = new Vector2(0, .5f),
                Color = color
            });
            #endregion
            #region Index4
            dataBindings.Add(VP[4], P[2], new VertexData()
            {
                //Position = VP[4],
                Normal = -Vector3.UnitY,
                UV = new Vector2(.5f, 1),
                Color = color
            });
            dataBindings.Add(VP[4], P[3], new VertexData()
            {
                //Position = VP[4],
                Normal = -Vector3.UnitY,
                UV = new Vector2(.5f, 1),
                Color = color
            });
            dataBindings.Add(VP[4], P[4], new VertexData()
            {
                //Position = VP[4],
                Normal = -Vector3.UnitY,
                UV = new Vector2(.5f, 1),
                Color = color
            });
            dataBindings.Add(VP[4], P[5], new VertexData()
            {
                //Position = VP[4],
                Normal = -Vector3.UnitY,
                UV = new Vector2(.5f, 1),
                Color = color
            });
            #endregion
            #region Index5
            dataBindings.Add(VP[5], P[4], new VertexData()
            {
                //Position = VP[5],
                Normal = -Vector3.UnitZ,
                UV = new Vector2(0, 0),
                Color = color
            });
            dataBindings.Add(VP[5], P[5], new VertexData()
            {
                //Position = VP[5],
                Normal = -Vector3.UnitZ,
                UV = new Vector2(1, 0),
                Color = color
            });
            dataBindings.Add(VP[5], P[6], new VertexData()
            {
                //Position = VP[5],
                Normal = -Vector3.UnitZ,
                UV = new Vector2(0, 1),
                Color = color
            });
            dataBindings.Add(VP[5], P[7], new VertexData()
            {
                //Position = VP[5],
                Normal = -Vector3.UnitZ,
                UV = new Vector2(1, 1),
                Color = color
            });
            #endregion
            #endregion//end region for databindings

            //lets try this operation with a single tri
            //oh god this is gonna break down completely isn't it
            //I'll add an edge case for boundary faces in a sec when it collapses
            



            generatedCache.Generate(VP, P, dataBindings);
            generatedCache.PropogateDegree();
            for (int i = 0; i < divisions; i++)
            {
                generatedCache = generatedCache.LoopSubdivison();
            }
            //generatedCache.SplitEdge2(generatedCache.Edges[0]);

           

            foreach(Edge e in generatedCache.Edges)
            {
                if(e.HalfEdge.Edge != e.HalfEdge.twin.Edge)
                {
                    throw new Exception("UnexpectedEdgePairing");
                }
            }


            //generatedCache.LoopSubdivison();
            generatedCache.PropogateDegree();
            
            
            //generatedCache.Sphereize(radius);
            //generatedCache.PropogateNormals();
            //generatedCache.PropogateNormals2();
            //generatedCache.PropogateFaceNormals();


        }

        Model GenerateModel(GraphicsDevice graphics)
        {
            //prep return model
           // Model ret;

            //prep buffers
            IndexBuffer index;// = //new IndexBuffer(GraphicsDevice,IndexElementSize.SixteenBits,0)

            VertexBuffer vertex;

            //pref model data lists
            List<VertexPositionNormalColorTexture> verticies = new List<VertexPositionNormalColorTexture>();
            List<short> indicies = new List<short>();
            

            //lets loop over all our half edges 
            //I guess I meant faces so each face
            foreach(Face f in generatedCache.Faces)
            {
                //go over each half edge
                HalfEdge hIter = f.HalfEdge;
                do
                {
                    //create temp verticy
                    VertexPositionNormalColorTexture edgeVert = new VertexPositionNormalColorTexture(
                      hIter.Vertex.Position, hIter.Data.Normal, hIter.Data.Color, hIter.Data.UV);
                    //VertexPositionNormalColor edgeVert = new VertexPositionNormalColor(hIter.Vertex.Position, hIter.Data.Normal, hIter.Data.Color);
                    //check if vertex exists
                    if(!verticies.Contains(edgeVert))
                    {
                        //if not add the vert to vertex list
                        verticies.Add(edgeVert);

                        //add index
                        indicies.Add((short)verticies.IndexOf(edgeVert));
                    }
                    else
                    {
                        //if exists just add indicie
                        indicies.Add((short)verticies.IndexOf(edgeVert));
                    }

                    hIter = hIter.next;
                } while (hIter != f.HalfEdge);
            }
            //okay so thats faces done lets create our buffers

            index = new IndexBuffer(graphics, typeof(short), indicies.Count, BufferUsage.None);
            index.SetData<short>(indicies.ToArray());
            vertex = new VertexBuffer(graphics, typeof(VertexPositionNormalColorTexture), verticies.Count, BufferUsage.None);
            vertex.SetData(verticies.ToArray());
            BasicEffect partEffect = new BasicEffect(graphics);
            partEffect.VertexColorEnabled = true;
            partEffect.EnableDefaultLighting();

            ModelMeshPart xPart = new ModelMeshPart();
            //{
            //    Effect = partEffect,
            //    IndexBuffer = index,
            //    VertexBuffer = vertex,
            //    NumVertices = verticies.Count,
            //    PrimitiveCount = indicies.Count / 3,
            //    StartIndex = 0,
            //    VertexOffset = 0
            //};
            
            xPart.IndexBuffer = index;
            xPart.VertexBuffer = vertex;
            xPart.NumVertices = verticies.Count;
            xPart.PrimitiveCount = indicies.Count / 3;
            xPart.StartIndex = 0;
            xPart.VertexOffset = 0;

            List<ModelMeshPart> mmeshPartList = new List<ModelMeshPart>()
                    {
                        xPart
                    };

            List<ModelMesh> mmeshList = new List<ModelMesh>()
                {
                    new ModelMesh(graphics,
                   mmeshPartList )
                };
            Model ret = new Model(graphics, new List<ModelBone>(),mmeshList
                
                );
            ret.Meshes[0].MeshParts[0].Effect = partEffect;
            return ret;


            //uh lets get all the verticies I guess

        }

        //public override void Initialize(GraphicsDevice graphicsDevice)
        //{
        //    effect = new BasicEffect(graphicsDevice);
        //    effect.VertexColorEnabled = true;
        //    effect.EnableDefaultLighting();
        //}
    }
    //old primitive 3 poly list
    Polygons.Add(new List<Vector3>()
            {
                vertexList[2].Position,
                vertexList[1].Position,
                vertexList[0].Position
});
            Polygons.Add(new List<Vector3>()
            {
                vertexList[2].Position,
                vertexList[1].Position,
                vertexList[0].Position
            });
            Polygons.Add(new List<Vector3>()
            {
                vertexList[2].Position,
                vertexList[1].Position,
                vertexList[0].Position
            });
            Polygons.Add(new List<Vector3>()
            {
                vertexList[2].Position,
                vertexList[1].Position,
                vertexList[0].Position
            });
            Polygons.Add(new List<Vector3>()
            {
                vertexList[2].Position,
                vertexList[1].Position,
                vertexList[0].Position
            });
            Polygons.Add(new List<Vector3>()
            {
                vertexList[2].Position,
                vertexList[1].Position,
                vertexList[0].Position
            });
            Polygons.Add(new List<Vector3>()
            {
                vertexList[2].Position,
                vertexList[1].Position,
                vertexList[0].Position
            });
}



﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BaseEngine;
using BaseEngine.Components;
using BaseEngine.Rendering;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MemoryLeakEditor
{
    //for now we're just going to recreate the existing functionality we just pulled out of rendering 
    class EditorRenderingModule : IRenderingModule
    {
        public short drawPriority => throw new NotImplementedException();
        Scene gameScene;
        public ICamera Camera { get; set; }
        public GraphicsDevice graphics { get; set; }
        public RenderTargetManager TargetManager { get; set; }
        public List<IModelMesh> meshes = new List<IModelMesh>();
        List<IModelMesh> frameMesh = new List<IModelMesh>();

        public void Draw(GameTime gameTime)
        {
            meshes.Clear();
            frameMesh.Clear();

            foreach (GameObject go in gameScene.gameObjects)
            {
                foreach (Component c in go.components.Values)
                {
                    if (c is IModelMesh)
                    {
                        meshes.Add((IModelMesh)c);
                    }
                }
            }

            BoundingFrustum cameraFrustrum = Camera.CameraFrustrum;

            foreach(var m in meshes)
            {
                foreach(ModelMesh mesh in m.DrawModel.Meshes)
                {
                    if(cameraFrustrum.Intersects( mesh.BoundingSphere))
                    {
                        frameMesh.Add(m);
                        break;
                    }
                }
            }
            foreach(var m in frameMesh)
            {
                foreach(ModelMesh mesh in m.DrawModel.Meshes)
                {
                    foreach(Effect e in mesh.Effects)
                    {
                        var bE = e as BasicEffect;

                        bE.World = m.WorldTransform;
                        bE.View = Camera.View;
                        bE.Projection = Camera.Projection;
                    }
                    mesh.Draw();
                }
                
            }
        }
        
        public void Load(Scene gameScene)
        {
            this.gameScene = gameScene;
            //do I need to do this when all meshes should be added by the editor.
        }
    }
}

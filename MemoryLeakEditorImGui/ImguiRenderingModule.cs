﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImGuiNET;
using BaseEngine.Components;
using BaseEngine.Rendering;
using BaseEngine;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MemoryLeakEditor
{
    public delegate void ImguiFrame();
    public interface IImguiFrame
    {
        ImguiFrame Frame { get; set; }
    }public class ImguiCollectionComponent : Component
    {
        public List<IImguiFrame> Frames = new List<IImguiFrame>();
    }
    public class ImguiComponent : Component, IImguiFrame
    {
        ImguiFrame IImguiFrame.Frame { get; set; }
    }
    class ImguiRenderingModule : IRenderingModule
    {
        ImGuiRenderer renderer { get; set; }

        public short drawPriority { get; set; }
        public ICamera Camera { get; set; }
        public GraphicsDevice graphics { get; set; }
        public RenderTargetManager TargetManager { get; set; }
        public List<IImguiFrame> ImguiComponents = new List<IImguiFrame>();

        public void Draw(GameTime gameTime)
        {
            renderer.BeforeLayout(gameTime);
            foreach(var frame in ImguiComponents)
            {
                frame.Frame.Invoke();
            }
            renderer.AfterLayout();
        }
        
        public void Load(Scene gameScene)
        {
            //not going to worry about this right now
            //the component itself should just manage the whole list.
            //so adding and removing isn't adding an absurd amount of game objects.
            //this should just throw the whole game, honestly
        }
    }
}

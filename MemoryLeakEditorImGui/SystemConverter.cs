﻿using Microsoft.Xna.Framework;
using Num = System.Numerics;

namespace MemoryLeakEditor
{
    /// <summary>
    /// convert from system number formats to monogame formats
    /// </summary>
    class SystemConverter
    {

        public static Vector2 Convert(Num.Vector2 v)
        {
            return new Vector2(v.X, v.Y);
        }
        public static Num.Vector2 Convert(Vector2 v)
        {
            return new Num.Vector2(v.X, v.Y);
        }
        public static Vector3 Convert(Num.Vector3 v)
        {
            return new Vector3(v.X, v.Y, v.Z);
        }
        public static Num.Vector3 Convert(Vector3 v)
        {
            return new Num.Vector3(v.X, v.Y, v.Z);
        }
        public static Vector3 Convert(ref Num.Vector3 v)
        {
            return new Vector3(v.X, v.Y, v.Z);
        }
        public static Num.Vector3 Convert(ref Vector3 v)
        {
            return new Num.Vector3(v.X, v.Y, v.Z);
        }
        public static Num.Vector3 Convert(Euler e)
        {
            return new Num.Vector3(e.Roll, e.Pitch, e.Yaw);
        }
        public static Num.Vector4 Convert(Quaternion q)
        {
            return new Num.Vector4( q.X, q.Y, q.Z,q.W);
        }
        public static Quaternion Convert(Num.Vector4 v)
        {            
            return new Quaternion( v.X, v.Y, v.Z,v.W);            
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MemoryLeakEditor
{
    public struct Line
    {
        public Vector3 A;
        public Vector3 B;
        public Color C;
    }
    public class DebugLineRenderer
    {
        List<Line> lines = new List<Line>();

        List<VertexPositionColor> verticies = new List<VertexPositionColor>();
        List<short> indices = new List<short>();

        

        public void AddLine(Vector3 a, Vector3 b, Color c)
        {
            verticies.Add(new VertexPositionColor(a, c));
            verticies.Add(new VertexPositionColor(b, c));

            indices.Add((short)(verticies.Count - 2));
            indices.Add((short)(verticies.Count - 1));

        }
        public void Clear()
        {
            verticies = new List<VertexPositionColor>();
            indices = new List<short>();
        }
        

        public void Draw(GraphicsDevice g)
        {
            if (verticies.Count >= 2)
            {
                g.DrawUserIndexedPrimitives<VertexPositionColor>(
                    PrimitiveType.LineList,
                    verticies.ToArray(),
                    0,
                    verticies.Count,
                    indices.ToArray(),
                    0,
                    indices.Count / 2);
            }
        }
    }
}

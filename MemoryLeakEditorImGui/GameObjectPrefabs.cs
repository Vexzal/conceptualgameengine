﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using BaseEngine;
using BaseEngine.Components;
using BaseEngine.BepuPhysics;
using MemoryLeakEditor.EnginePrimitives;
using BVector3 = BEPUutilities.Vector3;
using BEPUphysics.CollisionShapes.ConvexShapes;
using BEPUphysics.Entities;
using BEPUphysics.Entities.Prefabs;
using MemoryLeakEditor.ComponentTags;
using ConversionHelper;
using Microsoft.Xna.Framework.Content;
//using BEPUutilities;
using Matrix3x3 = BEPUutilities.Matrix3x3;

namespace MemoryLeakEditor

{
    public struct Asset
    {
        public string Path;
        public AssetTag Tag;
        public Asset(string path, AssetTag tag)
        {
            Path = path;
            Tag = tag;
        }
    }
    public class AssetModule
    {
        ContentManager Content { get; set; }
        List<Asset> Assets = new List<Asset>();//this could probably be an array
        Dictionary<string, Asset> AssetCollection = new Dictionary<string, Asset>();//sooo. 
        //so the content manager stores everything already by default it does the library functionality I already wanted.
        //so the module is defining a specified library
        //so.
        //we can init. 

        public void InitLoad()
        {
            foreach(Asset a in AssetCollection.Values)
            {
                a.Tag.Load(a.Path, Content);//this should load in all the types
            }
        }
        public dynamic this[string name]
        {
            get
            {
                if(AssetCollection.ContainsKey(name))
                {
                    return AssetCollection[name].Tag.Load(AssetCollection[name].Path, Content);
                }
                return null;
            }
        }
        public void Unload()
        {
            Content.Unload();
        }

        //so we need a list of asset names
    }
    public interface AssetTag
    {       
       dynamic Load(string name,ContentManager content);
    }
    public abstract class AssetTagC
    {
        public abstract object Load(string name);
    }
    public class ModelAsset : AssetTag
    {
        dynamic AssetTag.Load(string name, ContentManager content)
        {
            return content.Load<Model>(name);//lets. so.
        }
    }
    public class TextureAsset : AssetTag
    {
        Microsoft.Xna.Framework.Content.ContentManager manager;//what if this is just passed to the thing maybe? hmm.
        public Texture2D Load(string name)
        {
            return manager.Load<object>(name) as Texture2D; 
        }
        //this doesn't work
        dynamic AssetTag.Load(string name,ContentManager content)
        {
            return content.Load<Texture2D>(name);
            //return Load(name);
        }
    }
    public partial class Editor
    {
        public static BVector3 Gravity = new BVector3(0, 0, -9.81f);
        #region Prefabs
        public GameObject AddCube()
        {
            GameObject cubeObject = new GameObject();
            //need a query to test for duplicate names
            cubeObject.GetComponent<TransformComponent>().Tag = new TransformTag();
            
            MeshComponent mesh = new MeshComponent()
            {
                //mesh = convertPrimitive(new CubePrimitive(2, 2, 2, Color.White))
                mesh = CubePrimitive2.PrimitiveModel(new CubePrimitive2(2, 2, 2, Color.White),GraphicsDevice)
            };
            cubeObject.AddComponent(mesh);
           

            EntityComponent physics = new EntityComponent()
            {
                shape = new BoxShape(2, 2, 2),
                mass = 0,
                Offset = Matrix.Identity,
                dynamic = false
            };
            cubeObject.AddComponent(physics);
            cubeObject.Name = ValidateName("Cube", cubeObject);
            cubeObject.Initialize();
            editorScene.AddGameObject(cubeObject);
            //renderer.AddMesh(mesh);
            //base.physics.AddEntity(physics);

            SceneSelectionSpace.Add(GenerateSceneShape(cubeObject));//this should do it? yeah. next up is raycast in the 3d view
            
            return cubeObject;

        }
        
        public GameObject AddSphereDiamond()
        {
            GameObject SphereObject = new GameObject();
            SphereObject.GetComponent<TransformComponent>().Tag = new TransformTag();
            
            MeshComponent mesh = new MeshComponent()
            {
                //mesh = convertPrimitive(new SphereDiamondPrimitive(1, 1, Color.White))
                mesh = SubdivDiamondPrimitive.PrimitiveModel(new SubdivDiamondPrimitive(1, 3, Color.White), GraphicsDevice)
            };

            //BasicEffect e = mesh.mesh.Meshes[0].MeshParts[0].Effect as BasicEffect;
            //e.Texture = Content.Load<Texture2D>(@"DebugTextures/zandraicon");
            //e.TextureEnabled = true;

            //((BasicEffect)(mesh.mesh.Meshes[0].MeshParts[0].Effect)).Texture = Content.Load<Texture2D>(@"DebugTextures/zandra");
            SphereObject.AddComponent(mesh);
            EntityComponent physics = new EntityComponent()
            {
                shape = new SphereShape(1),
                mass = 0,
                Offset = Matrix.Identity,
                dynamic = false
            };
            SphereObject.AddComponent(physics);
            SphereObject.Initialize();
            editorScene.AddGameObject(SphereObject);
            SphereObject.Name = ValidateName("Sphere", SphereObject);
            SceneSelectionSpace.Add(GenerateSceneShape(SphereObject));
            return SphereObject;
        }
        #endregion

        #region Utilities
        Model convertPrimitive<T>(DrawablePrimitive<T> primitive) where T : struct, IVertexType
        {
            primitive.Initialize(GraphicsDevice);
            if (primitive.primitiveType == PrimitiveType.LineList || primitive.primitiveType == PrimitiveType.LineStrip)
            {
                throw new Exception("Primitive must be triangle");
            }
            Model ret;
            //mesh part
            IndexBuffer indexBuffer = new IndexBuffer(GraphicsDevice, typeof(short), primitive.indices.Length, BufferUsage.None);
            indexBuffer.SetData(primitive.indices);
            VertexBuffer vertexBuffer = new VertexBuffer(GraphicsDevice, typeof(T), primitive.vertexCount, BufferUsage.None);
            vertexBuffer.SetData(primitive.vertices);
            ModelMeshPart part = new ModelMeshPart();
            part.IndexBuffer = indexBuffer;
            part.VertexBuffer = vertexBuffer;
            part.NumVertices = primitive.vertexCount;
            part.PrimitiveCount = primitive.primitiveCount;
            //part.Effect = new BasicEffect(GraphicsDevice) { LightingEnabled = true };

            //mesh 
            ModelMesh mesh = new ModelMesh(GraphicsDevice, new List<ModelMeshPart>() { part });

            ret = new Model(GraphicsDevice, new List<ModelBone>(), new List<ModelMesh>() { mesh });
            //ret.Meshes[0].MeshParts[0].Effect = new BasicEffect(GraphicsDevice);
            ret.Meshes[0].MeshParts[0].Effect = primitive.effect;
            return ret;
        }
        //name management
        public string ValidateName(string targetName,GameObject go)
        {
            string targetReturn = targetName;
            int iterate = 0;
            //check list for existing
            bool returnable = false;

            if (editorScene.gameObjects.Count == 0)
                return targetReturn;

            while (!returnable)
            {
                List<string> names = editorScene.gameObjects.Select(o => o.Name).ToList<string>();
                if (names.Contains(targetReturn))
                {
                    if(editorScene.gameObjects[names.IndexOf(targetReturn)] != go)
                    {
                        iterate++;
                        targetReturn = targetName + "." + iterate;
                    }
                    else
                    {
                        return targetReturn;
                    }
                }
                else
                {
                    return targetReturn;
                }                    
            }               
            return targetReturn;
        }

        public SelectableEntity GenerateSceneShape(GameObject go)
        {
            Vector3[] CleanVerticies( Vector3[] list)
            {
                List<Vector3> retV = new List<Vector3>();
                foreach(Vector3 v in list)
                {
                    if (!retV.Contains(v))
                        retV.Add(v);
                }
                return retV.ToArray();
            }
            MeshComponent mesh = go.GetComponent<MeshComponent>();
            TransformComponent transform = go.GetComponent<TransformComponent>();
            // List<BVector3> vectorList = MathConverter.Convert(mesh.vPositionList).ToList(); //new List<BVector3>();//I haven't converted mesh here and lets, okay lets , fuck.
            //ConvexHullShape newShape = new ConvexHullShape(vectorList);     
            ConvexHullShape newShape = new ConvexHullShape(MathConverter.Convert(CleanVerticies(mesh.vPositionList)));
            SelectableEntity ReturnEntity = new SelectableEntity(MathConverter.Convert(transform.Position), newShape, ConvertTo3x3(transform.GetPhysicsTransform()));//now we need, call backs?'
            ReturnEntity.Tag = go; //this will be grabbed when the shape is queued in the raycast.

            //now I need, update callbacks for the other, the mesh update and transform update
            transform.ComponentUpdated += ReturnEntity.EngineTransformCallback;
            mesh.ComponentUpdated += ReturnEntity.EngineMeshCallback;

            return ReturnEntity;
        }

        public class SelectableEntity : TransformableEntity
        {
            ConvexShape _shape;
            BEPUutilities.Matrix _transform;
            public SelectableEntity(BVector3 position, ConvexShape shape, Matrix3x3 transform) : base(position, shape, transform)
            {
                _shape = shape;
                //_transform = transform;
                _transform = new BEPUutilities.Matrix(transform.M11, transform.M12, Transform.M13, 0, Transform.M21, Transform.M22, Transform.M23, 0, Transform.M31, transform.M32, transform.M33, 0, 0, 0, 0, 1);
            }
            public void EngineTransformCallback(ComponentUpdateArguments e)
            {
                _transform = MathConverter.Convert(((TransformComponent)e.Component).GetParentedTransform());
                this.Transform = ConvertTo3x3( _transform);
                this.Position = _transform.Translation;
                
            }
            public void EngineMeshCallback(ComponentUpdateArguments e)
            {
                _shape = new ConvexHullShape(MathConverter.Convert(((MeshComponent)e.Component).vPositionList));
            }
        }


        //implement later
        //this will find the convex hull of a Model.
        public BVector3[] ConvexHull(Model mesh)
        {
            return new BVector3[0];
        }
        //this ones easier to understand,its a list of the
        public BVector3[] ConvexCube(MeshComponent mesh)
        {
            //this one is easier
            //alright we start with 4 corners. 
            float MaxX = float.MinValue;
            float MaxY = float.MinValue;
            float MaxZ = float.MinValue;
            float MinX = float.MaxValue;
            float MinY = float.MaxValue;
            float MinZ = float.MaxValue;

            List<Vector3> positionList = mesh.vPositionList.ToList();
            
            foreach(var position in positionList)
            {
                if(position.X > MaxX)
                {
                    MaxX = position.X;
                }
                else if(position.X<MinX)
                {
                    MinX = position.X;
                }
                if (position.Y > MaxY)
                {
                    MaxY = position.Y;
                }
                else if (position.Y < MinY)
                {
                    MinY = position.Y;
                }
                if (position.Z > MaxZ)
                {
                    MaxZ = position.Z;
                }
                else if (position.Z < MinZ)
                {
                    MinZ = position.Z;
                }
            }

            //generate corners      //+X+Y+Z
            return new BVector3[8] {new BVector3(MaxX,MaxY,MaxZ),
                                    //-X+Y+Z
                                    new BVector3(MinX,MaxY,MaxZ),
                                    //+X-Y+Z
                                    new BVector3(MaxX,MinY,MaxZ),
                                    //-X-Y+Z
                                    new BVector3(MinX,MinY,MaxZ),
                                    //+X+Y-Z
                                    new BVector3(MaxX,MaxY,MinZ),
                                    //-X+Y-Z
                                    new BVector3(MinX,MaxY,MinZ),
                                    //+X-Y-Z
                                    new BVector3(MaxX,MinY,MinZ),
                                    //-X-Y-Z
                                    new BVector3(MinX,MinY,MinZ) };
        }
        public static BEPUutilities.Matrix3x3 ConvertTo3x3(BEPUutilities.Matrix m)
        {
            //var bM = MathConverter.Convert(m);
            return new BEPUutilities.Matrix3x3(m.M11, m.M12, m.M13, m.M21, m.M22, m.M23, m.M31, m.M32, m.M33);
        }
        #endregion

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MemoryLeakEditor
{
    class SceneCamera
    {
        public float xAngel;
        public float zAngel;
        
        Matrix focus;
        Matrix pivot;
        Matrix arm;
        Matrix camera;

        public Matrix View
        {
            get => Matrix.CreateLookAt(
                Vector3.Transform(Vector3.Zero, 
                    Matrix.CreateTranslation(0,-distance,0)* pivot),
                focus.Translation,
                Up);
        }
        
        public Matrix Projection
        {
            get => Matrix.CreatePerspectiveFieldOfView(FOV, aspect, nearClipPlane, farClipPlane);
        }
        //camera settings
        public float distance;
        public Vector3 Up;
        //projection settings
        public float FOV;
        public float nearClipPlane;
        public float farClipPlane;
        public float aspect;
        public SceneCamera(GraphicsDevice device, float distance)
        {
            xAngel = -45;
            zAngel = 45;

            aspect = device.Viewport.AspectRatio;
            FOV = MathHelper.ToRadians(30);
            nearClipPlane = 1;
            farClipPlane = 100;
            this.distance = distance;
            Up = Vector3.UnitZ;

            pivot = Matrix.CreateRotationX(xAngel) * Matrix.CreateRotationZ(zAngel);//Matrix.CreateFromYawPitchRoll(0, MathHelper.ToRadians(xAngel), MathHelper.ToRadians(zAngel));
            arm = Matrix.CreateTranslation(new Vector3(0, -distance, 0));
            camera = Matrix.CreateLookAt(Vector3.UnitY, Vector3.Zero, Up);            
        }
        public void UpdatePivot()
        {
            //pivot = Matrix.CreateFromYawPitchRoll(
            //    0,
            //    MathHelper.ToRadians(xAngel),
            //    MathHelper.ToRadians(zAngel));
            pivot = Matrix.CreateRotationX(MathHelper.ToRadians(xAngel)) 
                * Matrix.CreateRotationZ(MathHelper.ToRadians(zAngel));
        }
        public void UpdateProjection()
        {

        }
        public void UpdateCamera()
        {
            camera = Matrix.CreateLookAt(Vector3.UnitY, Vector3.Zero, Up);
            
        }
        private Vector3 GetCameraZ()
        {
            return Vector3.Normalize(new Vector3(View.M13, View.M23, View.M33));
        }
        private Vector3 GetCameraX()
        {
            return Vector3.Normalize(new Vector3(View.M11, View.M21, View.M31));
        }
        private Vector3 GetCameraY()
        {
            return Vector3.Normalize(new Vector3(View.M12, View.M22, View.M31));
        }
        public BoundingFrustum CameraFrustrum
        {
            get { return new BoundingFrustum(View * Projection); }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImGuiNET;
using BaseEngine;
using BaseEngine.Components;

using BaseEngine.Physics;
using Num = System.Numerics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MemoryLeakEditor.EnginePrimitives;
using MemoryLeakEditor.ComponentTags;
namespace MemoryLeakEditor
{
    public class gameObjectHolder
    {
        public GameObject gameObject;
    }
    public partial class Editor
    {
        private enum AddedPrefab
        {
            Off,
            Cube,
            SubdivSphere,
        }
        bool addPrefab = false;
        List<GameObject> topGameObjects;
        private void DrawSceneList()
        {
            topGameObjects = editorScene.gameObjects.Where(go => go.Parent == null).ToList();
            ImGui.SetNextWindowSize(new Num.Vector2(100, 500), ImGuiCond.FirstUseEver);
            ImGui.SetNextWindowPos(new Num.Vector2(300, 400), ImGuiCond.FirstUseEver);
            ImGui.Begin("SceneList");
            //GameObject selectedObject = activeObject;
            //ImGuiTreeNodeFlags nodeFlags = ImGuiTreeNodeFlags.OpenOnDoubleClick | ImGuiTreeNodeFlags.OpenOnArrow;
            //gameScene.gameObjects.IndexOf((go))
            void DragDropEmpty()
            {                
                gameObjectHolder xdrag = new gameObjectHolder();
                GameObject drag = new GameObject();
                int xy = 13;
                unsafe
                {
                    IntPtr valuePointer = (IntPtr)(&xy);

                    if (ImGui.BeginDragDropSource(ImGuiDragDropFlags.None))
                    {
                        ImGui.SetDragDropPayload("TreeValue", valuePointer, sizeof(int));
                    }   
                }                
            }
            void RecursiveTree(GameObject go)
            {
                
                ImGuiTreeNodeFlags nodeFlags = ImGuiTreeNodeFlags.OpenOnDoubleClick | ImGuiTreeNodeFlags.OpenOnArrow | ImGuiTreeNodeFlags.DefaultOpen;
                if (go.Children.Count == 0)
                    nodeFlags |= ImGuiTreeNodeFlags.Leaf;
                //if (go == activeObject)
                //  nodeFlags |= ImGuiTreeNodeFlags.Selected;                                
                if (selectedObjects.Contains(go))
                    nodeFlags |= ImGuiTreeNodeFlags.Selected;
                bool node_opened = ImGui.TreeNodeEx(go.Name, nodeFlags);
                if (ImGui.IsItemClicked())
                {
                    selectedObjects = new List<GameObject>();
                    selectedObjects.Add(go);
                }
                //    selectedObject = go;
                unsafe
                {
                    if (ImGui.BeginDragDropSource(ImGuiDragDropFlags.None))
                    {
                        var dragIndex = editorScene.gameObjects.IndexOf(go);
                        ImGui.SetDragDropPayload("SceneTreeObject", (IntPtr)(&dragIndex), sizeof(int));
                        ImGui.EndDragDropSource();
                    }

                    if (ImGui.BeginDragDropTarget())
                    {
                        ImGuiPayloadPtr payload = ImGui.AcceptDragDropPayload("SceneTreeObject");
                        if (payload.NativePtr != null)
                        {
                            if (payload.DataSize == sizeof(int))
                            {
                                int* payloadData = (int*)payload.Data;
                                int payload_n = *payloadData;
                                //ClearGameObject(editorScene.gameObjects[payload_n]);
                                //go.AddChild(editorScene.gameObjects[payload_n]);
                                //var payloadTransform = editorScene.gameObjects[payload_n].GetComponent<TransformComponent>();
                                //var payloadTag = (TransformTag)payloadTransform.Tag;
                                //payloadTag.SetWithParent(payloadTransform);
                                EngineSetParent(go, editorScene.gameObjects[payload_n]);
                            }
                            ImGui.EndDragDropTarget();
                        }
                    }                    
                }
                if (node_opened)
                {
                    foreach (GameObject child in go.Children)
                    {
                        RecursiveTree(child);
                    }
                    ImGui.TreePop();
                }
                //if (activeObject != selectedObject)
                //    activeObject = selectedObject;
            }
            ImGuiTreeNodeFlags scene_flags = ImGuiTreeNodeFlags.DefaultOpen;
            if (ImGui.TreeNodeEx("Scene",scene_flags))
            {
                unsafe
                {
                    if (ImGui.BeginDragDropTarget())
                    {
                        ImGuiPayloadPtr payload = ImGui.AcceptDragDropPayload("SceneTreeObject");
                        if (payload.NativePtr != null)
                        {
                            if (payload.DataSize == sizeof(int))
                            {
                                int* payloadData = (int*)payload.Data;
                                int payload_n = *payloadData;
                                ClearGameObject(editorScene.gameObjects[payload_n]);
                                //go.AddChild(editorScene.gameObjects[payload_n]);
                            }
                        }
                        ImGui.EndDragDropTarget();
                    }                    
                }
                foreach (GameObject go in topGameObjects)
                {
                    RecursiveTree(go);
                }                
            }
            if(ImGui.Button("Add Object"))
            {
                //addPrefab = true;
                ImGui.OpenPopup("Prefab_Popup");
                
            }
            //add this to the tree itself too
            if (ImGui.BeginPopup("Prefab_Popup"))
            {
                if (ImGui.Selectable("Cube"))
                {
                    addOject = AddCube();
                    addPrefab = false;
                    addingOperaton = AddedPrefab.Cube;
                }
                if(ImGui.Selectable("Subdiv Sphere"))
                {
                    addOject = AddSphereDiamond();
                    addPrefab = false;
                    addingOperaton = AddedPrefab.SubdivSphere;
                }
                ImGui.EndPopup();
            }
            if(addPrefab)
            {
                
            }
            if(addPrefab)
            {
                //ImGui.OpenPopup("Prefabs");
                ImGui.Begin("Prefabs");
                if (ImGui.Button("Cube"))
                {
                    addOject = AddCube();
                    addPrefab = false;
                    addingOperaton = AddedPrefab.Cube;
                }
                
                ImGui.End();
            }
            if(addingOperaton!= AddedPrefab.Off)
            {
                if (addingOperaton == AddedPrefab.Cube)
                    DrawCubeProeprties();
                if (addingOperaton == AddedPrefab.SubdivSphere)
                {

                }
            }
            ImGui.End();
        }

        //area for adding objects //move to its own section TODO
        AddedPrefab addingOperaton = AddedPrefab.Off;
        GameObject addOject;

        float xLength = 2;
        float yLength = 2;
        float zLength = 2;
        Num.Vector3 cubeColor = Num.Vector3.One;
        private void DrawCubeProeprties()
        {
            ImGui.Begin("Cube Properties");
            var BeforeTextName = addOject.Name;
            ImGui.InputText("Object Name", ref addOject.Name, 10);
            var AfterTextName = addOject.Name;
            
            ImGui.InputFloat("X", ref xLength);
            ImGui.InputFloat("Y", ref yLength);
            ImGui.InputFloat("Z", ref zLength);
            ImGui.ColorPicker3("Color", ref cubeColor);
            if (ImGui.Button("Accept"))
            {
                addingOperaton = AddedPrefab.Off;
                addOject.Name = ValidateName(addOject.Name, addOject);
            }
            ImGui.End();
            //addOject.GetComponent<MeshComponent>().mesh = convertPrimitive(new CubePrimitive(xLength, yLength, zLength, new Color(SystemConverter.Convert(cubeColor))));
            addOject.GetComponent<MeshComponent>().mesh = CubePrimitive2.PrimitiveModel(new CubePrimitive2(xLength, yLength, zLength, new Color(SystemConverter.Convert(cubeColor))),GraphicsDevice);
            //TODO Reorder pHysics, allow shape editing, figure out structure
            //addOject.GetComponent<EntityComponent>()
        }
        private void ClearGameObject(GameObject go)
        {
            if (go.Parent != null)
                go.Parent.RemoveChild(go);

        }

        void EngineSetParent(GameObject Parent, params GameObject[] children)
        {
            foreach (var go in children)
            {
                //ClearGameObject(go);
                Parent.AddChild(go);
                var payloadTransform = go.GetComponent<TransformComponent>();
                var payloadTag = (TransformTag)payloadTransform.Tag;
                payloadTag.SetWithParent(payloadTransform);


                //ClearGameObject(editorScene.gameObjects[payload_n]);
                //go.AddChild(editorScene.gameObjects[payload_n]);
                //var payloadTransform = editorScene.gameObjects[payload_n].GetComponent<TransformComponent>();
                //var payloadTag = (TransformTag)payloadTransform.Tag;
                //payloadTag.SetWithParent(payloadTransform);
            }
        }
    }
}

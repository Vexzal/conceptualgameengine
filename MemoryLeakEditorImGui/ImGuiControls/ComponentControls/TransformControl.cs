﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Num = System.Numerics;
using BaseEngine;
using BaseEngine.Components;
using ImGuiNET;
using SConvert = MemoryLeakEditor.SystemConverter;
using Microsoft.Xna.Framework;
using MemoryLeakEditor.ComponentTags;
namespace MemoryLeakEditor
{
    public partial class Editor
    {        
        Num.Vector3 Rotation; //Euler                
        int sampleSize;
        public void TransformInspector(Component c)
        {
            ImGui.BeginChild("Transform Component");            
            ImGui.Text("      ");
            ImGui.SameLine();
            ImGui.InputFloat3("Transform##value", ref ((TransformTag)c.Tag).position);
            if (ImGui.Button("Apply"))
                SetRotation((TransformTag)c.Tag);
            ImGui.SameLine();
            ImGui.InputFloat3("Set Rotation", ref Rotation);
            ImGui.Text("      ");
            ImGui.SameLine();
            ImGui.InputFloat4("Rotation##value", ref ((TransformTag)c.Tag).qrotation);
            ImGui.Text("      ");
            ImGui.SameLine();
            ImGui.InputFloat3("Scale##value", ref ((TransformTag)c.Tag).scale);
            ImGui.EndChild();
            SetTagTransform(((TransformComponent)c));
        }
        private void SetRotation(TransformTag t)
        {
            t.qrotation = SConvert.Convert(
                Quaternion.CreateFromRotationMatrix(
                    Matrix.CreateRotationX(MathHelper.ToRadians(Rotation.X)) *
                    Matrix.CreateRotationY(MathHelper.ToRadians(Rotation.Y)) *
                    Matrix.CreateRotationZ(MathHelper.ToRadians(Rotation.Z))));
            //t.qrotation = SConvert.Convert(Quaternion.CreateFromYawPitchRoll(Rotation.Z, Rotation.X, Rotation.Y));
           
        }
        private void SetTagTransform(TransformComponent t)
        {
            var tag = ((TransformTag)t.Tag);
            t.Position = SConvert.Convert(tag.position);
            t.Rotation = SConvert.Convert(tag.qrotation);
            t.Scale = SConvert.Convert(tag.scale);

        }        
               
        //ue4 implmentation
        Euler ToEuler(Quaternion q)
        {
            float singularityTest = q.Z * q.X - q.W * q.Y;
            float YawY = 2f * (q.W * q.Z + q.X * q.Y);
            float YawX = (1f - 2f * (q.Y * q.Y + q.Z * q.Z));

            float SINGULARITY_THRESHOLD = 0.4999995f;
            //float SINGULARITY_THRESHOLD = .5f;
            //const float SINGULARITY_THRESHOLD = .998f;
            //Vector3 eulerFromQuat = new Vector3();
            Euler eulerFromQuat = new Euler();
            //pitch = x, yaw is = z, roll  = y
            if (singularityTest < -SINGULARITY_THRESHOLD)
            {
                eulerFromQuat.Pitch = -90f;
                eulerFromQuat.Yaw = MathHelper.ToDegrees((float)Math.Atan2(YawY, YawX));
                eulerFromQuat.Roll = NormalizedAngle(-eulerFromQuat.Yaw - MathHelper.ToDegrees((2f * (float)Math.Atan2(q.X, q.W))));
            }
            else if (singularityTest > SINGULARITY_THRESHOLD)
            {
                eulerFromQuat.Pitch = 90f;
                eulerFromQuat.Yaw = MathHelper.ToDegrees((float)Math.Atan2(YawY, YawX));
                eulerFromQuat.Roll = NormalizedAngle(eulerFromQuat.Yaw - MathHelper.ToDegrees(2f * (float)Math.Atan2(q.X, q.W)));
            }
            else
            {
                eulerFromQuat.Pitch = MathHelper.ToDegrees((float)Math.Asin(2f * singularityTest));
                eulerFromQuat.Yaw = MathHelper.ToDegrees((float)Math.Atan2(YawY, YawX));
                eulerFromQuat.Roll = MathHelper.ToDegrees((float)Math.Atan2(-2f * (q.W * q.X + q.Y * q.Z), (1f - 2f * (q.X * q.X + q.Y * q.Y))));
            }
            return eulerFromQuat;
        }
        //ue4 implementation
        Quaternion ToQuat(Euler euler)
        {
            const float DEG_TO_RAD = (float)(Math.PI / 180f);
            const float DIVIDE_BY_2 = DEG_TO_RAD / 2f;
            float SP, SY, SR;
            float CP, CY, CR;
            SP = (float)Math.Sin(euler.Pitch * DIVIDE_BY_2);
            CP = (float)Math.Cos(euler.Pitch * DIVIDE_BY_2);
            SY = (float)Math.Sin(euler.Yaw * DIVIDE_BY_2);
            CY = (float)Math.Cos(euler.Yaw * DIVIDE_BY_2);
            SR = (float)Math.Sin(euler.Roll * DIVIDE_BY_2);
            CR = (float)Math.Cos(euler.Roll * DIVIDE_BY_2);

            Quaternion rotationQuat = Quaternion.Identity;
            rotationQuat.X = CR * SP * SY - SR * CP * CY;
            rotationQuat.Y = -CR * SP * CY - SR * CP * SY;
            rotationQuat.Z = CR * CP * SY - SR * SP * CY;
            rotationQuat.W = CR * CP * CY + SR * SP * SY;

            return rotationQuat;

        }
        float NormalizedAngle(float angle)
        {
            float ClampAngle(float inAngle)
            {
                inAngle = inAngle % 360f;
                if (inAngle < 0)
                {
                    inAngle += 360;
                }
                return inAngle;
            }

            angle = ClampAngle(angle);
            if (angle > 180)
            {
                angle -= 360f;
            }
            return angle;
        }
        Euler ToEuler(Vector3 axis, float angle)
        {
            Euler ret;
            double s = Math.Sin(angle);
            double c = Math.Cos(angle);
            double t = 1 - c;
            if ((axis.X * axis.Y * t + axis.Z * s) > .998)
            {
                ret.Yaw = MathHelper.ToDegrees(2f * (float)Math.Atan2(axis.X * Math.Sin(angle / 2), Math.Cos(angle / 2)));
                ret.Pitch = 90;
                ret.Roll = 0;
            }
            else if ((axis.X * axis.Y * t + axis.Z * s) < -.998)
            {
                ret.Yaw = -MathHelper.ToDegrees(2f * (float)Math.Atan2(axis.X * Math.Sin(angle / 2), Math.Cos(angle / 2)));
                ret.Pitch = -90;
                ret.Roll = 0;
            }
            else
            {
                ret.Yaw =   MathHelper.ToDegrees((float)Math.Atan2(axis.Y * s - axis.X * axis.Z * t, 1 - (axis.Y * axis.Y + axis.Z * axis.Z) * t));
                ret.Pitch = MathHelper.ToDegrees((float)Math.Asin(axis.X * axis.Y * t + axis.Z * s));
                ret.Roll =  MathHelper.ToDegrees((float)Math.Atan2(axis.X * s - axis.Y * axis.Z * t, 1 - (axis.X * axis.X + axis.Z * axis.Z) * t));
            }
            return ret;
        }
        Vector3 Convert(Euler e)
        {
            return new Vector3(e.Roll, e.Pitch, e.Yaw);
        }
        Euler Convert(Vector3 v)
        {
            return new Euler { Roll = v.X, Pitch = v.Y, Yaw = v.Z };
        }
       
    }
}

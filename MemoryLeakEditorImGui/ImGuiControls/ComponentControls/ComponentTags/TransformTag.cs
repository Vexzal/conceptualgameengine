﻿using Microsoft.Xna.Framework;
using Num = System.Numerics;
using BaseEngine.Components;
using S = MemoryLeakEditor.SystemConverter;

namespace MemoryLeakEditor.ComponentTags
{
    class TransformTag
    {
        public Num.Vector3 position;
        public Num.Vector3 rotation;
        public Num.Vector4 qrotation;
        public Num.Vector3 scale;
        public TransformTag()
        {
            position = Num.Vector3.Zero;
            rotation = Num.Vector3.Zero;
            scale = Num.Vector3.One;
            qrotation = SystemConverter.Convert(Quaternion.Identity);
        }
        public TransformTag Clone()
        {
            return new TransformTag()
            {
                position = this.position,
                rotation = this.rotation,
                qrotation = this.qrotation,
                scale = this.scale
            };
        }
        public static TransformTag operator +(TransformTag a, TransformTag b)
        {
            Quaternion qA = SystemConverter.Convert(a.qrotation);
            Quaternion qB = SystemConverter.Convert(b.qrotation);
            Num.Vector4 result = SystemConverter.Convert(Quaternion.Concatenate(qA, qB));
            return new TransformTag() { position = a.position + b.position, rotation = a.rotation + b.rotation, scale = a.scale + b.scale, qrotation = result}; 
        }

        public void SetWithParent(TransformComponent c)
        {
            position = S.Convert(c.Position);
            qrotation = S.Convert(c.Rotation);
            scale = S.Convert(c.Scale);
        }
        
        //okay so I've got to out this somehow so funnnn times
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using ImGuiNET;
using BaseEngine;
using BaseEngine.Components;

namespace MemoryLeakEditor
{
    public partial class Editor
    {
        bool newSelection = false;
        //GameObject activeObject = null;//moving this to scene view main, as it really belongs there
        delegate void ComponentInspector(Component component);
        Dictionary<Type, ComponentInspector> componentInspectors = new Dictionary<Type, ComponentInspector>();

        private void GameObjectInspector()
        {
            ImGuiWindowFlags inspectorFlags = ImGuiWindowFlags.AlwaysAutoResize;
            ImGui.Begin(selectedObjects[0].Name + "##inspector");
            
            foreach (Type component in selectedObjects[0].components.Keys)
            {
                if (componentInspectors.Keys.Contains(component))
                    componentInspectors[component](selectedObjects[0].components[component]);
            }
            ImGui.End();
        }
    }
}

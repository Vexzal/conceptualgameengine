﻿using System;
using System.Collections.Generic;
using ImGuiNET;
using Num = System.Numerics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using MemoryLeakEditor.ComponentTags;
using BaseEngine;
using BaseEngine.Components;
using Sys = MemoryLeakEditor.SystemConverter;
using BRay = BEPUutilities.Ray;
using ConversionHelper;
using BEPUphysics;

using BEPUphysics.BroadPhaseEntries.MobileCollidables;

namespace MemoryLeakEditor
{


    [Flags]
    public enum AxisLock
    {
        None = 1<<0,
        X = 1<<1,
        Y = 1<<2,
        Z = 1<<3,
    }
    [Flags]
    public enum EditorCommands
    {
        None = 1,
        Transform = 1<<1,
        Rotating = 1<<2,
        Moving = 1<<3,
        Scaling = 1<<4,
        Snapping = 1<<5
    }
    public partial class Editor2
    {
        #region Properties
        Model cursor;
        Num.Vector2 Editor3dPosition = new Num.Vector2(0,0);
        Vector2 EditorScaleFactor = new Vector2(1,1);

        Vector3 EditorMouseOld;
        Vector3 EditorMouseOperation;
        Vector3 EditorMousePosition;

        Vector3 EditorCursorPosition;
        Vector3 EditorCursorDirection;

        int oldScrollState = 0;
        int newScrollState = 0;
        float scrollDelta;
        float scrollSensitivity = .01f;
        private float xDelta;
        private float yDelta;
        Num.Vector2 oldPos;
        Num.Vector2 barOffset = new Num.Vector2(20, 40);
        Num.Vector2 oldSize = new Num.Vector2(520, 440);
        
        //bool rotating;
        //bool moving;
        AxisLock axisLock = AxisLock.None;
        EditorCommands activeCommand = EditorCommands.None;
        float oldAngle;
        Num.Vector2 rotDelta;
        Num.Vector2 windowCenter;

        List<float> succesiveAngles = new List<float>();

        #endregion

        protected void SceneDraw()
        {            
            oldstate = state;
            state = Keyboard.GetState();
            EditorMouseOld = EditorMousePosition;
            EditorMousePosition = EditorCursorPosition;
            ScenePropertiesWindow();

            ImGui.SetNextWindowPos(new Num.Vector2(0, 0), ImGuiCond.FirstUseEver);
            ImGui.SetNextWindowSize(new Num.Vector2(sceneRender.Width, sceneRender.Height) /**+ barOffset*/, ImGuiCond.Appearing);
            ImGui.Begin("3D editor", ImGuiWindowFlags.NoTitleBar | ImGuiWindowFlags.NoScrollbar | ImGuiWindowFlags.NoCollapse);
            ImGui.Image(imGuiScene, new Num.Vector2(sceneRender.Width, sceneRender.Height), Num.Vector2.Zero, Num.Vector2.One);

            oldScrollState = newScrollState;
            newScrollState = Mouse.GetState().ScrollWheelValue;
            scrollDelta = newScrollState - oldScrollState;
            if (scrollDelta != 0)
                sceneCamera.distance -= scrollDelta * scrollSensitivity;
             
            if(keyPress(Keys.F1))
            {
                sceneCamera.xAngel = 0;
                sceneCamera.zAngel = 0;
                sceneCamera.UpdatePivot();
            }            
            if(keyPress(Keys.F2))
            {
                sceneCamera.xAngel = 0;
                sceneCamera.zAngel = 90;
                sceneCamera.UpdatePivot();
            }
            if(keyPress(Keys.F4))
            {
                sceneCamera.xAngel = -45;
                sceneCamera.zAngel = 45;
                sceneCamera.UpdatePivot();
            }
            if((activeCommand & EditorCommands.Transform) == EditorCommands.Transform && keyPress(Keys.X))
            {
                if ((axisLock & AxisLock.X) == AxisLock.X)
                {
                    axisLock = AxisLock.None;
                }
                else
                {
                    axisLock = AxisLock.X;
                    if (Keyboard.GetState().IsKeyDown(Keys.LeftShift))
                    {
                        axisLock = AxisLock.Y | AxisLock.Z;
                    }
                }
            }
            if ((activeCommand & EditorCommands.Transform) == EditorCommands.Transform && keyPress(Keys.Y))
            {
                if ((axisLock & AxisLock.Y) == AxisLock.Y)
                {
                    axisLock = AxisLock.None;
                }
                else
                {
                    axisLock = AxisLock.Y;
                    if (Keyboard.GetState().IsKeyDown(Keys.LeftShift))
                    {
                        axisLock = AxisLock.X | AxisLock.Z;
                    }
                }
            }
            if ((activeCommand & EditorCommands.Transform) == EditorCommands.Transform && keyPress(Keys.Z))
            {
                if ((axisLock & AxisLock.Z) == AxisLock.Z)
                {
                    axisLock = AxisLock.None;
                }
                else
                {
                    axisLock = AxisLock.Z;
                    if (Keyboard.GetState().IsKeyDown(Keys.LeftShift))
                    {
                        axisLock = AxisLock.X | AxisLock.Y;
                    }
                }
            }            
            if((activeCommand & EditorCommands.Transform) != EditorCommands.Transform && Keyboard.GetState().IsKeyDown(Keys.G))
            {
                
                if(activeObject!=null)
                {
                    if (Keyboard.GetState().IsKeyDown(Keys.LeftAlt) || Keyboard.GetState().IsKeyDown(Keys.RightAlt))
                    {
                        ((TransformTag)(activeObject.GetComponent<TransformComponent>().Tag)).position = new Num.Vector3(0, 0, 0);
                    }
                    else
                    {
                        activeCommand |= EditorCommands.Moving | EditorCommands.Transform;
                        //moving = true;
                        oldPos = ImGui.GetMousePos();//old mouse function
                                                     //oldPos = new Num.Vector2(EditorCursorPosition.X, EditorCursorPosition.Y);//using cursor normalized position
                        EditorMouseOperation = EditorCursorPosition;
                        var xTag = (TransformTag)activeObject.GetComponent<TransformComponent>().Tag;
                        originalTag = new TransformTag();
                        originalTag.qrotation = xTag.qrotation;
                        originalTag.position = xTag.position;
                        originalTag.scale = xTag.scale;
                        editTag = new TransformTag();
                        //windowCenter = ImGui.GetWindowPos() + (ImGui.GetWindowSize() / 2);
                    }
                }
            }          
            if ((activeCommand & EditorCommands.Moving) == EditorCommands.Moving)
            {
                var alignedPosition = ImGui.GetMousePos();                
                var xnaPos = Mouse.GetState().Position;                
                var originalUnprojected = Vector3.Transform(new Vector3(oldPos.X,          oldPos.Y,          sceneCamera.distance), Matrix.Invert(sceneCamera.Projection));
                var unprojectedPosition = Vector3.Transform(new Vector3(alignedPosition.X, alignedPosition.Y, sceneCamera.distance), Matrix.Invert(sceneCamera.Projection));
                var upDelta = unprojectedPosition - originalUnprojected;
                float scaling = (ImGui.GetWindowSize().X / graphics.PreferredBackBufferWidth) / (50 - sceneCamera.distance);
                upDelta /= 10;
                
                ImGui.SetNextWindowPos(new Num.Vector2(100, 200));
                ImGui.SetNextWindowSize(new Num.Vector2(400, 100));
                ImGui.Begin("MoveMentDebug");
                var debugValue = Sys.Convert(unprojectedPosition);
                var debugDelta = Sys.Convert(upDelta);
                ImGui.InputFloat3("Unprojected Aligned Mouse", ref debugValue);
                ImGui.InputFloat3("Unprojected Delta", ref debugDelta);
                ImGui.End();

                var activePosition = ImGui.GetMousePos();
                var deltaPosition = activePosition - oldPos;

                var InverseProjection = Matrix.Invert(sceneCamera.Projection);
                float positionFactor = ImGui.GetWindowSize().X / (float)graphics.PreferredBackBufferWidth;
                var InverseViewProjection = Matrix.Invert(sceneCamera.View * sceneCamera.Projection);
                var inverseOriginal = Vector3.Transform(new Vector3(oldPos.X, oldPos.Y, 1), InverseViewProjection);
                var inverseActive = Vector3.Transform(new Vector3(alignedPosition.X, alignedPosition.Y, 1), InverseViewProjection);

                var factorOriginal = Vector3.Transform(new Vector3(oldPos.X * positionFactor, oldPos.Y * positionFactor, 1), InverseProjection);
                var factoredAligned = Vector3.Transform(new Vector3(alignedPosition.X * positionFactor, alignedPosition.Y * positionFactor, 1), InverseProjection);
                var factorDelta = factoredAligned - factorOriginal;
                ImGui.SetNextWindowPos(new Num.Vector2(0, 500));
                ImGui.SetNextWindowSize(new Num.Vector2(400, 100));
                ImGui.Begin("World Aligned Debug");
                //var debugActive = new Num.Vector3(inverseActive.X, inverseActive.Y, inverseActive.Z);
                //ImGui.InputFloat3("Active Position", ref debugActive);
                var debugFactored = new Num.Vector3(factoredAligned.X, factoredAligned.Y, factoredAligned.Z);
                ImGui.InputFloat3("Factored Position", ref debugFactored);
                var debugOriginal = new Num.Vector3(factorOriginal.X, factorOriginal.Y, factorOriginal.Z);
                ImGui.InputFloat3("Factored Original", ref debugOriginal);
                var debugFDelta = new Num.Vector2(factorDelta.X, factorDelta.Y);
                ImGui.InputFloat2("Factored delta", ref debugFDelta);
                ImGui.End();

                //lets just go from scratch here
                var currentPosition = new Num.Vector2(EditorCursorPosition.X, EditorCursorPosition.Y);

                

                var xValue = EditorMousePosition - EditorMouseOperation;

                if (factorDelta != Vector3.Zero)
                {
                    if (Keyboard.GetState().IsKeyDown(Keys.LeftShift))                        
                        ViewMove(upDelta.X,upDelta.Y, scaling * .1f);
                    else                        
                        ViewMove(factorDelta.X,factorDelta.Y, 1f/10);
                }
                //alright lets do some extra stuff here// 
                //so I wanna just go straigt to world space
                //so invert world projection
                


            }
            if((activeCommand & EditorCommands.Transform) != EditorCommands.Transform && Keyboard.GetState().IsKeyDown(Keys.R))
            {
                if (activeObject != null)
                {
                    
                    activeCommand |= EditorCommands.Rotating | EditorCommands.Transform;
                    
                    //rotating = true;
                    oldPos = ImGui.GetMousePos(); //get mouse on screen
                    

                    var xTag= (TransformTag)activeObject.GetComponent<TransformComponent>().Tag;
                    originalTag = new TransformTag();
                    originalTag.qrotation = xTag.qrotation;
                    originalTag.position = xTag.position;
                    originalTag.scale = xTag.scale;
                    editTag = new TransformTag();
                    windowCenter = ImGui.GetWindowPos() + (ImGui.GetWindowSize() / 2);
                    var xPos = Num.Vector2.Normalize(oldPos - windowCenter);
                    //oldAngle = (float)Math.Acos(xPos.Y);
                    //oldAngle = Math.Abs(oldAngle);
                    oldAngle = (float)Math.Atan2(xPos.X, xPos.Y);
                    succesiveAngles.Add(0);
                    
                }
            }
            var enumVal = activeCommand & EditorCommands.Rotating;
            if ((activeCommand & EditorCommands.Rotating) == EditorCommands.Rotating)
            {             
                var activePosition = ImGui.GetMousePos();
                ImGui.GetOverlayDrawList().AddLine(windowCenter, activePosition, ImGui.GetColorU32(ImGuiCol.Button));
                
                var deltaPosition = Num.Vector2.Normalize((activePosition - windowCenter));
                //float newAngle = (float)Math.Acos(deltaPosition.Y);
                //newAngle = Math.Abs(newAngle);
                float newAngle = (float)Math.Atan2(deltaPosition.X, deltaPosition.Y);
                float deltaAngle = newAngle - oldAngle;
                
                if (deltaAngle != 0)
                {
                    succesiveAngles.Add(deltaAngle);
                    fullAngle += deltaAngle;

                    ViewRotate(fullAngle);
                }
                oldAngle = newAngle;
                //ViewRotate(0);
                ImGui.SetNextWindowPos(new Num.Vector2(100, 200));
                ImGui.SetNextWindowSize(new Num.Vector2(400, 300));
                ImGui.Begin("RotationDebug");

                float[] angleArray = succesiveAngles.ToArray();
                var index = 0;
                if (angleArray.Length > 20)
                    index = angleArray.Length - 20;

                ImGui.InputFloat2("MouseNormalizedToWidgetCenter", ref deltaPosition);
                ImGui.InputFloat("deltaAngle", ref deltaAngle);
                
                ImGui.PlotLines("AngleOverTime", ref angleArray[index], Math.Min(angleArray.Length, 20));
                ImGui.End();                
                
            }
            if((activeCommand & EditorCommands.Transform) != EditorCommands.Transform && Keyboard.GetState().IsKeyDown(Keys.S))
            {
                if(activeObject != null)
                {
                    activeCommand |= EditorCommands.Scaling | EditorCommands.Transform;
                    oldPos = ImGui.GetMousePos();
                    var xTag = (TransformTag)activeObject.GetComponent<TransformComponent>().Tag;
                    originalTag = new TransformTag();
                    originalTag.qrotation = xTag.qrotation;
                    originalTag.position = xTag.position;
                    originalTag.scale = xTag.scale;
                    editTag = new TransformTag(); 
                }
            }
            if((activeCommand & EditorCommands.Scaling) == EditorCommands.Scaling)
            {

                var activePosition = ImGui.GetMousePos();
                var deltaPosition = activePosition - oldPos;
                float delta = deltaPosition.Length();
                delta = (deltaPosition.X + -deltaPosition.Y) / 2;
                
                if(delta != 0)
                {
                    ViewScale(delta * .01f);
                }
            }
            if (ImGui.IsMouseClicked(2))
            {
                oldPos = ImGui.GetMousePos();                                    
            }
            if(ImGui.IsMouseClicked(0))
            {
                
                if ((activeCommand & EditorCommands.None) == EditorCommands.None)
                {
                    BRay selectRay = new BRay(
                        MathConverter.Convert(EditorCursorPosition),
                        MathConverter.Convert(EditorCursorDirection));
                    RayCastResult rayResult;
                    SceneSelectionSpace.RayCast(selectRay, out rayResult
                        );

                    if(rayResult.HitObject!=null)
                    {
                        activeObject = (GameObject)((EntityCollidable)rayResult.HitObject).Entity.Tag;
                    }
                }

                if ((activeCommand & EditorCommands.Rotating) == EditorCommands.Rotating)
                {
                    activeCommand |= ~EditorCommands.Rotating | ~EditorCommands.Transform;
                    //rotating = !rotating;
                    activeCommand = EditorCommands.None;
                    Rotate();
                }
                if((activeCommand & EditorCommands.Moving) == EditorCommands.Moving)
                {
                    activeCommand |= ~EditorCommands.Moving | ~EditorCommands.Transform;
                    activeCommand = EditorCommands.None;
                }
                if((activeCommand & EditorCommands.Scaling) == EditorCommands.Scaling)
                {
                    activeCommand = EditorCommands.None;
                }
                axisLock = AxisLock.None;
            }
            if(ImGui.IsMouseClicked(1))
            {
                if((activeCommand & EditorCommands.Transform) == EditorCommands.Transform)
                {
                    ClearOperation();
                    activeCommand = EditorCommands.None;
                    axisLock = AxisLock.None;
                }
            }
            if (ImGui.IsMouseDown(2))
            {
                if (Keyboard.GetState().IsKeyDown(Keys.LeftShift))
                {
                    var delta = EditorMousePosition - EditorMouseOld;
                    Vector3 xDirection = sceneCamera.GetCameraX() * delta.X * .05f;
                    Vector3 zDirection = sceneCamera.GetCameraZ() * delta.Y * .05f;
                    sceneCamera.FocusPoint += delta; //+ zDirection;
                }
                else
                {


                    var delta = ImGui.GetMousePos() - oldPos;

                    sceneCamera.zAngel -= delta.X;// * cameraSensitivity;
                    sceneCamera.xAngel -= delta.Y;// * cameraSensitivity;
                    sceneCamera.UpdatePivot();
                    Mouse.SetPosition((int)oldPos.X, (int)oldPos.Y);
                }
            }
            //oldPos = ImGui.GetMousePos();
            if (ImGui.GetWindowSize() != oldSize)
                AdjustRender(ImGui.GetWindowSize() /*- barOffset*/);
            oldSize = ImGui.GetWindowSize();
            Editor3dPosition = ImGui.GetWindowPos();
            var windowsize = ImGui.GetWindowSize();
            EditorScaleFactor = new Vector2(
                windowsize.X / graphics.PreferredBackBufferWidth,
                windowsize.Y / graphics.PreferredBackBufferHeight);
            ImGui.End();

        }

        float cameraSensitivity = .1f;
        
        protected void ScenePropertiesWindow()
        {            
            ImGui.Begin("ViewportProperties");
            ImGui.SliderFloat("Camera Sensitivity", ref cameraSensitivity, 0, 100);
            ImGui.SliderFloat("Camera Zoom", ref sceneCamera.distance, 0, 1000);
            ImGui.End();
        }

        TransformTag oldFrame;
        //rotation operation
        float fullAngle = 0;
        TransformTag originalTag;
        TransformTag editTag;
        /// <summary>
        /// takes in the delta angle per frame. 
        /// Needs old mouse posiiton, new mouse position, the normalized vector of the mouse position frm the center of the control
        /// </summary>
        /// <param name="angle"></param>
        /// 
        #region Operations
        public void ViewRotate(float angle)//this is going to need more complicated input
        {
            var Angle = MathHelper.ToDegrees(angle);
            //Matrix rotationX = Matrix.CreateFromAxisAngle(sceneCamera.GetCameraX(), MathHelper.ToRadians(rotDelta.Y));
            //Matrix rotationZ = Matrix.CreateFromAxisAngle(sceneCamera.GetCameraZ(), MathHelper.ToRadians(rotDelta.X));
            //Quaternion xRot = Quaternion.CreateFromAxisAngle(sceneCamera.GetCameraX(), MathHelper.ToRadians(delta.Y));
            //Quaternion yRot = Quaternion.CreateFromAxisAngle(sceneCamera.GetCameraY(), MathHelper.ToRadians(delta.X));
            //Quaternion jointRot = Quaternion.Concatenate(xRot, yRot);
            Matrix Local = Matrix.CreateRotationX(originalTag.rotation.X) * Matrix.CreateRotationY(originalTag.rotation.Y) * Matrix.CreateRotationZ(originalTag.rotation.Z);

            var axis = sceneCamera.GetCameraZ();
            switch(axisLock)
            {
                case AxisLock.X:
                    axis = Vector3.UnitX;
                    break;
                case AxisLock.Y:
                    axis = -Vector3.UnitY;
                    break;
                case AxisLock.Z:
                    axis = Vector3.UnitZ;
                    break;
                case AxisLock.X | AxisLock.Y:
                    axis = Vector3.UnitZ;
                    break;
                case AxisLock.Y | AxisLock.Z:
                    axis = Vector3.UnitX;
                    break;
                case AxisLock.Z | AxisLock.X:
                    axis = -Vector3.UnitY;
                    break;
            }
            //axis = Vector3.Normalize(new Vector3(Local.M31, Local.M32, Local.M33)); 
            Quaternion yRot = Quaternion.CreateFromAxisAngle(axis, angle);
            editTag.qrotation = Sys.Convert(yRot);
            editTag.rotation = Sys.Convert(ToEuler(yRot));
            //editTag.rotation = Sys.Convert(ToEuler(axis, angle));
            //editTag.rotation.X = axis.X * Angle;
            //editTag.rotation.Y = axis.Y * Angle;
            //editTag.rotation.Z = axis.Z * Angle;
            editTag.scale = Num.Vector3.Zero;            
            activeObject.GetComponent<TransformComponent>().Tag = originalTag + editTag;            
        }

        Plane XYPlane = new Plane(Vector3.Zero, Vector3.UnitX, Vector3.UnitY);
        Plane YZPlane = new Plane(Vector3.Zero, Vector3.UnitY, Vector3.UnitZ);
        Plane ZXPlane = new Plane(Vector3.Zero, Vector3.UnitZ, Vector3.UnitX);
        public void ViewMove(float deltaX, float deltaY,float sensitivity)
        {
           // PlaneAlign(deltaX,deltaY);
            var screenXAxis = Vector3.Normalize(sceneCamera.GetCameraX());
            var screenYAxis = Vector3.Normalize(sceneCamera.GetCameraY());

            Vector3 LockAxisX = Vector3.Zero;
            Vector3 LockAxisY = Vector3.Zero;

            /** if((axisLock & AxisLock.None) == AxisLock.None )
            {
                LockAxisX = Vector3.Normalize(sceneCamera.GetCameraX());
                LockAxisY = Vector3.Normalize(sceneCamera.GetCameraY());
            }
            if((axisLock & AxisLock.X) == AxisLock.X)
            {
                LockAxisX = Vector3.UnitX;
            }
            if((axisLock & AxisLock.Y) == AxisLock.Y)
            {
                if(LockAxisX != Vector3.Zero)
                {
                    LockAxisY = Vector3.UnitY;
                }
                else
                {
                    LockAxisX = Vector3.UnitY;
                }
            }
            if((axisLock & AxisLock.Z) == AxisLock.Z)
            {
                if(LockAxisX != Vector3.Zero)
                {
                    LockAxisY = Vector3.UnitZ;
                }
                else
                {
                    LockAxisX = Vector3.UnitZ;
                }
            }

            if((axisLock & AxisLock.X) == AxisLock.X)
            {
                editTag.position = Sys.Convert(LockAxisX * deltaX * sensitivity + LockAxisY * -deltaY * sensitivity);
                var stop = "stop";
            } */

            ImGui.SetNextWindowPos(new Num.Vector2(200, 0));
            ImGui.Begin("Dot Product", ImGuiWindowFlags.AlwaysAutoResize);
            var debugDot = Vector3.Dot(Vector3.UnitX, sceneCamera.GetCameraX());
            ImGui.InputFloat("Camera x unit x dot product", ref debugDot);
            var debugYdot = Vector3.Dot(Vector3.UnitY, sceneCamera.GetCameraX());
            
            ImGui.End();

            switch(axisLock)
            {
                case AxisLock.X:
                    //editTag.position = Sys.Convert(Vector3.UnitX * (deltaX > deltaY ? deltaX : 0) + Vector3.UnitX * (deltaY > deltaX ? deltaY : 0));
                    //editTag.position = Sys.Convert(Vector3.UnitX * deltaX );   
                    editTag.position = PlaneAlign2(deltaX, deltaY, XYPlane);
                    editTag.position.Y = 0;
                    break;
                case AxisLock.Y:
                    //editTag.position = Sys.Convert(Vector3.UnitY * (deltaX > deltaY ? deltaX : 0) + Vector3.UnitY * (deltaY > deltaX ? deltaY : 0));
                    //editTag.position = Sys.Convert(Vector3.UnitY * deltaX);        
                    editTag.position = PlaneAlign2(deltaX, deltaY, YZPlane);
                    editTag.position.Z = 0;
                    break;
                case AxisLock.Z:
                    //editTag.position = Sys.Convert(Vector3.UnitZ * deltaX + Vector3.UnitZ * deltaY);
                    //editTag.position = Sys.Convert(Vector3.UnitZ * deltaX);
                    editTag.position = PlaneAlign2(deltaX, deltaY, ZXPlane);
                    editTag.position.X = 0;
                    break;
                case (AxisLock.X | AxisLock.Y):
                    editTag.position = PlaneAlign2(deltaX, deltaY, XYPlane);
                    break;                
                case (AxisLock.Y | AxisLock.Z):
                    editTag.position = PlaneAlign2(deltaX, deltaY, YZPlane);
                    break;
                case (AxisLock.Z | AxisLock.X):
                    editTag.position = PlaneAlign2(deltaX, deltaY, ZXPlane);
                    break;
                default:
                    editTag.position = PlaneAlign2(deltaX, deltaY, new Plane(Vector3.Zero, sceneCamera.GetCameraX(),sceneCamera.GetCameraY()));
                    break;

            }

            //editTag.position = Sys.Convert(screenXAxis * deltaX * sensitivity + screenYAxis * -deltaY * sensitivity);
            //editTag.position = Sys.Convert(LockAxisX * deltaX * sensitivity + LockAxisY * -deltaY * sensitivity);
            //PlaneAlign(deltaX, deltaY);
            editTag.scale = Num.Vector3.Zero;

            activeObject.GetComponent<TransformComponent>().Tag = originalTag + editTag;


        }
        public void ViewScale(float delta)
        {
            editTag.scale = Num.Vector3.Zero;
            switch(axisLock)
            {
                case AxisLock.X:
                    editTag.scale.X = delta;
                    break;
                case AxisLock.Y:
                    editTag.scale.Y = delta;
                    break;
                case AxisLock.Z:
                    editTag.scale.Z = delta;
                    break;
                case AxisLock.X | AxisLock.Y:
                    editTag.scale.X = delta;
                    editTag.scale.Y = delta;
                    break;
                case AxisLock.Y | AxisLock.Z:
                    editTag.scale.Y = delta;
                    editTag.scale.Z = delta;
                    break;
                case AxisLock.Z | AxisLock.X:
                    editTag.scale.Z = delta;
                    editTag.scale.X = delta;
                    break;
                default:
                    editTag.scale.X = delta;
                    editTag.scale.Y = delta;
                    editTag.scale.Z = delta;
                    break;
            }

            activeObject.GetComponent<TransformComponent>().Tag = originalTag + editTag;
        }
        public void ClearOperation()
        {
            activeObject.GetComponent<TransformComponent>().Tag = originalTag;
        }
        public void PlaneOrderingTest()
        {
            var xyNormal = new Plane(Vector3.Zero, Vector3.UnitX, Vector3.UnitY).Normal;
            var xzNormal = new Plane(Vector3.Zero, Vector3.UnitX, Vector3.UnitZ).Normal;
            var yzNormal = new Plane(Vector3.Zero, Vector3.UnitY, Vector3.UnitZ).Normal;

            var stop = "stop";
        }
        public void Rotate()
        {

        }
        //so lets try constructing a plane from the axis and then we'll cast the mouse to it and see what we get
        public Num.Vector3 PlaneAlign2(float deltaX, float deltaY, Plane alignPlane)
        {
            //so this iteration is aligning a position with the camera and then flattening that to the plane
            //either scale down or use raycast or see if you can just...remove the locked component
            //camerapoint
            var activePoint = sceneCamera.GetCameraX() * deltaX + sceneCamera.GetCameraY() * -deltaY; //correct with testing
            //start with ray
            
            Ray rayUp = new Ray(activePoint, alignPlane.Normal);
            Ray rayDown = new Ray(activePoint, -alignPlane.Normal);

            float? intersectUp = rayUp.Intersects(alignPlane);
            float? intersectDown = rayDown.Intersects(alignPlane);

            return Sys.Convert(activePoint + alignPlane.Normal * ((intersectUp != null) ? (float)intersectUp : -(float)intersectDown));
            
        }
        public Num.Vector3 PlaneAlign(float deltaX, float deltaY, Plane alignPlane)
        {
           
            //test
            //Plane alignPlane = new Plane(Vector3.Zero, Vector3.UnitX, Vector3.UnitZ);
            //Plane alignPlane = new Plane(Vector3.UnitZ, 0);
            //Ray pointRay = new Ray(sceneCamera.GetCameraPosition(), -sceneCamera.GetCameraZ());
            //float? rayIntersect = pointRay.Intersects(alignPlane);
            //var stop = "stop";

            //implement
            //first find unit per pixel
            //haven't tested this so stop on first pass
            var zeroPoint = Vector3.Transform(Vector3.Zero, sceneCamera.View * sceneCamera.Projection);
            var unitPoint = Vector3.Transform(Vector3.Normalize(Vector3.One), sceneCamera.View * sceneCamera.Projection);
            //var unitPoint = Vector3.Transform(Vector3.UnitX, sceneCamera.View * sceneCamera.Projection);
            var unitDelta =  unitPoint - zeroPoint;

            //use this to range the delta
            var DeltaX = deltaX / unitDelta.X;
            var DeltaY = deltaY / unitDelta.Y;

            //so lets just assume we're on the xy plane
            //Plane alignPlane = new Plane(Vector3.Zero, Vector3.UnitX, Vector3.UnitY);
            //Plane alignPlane = new Plane(Vector3.Zero, sceneCamera.GetCameraX(), sceneCamera.GetCameraY());
            
            

            var activeCameraPoint = sceneCamera.GetCameraPosition() + (sceneCamera.GetCameraX() * deltaX) + (sceneCamera.GetCameraY() * -deltaY); 
            Ray pointRayForward = new Ray(
                activeCameraPoint, 
                -sceneCamera.GetCameraZ());
            Ray pointRayBackward = new Ray(
                activeCameraPoint,
                sceneCamera.GetCameraZ());

            float? intersectForward = pointRayForward.Intersects(alignPlane);
            float? intersectBackward = pointRayBackward.Intersects(alignPlane);
            //ImGui.SetNextWindowSize(new Num.Vector2(400,100));
            ImGui.Begin("PointRay Debug",ImGuiWindowFlags.AlwaysAutoResize);
            var debugUnitDelta = Sys.Convert(unitDelta);
            ImGui.InputFloat3("Pixels Per Unit Debug",ref debugUnitDelta);
            float debugIntersect = (intersectForward != null) ? (float)intersectForward : -(float)intersectBackward;
            ImGui.InputFloat("DebugFloatPoint", ref debugIntersect);
            var cameraPosition = Sys.Convert(sceneCamera.GetCameraPosition());
            ImGui.InputFloat3("Camera Position", ref cameraPosition);
            var cameraAdjusted = Sys.Convert(activeCameraPoint);
            ImGui.InputFloat3("Active Point", ref cameraAdjusted);
            var targetedPosition = Sys.Convert(activeCameraPoint + (sceneCamera.GetCameraZ() * ((intersectForward != null) ? (float)intersectForward : -(float)intersectBackward)));
            ImGui.InputFloat3("Targeted Position", ref targetedPosition);
            ImGui.End();

            return Sys.Convert(activeCameraPoint + (sceneCamera.GetCameraZ() * ((intersectForward != null) ? -(float)intersectForward : (float)intersectBackward)));

        }
        #endregion
        KeyboardState state;
        KeyboardState oldstate;
        public bool keyPress(Keys key)
        {            
            return oldstate.IsKeyUp(key) && state.IsKeyDown(key);
        }
        //public Vector3 ViewportUnproject(
        //    Vector3 source, 
        //    Matrix projection, 
        //    Matrix view,
        //    Matrix world,
        //    int width, 
        //    int height)
        //{
        //    Matrix matrix = Matrix.Invert(Matrix.Multiply(Matrix.Multiply(world, view), projection));
        //    source.X = 
        //}

    }
}

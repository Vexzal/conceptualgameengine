﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MemoryLeakEditor.ComponentTags;

using BaseEngine;
using BaseEngine.Input;
using BaseEngine.Components;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

//using BEPUutilities;
using BEPUphysics;
using BRay = BEPUutilities.Ray;
using ConversionHelper;
using BEPUphysics.BroadPhaseEntries.MobileCollidables;

using S = MemoryLeakEditor.SystemConverter;
using Num = System.Numerics;

namespace MemoryLeakEditor
{
    //so this is all basically a scripting component.
    //you'd have another component thats just a prefab generator
    //another one thats just 
    public partial class Editor
    {
        //alright we need an update method that updates plane information
        //and then we need 
        [Flags]
        public enum AxisState
        {
            None = 1<<1,
            X = 1<<2,
            Y = 1<<3,
            Z = 1<<4
        }
        public enum EditorOperations
        {
            Idle,
            Move,
            Rotate,
            Scale,
            Snapping
        }

        //these are all our fancy position data stuff

        Vector2 MousePosition;
        Vector2 MousePositionOld;

        Vector3 UnprojectedOld;
        Vector3 UnprojectedPosition;
        Vector3 UnprojectedDirection;

        InputState SceneInputState;
        EditorOperations ActiveOperation;
        AxisState axisLock;
        Plane operatingPlane;
        Plane XYPlane = new Plane(Vector3.Zero, Vector3.UnitX, Vector3.UnitY);
        Plane YZPlane = new Plane(Vector3.Zero, Vector3.UnitY, Vector3.UnitZ);
        Plane ZXPlane = new Plane(Vector3.Zero, Vector3.UnitZ, Vector3.UnitX);
        Plane CameraPlane;

        //selection
        //GameObject activeObject = null;
        //multi selection
        GameObject leadObject = null;//I don't, blender uses this but I don't really need it.
        List<GameObject> selectedObjects = new List<GameObject>();
        List<TransformTag> originalTags = new List<TransformTag>();
        GameObject multiSelectParent;
        //so we need.
        //so we need to add shift click selection mode
        //  which keeps track of the object selection list
        //then we need to backapply the parent object transforms when removing multi selection
        //that part, I don't know how to do that part with the current setup.
        //I guess just taking the transform tags, since
        //yeah just add the transform tags.


        //or we could just update axis locking 

        public void InitializeScene()
        {            
            SceneInputState = new InputState();

            SceneInputState.AddDispatcher(new KeyboardEventDispatcher("Keyboard"));
            SceneInputState.AddDispatcher(new MouseEventDispatcher("Mouse"));

            ActiveOperation = EditorOperations.Idle;

            CameraPlane = new Plane(Vector3.Zero, sceneCamera.GetCameraX(), sceneCamera.GetCameraZ());
            

            //inputse
            string MouseInputBinding = "MouseUnprojectedMove";
            string MouseMiddleDragBinding = "MouseMiddleDrag";
            string MouseMiddleClickBinding = "MouseMiddleClick";
            string MouseScrollBinding = "MouseScroll";
            string MouseLeftClickBinding = "MouseLeftClick";
            string MouseRightClickBinding = "MouseRightClick";
            string ShiftSelect = "MouseShiftLeftClick";
            string PanCam = "PanCamera";
            string Focus = "Focus";

            string GPress = "GPressEvent";
            string AltGPress = "AltGPressEvent";

            string RPress = "RPressEvent";
            string AltRPress = "AltRPressEvent";

            string SPress = "SPressEvent";
            string AltSPress = "AltSPressEvent";


            string AltP = "AltP";//this is alll wrong but alright lets go!

            string XKey = "XKey";
            string YKey = "YKey";
            string ZKey = "ZKey";
            string XShiftKey = "XShift";
            string YShiftKey = "YShift";
            string ZShiftKey = "ZShift";


            //remember to add keyboardstate check in mouse when adding modifier checks
            SceneInputState.AddBinding<MouseEventDispatcher>(new MouseBinding(new AxisBinding(MouseInputBinding)));
            SceneInputState.AddBinding<MouseEventDispatcher>(new MouseBinding(new ButtonBinding(MouseMiddleDragBinding), new MouseButtonCombo(MouseButton.Middle,Modifiers.Null)));
            SceneInputState.AddBinding<MouseEventDispatcher>(new MouseBinding(new ButtonBinding(MouseMiddleClickBinding), new MouseButtonCombo(MouseButton.Middle,Modifiers.Null)));
            SceneInputState.AddBinding<MouseEventDispatcher>(new MouseBinding(new ButtonBinding(MouseLeftClickBinding), new MouseButtonCombo(MouseButton.Left,Modifiers.Null)));
            SceneInputState.AddBinding<MouseEventDispatcher>(new MouseBinding(new ButtonBinding(MouseRightClickBinding), new MouseButtonCombo(MouseButton.Right, Modifiers.Null)));
            SceneInputState.AddBinding<MouseEventDispatcher>(new MouseBinding(new FloatBinding(MouseScrollBinding)));
            SceneInputState.AddBinding<MouseEventDispatcher>(new MouseBinding(new ButtonBinding(ShiftSelect), new MouseButtonCombo(MouseButton.Left, Modifiers.Shift)));
            SceneInputState.AddBinding<MouseEventDispatcher>(new MouseBinding(new ButtonBinding(PanCam), new MouseButtonCombo(MouseButton.Middle, Modifiers.Shift)));
            SceneInputState.AddBinding<KeyboardEventDispatcher>(new KeyboardBind(new ButtonBinding(Focus), new KeyboardKeyCombo(Keys.F, Modifiers.Null)));

            SceneInputState.AddBinding<KeyboardEventDispatcher>(new KeyboardBind(new ButtonBinding(GPress), new KeyboardKeyCombo(Keys.G, Modifiers.Null)));
            SceneInputState.AddBinding<KeyboardEventDispatcher>(new KeyboardBind(new ButtonBinding(AltGPress), new KeyboardKeyCombo(Keys.G, Modifiers.Alt)));

            SceneInputState.AddBinding<KeyboardEventDispatcher>(new KeyboardBind(new ButtonBinding(RPress), new KeyboardKeyCombo(Keys.R, Modifiers.Null)));
            SceneInputState.AddBinding<KeyboardEventDispatcher>(new KeyboardBind(new ButtonBinding(AltRPress), new KeyboardKeyCombo(Keys.R, Modifiers.Alt)));

            SceneInputState.AddBinding<KeyboardEventDispatcher>(new KeyboardBind(new ButtonBinding(SPress), new KeyboardKeyCombo(Keys.S, Modifiers.Null)));
            SceneInputState.AddBinding<KeyboardEventDispatcher>(new KeyboardBind(new ButtonBinding(AltSPress), new KeyboardKeyCombo(Keys.S, Modifiers.Alt)));

            SceneInputState.AddBinding<KeyboardEventDispatcher>(new KeyboardBind(new ButtonBinding(AltP), new KeyboardKeyCombo(Keys.P, Modifiers.Alt)));

            SceneInputState.AddBinding<KeyboardEventDispatcher>(new KeyboardBind(new ButtonBinding(XKey), new KeyboardKeyCombo(Keys.X, Modifiers.Null)));
            SceneInputState.AddBinding<KeyboardEventDispatcher>(new KeyboardBind(new ButtonBinding(YKey), new KeyboardKeyCombo(Keys.Y, Modifiers.Null)));
            SceneInputState.AddBinding<KeyboardEventDispatcher>(new KeyboardBind(new ButtonBinding(ZKey), new KeyboardKeyCombo(Keys.Z, Modifiers.Null)));
            SceneInputState.AddBinding<KeyboardEventDispatcher>(new KeyboardBind(new ButtonBinding(XShiftKey), new KeyboardKeyCombo(Keys.X, Modifiers.Shift)));
            SceneInputState.AddBinding<KeyboardEventDispatcher>(new KeyboardBind(new ButtonBinding(YShiftKey), new KeyboardKeyCombo(Keys.Y, Modifiers.Shift)));
            SceneInputState.AddBinding<KeyboardEventDispatcher>(new KeyboardBind(new ButtonBinding(ZShiftKey), new KeyboardKeyCombo(Keys.Z, Modifiers.Shift)));

            //SceneInputState[MouseInputBinding]
            //camera controls
            SceneInputState.GetAxis(MouseInputBinding).Axis += MouseUpdateEvent;
            SceneInputState.GetButton(MouseMiddleDragBinding).Down += MouseSwivelEvent;
            SceneInputState.GetButton(MouseMiddleClickBinding).Press += MouseSwivelInitial;
            SceneInputState.GetFloat(MouseScrollBinding).FloatVal += Zoom;
            SceneInputState.GetButton(MouseLeftClickBinding).Press += Select;
            SceneInputState.GetButton(ShiftSelect).Press += MultiSelect;
            SceneInputState.GetButton(PanCam).Down += PanCamera;
            SceneInputState.GetButton(PanCam).Press += PanInitialize;
            SceneInputState.GetButton(Focus).Press += FocusCamera;
            SceneInputState.GetAxis(MouseInputBinding).Axis += PanPositionValues;
            //movement
            SceneInputState.GetButton(GPress).Press += SetMoveState;
            SceneInputState.GetAxis(MouseInputBinding).Axis += MoveOperation;
            SceneInputState.GetButton(AltGPress).Press += ResetMove;
            //rotation
            SceneInputState.GetButton(RPress).Press += SetRotationState;
            SceneInputState.GetAxis(MouseInputBinding).Axis += RotationOperation;
            SceneInputState.GetButton(AltRPress).Press += ResetRotation;
            SceneInputState.GetButton(MouseLeftClickBinding).Press += ClearRotDebugLine;
            SceneInputState.GetButton(MouseRightClickBinding).Press += ClearRotDebugLine;
            //Scale
            SceneInputState.GetButton(SPress).Press += SetScaleState;
            SceneInputState.GetAxis(MouseInputBinding).Axis += ScaleOperation;
            SceneInputState.GetButton(AltSPress).Press += ResetScale;

            //operations
            SceneInputState.GetButton(AltP).Press += SetParent;

            //general transform
            SceneInputState.GetButton(MouseLeftClickBinding).Press += ConfirmTransform;
            SceneInputState.GetButton(MouseRightClickBinding).Press += CancleTransform;



            //axisLock
            SceneInputState.GetButton(XKey).Press += LockX;
            SceneInputState.GetButton(YKey).Press += LockY;
            SceneInputState.GetButton(ZKey).Press += LockZ;
            SceneInputState.GetButton(XShiftKey).Press += LockYZ;
            SceneInputState.GetButton(YShiftKey).Press += LockZX;
            SceneInputState.GetButton(ZShiftKey).Press += LockXY;
            

        }
        #region AxisLock

        void LockX()
        {
            if(ActiveOperation != EditorOperations.Idle)
                axisLock = AxisState.X;
        }
        void LockY()
        {
            if (ActiveOperation != EditorOperations.Idle)
                axisLock = AxisState.Y;
        }
        void LockZ()
        {
            if (ActiveOperation != EditorOperations.Idle)
                axisLock = AxisState.Z;
        }
        void LockXY()
        {
            if (ActiveOperation != EditorOperations.Idle)
                axisLock = AxisState.X | AxisState.Y;
        }
        void LockYZ()
        {
            if (ActiveOperation != EditorOperations.Idle)
                axisLock = AxisState.Y | AxisState.Z;
        }
        void LockZX()
        {
            if (ActiveOperation != EditorOperations.Idle)
                axisLock =  AxisState.Z | AxisState.X;
        }

        #endregion
        //lets start with selection and move
        TransformTag originalTag;
        TransformTag editTag;
        #region Move

        Vector3 StartMove;

        public void SetMoveState()
        {
            if(ActiveOperation == EditorOperations.Idle)
            {
                //if(activeObject != null)
                if(selectedObjects.Count != 0)
                {
                    ActiveOperation = EditorOperations.Move;
                    //var activeTag = (TransformTag)activeObject.GetComponent<TransformComponent>().Tag;

                    //originalTag = new TransformTag();
                    //originalTag.qrotation = activeTag.qrotation;
                    //originalTag.position = activeTag.position;
                    //originalTag.scale = activeTag.scale;
                    originalTags = new List<TransformTag>();
                    foreach(GameObject go in selectedObjects)
                    {
                        originalTags.Add(((TransformTag)go.GetComponent<TransformComponent>().Tag).Clone());
                    }

                    editTag = new TransformTag();
                    StartMove = UnprojectedPosition + UnprojectedDirection * sceneCamera.distance;//yeah like this
                }
            }
        }
        //do I do, two plane algins or do I just get the
        //the unprojected position at the cameras distance
        //hmm
        //oh I can just get that during the move start
        //so now that thats aligned already we can just use the active plane
        public void MoveOperation(Vector2 v)
        {
            var ActivePoint = UnprojectedPosition + UnprojectedDirection * sceneCamera.distance;
            if(ActiveOperation == EditorOperations.Move)
            {
                switch (axisLock)
                {
                    case AxisState.None:
                        editTag.position = S.Convert(   ActivePoint - StartMove);
                        break;
                    case AxisState.X:
                        editTag.position = S.Convert(
                            PlaneAlignedPosition(XYPlane, ActivePoint) - PlaneAlignedPosition(XYPlane, StartMove));
                        editTag.position.Y = 0;
                        break;
                    case AxisState.Y:
                        editTag.position = S.Convert(
                            PlaneAlignedPosition(YZPlane, ActivePoint) - PlaneAlignedPosition(YZPlane, StartMove));
                        editTag.position.Z = 0;
                        break;
                    case AxisState.Z:
                        editTag.position = S.Convert(
                            PlaneAlignedPosition(ZXPlane, ActivePoint) - PlaneAlignedPosition(ZXPlane, StartMove));
                        editTag.position.X = 0;
                        break;
                    case (AxisState.X | AxisState.Y):
                        editTag.position = S.Convert(
                            PlaneAlignedPosition(XYPlane, ActivePoint) - PlaneAlignedPosition(XYPlane, StartMove));
                        break;
                    case (AxisState.Y | AxisState.Z):
                        editTag.position = S.Convert(
                            PlaneAlignedPosition(YZPlane, ActivePoint) - PlaneAlignedPosition(YZPlane, StartMove));
                        break;
                    case (AxisState.Z | AxisState.X):
                        editTag.position = S.Convert(
                            PlaneAlignedPosition(ZXPlane, ActivePoint) - PlaneAlignedPosition(ZXPlane, StartMove));
                        break;                        
                }
                editTag.scale = System.Numerics.Vector3.Zero;
                //activeObject.GetComponent<TransformComponent>().Tag = originalTag + editTag;
                foreach(GameObject go in selectedObjects)
                {
                    go.GetComponent<TransformComponent>().Tag = originalTags[selectedObjects.IndexOf(go)] + editTag;
                    SetTagTransform(go.GetComponent<TransformComponent>());
                }
            }
        }
        public Vector3 PlaneAlignedPosition(Plane plane, Vector3 position)
        {
            Vector3 ret = Vector3.Zero;
            var cameraforward = sceneCamera.GetCameraY();
            Ray rayUp = new Ray(position, UnprojectedDirection);//new Ray(position, plane.Normal);
            Ray rayDown = new Ray(position, -UnprojectedDirection); //new Ray(position, -plane.Normal);

            float? intersectUp = rayUp.Intersects(plane);
            float? intersectDown = rayDown.Intersects(plane);

            //check for perfect align edge case
            if(intersectUp == null && intersectDown == null)
            {
                rayUp = new Ray(position, plane.Normal);
                rayDown = new Ray(position, -plane.Normal);

                intersectUp = rayUp.Intersects(plane);
                intersectDown = rayDown.Intersects(plane);
                return position + (plane.Normal * ((intersectUp != null) ? (float)intersectUp : -(float)intersectDown));
            }

            return position + (UnprojectedDirection * ((intersectUp != null) ? (float)intersectUp : -(float)intersectDown));//this should do it
        }
        public void ResetMove()
        {
            //if(activeObject!=null)
            //{
            //    ((TransformTag)(activeObject.GetComponent<TransformComponent>().Tag)).position = Num.Vector3.Zero;
            //}
            foreach(var go in selectedObjects)
            {
                ((TransformTag)(go.GetComponent<TransformComponent>().Tag)).position = Num.Vector3.Zero;
                SetTagTransform(go.GetComponent<TransformComponent>());
            }
        }
        #endregion
        #region Rotation
        
        Vector3 RotStart;        
        Vector3 RotNormal;
        Vector3 CenterUnprojected;
        Vector2 TwoRot;
        bool RotSingleAxis; //operations are either single axis or dual axis, single axis is defualt and as such true
        public void SetRotationState()
        {
            if (ActiveOperation == EditorOperations.Rotate)
            {
                RotSingleAxis = !RotSingleAxis;//flip operation
            }
            if (ActiveOperation == EditorOperations.Idle)
            {
                //if(activeObject != null)
                if(selectedObjects.Count != 0)
                {
                    ActiveOperation = EditorOperations.Rotate;
                    //var activeTag = (TransformTag)activeObject.GetComponent<TransformComponent>().Tag;
                    //originalTag = new TransformTag();
                    //originalTag.qrotation = activeTag.qrotation;
                    //originalTag.position = activeTag.position;
                    //originalTag.scale = activeTag.scale;
                    originalTags = new List<TransformTag>();
                    foreach (GameObject go in selectedObjects)
                    {
                        originalTags.Add(((TransformTag)go.GetComponent<TransformComponent>().Tag).Clone());
                    }

                    editTag = new TransformTag();
                    //RotStart = UnprojectedPosition;


                    //RotStart = UnprojectedPosition - sceneCamera.GetCameraPosition();
                    CenterUnprojected = GraphicsDevice.Viewport.Unproject(new Vector3(graphics.PreferredBackBufferWidth / 2, graphics.PreferredBackBufferHeight / 2, 0), sceneCamera.Projection, sceneCamera.View, Matrix.Identity);

                    RotNormal = Vector3.Normalize(GraphicsDevice.Viewport.Unproject(new Vector3(graphics.PreferredBackBufferWidth / 2, graphics.PreferredBackBufferHeight, 1), sceneCamera.Projection, sceneCamera.View, Matrix.Identity));
                    
                    RotStart = Vector3.Normalize( UnprojectedPosition - CenterUnprojected);

                    RotSingleAxis = true;
                    TwoRot = Vector2.Zero;                    

                }
            }
            
        }        
        public void RotationOperation(Vector2 v)
        {
            //so the general idea is. 
            //we get the angle by taking the angle of the initial position using dot product and arccos
            //and then get the difference of the current angle with arccos
            //or we use. we use the initial direction to get the first dot product, that should work well    
            if (ActiveOperation == EditorOperations.Rotate)
            {
                if (RotSingleAxis)
                {
                    debugLine.Clear();
                    
                    debugLine.AddLine(CenterUnprojected, UnprojectedPosition, Color.Blue);

                    var sceneCameraPosition = sceneCamera.GetCameraPosition();
                    var sceneCameraTrans = sceneCamera.FocusPoint;
                    var zeroedUnprojected = UnprojectedPosition - (UnprojectedDirection * sceneCamera.distance);
                    //var rotActive = UnprojectedPosition - sceneCamera.GetCameraPosition();
                    var rotActive = Vector3.Normalize(UnprojectedPosition - CenterUnprojected);
                    //var DotAffirmation = Vector3.Dot(Vector3.Normalize(RotStart), Vector3.Normalize( rotActive));
                    //float activeAngle = (float)Math.Acos(Vector3.Dot(Vector3.Normalize(RotStart), Vector3.Normalize(rotActive)));

                    float dot = Vector3.Dot(RotStart, rotActive);
                    //float det = new Matrix(Vector4.Zero, Vector4.Zero,Vector4.Zero,Vector4.Zero).Determinant();//alright we need a matrix from, our values lets go
                    float det =
                        new Matrix(new Vector4(RotStart, 0),
                                   new Vector4(rotActive, 0),
                                   new Vector4(RotNormal, 0),
                                   new Vector4(0, 0, 0, 1)).Determinant();

                    //float activeAngle = (float)Math.Acos(Vector3.Dot(RotStart, rotActive));
                    float activeAngle = -(float)Math.Atan2(det, dot);
                    switch (axisLock)
                    {
                        case AxisState.X:
                            editTag.qrotation = S.Convert(Quaternion.CreateFromAxisAngle(Vector3.UnitX, activeAngle));
                            break;
                        case AxisState.Y:
                            editTag.qrotation = S.Convert(Quaternion.CreateFromAxisAngle(Vector3.UnitY, activeAngle));
                            break;
                        case AxisState.Z:
                            editTag.qrotation = S.Convert(Quaternion.CreateFromAxisAngle(Vector3.UnitZ, activeAngle));
                            break;
                        default:
                            editTag.qrotation = S.Convert(Quaternion.CreateFromAxisAngle(sceneCamera.GetCameraZ(), activeAngle));
                            break;
                    }
                }
                else
                {
                    var delta = MousePosition - MousePositionOld;
                    TwoRot += delta;
                    //I think this is it.
                    editTag.qrotation = S.Convert(Quaternion.Concatenate(Quaternion.CreateFromAxisAngle(sceneCamera.GetCameraY(), TwoRot.X * .01f), Quaternion.CreateFromAxisAngle(sceneCamera.GetCameraX(), TwoRot.Y * .01f)));
                }
                editTag.scale = Num.Vector3.Zero;
                foreach (GameObject go in selectedObjects)
                {
                    go.GetComponent<TransformComponent>().Tag = originalTags[selectedObjects.IndexOf(go)] + editTag;
                    SetTagTransform(go.GetComponent<TransformComponent>());
                }
                //activeObject.GetComponent<TransformComponent>().Tag = originalTag + editTag;

            }

        }
        public void ClearRotDebugLine()
        {
            if(ActiveOperation== EditorOperations.Rotate)
            {
                debugLine.Clear();
            }
        }
        public void ResetRotation()
        {
            //if(activeObject!=null)
            //{
            //    ((TransformTag)activeObject.GetComponent<TransformComponent>().Tag).qrotation = S.Convert(Quaternion.Identity);
            //}
            foreach(var go in selectedObjects)
            {
                ((TransformTag)(go.GetComponent<TransformComponent>().Tag)).position = Num.Vector3.Zero;
                SetTagTransform(go.GetComponent<TransformComponent>());
            }
        }
        #endregion
        #region Scale
        Vector3 ScaleStart;//lets move this to 0 position, see if that helps
        

        public void SetScaleState()
        {
            if(ActiveOperation == EditorOperations.Idle)
            {
                //if(activeObject != null)
                if(selectedObjects.Count != 0)
                {
                    ActiveOperation = EditorOperations.Scale;
                    //var activeTag = (TransformTag)activeObject.GetComponent<TransformComponent>().Tag;

                    //originalTag = new TransformTag();
                    //originalTag.position = activeTag.position;
                    //originalTag.qrotation = activeTag.qrotation;
                    //originalTag.scale = activeTag.scale;
                    originalTags = new List<TransformTag>();
                    foreach (GameObject go in selectedObjects)
                    {
                        originalTags.Add(((TransformTag)go.GetComponent<TransformComponent>().Tag).Clone());
                    }

                    editTag = new TransformTag();
                    ScaleStart = UnprojectedPosition + UnprojectedDirection * sceneCamera.distance;

                    CenterUnprojected = GraphicsDevice.Viewport.Unproject(new Vector3(graphics.PreferredBackBufferWidth / 2, graphics.PreferredBackBufferHeight / 2, 0), sceneCamera.Projection, sceneCamera.View, Matrix.Identity);
                    var centerNormal = Vector3.Normalize( GraphicsDevice.Viewport.Unproject(new Vector3(graphics.PreferredBackBufferWidth / 2, graphics.PreferredBackBufferHeight / 2, 1), sceneCamera.Projection, sceneCamera.View, Matrix.Identity) - CenterUnprojected);
                    CenterUnprojected = CenterUnprojected + centerNormal * sceneCamera.distance;
                }
            }
        }
        public void ScaleOperation(Vector2 v)
        {
            if(ActiveOperation == EditorOperations.Scale)
            {
                //float ScaleFactor = 8f;
                float startDistance = (ScaleStart - CenterUnprojected ).Length();
                float activeDistance = ((UnprojectedPosition +(UnprojectedDirection * sceneCamera.distance)) - CenterUnprojected ).Length();
                // float scaleValue = (UnprojectedPosition - ScaleStart).Length();
                float scaleValue = activeDistance - startDistance;
                //scaleValue *= ScaleFactor;
                switch(axisLock)
                {
                    case AxisState.X:
                        editTag.scale = new Num.Vector3(scaleValue, 0,0);
                        break;
                    case AxisState.Y:
                        editTag.scale = new Num.Vector3(0, scaleValue, 0);
                        break;
                    case AxisState.Z:
                        editTag.scale = new Num.Vector3(0, 0, scaleValue);
                        break;
                    case (AxisState.X | AxisState.Y):
                        editTag.scale = new Num.Vector3(scaleValue, scaleValue, 0);
                        break;
                    case (AxisState.Y | AxisState.Z):
                        editTag.scale = new Num.Vector3(0, scaleValue, scaleValue);
                        break;
                    case (AxisState.Z | AxisState.X):
                        editTag.scale = new Num.Vector3(scaleValue, 0, scaleValue);
                        break;
                    case AxisState.None:
                        editTag.scale = new Num.Vector3(scaleValue, scaleValue, scaleValue);
                        break;
                }

                //activeObject.GetComponent<TransformComponent>().Tag = originalTag + editTag;
                foreach (GameObject go in selectedObjects)
                {
                    go.GetComponent<TransformComponent>().Tag = originalTags[selectedObjects.IndexOf(go)] + editTag;
                    SetTagTransform(go.GetComponent<TransformComponent>());
                }
            }
        }
        public void ResetScale()
        {
            //if(activeObject != null)
            //{
            //    ((TransformTag)activeObject.GetComponent<TransformComponent>().Tag).scale = Num.Vector3.One;
            //}
            foreach(var go in selectedObjects)
            {
                ((TransformTag)go.GetComponent<TransformComponent>().Tag).scale = Num.Vector3.One;
            }
        }

        #endregion


        void SetParent()
        {
            if (ActiveOperation == EditorOperations.Idle)
            {
                if(selectedObjects.Count > 1)
                {
                    var activeObject = selectedObjects[0];
                    List<GameObject> children = selectedObjects.GetRange(1, selectedObjects.Count - 1);

                    EngineSetParent(activeObject, children.ToArray());
                }
            }
        }
        //okay so all of this is just, logic scripting.
        #region General Mouse
        public void CancleTransform()
        {
            if(ActiveOperation == EditorOperations.Move || ActiveOperation == EditorOperations.Rotate || ActiveOperation == EditorOperations.Scale)
            {
                ActiveOperation = EditorOperations.Idle;
                //activeObject.GetComponent<TransformComponent>().Tag = originalTag;
                foreach(GameObject go in selectedObjects)
                {
                    go.GetComponent<TransformComponent>().Tag = originalTags[selectedObjects.IndexOf(go)];
                }
                axisLock = AxisState.None;
            }
        }
        public void ConfirmTransform()
        {
            if (ActiveOperation == EditorOperations.Move || ActiveOperation == EditorOperations.Rotate || ActiveOperation == EditorOperations.Scale)
            {
                ActiveOperation = EditorOperations.Idle;
                axisLock = AxisState.None;
            }
        }



        public void MouseUpdateEvent(Vector2 v)
        {
            UnprojectedOld = UnprojectedPosition;
            UnprojectedPosition = GraphicsDevice.Viewport.Unproject(new Vector3(v.X, v.Y, 0), sceneCamera.Projection, sceneCamera.View, Matrix.Identity);
            var UnprojectedUnit = GraphicsDevice.Viewport.Unproject(new Vector3(v.X, v.Y, 1), sceneCamera.Projection, sceneCamera.View, Matrix.Identity);
            UnprojectedDirection = Vector3.Normalize(UnprojectedUnit - UnprojectedPosition);
            //mmm yeah?
            MousePositionOld = MousePosition;
            MousePosition = v;
            
        }
        public void Select()
        {
            if(ActiveOperation == EditorOperations.Idle)
            {
                //BRay selectRay = new BRay(
                //    MathConverter.Convert(UnprojectedPosition),
                //    MathConverter.Convert(UnprojectedDirection));
                RayCastResult rayResult = SelectionRay();
                //RayCastResult rayResult;
                //SceneSelectionSpace.RayCast(selectRay, out rayResult);

                if (rayResult.HitObject!=null)
                {
                    selectedObjects = new List<GameObject>();
                    selectedObjects.Add((GameObject)((EntityCollidable)rayResult.HitObject).Entity.Tag);
                    //activeObject = selectedObjects[0];
                }
            }
            //I have to update, the system for modifier keys
            //hmm
        }
        public void MultiSelect()
        {
            if(ActiveOperation == EditorOperations.Idle)
            {
                ////so we need to create a new multitrnasform object
                //multiSelectParent = new GameObject();
                ////by defualt it creates a transform component yeah? and a transform tag?
                //multiSelectParent.GetComponent<TransformComponent>().Tag = new TransformTag();
                ////so we don't set this yet we need to know first
                //if (activeObject != null)
                //{
                //    multiSelectParent.AddChild(activeObject);//this doesn't work.this only works if the parent isn't set. 
                //    //okay so parenting breaks down with this so what if we just.
                //    //just perform this operation on all the selected objects
                //    //so none of this parenting stuff or clear transform just a list of gameobjects and an edittag.//Also original tag for the sake of, the whole point.so lets go
                //}
                //so we need an active object because of course. 
                //

                //BRay selectRay = new BRay(
                //MathConverter.Convert(UnprojectedPosition),
                //MathConverter.Convert(UnprojectedDirection));
                //RayCastResult rayResult;
                //SceneSelectionSpace.RayCast(selectRay, out rayResult);
                RayCastResult rayResult = SelectionRay();

                if (rayResult.HitObject!=null)
                {
                    var hitObject = (GameObject)((EntityCollidable)rayResult.HitObject).Entity.Tag;

                    if(selectedObjects.Contains(hitObject))
                    {
                        selectedObjects.Remove(hitObject);
                        selectedObjects.Insert(0, hitObject);
                    }
                    else
                    {
                        selectedObjects.Insert(0, hitObject);
                    }
                    //selectedObjects.Add();
                    

                    //activeObject = selectedObjects[0];
                }


                //cool so we need to make sure 
                //now we need to//
                //we don't have to actually set the active object until 

            }
        }
        RayCastResult SelectionRay()
        {
            BRay selectRay = new BRay(
                MathConverter.Convert(UnprojectedPosition),
                MathConverter.Convert(UnprojectedDirection));
            RayCastResult rayResult;
            SceneSelectionSpace.RayCast(selectRay, out rayResult);
            return rayResult;
        }
        //I guess this is obsolete huh
        Vector2 MouseTurntableStart;
        private void MouseSwivelInitial()
        {
            MouseTurntableStart = MousePosition;            
        }
        private void MouseSwivelEvent()
        {
            //var delta = UnprojectedPosition - UnprojectedOld;
            if (ActiveOperation == EditorOperations.Idle)
            {
                var delta = MousePosition - MousePositionOld;
                sceneCamera.zAngel -= delta.X;
                sceneCamera.xAngel -= delta.Y;
                //sceneCamera.UpdateProjection();
                sceneCamera.UpdatePivot();
            }
            //so the only problem with this is out of boundsing the program but thats fine.
        }
        #endregion;
        float oldScrollvalue = 0;
        float ScrollValue = 0;
        float scrollSensitivity = .01f;
        private void Zoom(float v)
        {
            
            oldScrollvalue = ScrollValue;
            ScrollValue = v;
            if (ActiveOperation == EditorOperations.Idle)
            {
                if (ScrollValue != oldScrollvalue)
                {
                    sceneCamera.distance -= (ScrollValue - oldScrollvalue) * scrollSensitivity;
                    sceneCamera.UpdatePivot();
                }
            }
        }

        Matrix panView = Matrix.Identity;
        Matrix panProjection = Matrix.Identity;

        private void PanInitialize()
        {
            panView = sceneCamera.View;
            panProjection = sceneCamera.Projection;
        }
        Vector3 UnprojectedPan;
        Vector3 UnprojectedPanOld;
        private void PanPositionValues(Vector2 v)
        {
            UnprojectedPanOld = UnprojectedPan;
            UnprojectedPan = GraphicsDevice.Viewport.Unproject(new Vector3(v.X, v.Y, sceneCamera.distance), panProjection, panView, Matrix.Identity);
            
        }
        private void PanCamera()
        {
            if(ActiveOperation == EditorOperations.Idle)
            {

                var delta = UnprojectedPosition - UnprojectedOld;
                //var delta = UnprojectedPan - UnprojectedPanOld;
                sceneCamera.FocusPoint += delta;
            }
        }
        private void FocusCamera()
        {
            if(ActiveOperation == EditorOperations.Idle)
            {
                if(selectedObjects.Count > 0)
                {
                    var Transform = selectedObjects[0].GetComponent<TransformComponent>();
                    sceneCamera.FocusPoint = Transform.Position;
                }
            }
        }

        //rotating takes the xy and rotates        
    }
}

﻿
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Dynamic;
using System.IO;
using System.Linq;
using Num = System.Numerics;
using ImGuiNET;
using MemoryLeakEditor.EnginePrimitives;
using BaseEngine;
using BaseEngine.Input;
using BaseEngine.Components;
using BaseEngine.Rendering;
using BaseEngine.BepuPhysics;
using SConvert = MemoryLeakEditor.SystemConverter;
using MemoryLeakEditor.ComponentTags;
using BEPUphysics;
using BEPUphysics.CollisionShapes.ConvexShapes;
using Matrix3x3 = BEPUutilities.Matrix3x3;
using BVector3 = BEPUutilities.Vector3;

namespace MemoryLeakEditor
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public partial class Editor : CoreGame//started replacing this but it doesn't, actually matter huh. I need a second Physics context though
    {
        EditorRenderingModule rModule;//so lets just start rebuilding the editor itself as a game.
        //SpriteBatch spriteBatch;
        ImGuiRenderer imGuiRenderer;//need to add this as a rendering module to the editor.
        //non updated physics space used to select objects
        //at least I hope its doesn't need to be updated
        Space SceneSelectionSpace = new Space();
        InputState ActiveState;
        RenderTarget2D sceneRender;
        IntPtr imGuiScene;
        Scene editorScene;
        RasterizerState defualtRasterizerState;

        
        //GameObject selectedObject;
        EditorCamera sceneCamera;

        public Editor()
        {
            
            graphics.PreferredBackBufferWidth = 1024;
            graphics.PreferredBackBufferHeight = 768;
            graphics.PreferMultiSampling = true;
            
            IsMouseVisible = true;
            Window.AllowUserResizing = true;
            Window.ClientSizeChanged += AdjustBackBuffer;
            Content.RootDirectory = "Content";

        }

        private void AdjustBackBuffer(object sender, EventArgs e)
        {
            graphics.PreferredBackBufferHeight = GraphicsDevice.Viewport.Height;
            graphics.PreferredBackBufferWidth = GraphicsDevice.Viewport.Width;
            AdjustRender(new Num.Vector2(graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight));
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            EngineServices.renderer = new RenderingService(this);
            EngineServices.renderer.primaryModule = new EditorRenderingModule();
            EngineServices.physics = new BepuPhysicsService(this);
            //Window.IsBorderless = true;
            Window.AllowUserResizing = true;
            //Window.AllowAltF4 = false;
            
            //Window.Position = new Point(0, 0);
            //EngineServices.physics.Module = new BepuPhysicsModule();//strings would work better with runtime changes.
            //I hate this.
            //alternatively it requires more work with drawing and updating.
            //but so does everything because that doesnt' handle dynamic loading
            //like all of that is in these over ride games but really these override games shouldn't exist.
            //thats kind of the point isn't it modularity means over riding like this is a bad state.
            //you want everything managed by the main class.
            //and then you just tell it to add things into packages that it knows how to load.
            defualtRasterizerState = GraphicsDevice.RasterizerState;
            imGuiRenderer = new ImGuiRenderer(this);
            imGuiRenderer.RebuildFontAtlas();
            
            base.Initialize();

        }

        
        
        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {            

            //cursor = Content.Load<Model>("EngineCursor");

            editorScene = new Scene("gamescene");//so right now this is a scene used for the editor but should be contained in some other component.
            sceneCamera = new EditorCamera(GraphicsDevice, 10);//asset.
            
            //renderer.rCamera = sceneCamera;//don't know how to resolve this quite yet
            //renderer.gameScene = editorScene;

            //physics.gameScene = editorScene;

            rModule = new EditorRenderingModule();
            rModule.graphics = GraphicsDevice;
            rModule.Camera = sceneCamera;//this should be abstracted.
            //need to create an imgui rendering module, and imgui rendering components.
            EngineServices.renderer.primaryModule = rModule;

            sceneRender = new RenderTarget2D(GraphicsDevice, graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight,false,SurfaceFormat.Color,DepthFormat.Depth24Stencil8);
            //oldSize = new Num.Vector2(500, 400);
            sceneCamera.aspect = 500f / 400f;
            sceneGrid.Initialize(GraphicsDevice);
            sceneCube.Initialize(GraphicsDevice);

            //Load editor scene
            InitializeScene();



            //lets add the cube as a default object;
            //AddCube(); //0
            //Nw we're gonna add a bunch of empty game objects
            editorScene.AddGameObject(new GameObject() { Name = "EmptyChild" }); //1
            editorScene.AddGameObject(new GameObject() { Name = "Empty"});//2
            editorScene.AddGameObject(new GameObject() { Name = "Nominal" });//3
            editorScene.AddGameObject(new GameObject() { Name = "EmptyChildChild" });//4
            AddCube();
            AddSphereDiamond();

            foreach(GameObject go in editorScene.gameObjects)
            {
                if(go.Name != "Cube")
                {
                    go.GetComponent<TransformComponent>().Tag = new TransformTag();
                }
            }
            //GameScene.gameObjects[1].SetParent(GameScene.gameObjects[2]);
            editorScene.gameObjects[2].AddChild(editorScene.gameObjects[1]);
            editorScene.gameObjects[1].AddChild(editorScene.gameObjects[4]);
            //GameScene.gameObjects[4].SetParent(GameScene.gameObjects[1]);

            //the whole fucking gamescene here is convoluted as hell I need to, simplify 

            //uh oh
            //okay so initialize is a thing thats supposed to be a call once,
            //but since components are added on the fly initializing them is gonna be, harder
            //though I could just, not? maybe?
            //the rendering needs access to the transform component tho...
            //I could just create a, editor object, that contains the drawables and references the game object
            //for now just, initalize and draw the render scene in the, the render target

            //I think I did this with a like initialize event?
            //I could use reflection or like grab or something, who knows??

            componentInspectors.Add(typeof(TransformComponent), (ComponentInspector)TransformInspector);

            base.LoadContent();
            EngineServices.renderer.Load(editorScene);
            EngineServices.physics.Load(editorScene);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            //ImGui.Begin("name");
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();
            KeyboardState state;
            string stuff = "";
            
            state = Keyboard.GetState();

            foreach (var key in state.GetPressedKeys())
            {
                stuff += key;
            }

            //renderer.UpdateEditor();
            // TODO: Add your update logic here
            if (stuff != "")
                SceneSelectionSpace.Update();
            BEPUphysics.CollisionRuleManagement.CollisionGroup ColGroup = new BEPUphysics.CollisionRuleManagement.CollisionGroup();
            BEPUphysics.Entities.Prefabs.Box box = new BEPUphysics.Entities.Prefabs.Box(BVector3.Zero, 0, 0, 0);
            box.CollisionInformation.CollisionRules.Group = ColGroup;
            SceneInputState.Update();
            RayCastResult result;
            SceneSelectionSpace.RayCast(
                new BEPUutilities.Ray(),
                100,
                (BEPUphysics.BroadPhaseEntries.BroadPhaseEntry entry) => (entry.CollisionRules.Group == ColGroup),
                out result
                );
            //fuuun
            //alright so I need a collision group for 

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            GraphicsDevice.RasterizerState = defualtRasterizerState;
            GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            DrawEditor(gameTime);
            //GraphicsDevice.Clear(Color.LightPink);
            
            imGuiRenderer.BeforeLayout(gameTime);
            ImguiExamp();
            ImguiLamp();            
            ImGuiEditorLayout();
            imGuiRenderer.AfterLayout();

            
            base.Draw(gameTime);
        }
        public void ImguiExamp()
        {
            ImGui.BeginMainMenuBar();
            //ImGui.SetNextWindowContentSize(new Num.Vector2(10, 10));
            ImGui.Begin("Parent Examp Window");
            ImGui.Begin("Examp Window",ImGuiWindowFlags.MenuBar | ImGuiWindowFlags.ChildWindow );
            ImGui.Button("Click Here",new Num.Vector2(50,200));
            ImGui.SameLine();
            ImGui.Button("Lorum Ipsum Dolor Salt");
            ImGui.SameLine(0, 10);
            ImGui.Button("Lorum Ipsum Dolor Salt");
            if (ImGui.BeginMenuBar())
            {
                if (ImGui.BeginMenu("Lamp"))
                {
                    if (ImGui.BeginMenu("ExtraMenu"))
                    {
                        ImGui.MenuItem("someItem");
                        ImGui.MenuItem("New");
                        ImGui.MenuItem("Edit");
                        ImGui.EndMenu();
                    }
                    ImGui.EndMenu();
                }
                if (ImGui.BeginMenu("Examp Menu"))
                {
                    ImGui.MenuItem("action");
                    ImGui.EndMenu();
                }
            }
            ImGui.End();
            ImGui.End();
        }


        Grid sceneGrid = new Grid(20, 10, Color.Gray);
        Matrix gridMatrix = Matrix.Identity;
        CubePrimitive sceneCube = new CubePrimitive(2, 2, 2, Color.Aqua);
        Matrix cubeMatrix = Matrix.Identity;

        Matrix debugNormalMatrix = Matrix.Identity;

        DebugLineRenderer debugLine = new DebugLineRenderer();

        protected void DrawEditor(GameTime gameTime)
        {
            GraphicsDevice.SetRenderTarget(sceneRender);
            GraphicsDevice.Clear(Color.LightGray);
            //GraphicsDevice.RasterizerState = defualtRasterizerState;
            
            
            sceneGrid.World = gridMatrix;
            sceneGrid.View = sceneCamera.View;
            sceneGrid.Projection = sceneCamera.Projection;
            sceneGrid.Apply();
            this.GraphicsDevice.DrawUserIndexedPrimitives<VertexPositionColor>(PrimitiveType.LineList,
                sceneGrid.vertices,
                0,
                sceneGrid.vertexCount,
                sceneGrid.indices,
                0,
                sceneGrid.primitiveCount);

            debugLine.Draw(GraphicsDevice);

            //renderer.DrawEditor();
            EngineServices.renderer.Draw(gameTime);
            DrawCursor();

            GraphicsDevice.SetRenderTarget(null);
            //imGuiScene = imGuiRenderer.BindTexture(sceneRender);//replace this with a normal texture draw, really
            spriteBatch.Begin();
            spriteBatch.Draw(sceneRender, Vector2.Zero, Color.White);
            spriteBatch.End();
        }
        Num.Vector3 cameraReference;
        Num.Vector3 actualMouse;
        void DrawCursor()
        {            
           //EditorCursorPosition = GraphicsDevice.Viewport.Unproject(new Vector3(Mouse.GetState().Position.ToVector2() - SConvert.Convert(Editor3dPosition), 0), sceneCamera.Projection, sceneCamera.View, Matrix.Identity);
            //cameraReference = SConvert.Convert(EditorCursorPosition);

            //EditorCursorDirection = Vector3.Normalize( GraphicsDevice.Viewport.Unproject(new Vector3(Mouse.GetState().Position.ToVector2() - SConvert.Convert(Editor3dPosition), 1), sceneCamera.Projection, sceneCamera.View, Matrix.Identity ) - EditorCursorPosition );
            //var CameraOffset = EditorCursorPosition + EditorCursorDirection * sceneCamera.distance;
            //cameraReference = SConvert.Convert(CameraOffset);            
        }
        

        private float f = 0.0f;

        private bool show_test_window = false;
        private bool show_another_window = false;
        private Num.Vector3 clear_color = new Num.Vector3(114f / 255f, 144f / 255f, 154f / 255f);
        private byte[] _textBuffer = new byte[100];

       protected void ImguiLamp()
        {
            ImGui.Begin("Name");
            ImGui.Begin("Lamp");
            ImGui.Text("Lorim Ipsum Dim Allum");
            ImGui.StyleColorsLight();
            float lamp = 0;
            ImGui.End();
            ImGui.InputFloat("flamp", ref lamp);
            
            ImGui.End();

        }

        //okay lets.
        //so we'll create a set of state containers and widgets.
        //and we'll.
        //so building a window is like.
        //game objects but with windows.
        //and components for widgets.
        //what do I want this process to acturally look like.
        //I don't want just an un ending string of proprties and values.
        //
        protected virtual void ImGuiEditorLayout()
        {

            //SceneDraw();
            DrawSceneList();//I guess this is the tree, it should be labled more clearly

            //if (activeObject != null)
            if(selectedObjects.Count != 0)
                GameObjectInspector();

            ImGui.ShowDemoWindow();
        }

        //replace num.vector2 it was for comversion simplicity but we can just use a monogame vector
        private void AdjustRender(Num.Vector2 size)
        {            
            sceneRender = new RenderTarget2D(GraphicsDevice, (int)size.X, (int)size.Y,false,SurfaceFormat.Color,DepthFormat.Depth24Stencil8);
            if (sceneCamera != null) 
                sceneCamera.aspect = size.X / size.Y;
            
        }
        
        //example layout
        
    }
}
    


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;


namespace EngineGame
{
    struct ClippingVector
    {
        public Vector3 Position;
        public Vector3 ABVector;
        public Vector3 ACVector;
    }
    class DemoEffectGame : Game
    {
        public GraphicsDeviceManager graphics;
        Model UIModel;
        Matrix ATransform;
        Matrix BTransform;
        Matrix Projection;
        Effect GUIEffect;

        ClippingVector ScreenClip;
        ClippingVector CanvasClip;
        float margin = 5;

        Vector2 ParentSize;
        Vector2 ChildSize;



        

        public DemoEffectGame()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

        }
        protected override void LoadContent()
        {
            UIModel = Content.Load<Model>("GUIModel");
            GUIEffect = Content.Load<Effect>("ImGuiEffect");
            base.LoadContent();

            ATransform = Matrix.CreateTranslation(50,50,0);//200 BY 200
            BTransform = Matrix.CreateTranslation(0, 100, 0);//100 by 100


            ScreenClip = new ClippingVector()
            {
                Position = new Vector3(0, 0, 0),
                ABVector = new Vector3(graphics.PreferredBackBufferWidth, 0, 0),
                ACVector = new Vector3(0, graphics.PreferredBackBufferHeight, 0),

            };
            CanvasClip = new ClippingVector()
            {
                //Position = //canvas poistion

                Position = new Vector3(55, 55, 0),
                ABVector = new Vector3(195, 0, 0),
                ACVector = new Vector3(0, 195, 0)
            };//since we're not transforming the panel rotationally this is all I need for that.

            Projection = Matrix.CreateOrthographicOffCenter(0, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height, 0, 0, -1);
        }
        protected override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }
        //heck. wait no thats fine its drawing the model twice I'm not doing instancing yet.
        protected override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
            GUIEffect.Parameters["Projection"].SetValue(Projection);
            GUIEffect.Parameters["Margin"].SetValue(5);
            //so we set clipping stuff then draw model
            //alright SO
            GUIEffect.Parameters["ClippingPosition"].SetValue(ScreenClip.Position);
            GUIEffect.Parameters["ClippingA"].SetValue(ScreenClip.ABVector);
            GUIEffect.Parameters["ClippingB"].SetValue(ScreenClip.ACVector);
            GUIEffect.Parameters["Size"].SetValue(new Vector2(200, 200));
            GUIEffect.Parameters["Color"].SetValue(Color.DeepPink.ToVector4());
            DrawModel(UIModel, ATransform);



        }

        void DrawModel(Model m,Matrix World)
        {
            foreach(ModelMesh mesh in m.Meshes)
            {
                foreach(ModelMeshPart part in mesh.MeshParts)
                {
                    part.Effect = GUIEffect;
                    part.Effect.Parameters["WorldTransform"].SetValue(World);
                   
                }
                mesh.Draw();
            }
        }
    }
}

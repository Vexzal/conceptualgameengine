﻿#if OPENGL
	#define SV_POSITION POSITION
	#define VS_SHADERMODEL vs_3_0
	#define PS_SHADERMODEL ps_3_0
#else
	#define VS_SHADERMODEL vs_4_0_level_9_1
	#define PS_SHADERMODEL ps_4_0_level_9_1
#endif

matrix WorldViewProjection;

//lets talk about data compression here
//so if I have an array for data accessed through weights as indicies,
//I need to assign those indicies to each pass.
//thats the thing isn't it.
//if I'm not gonna bother generating unique verticies in memory whats the point.
//what I want is, instanced drawing for ui elements.


matrix WorldTransform;
matrix Projection;
//alright so we need to store, 

float3 clippingPosition; //starting position. // should be transformed.

float3 ClippingA; // a minus top corner.
float3 ClippingB; //b minus top corner.

float2 Size;
float Margin;
float4 Color; 
texture Image;
//what is text data I need.
//

sampler GUISampler = sampler_state
{
    Texture = (Image);
    AddressU = clamp;
    AddressV = clamp;
    MagFilter = Linear;
    MinFilter = Linear;
    MipFilter = Linear;
};


struct VertexShaderInput
{
	float4 Position : POSITION0;
	float4 Color : COLOR0;
    float4 DataColor : COLOR1;//color containing only rg colors for corners
    //I don't need normal.
    //I should have uv in case.
    //float2 TexCoord : TEXCOORD0;
    //this should do it for now.
    
};

struct VertexShaderOutput
{
    float4 DummyPos : SV_Position;
	float4 Position : COLOR0;
	float4 Color : COLOR1;
    
    float2 TexCoord : TEXCOORD0;
};

VertexShaderOutput MainVS(in VertexShaderInput input)
{
	VertexShaderOutput output = (VertexShaderOutput)0;
    //hmm. well okay this is pre alignment so its likely in world space as a flat object so whatever
            
    float2 PositionData = input.DataColor.xy;
                                                                           //should be 0 or 1
    float4 ajustedPosition = input.Position + (float4(Size.x - 2, 0, 0, 0) * input.DataColor.x + float4(0, Size.y - 2, 0, 0) * input.DataColor.y);
    
    output.Position = mul(mul(ajustedPosition, WorldViewProjection), Projection);
    output.DummyPos = output.Position;
	output.Color = input.Color;
    
    
	return output;
}

float4 ColorPS(VertexShaderOutput input) : COLOR
{
    //so dot values which is.
    
    float ADot = dot(ClippingA, ClippingA);
    float BDot = dot(ClippingB, ClippingB);
    float PositionDot = dot(clippingPosition, input.Position);
    //greater than zero, less than a dot and b dot.
    clip(PositionDot);
    clip(ADot - PositionDot);
    clip(BDot - PositionDot);
    //alright now that THATS out of the way lets do colors! yayyy
    //hmm. I don't think I want to use vertex color at all since its pretty static.
    //so.
    //hmm okay this is a problem.
    //how do  I set weight data when its order dependant.
    //heck.
    
    
    //alright so FUCK.
    //okay lets go ahead and func in a texture. wait shit so a texture pass and a non texture pass.
    //hmm.
    
    
    
    return Color;
}

technique BasicColorDrawing
{
	pass P0
	{
		VertexShader = compile VS_SHADERMODEL MainVS();
		PixelShader = compile PS_SHADERMODEL ColorPS();
	}
};
﻿using Microsoft.Xna.Framework;
using BaseEngine.Components;
namespace BaseEngine.BepuPhysics
{
    
    public interface IPhysicsModule
    {
        void Add(IBepuPhysicsComponent component);
        void Remove(IBepuPhysicsComponent component);
        void Load(Scene GameScene);
        void Update(GameTime gameTime);
    }

    
    //public interface IBepuPhysicsComponent 
    //{
    //    BEPUphysics.ISpaceObject SpaceBody { get; set; }
    //}
}

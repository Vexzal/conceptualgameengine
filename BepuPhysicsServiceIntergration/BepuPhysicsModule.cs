﻿using Microsoft.Xna.Framework;
using BEPUphysics;
using BaseEngine.Components;
using BEPUutilities.Threading;
namespace BaseEngine.BepuPhysics
{
    public class BepuPhysicsModule : IPhysicsModule
    {
        Space BepuSpace;
        public BepuPhysicsModule()
        {
            BepuSpace = new Space();
            BepuSpace.ForceUpdater.Gravity = new BEPUutilities.Vector3(0, 0, -9.81f);
        }
        public BepuPhysicsModule(IParallelLooper threadLooper)
        {
            BepuSpace = new Space(threadLooper);
            BepuSpace.ForceUpdater.Gravity = new BEPUutilities.Vector3(0, 0, -9.81f);
        }
        public void Add(IBepuPhysicsComponent Component)
        {
            BepuSpace.Add((Component).SpaceBody);
            //throw new NotImplementedException();
        }
        public void Remove(IBepuPhysicsComponent Component)
        {
            BepuSpace.Remove((Component).SpaceBody);
            //throw new NotImplementedException();
        }
        public void Load(Scene GameScene)
        {
            foreach (GameObject go in GameScene.gameObjects)
            {
                foreach (Component c in go.components.Values)
                {
                    if (c is IBepuPhysicsComponent)
                    {
                        Add((IBepuPhysicsComponent)c);
                        break;
                    }
                }
            }
            //throw new NotImplementedException();
        }

        public void Update(GameTime gameTime)
        {
            BepuSpace.Update((float)gameTime.ElapsedGameTime.TotalSeconds);

        }

    }
}

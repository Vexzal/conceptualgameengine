﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

using BEPUphysics;
using BEPUutilities.Threading;
using BaseEngine.Components;
//using BaseEngine.Physics;

namespace BaseEngine.BepuPhysics
{
    public class BepuPhysicsService : Service, IUpdateService //update this
    {
        CoreGame game;//why is this, here. hmm.
                      //public Space physicsSpace { get; private set; }
                      // public IPhysicsModule Module { get; set; }//I don't need this
                      //List<PhysicsComponent> entities = new List<PhysicsComponent>();
        Space BepuSpace;
        public BepuPhysicsService(CoreGame _game)
        {
            game = _game;
            BepuSpace = new Space();
            BepuSpace.ForceUpdater.Gravity = new BEPUutilities.Vector3(0, 0, -9.81f);
            //physicsSpace = new Space();
            //physicsSpace.ForceUpdater.Gravity = new BEPUutilities.Vector3(0, 0, -9.81f);

        }
        public BepuPhysicsService(CoreGame _game, IParallelLooper looper)
        {
            game = _game;
            BepuSpace = new Space(looper);
            BepuSpace.ForceUpdater.Gravity = new BEPUutilities.Vector3(0, 0, -9.81f);
            //physicsSpace = new Space();
            //physicsSpace.ForceUpdater.Gravity = new BEPUutilities.Vector3(0, 0, -9.81f);

        }
        public void Add(IBepuPhysicsComponent Component)
        {
            BepuSpace.Add((Component).SpaceBody);
            //throw new NotImplementedException();
        }
        public void Remove(IBepuPhysicsComponent Component)
        {
            BepuSpace.Remove((Component).SpaceBody);
            //throw new NotImplementedException();
        }
        public override void Load(Scene GameScene)
        {
            foreach (GameObject go in GameScene.gameObjects)
            {
                foreach (Component c in go.components.Values)
                {
                    if (c is IBepuPhysicsComponent)
                    {
                        Add((IBepuPhysicsComponent)c);
                        break;
                    }
                }
            }
        }
        public void Update(GameTime gameTime)
        {
            BepuSpace.Update((float)gameTime.ElapsedGameTime.TotalSeconds);
        }
        delegate void Loading(Space space);
        event Loading LoadScene;

    }
}

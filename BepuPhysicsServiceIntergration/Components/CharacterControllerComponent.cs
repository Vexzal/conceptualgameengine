﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseEngine;
using BaseEngine.Components;
using Microsoft.Xna.Framework;
using BEPUphysics;
using BEPUphysics.Character;
using ConversionHelper;

using BVector2 = BEPUutilities.Vector2;
using BVector3 = BEPUutilities.Vector3;


namespace BaseEngine.BepuPhysics
{
    public class CharacterControllerComponent : Component, IBepuPhysicsComponent
    {
        public ISpaceObject SpaceBody
        {
            get => character;
        }
        public Matrix Offset { get; set; }
        public BVector3 Gravity { get; set; }
        public CharacterController character;
        public TransformComponent transform;
        #region ControllerProperties
        public float height = 1.7f;
        public float crouchHeight = 1.19f;
        public float proneHeight = .51f;
        public float radius = .6f;
        public float margin = 0.1f;
        public float mass = 10;
        public float maximumTractionSlope = .8f;
        public float maximumSupportSlope = 1.3f;
        public float standingSpeed = 8;
        public float crouchingSpeed = 3;
        public float proneSpeed = 1.5f;
        public float traction = 1000;
        public float slidingSpeed = 6;
        public float slidingForce = 50;
        public float airSpeed = 1;
        public float airForce = 250;
        public float jumpSpeed = 4.5f;
        public float slidingJumpSpeed = 3;
        public float maximumGlueForce = 5000;
        #endregion
        //TransformContainer transform;

        //Matrix vOffset;
        //CharacterInput oldInput;
        public CharacterInput input;
        
        //public void Load(Space space)
        //{
        //    space.Add(character);
        //}
        public CharacterControllerComponent()
        {
            Offset = Matrix.Identity; 
            Gravity = new BVector3(0, 0, -9.81f);
            input = new CharacterInput();
            input.characterStance = Stance.Standing;
            input.direction = BVector2.Zero;
            input.jump = false;
        }
        public CharacterControllerComponent(GameObject go):base(go)
        {
            Offset = Matrix.Identity;
            input = new CharacterInput();
            input.characterStance = Stance.Standing;
            input.direction = BVector2.Zero; 
            input.jump = false;
            //character = new CharacterController()
        }
        public override void Initialize()
        {
            transform = gameObject.GetComponent<TransformComponent>();

            character = new CharacterController(MathConverter.Convert(transform.Position),height,
                crouchHeight,proneHeight,radius,margin,mass,maximumTractionSlope,maximumSupportSlope,
                standingSpeed,crouchingSpeed,proneSpeed,traction,slidingSpeed,slidingForce,
                airSpeed,airForce,jumpSpeed,slidingJumpSpeed,maximumGlueForce);

            character.Down = BVector3.Normalize(Gravity);
            //character.Body.Gravity = Gravity;
            character.Body.PositionUpdated += UpdateVisualTransform;
        }

        public void Move(Vector2 Move)
        {
            //input.direction = MathConverter.Convert(Move);
            character.HorizontalMotionConstraint.MovementDirection = MathConverter.Convert(Move);
        }
        public void Jump()
        {
            //input.jump = true;
            character.Jump();
        }
        public void Jump(bool doJump)
        {
            character.TryToJump = doJump;
        }
        public void Crouch()
        {
            character.StanceManager.DesiredStance = Stance.Crouching;
        }
        public void Prone()
        {
            character.StanceManager.DesiredStance = Stance.Prone;
        }
        public void Stand()
        {
            character.StanceManager.DesiredStance = Stance.Standing;
        }
        public void Teleport(Vector3 Location)
        {
            character.Body.Position = MathConverter.Convert(Location);     
            
        }
        public float StandingSpeed
        {
            get { return character.StandingSpeed; }
            set {
                character.StandingSpeed = value; }
        }        
        public void DebugStop()
        {
            int x = 2;
        }
        public void UpdateMotion()
        {
            //oldInput = input;
            character.StanceManager.DesiredStance = input.characterStance;
            character.HorizontalMotionConstraint.MovementDirection = input.direction;
            if (input.jump)
                character.Jump();
        }

        private void UpdateVisualTransform(BEPUphysics.Entities.Entity obj)
        {
            transform.SetTransform(Offset * MathConverter.Convert(character.Body.WorldTransform));
        }
        
    }
    public struct CharacterInput
    {
        public Stance characterStance;
        public BEPUutilities.Vector2 direction;
        public bool jump;        
    }
}

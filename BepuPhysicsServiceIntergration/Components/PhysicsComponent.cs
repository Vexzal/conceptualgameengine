﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BEPUphysics;
using BEPUphysics.Entities;
using BaseEngine.Components;
using Microsoft.Xna.Framework;
using BVector3 = BEPUutilities.Vector3;

namespace BaseEngine.Physics
{
    interface IPhysicsObjectComponent
    {
        void GetBody(Space space);
    }
    
    public abstract class PhysicsComponent : Component
    {
        public abstract ISpaceObject Body { get; }
        public Matrix offset;
        public BVector3 Gravity;
        public TransformComponent transform;
        public PhysicsComponent()
        {
            
        }
        public PhysicsComponent(GameObject go) : 
            base(go)
        {

        }
        public abstract void Load(Space space);
        
    }
}

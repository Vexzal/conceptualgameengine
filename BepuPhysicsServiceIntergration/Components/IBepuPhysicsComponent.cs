﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using BVector3 = BEPUutilities.Vector3;

namespace BaseEngine.Components
{
    public interface IBepuPhysicsComponent
    {
        BEPUphysics.ISpaceObject SpaceBody { get; }
        Matrix Offset { get; set; }
        BVector3 Gravity { get; set; }        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseEngine;
using BaseEngine.Components;
using BEPUphysics;
using BEPUphysics.Entities;
using BEPUphysics.CollisionShapes;
using BVector3 = BEPUutilities.Vector3;
using Microsoft.Xna.Framework;
using ConversionHelper;
namespace BaseEngine.Components
{

    //bepu notes
    //vector cmponent breakdown
    /*
     * right is +x
     * up is +y
     * back is +z
*/
    public class EntityComponent : Component, IBepuPhysicsComponent
    {
        Entity body;
        public ISpaceObject SpaceBody
        {
            get => body;
        }
        public Matrix Offset { get; set; }
        public BVector3 Gravity { get; set; }
        public TransformComponent transform;

        public EntityShape shape;
        
        //Matrix offset; //offset between the visual origin and the physics shape
        //should this be in the mesh instead? since the data is for it. 
        public float mass; 
        public bool dynamic;
        public EntityComponent()
        {
            Offset = Matrix.Identity;
            Gravity = new BVector3(0, 0, -9.81f);
        }
        public EntityComponent(GameObject go):
            base(go)
        {
            Offset = Matrix.Identity;
            Gravity = new BVector3(0, 0, -9.81f);
        }
        /// <summary>
        /// Called after 
        /// </summary>
        public override void Initialize()
        {
            //BEPUphysics.Entities.Prefabs.Box EmptBox = new BEPUphysics.Entities.Prefabs.Box(BVector3.Zero, 1, 1, 1);
            //Entity empttity = new Entity(
            //    new BEPUphysics.CollisionShapes.ConvexShapes.BoxShape(1, 1, 1));
            transform = gameObject.GetComponent<TransformComponent>();
            body = new Entity(shape);
            body.WorldTransform = transform.GetPhysicsTransform();
            body.PositionUpdated += UpdateVisualTransform;
            body.Gravity = Gravity;
            if (dynamic)
                body.Mass = mass;

        }                      
        
        private void UpdateVisualTransform(Entity obj)
        {
            //RetOrientation = new Matrix(
            //obj.WorldTransform.M11, obj.WorldTransform.M12, obj.WorldTransform.M13, obj.WorldTransform.M14,
            //obj.WorldTransform.M21, obj.WorldTransform.M22, obj.WorldTransform.M23, obj.WorldTransform.M24,
            //obj.WorldTransform.M31, obj.WorldTransform.M32, obj.WorldTransform.M33, obj.WorldTransform.M34,
            //obj.WorldTransform.M41, obj.WorldTransform.M42, obj.WorldTransform.M43, obj.WorldTransform.M44);
            //Vector3.Transform(ref RetPosition, ref RetOrientation, out RetPosition);
            //Quaternion.CreateFromRotationMatrix(ref RetOrientation, out RetRotation);

            //transform.SetRotation(RetRotation);
            //Matrix x = MathConverter.Convert(body.WorldTransform);
            var x = obj.Orientation;
            transform.SetTransform(Offset * MathConverter.Convert(obj.WorldTransform));
            
        }

        //public override void Load(Space space)
        //{
        //    space.Add(body);            
        //}
    }
}

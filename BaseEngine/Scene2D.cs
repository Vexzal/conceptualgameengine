﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using BaseEngine.Components;
using System.Collections.Specialized;

namespace BaseEngine
{
    public class CanvasComponent
    {
        Scene2D scene;//I don't like this.
        //a canvas renders to a plane, that can be parented to like a camera or whatever. 
    }
    class Scene2D
    {
        List<GameObject> Collection = new List<GameObject>();
    }
    //what I want is as collection of 2d stuff like this so now also, components I guess.
    //class GameObject2D : INode<GameObject2D>
    //{
    //    public string name;
    //    public GameObject2D Parent { get; private set; }
    //    public List<GameObject2D> Children { get; private set; }
        
    //    public GameObject2D

    //}

    //I...I don't actually want anchored elements for tool guis?
    //I didn't think it through but thats largely game stuff.
    //but like the loose floaty windows is what I want though right??
    //so I'm gonna refigure the Imgui control considering that
    //because what was holding me up was no comprehensive way of managing anchors and margins.
    //you have the two seperate controls.
    public class Object2D
    {
        Vector2 Position;
        Vector2 Size;
        //Vector2 TopAnchor;
        //Vector2 BotAnchor;
        Object2D Parent;
        Texture2D Texture;
        Rectangle Bounds;
    }
    public class TransformComponent2D : Component
    {
        Vector2 Position;
        double Rotation;
        Vector2 Scale;

        //hmm
    }
    //okay lets, set here the 
    //accessing basic info from here.
    //actually if I could ask 
    //public static class MemoryEngine
    //{
    //    public static Game ActiveGame;
    //}
    public class SpriteComponent : Component
    {
        public  Texture2D Sprite;
        public int Width
        {
            get => Sprite.Width;
        }
        public int Height
        {
            get => Sprite.Height;  
        }
        /// <summary>
        /// Origin Alignment
        /// </summary>
        Vector2 Alignment;

        public float AlignmentX
        {
            get
            {
                return Alignment.X;
            }
            set
            {
                Alignment.X = MathHelper.Clamp(value, 0, 1);
            }
        }
        public float AlignmentY
        {
            get
            {
                return Alignment.Y;
            }
            set
            {
                Alignment.Y = MathHelper.Clamp(value, 0, 1);
            }
        }
        public void SetAlignment(Vector2 value)
        {
            Alignment = new Vector2(MathHelper.Clamp(value.X, 0, 1), MathHelper.Clamp(value.Y, 0, 1));
        }
        

        //hm.     
        //alright what if. what if we just. had a sprite thing that managed sprites like 

        public override void Initialize()
        {                        

            base.Initialize();
        }
    }
    //OHHH

    public class MemoryWindow
    {
        public Point Position;
        public Point Size;
        Point MinSize;
        Vector2 ContentSize;

        public Vector4 Margins;
        Point UpperMargin;
        Point LowerMargin;
        Rectangle ClippingRectangle;
        Rectangle ContentsRange;
        //{
        //    get => new Rectangle(Position + new Point((int)Margins.X, (int)Margins.Y), Size + new Point((int)Margins.Z, (int)Margins.W));//I don't like this
        //}
        internal void ClipSize()
        {
            if(Size.X < MinSize.X)            
                Size.X = MinSize.X;
            
            if (Size.Y < MinSize.Y)
                Size.Y = MinSize.Y;
        }
            /// <summary>
            /// for internal use  to update Clipping rectangle
            /// </summary>
        internal void UpdateRectangle()
        {
            
            ClippingRectangle = new Rectangle(Position, Size);
            ContentsRange = new Rectangle(Position + UpperMargin, Size - LowerMargin);
        }


        /// <summary>
        /// idea is to use this to keep track of widget height
        /// </summary>
        public float Height;
        //then, elements?

    }

    //alright how about storing vertex data in a style container
    //getting that vertex data with passing width height components
    //then getting positioning from parents.
    //hmm.
    //a focus isn't changing within a frame right?
    //so using the windows focus depth, for positioning on widgets on frame
    //should be fine.
    class MemoryDrawCommand
    {
        string styleID;
        Rectangle clippingRectangle;
    }
    struct Widget
    {

    }

    public class IMGUICanvasComponet : Component
    {
        //so like have imgui canvas as a component, that you access through scripting.
        //int hot;
        //int active
        string Hot;
        string Active;
        Point MousePos;

  


        //okay so there are two collections
        //the list and the dictionary.
        //dictionary is used to access 
        MemoryWindow ActiveWindow;

        public List<MemoryWindow> DrawList; //figure out what I'm actually adding here. some kind of primitive.
        //then some list of drawing commands.
        //what does that , need?
        
        public Dictionary<string, MemoryWindow> Windows;//gui elements.
        //public OrderedDictionary Windows2;
        //ahhh this is complicated doing focusing means sorting of some kind.
        //fortunately it shouldn't be complicated, just move all associated gui elements to front but, hmm
        public IMGUICanvasComponet()
        {
             
            
        }
        void Focus(string id)
        {
            var focus = Windows[id];
            //Windows.Remove(id);
            DrawList.Remove(focus);
            DrawList.Insert(0, focus);

        }
        public void SetPosition(Point Position)
        {
            ActiveWindow.Position = Position;
            throw new NotImplementedException();
            
        }
        public void SetSize(Point Size)
        {
            ActiveWindow.Size = Size;
            throw new NotImplementedException();
        }
        public void Begin(string id)
        {
            if (!Windows.ContainsKey(id))
            {
                Windows.Add(id, new MemoryWindow());
                DrawList.Add(Windows[id]);
            }
            ActiveWindow = Windows[id];
                
        }
        public void End(string id)
        {
            ActiveWindow = null;
            throw new NotImplementedException();
        }


    }


    /**kinds of 2d objects and how you want to draw them
    *sprites. 
    *   -Need linear upscaling and 1-1 drawing.
    *   -uv wrapping would work here I think.
    *   -but that'd be like optional for large tiles.
    *   -eugh this is so hard >:(
    *Okay so uh
    * also ui components which
    *   scale with margins
    *   ordering and logical components.
    *   okay let me. back up.
    *   how does this work with 
     */   

    //alright lets do something because I hate myself.
    //what if I have seperate structures for buildin the game and storing the game.
    //like convert everything to game objects at runtime but before that just, have seperate trees of stuff
}

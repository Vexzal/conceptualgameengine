﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using BaseEngine.Rendering;
//using BaseEngine.Physics;
using BaseEngine.Components;
//using BaseEngine.Input;
using BaseEngine.InputPolling;


namespace BaseEngine
{
    
    public interface IRenderingService
    {
        void Draw(GameTime gameTime);
    }
    public interface IUpdateService
    {
        void Update(GameTime gameTime);
    }
    public class ConfigManager //core config file containing definitions for other config files.
    {
        public LoadDevices deviceLoader;
        //so we have.
    }
    public static class defaultConfig
    {
       public static ConfigManager GetManager()
        {
            ConfigManager ret = new ConfigManager();
            //ret.deviceLoader = new LoadDevices(void()=>{ MemoryInput.AddDevice(new MemoryKeyboard); });
            //ret.deviceLoader = DeviceLoad => { MemoryInput.AddDevice(new MemoryKeyboard)}
            return ret;
        }
        static void LoadDevices()
        {

        }
    }

    public delegate void LoadDevices();
    public partial class CoreGame : Game//OHHH SHIT//can't fix that now but fix it soon //what the fuck was this about
    {
        public Scene gameScene;
        public SpriteBatch spriteBatch;
        public GraphicsDeviceManager graphics;
        //public InputHandler Handler;
        
        //public RenderingService renderer;
        public ContentManager GlobalContent;
        public dynamic EngineServices = new ServiceManager();
        //public PhysicsModule physics;
        //physics is not integrated into  the core game
        LoadDevices deviceLoader;
        //PHYSICS IS NOT INTEGRATED INTO THE CORE GAME;
        //FIX THAT FIRST;

        //byte PreTick = 0;
        //byte GameTick = 127;
        //byte LateTick = 255;//this aint gonna do it
        event GameTickDelegate EngineTick;
        event GameTickDelegate PreTick;
        event GameTickDelegate GameTick;  
        event GameTickDelegate LateTick;
        event GameTickDelegate DrawTick;
        List<GameTickDelegate> gameTickEvents = new List<GameTickDelegate>();//this could just be an array.//actually this could be removed alltogether what if logic management was its own service thing.
        //EUGHH NO BECAUSE HECK. HEECK 
        //services contain rendering and updating stuff so I need to divy them up somewhere.
        //I'll create.
        //so in pulling in services I'll put in over rides for update and draw stuff.
        //oookay so like
        //though its better to keep it as a list for now and convert to an array at load time with, wiggling for people adding update evens at runtime for whatever reason

        public CoreGame(/*string configPath*/)
        {
            var ConfigManager = new ConfigManager();//replace with loader

            deviceLoader = ConfigManager.deviceLoader;
            deviceLoader.Invoke();//so as a default

            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            GlobalContent.RootDirectory = "Content";
            //Handler = new InputHandler();
            //PreTick += Handler.Update;
            
            gameTickEvents.Add(PreTick);
            gameTickEvents.Add(GameTick);
            gameTickEvents.Add(LateTick);
            //renderer = new RenderingService(this);

            //physics = new PhysicsModule(this);
        }
        protected override void Initialize()
        {
            //renderer.gameScene = gameScene;
            //physics.gameScene = gameScene;//why do this why not just load game scene through the parameter.
            base.Initialize();
        }
        protected override void LoadContent()//this. should have updating shouldnt it.
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
                        
            //renderer.Load();
            //physics.Load();
            base.LoadContent();
        }


        protected override void Update(GameTime gameTime)
        {
            
            foreach(IUpdateService service in EngineServices.Updatables)
            {
                service.Update(gameTime);
            }
            base.Update(gameTime);
        }
        protected override void Draw(GameTime gameTime)
        {
            foreach(IRenderingService service in EngineServices.Drawables)
            {
                //service.Draw(gameTime);
            }
            base.Draw(gameTime);
        }

        //public event GameTickDelegate GameTick;//called once every frame in update;
        //public event GameTickDelegate LateTick;//called after gametick every frame in update //what do you do with this.
        //so we want. Waant. actually if its just managing updates we need a game tick update collection and we can add x alt etc
        //so like





    }
    public delegate void GameTickDelegate(GameTime gameTime);
}

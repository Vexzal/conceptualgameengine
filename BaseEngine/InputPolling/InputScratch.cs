﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using System;

namespace BaseEngine.InputPolling
{
    
    public interface IInputDevice
    {
        string DeviceName { get; }
        void Update();
        //I..only really care about this with mouse buttons huh.
        //ButtonState GetState(IDeviceBinding binding);
        bool ButtonDown(IDeviceBinding binding);
        //bool ButtonPressed(IDeviceBinding binding);
        //bool ButtonReleased(IDeviceBinding binding);
        double Axis(IDeviceBinding binding);
    }

    public interface IDeviceBinding
    {
        string Device { get; }
        double AxisScale { get; }
        //key is defined in specific scope.
    }
    public enum MouseAux
    {
        MouseWheel,
        MouseX,
        MouseY
    }
    public class MouseButtonBinding : IDeviceBinding
    {
        public string Device => "Mouse";
        public MouseButton Button;
        public double AxisScale { get; private set; }
        public MouseButtonBinding(MouseButton button)
        {
            Button = button;
            AxisScale = 0;
        }
        public MouseButtonBinding(MouseButton button, double axisScale)
        {
            Button = button;
            AxisScale = axisScale;
        }

    }
    public class MouseAxisBinding : IDeviceBinding
    {
        public string Device => "Mouse";
        public MouseAux MouseAux;
        public double AxisScale { get; private set; }
        public MouseAxisBinding(MouseAux aux)
        {
            MouseAux = aux;
            AxisScale = 0;
        }
        public MouseAxisBinding(MouseAux aux,double scale)
        {
            MouseAux = aux;
            AxisScale = scale;
        }
    }
    public class MouseDevice : IInputDevice
    {
        MouseState oldState;
        MouseState state;
        public string DeviceName => "Mouse";
        public void Update()
        {
            oldState = state;
            state = Mouse.GetState();
        }
        public ButtonState GetState(MouseButton button)
        {

            switch (button)
            {
                case MouseButton.Left:
                    return state.LeftButton;
                case MouseButton.Right:
                    return state.RightButton;
                case MouseButton.Middle:
                    return state.MiddleButton;
                case MouseButton.xButton1:
                    return state.XButton1;
                case MouseButton.xButton2:
                    return state.XButton2;
                default:
                    return ButtonState.Released;
            }
            
        }
        public bool ButtonDown(IDeviceBinding binding)
        {
            if(binding is MouseButtonBinding)
            {
                switch((binding as MouseButtonBinding).Button)
                {
                    case MouseButton.Left:
                        return state.LeftButton == ButtonState.Pressed;
                    case MouseButton.Right:
                        return state.RightButton == ButtonState.Pressed;
                    case MouseButton.Middle:
                        return state.MiddleButton == ButtonState.Pressed;
                    case MouseButton.xButton1:
                        return state.XButton1 == ButtonState.Pressed;
                    case MouseButton.xButton2:
                        return state.XButton2 == ButtonState.Pressed;
                    default:
                        return false;
                }
            }
            else
            {
                switch((binding as MouseAxisBinding).MouseAux)
                {
                    case MouseAux.MouseWheel:
                        return state.ScrollWheelValue - oldState.ScrollWheelValue != 0;
                    case MouseAux.MouseX:
                        return state.X - oldState.X != 0;
                    case MouseAux.MouseY:
                        return state.Y - oldState.Y != 0;
                    default:
                        return false;
                        
                }
            }
        }
        public double Axis(IDeviceBinding binding)
        {
            if(binding is MouseButtonBinding)
            {
                var b = binding as MouseButtonBinding;
                switch(b.Button)
                {
                    case MouseButton.Left:
                        return (double)state.LeftButton * b.AxisScale;
                    case MouseButton.Right:
                        return (double)state.RightButton * b.AxisScale;
                    case MouseButton.Middle:
                        return (double)state.MiddleButton * b.AxisScale;
                    case MouseButton.xButton1:
                        return (double)state.XButton1 * b.AxisScale;
                    case MouseButton.xButton2:
                        return (double)state.XButton2 * b.AxisScale;
                    default:
                        return 0;
                }
            }
            else if(binding is MouseAxisBinding)
            {
                var b = binding as MouseAxisBinding;
                switch(b.MouseAux)
                {
                    case MouseAux.MouseWheel:
                        return (state.ScrollWheelValue - oldState.ScrollWheelValue) * b.AxisScale;
                    case MouseAux.MouseX:
                        return (state.X - oldState.X) * b.AxisScale;
                    case MouseAux.MouseY:
                        return (state.Y - oldState.Y) * b.AxisScale;
                    default:
                        return 0;
                }
            }
            return 0;
        }
        public Point Position => state.Position;
        

    }

    public class KeyboardBinding : IDeviceBinding
    {
        public string Device => "Keyboard";
        public Keys Key;
        public double AxisScale { get; private set; }
        public KeyboardBinding(Keys key)
        {
            Key = key;
            AxisScale = 0;
        }
        public KeyboardBinding(Keys key,float scale)
        {
            Key = key;
            AxisScale = scale;
        }
    }
    public class MemoryKeyboard : IInputDevice
    {
        KeyboardState oldKeyState;
        KeyboardState keyState;

        public void Update()
        {
            oldKeyState = keyState;
            keyState = Keyboard.GetState();
        }
        public string DeviceName => "Keyboard";

        public ButtonState GetState(IDeviceBinding binding)
        {
                return (keyState.IsKeyDown(((KeyboardBinding)binding).Key)) ? ButtonState.Pressed : ButtonState.Released;            
        }
        public bool ButtonDown(IDeviceBinding binding)
        {
            return keyState.IsKeyDown(((KeyboardBinding)binding).Key);
        }
        public bool ButtonPressed(IDeviceBinding binding)
        {
            var Key = ((KeyboardBinding)binding).Key;
            return keyState.IsKeyDown(Key) && oldKeyState.IsKeyUp(Key);
        }
        public bool ButtonReleased(IDeviceBinding binding)
        {
            var Key = ((KeyboardBinding)binding).Key;
            return keyState.IsKeyUp(Key) && oldKeyState.IsKeyDown(Key);
        }
        public double Axis(IDeviceBinding binding )  
        {            
            return ((keyState.IsKeyDown(((KeyboardBinding)binding).Key) ? 1 : 0)) * ((KeyboardBinding)binding).AxisScale;            
        }
    }
    

    public class JoyconButton : IDeviceBinding
    {
        public string Device => "Joycon" + Index;
        JoyconIndex Index;
        public JoyconButtons Button;
        public double AxisScale { get; private set; }
        public JoyconButton(JoyconButtons button )
        {
            Button = button;
            AxisScale = 0;
        }
        public JoyconButton(JoyconButtons button,double AxisScale)
        {
            AxisScale = 0;
            Button = button;
        }
    }
    public enum JoyconAux
    {
        JoystickX,
        JoystickY,
        GyroX,
        GyroY,
        GyroZ,
        AccelX,
        AccelY,
        AccelZ
    }
    public class JoyconAxis : IDeviceBinding
    {
        public string Device => "Joycon" + Index;
        JoyconIndex Index;
        public JoyconAux Axis;
        public double AxisScale { get; private set; }
        public JoyconAxis(JoyconAux button)
        {
            Axis = button;
            AxisScale = 0;
        }
        public JoyconAxis(JoyconAux button, double AxisScale)
        {
            AxisScale = 0;
            Axis = button;
        }
    }
    public class MemoryJoycon : IInputDevice
    {
        JoyconIndex Index;
        public string DeviceName => "Joycon" + Index;
        JoyconState state;
        JoyconState oldState;


        public MemoryJoycon(JoyconIndex index)
        {
            Index = index;
        }
        public void Update()
        {
            oldState = state;
            state = Joycon.GetState(Index);
            
        }
        public bool ButtonDown(IDeviceBinding binding)
        {
            if(binding is JoyconButton)
            {
                return state.IsButtonDown(((JoyconButton)binding).Button);
            }
            if(binding is JoyconAxis)
            {
                var axis = ((JoyconAxis)binding).Axis;
                switch(axis)
                {
                    case JoyconAux.JoystickX:
                        return state.ThumbStick.X != 0;
                    case JoyconAux.JoystickY:
                        return state.ThumbStick.Y != 0;
                    case JoyconAux.AccelX:
                        return state.Acclerometer.x != 0;
                    case JoyconAux.AccelY:
                        return state.Acclerometer.y != 0;
                    case JoyconAux.AccelZ:
                        return state.Acclerometer.z != 0;
                    case JoyconAux.GyroX:
                        return state.Gyroscope.pitch != 0;
                    case JoyconAux.GyroY:
                        return state.Gyroscope.roll != 0;
                    case JoyconAux.GyroZ:
                        return state.Gyroscope.yaw != 0;
                    default:
                        return false;
                } 
            }
            return false;
        }
        public double Axis(IDeviceBinding binding)
        {
            if(binding is JoyconButton)
            {
                return state.IsButtonDown(((JoyconButton)binding).Button) ? 1 * binding.AxisScale : 0;
            }
            if(binding is JoyconAxis)
            {
                var axis = ((JoyconAxis)binding).Axis;
                switch(axis)
                {
                    case JoyconAux.JoystickX:
                        return state.ThumbStick.X * binding.AxisScale;
                    case JoyconAux.JoystickY:
                        return state.ThumbStick.Y * binding.AxisScale;
                    case JoyconAux.AccelX:
                        return state.Acclerometer.x * binding.AxisScale;
                    case JoyconAux.AccelY:
                        return state.Acclerometer.y * binding.AxisScale;
                    case JoyconAux.AccelZ:
                        return state.Acclerometer.z * binding.AxisScale;
                    case JoyconAux.GyroX:
                        return state.Gyroscope.pitch * binding.AxisScale;
                    case JoyconAux.GyroY:
                        return state.Gyroscope.roll * binding.AxisScale;
                    case JoyconAux.GyroZ:
                        return state.Gyroscope.yaw * binding.AxisScale;
                    default:
                        return 0;
                } 
            }
            return 0;
        }

        public Vector2 ThumbStick => state.StickValue();
        public Vector3 Gyroscope => state.GetGyroVec3();
        public Vector3 Accelerometer => state.Acclerometer.Component;
    }

    public static class MemoryInput 
    {
        //device library
        static Dictionary<string, IInputDevice> deviceLibrary = new Dictionary<string, IInputDevice>();
        static Dictionary<string, IDeviceBinding[]> bindingAliases = new Dictionary<string, IDeviceBinding[]>();
         //I don't remember why I wanted this originally. I don't now if i need to store doubles and bools;
        static Dictionary<string, bool> aliasStorage = new Dictionary<string, bool>();
        public static void AddDevice(IInputDevice device)
        {
            if(!deviceLibrary.ContainsKey(device.DeviceName))
                deviceLibrary.Add(device.DeviceName, device);
        }
        public static void AddBinding(string alias, params IDeviceBinding[] bindings )
        {
            if (!bindingAliases.ContainsKey(alias))
                bindingAliases.Add(alias, bindings);
            else
                bindingAliases[alias] = bindings;
        }

        public static T GetDevice<T>(string Name) where T: IInputDevice
        {
            if(deviceLibrary.ContainsKey(Name))
            {
                return (T)deviceLibrary[Name];
            }
            return default(T);//this will probably return null but thats fine.
        }

        // can still get released status by comparing alias stored values.
        //so store last frame value for an alias, check if they're different this frame, and if so you got a press.
        public static bool GetButtonDown(string alias)
        {
            //throw new System.NotImplementedException("add frame storage for results");
            bool ret = false;

            IDeviceBinding[] bindings;
            IInputDevice Device;
            if (bindingAliases.TryGetValue(alias, out bindings))
            {
                for (int i = 0; i < bindings.Length && !ret; i++)
                {
                    if (deviceLibrary.TryGetValue(bindings[i].Device, out Device))
                    {
                        ret |= Device.ButtonDown(bindings[i]);
                    }
                }
            }
            
            aliasStorage[alias] = ret;
            return ret;
        }
        public static bool ButtonPressed(string alias)
        {
            
            bool oldValue;
            if (aliasStorage.TryGetValue(alias, out oldValue))
            {
                return (GetButtonDown(alias) && !oldValue);
            }
            else
            {
                aliasStorage[alias] = false;
                return false;
            }

        }
        public static bool ButtonReleased(string alias)
        {
            bool oldValue;
            if(aliasStorage.TryGetValue(alias, out oldValue))
            {
                return (!GetButtonDown(alias) && oldValue);
            }
            else
            {
                aliasStorage[alias] = false;
                return false;
            }
        }
        public static double GetAxis(string alias)
        {
            double ret = 0;
            IDeviceBinding[] bindings;
            IInputDevice Device;
            if(bindingAliases.TryGetValue(alias,out bindings))
            {
                for (int i = 0; i < bindings.Length; i++)
                {
                    var active = bindings[i];
                    if (active.AxisScale != 0)
                    {
                        //I should probably manage devices better. so  Idon't check for inactive device inputs;
                        if (deviceLibrary.TryGetValue(active.Device, out Device))
                        {
                            //throw new System.Exception("Modify Math Helper to work with doubles");
                            if(active.AxisScale > 0)
                            {
                                //ret = MathHelper.Max((float)ret, (float)Device.Axis(active));
                                ret = Math.Max(ret, Device.Axis(active));                                
                            }
                            else
                            {
                                //ret = MathHelper.Min((float)ret, (float)Device.Axis(active));
                                ret = Math.Min(ret, Device.Axis(active));
                            }
                        }
                    }
                }
                //return ret;
            }
            return ret;
        }


    }
}

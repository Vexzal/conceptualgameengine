﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
namespace BaseEngine
{
    public static class MemoryEngine
    {
        static CoreGame Game;
        public static void SetGame(CoreGame game)
        {
            Game = game;
        }

        public static GraphicsDevice GraphicsDevice => Game.GraphicsDevice;
        
    }
}

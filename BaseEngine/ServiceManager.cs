﻿using System;
using System.Dynamic;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseEngine.Services;
using System.Collections;

namespace BaseEngine
{
    //this will keep a list of all the services like rendering and audio or physics ai etc
    public class ServiceManager : DynamicObject /*,IEnumerable<Service>, *//*,IEnumerable<IUpdateService>, IEnumerable<IRenderingService>*/
    {
        // public List<Services.Service> services = new List<Services.Service>();

        //Dictionary<string, object> dictonary = new Dictionary<string, object>();
        List<IRenderingService> _Drawables = new List<IRenderingService>();
        List<IUpdateService> _Updatables = new List<IUpdateService>();
        //List<Service> services = new List<Service>();
        Dictionary<string, object> Services = new Dictionary<string, object>(); 

        //public Service this[string name]
        //{
        //    get
        //    {
        //        Service ret; 
        //        if (Services.TryGetValue(name, out ret))
        //            return ret;
        //        return null;
        //    }
        //    set
        //    {
        //        Services[name] = value;
        //        if (value is IUpdateService)
        //            if (!Updatables.Contains(value as IUpdateService))
        //                Updatables.Add(value as IUpdateService);

        //        if (value is IRenderingService)
        //            if (!Drawables.Contains(value as IRenderingService))
        //                Drawables.Add(value as IRenderingService);
        //    }
        //}
        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            string name = binder.Name.ToLower();
            return Services.TryGetValue(name, out result);
        }
        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            Services[binder.Name.ToLower()] = value;
            if(value is IUpdateService)
            {
                if(!_Updatables.Contains((IUpdateService)value))
                {
                    _Updatables.Add((IUpdateService)value);
                }

            }
            if (value is IRenderingService)
            {
                if (!_Drawables.Contains((IRenderingService)value))
                {
                    _Drawables.Add((IRenderingService)value);
                }

            }
            return true;
        }

        public int Count
        {
            get
            {
                return Services.Count;
            }
        }

        //public IEnumerator<Service> GetEnumerator()
        //{
        //    return ((IEnumerable<Service>)Services.Values).GetEnumerator();
        //}

        //public Service GetService()
        //{
        //    throw new NotImplementedException();
        //}

        //IEnumerator IEnumerable.GetEnumerator()
        //{
        //    return Services.GetEnumerator();
        //}

        public IEnumerable<IUpdateService> Updatables
        {
            //return ((IEnumerable<IUpdateService>)_Updatables).GetEnumerator();
            get => _Updatables;
        }
        public IEnumerable<IRenderingService> Drawables
        {
            //return ((IEnumerable<IRenderingService>)_Drawables).GetEnumerator();
            get => _Drawables;
        }
        //IEnumerator<IUpdateService> IEnumerable<IUpdateService>.GetEnumerator()
        //{
        //    return ((IEnumerable<IUpdateService>)Updatables).GetEnumerator();
        //}

        //IEnumerator<IRenderingService> IEnumerable<IRenderingService>.GetEnumerator()
        //{
        //    return ((IEnumerable<IRenderingService>)Drawables).GetEnumerator();
        //}
    }
}

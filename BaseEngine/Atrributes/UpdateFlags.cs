﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseEngine.Logic
{
    [AttributeUsage(AttributeTargets.Method)]
    public class Updatable : Attribute 
    {
        public int Order { get; set; }
        UpdateStage Stage { get; set; }
        public Updatable(UpdateStage stage)
        {
            Stage = stage;
            Order = 0;
        }
        public Updatable(UpdateStage stage, int order)
        {
            Stage = stage;
            Order = order;
        }
    }
    public enum UpdateStage
    {
        /// <summary>
        /// WARNING: For system and engine constants, be careful 
        /// </summary>
        SystemPolling,
        Update,
        LateUpdate,
        PostUpdate
    }
}

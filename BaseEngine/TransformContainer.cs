﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using ConversionHelper;

namespace BaseEngine.Components
{
    public class TransformContainerOld
    {
        //position
        float x;
        float y;
        float z;

        //rotation
        Quaternion rotation;

        //scale 
        float sx;
        float sy;
        float sz;

        public TransformContainerOld()
        {
            x = 0;
            y = 0;
            z = 0;

            rotation = Quaternion.Identity;

            sx = 1;
            sy = 1;
            sz = 1;
        }
        
        public Vector3 GetVector()
        {
            return new Vector3(x, y, z);
        }
        public Matrix GetWorld()
        {
            return Matrix.CreateScale(sx, sy, sz) * Matrix.CreateFromQuaternion(rotation) * Matrix.CreateTranslation(x, y, z);
        }
        public BEPUutilities.Matrix GetPhysicsTransform()
        {
            Matrix ret = Matrix.CreateFromQuaternion(rotation) * Matrix.CreateTranslation(x, y, z);
            return new BEPUutilities.Matrix(
                ret.M11, ret.M12, ret.M13, ret.M14,
                ret.M21, ret.M22, ret.M23, ret.M24,
                ret.M31, ret.M32, ret.M33, ret.M34,
                ret.M41, ret.M42, ret.M43, ret.M44);
        }
        public void SetPosition(float _x, float _y, float _z)
        {
            x = _x;
            y = _y;
            z = _z;
        }
        public void SetRotation(Quaternion rot)
        {
            rotation = rot;
            //Vector3 mVector;
            //BEPUutilities.Vector3 bVector;
            //bVector = BEPUutilities.Vector3.Zero;
            //mVector = (Vector3)bVector;

            //Matrix mMatrix;
            //BEPUutilities.Matrix bMatrix;
            //bMatrix = BEPUutilities.Matrix.Identity;
            //mMatrix = (Matrix)bMatrix;
        }
        public void SetScale(float _sx, float _sy, float _sz)
        {
            sx = _sx;
            sy = _sy;
            sz = _sz;
        }
    }

    public class TransformContainerOld2
    {
        
        Vector3 Position;
        Vector3 Euler;
        Quaternion Rotation;
        Vector3 Scale;
        public Matrix Transform;

        public Vector3 position
        {
            get {
                return new Vector3(Transform.M41, Transform.M42, Transform
              .M43);
            }
            set
            {
                Transform.M41 = value.X;
                Transform.M42 = value.Y;
                Transform.M43 = value.Z;
            }
        }
        public Quaternion rotation
        {
            get {
                Vector3 xHat = Vector3.Normalize(Transform.Right);
                Vector3 yHat = Vector3.Normalize(Transform.Up);
                Vector3 zHat = Vector3.Normalize(Transform.Backward);
                Matrix rotMatrix = new Matrix(
                    new Vector4(xHat, 0),
                    new Vector4(yHat, 0),
                    new Vector4(zHat, 0),
                    new Vector4(Vector3.Zero, 1));

                return Quaternion.CreateFromRotationMatrix(rotMatrix); }
            
        }
        public Vector3 scale
        {
            get
            {
                return new Vector3(
               Transform.Right.Length(), Transform.Up.Length(), Transform.Backward.Length());
            }            
        }
        

        public TransformContainerOld2()
        {
            //position = Vector3.Zero;
            //rotation = Quaternion.Identity;
            //scale = Vector3.One;

            Transform = Matrix.Identity;
            
        }
        public Matrix GetWorld()
        {
            return Transform;
        }
        public void SetTransform(Matrix m)
        {
            Transform = m;
        }
        public BEPUutilities.Matrix GetPhysicsTransform()
        {
            Vector3 xHat = Vector3.Normalize(Transform.Right);
            Vector3 yHat = Vector3.Normalize(Transform.Up);
            Vector3 zHat = Vector3.Normalize(Transform.Backward);
            Matrix rotMat = new Matrix(
                new Vector4(xHat, 0),
                new Vector4(yHat, 0),
                new Vector4(zHat, 0),
                new Vector4(position, 1));
            return MathConverter.Convert(rotMat);
        }
        
        
    }

    public class TransformContainer
    {
        Vector3 _position;
        Quaternion _rotation;
        Vector3 _scale;
        public Vector3 position
        {
            get => _position;
            set => _position = value;
        }
        public Quaternion rotation
        {
            get => _rotation;
            set => _rotation = value;
        }
        public Vector3 scale
        {
            get => _scale;
            set => _scale = value;
        }
        public Matrix Transform
        {
            get => Matrix.CreateScale(_scale) * Matrix.CreateFromQuaternion(_rotation) * Matrix.CreateTranslation(_position);
        }
        
        public TransformContainer()
        {
            position = Vector3.Zero;
            rotation = Quaternion.Identity;
            scale = Vector3.One;
        }
        public Matrix GetWorld()
        {
            return Transform;
        }
        public void SetTransform(Matrix m)
        {
            //Vector3 translation = Matrix.CreateTranslation(m.M41, m.M42, m.M43);
            //Matrix rotation = GetRotationMatrix(m);
            position = new Vector3(m.M41, m.M42, m.M43);
            rotation = Quaternion.CreateFromRotationMatrix(GetRotationMatrix(m));
            scale = GetScale(m);
        }
        private Vector3 GetScale(Matrix M)
        {
            Vector3 iHat = new Vector3(M.M11, M.M12, M.M13);
            Vector3 jHat = new Vector3(M.M21, M.M22, M.M23);
            Vector3 kHat = new Vector3(M.M31, M.M32, M.M33);

            return new Vector3(iHat.Length(), jHat.Length(), kHat.Length());
        }
        private Matrix GetRotationMatrix(Matrix m)
        {
            Vector3 iHat = Vector3.Normalize(new Vector3(m.M11, m.M12, m.M13));
            Vector3 jHat = Vector3.Normalize(new Vector3(m.M21, m.M22, m.M23));
            Vector3 kHat = Vector3.Normalize(new Vector3(m.M31, m.M32, m.M33));

            return new Matrix(
                new Vector4(iHat, 0),
                new Vector4(jHat, 0),
                new Vector4(kHat, 0),
                new Vector4(0, 0, 0, 1));

        }
        
        public BEPUutilities.Matrix GetPhysicsTransform()
        {
            Matrix rotMatrix = GetRotationMatrix(Transform);
            rotMatrix.Translation = _position;
            return MathConverter.Convert(rotMatrix);

        }
    }
}

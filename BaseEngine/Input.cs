﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace BaseEngine
{
    public static class ObsoleteInput
    {
        private static CoreGame _game;
        private static Point Center;
        private static KeyboardState oldKeyState;
        private static KeyboardState newKeyState;

        private static MouseState oldMouseState;
        private static MouseState newMouseState;

        public static bool CenterMouse = true;

        public static void Initialize(CoreGame game)
        {
            _game = game;
            Center = new Point(_game.graphics.PreferredBackBufferWidth / 2, _game.graphics.PreferredBackBufferHeight / 2);
            oldKeyState = newKeyState;
            newKeyState = Keyboard.GetState();
            oldMouseState = newMouseState;
            newMouseState = Mouse.GetState();
        }
        public static void Update()
        {
            oldKeyState = newKeyState;
            newKeyState = Keyboard.GetState();
            oldMouseState = newMouseState;
            newMouseState = Mouse.GetState();
        }

        public static bool KeyPressed(Keys key)
        {
            return oldKeyState.IsKeyUp(key) && newKeyState.IsKeyDown(key);
        }
        public static bool KeyReleased(Keys key)
        {
            return oldKeyState.IsKeyDown(key) && newKeyState.IsKeyUp(key);
        }
        public static bool KeyUp(Keys key)
        {
            return newKeyState.IsKeyUp(key);
        }
        public static bool KeyDown(Keys key)
        {
            return newKeyState.IsKeyDown(key);
        }
        public static bool MouseClick(MouseButton button)
        {
            switch (button)
            {
                case MouseButton.Left:
                    return oldMouseState.LeftButton == ButtonState.Released && newMouseState.LeftButton == ButtonState.Pressed;
                case MouseButton.Right:
                    return oldMouseState.RightButton == ButtonState.Released && newMouseState.RightButton == ButtonState.Pressed;
                case MouseButton.Middle:
                    return oldMouseState.MiddleButton == ButtonState.Released && newMouseState.MiddleButton == ButtonState.Pressed;
                case MouseButton.xButton1:
                    return oldMouseState.XButton1 == ButtonState.Released && newMouseState.XButton1 == ButtonState.Pressed;
                case MouseButton.xButton2:
                    return oldMouseState.XButton2 == ButtonState.Released && newMouseState.XButton2 == ButtonState.Pressed;
            }
            return false;
        }
        public static bool MouseRelease(MouseButton button)
        {
            switch (button)
            {
                case MouseButton.Left:
                    return oldMouseState.LeftButton == ButtonState.Pressed && newMouseState.LeftButton == ButtonState.Released;
                case MouseButton.Right:
                    return oldMouseState.RightButton == ButtonState.Pressed && newMouseState.RightButton == ButtonState.Released;
                case MouseButton.Middle:
                    return oldMouseState.MiddleButton == ButtonState.Pressed && newMouseState.MiddleButton == ButtonState.Released;
                case MouseButton.xButton1:
                    return oldMouseState.XButton1 == ButtonState.Pressed && newMouseState.XButton1 == ButtonState.Released;
                case MouseButton.xButton2:
                    return oldMouseState.XButton2 == ButtonState.Pressed && newMouseState.XButton2 == ButtonState.Released;
            }
            return false;
        }

        public static Point MouseDelta()
        {
            return oldMouseState.Position - newMouseState.Position;
        }
        public static Point MousePosition()
        {
            return newMouseState.Position;
        }
        public static Point FromCenter()
        {

            return newMouseState.Position - Center;
        }
        public static int ScrollDelta()
        {
            return oldMouseState.ScrollWheelValue - newMouseState.ScrollWheelValue;
        }
    }




    public enum MouseButton
    {
        Left,
        Right,
        Middle,
        xButton1,
        xButton2
    }
}

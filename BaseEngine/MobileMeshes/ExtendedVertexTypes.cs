﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace BaseEngine.MobileMeshes
{    
        public struct VertexPositionNormalColor : IVertexType
        {
            public Vector3 Position;
            public Vector3 Normal;
            public Color Color;

            public VertexPositionNormalColor(Vector3 position, Vector3 normal, Color color)
            {
                Position = position;
                Normal = normal;
                Color = color;
            }

            public static readonly VertexDeclaration VertexDeclaration = new VertexDeclaration(
                new VertexElement(0, VertexElementFormat.Vector3, VertexElementUsage.Position, 0),
                new VertexElement(12, VertexElementFormat.Vector3, VertexElementUsage.Normal, 0),
                new VertexElement(24, VertexElementFormat.Color, VertexElementUsage.Color, 0)
                );

            VertexDeclaration IVertexType.VertexDeclaration
            {
                get { return VertexDeclaration; }
            }
        }
        public struct VertexPositionNormalColorTexture : IVertexType
        {
            public Vector3 Position;
            public Vector3 Normal;
            public Color Color;
            public Vector2 TextureCoordinates;

            public VertexPositionNormalColorTexture(Vector3 position, Vector3 normal, Color color, Vector2 texturecoordinates)
            {
                Position = position;
                Normal = normal;
                Color = color;
                TextureCoordinates = texturecoordinates;
            }

            public static readonly VertexDeclaration VertexDeclaration = new VertexDeclaration(
                new VertexElement(0, VertexElementFormat.Vector3, VertexElementUsage.Position, 0),
                new VertexElement(12, VertexElementFormat.Vector3, VertexElementUsage.Normal, 0),
                new VertexElement(24, VertexElementFormat.Color, VertexElementUsage.Color, 0),
                new VertexElement(28, VertexElementFormat.Vector2, VertexElementUsage.TextureCoordinate, 0)
                );
            VertexDeclaration IVertexType.VertexDeclaration
            {
                get { return VertexDeclaration; }
            }

        }    
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Reflection;
using System.Reflection.Emit;
using Microsoft.CSharp;
using System.CodeDom.Compiler;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Graphics.PackedVector;


namespace BaseEngine.MobileMeshes
{
    //public class MobileMesh
    //{

    //}
    public class MobileMesh    //for index type 
    {
        //private static Face BoundaryFace = new Face()
        public static Model BuildModel(MobileMesh mesh, GraphicsDevice graphics, VertexDeclaration VertType,Effect e)
        {
            //return new Model(graphics, new List<ModelBone>(), new List<ModelMesh>());
            return mesh.GenerateModel(graphics, VertType, e);
        }

        //do I, actually need these?private delegate Model ModelBuildType(MobileMesh mesh, GraphicsDevice graphics, VertexDeclaration decl);

        Model GenerateModel(GraphicsDevice graphics, VertexDeclaration declaration,Effect e)
        {
            
            string usingStatements =
                  @"using System;
                    using Microsoft.Xna.Framework;
                    using Microsoft.Xna.Framework.Graphics;
                    using Microsoft.Xna.Framework.Graphics.PackedVector; 
                    using Face = BaseEngine.MobileMeshes.MobileMesh.Face;
                    using Edge = BaseEngine.MobileMeshes.MobileMesh.Edge;
                    using Vertex = BaseEngine.MobileMeshes.MobileMesh.Vertex;
                    using HalfEdge = BaseEngine.MobileMeshes.MobileMesh.HalfEdge;
                    ";
            #region StringBlock
            string NameSpaceDecleration =
                @"namespace BaseEngine.MobileMeshes
                  {
                      [STRUCT_INSERT]
                      public partial class ExtendedType
                      {
                          //[FunctionInsert]
                            Model Generate[VTYPE](GraphicsDevice graphics,MobileMesh m,Effect e )
            { 

                IndexBuffer indexB;
                VertexBuffer vertexB;

                List<[VTYPE]> verticies = new List<[VTYPE]>();//replace vertex position with [VERTEXTYPE]
                List<short> indicies = new List<short>();
                VertexElement[] elements = VertexPosition.VertexDeclaration.GetVertexElements();

                foreach(Face f in m.Faces)
                {
                    HalfEdge hIter = f.HalfEdge;
                    do
                    {
                        //this is the hard part
                        //need to rebuild 
                        VertexPosition edgeVert = new VertexPosition([VERT_ARGUMENTS]);

                        if(!verticies.Contains(edgeVert))
                        {
                            verticies.Add(edgeVert);
                            indicies.Add((short)verticies.IndexOf(edgeVert));
                        }
                        else
                        {
                            indicies.Add((short)verticies.IndexOf(edgeVert));
                        }

                        hIter = hIter.next;
                    } while (hIter != f.HalfEdge);
                }
                indexB = new IndexBuffer(graphics, typeof(short), indicies.Count, BufferUsage.None);
                indexB.SetData<short>(indicies.ToArray());
                vertexB = new VertexBuffer(graphics, [VTYPE].VertexDeclaration, verticies.Count, BufferUsage.None);
                vertexB.SetData(verticies.ToArray());

                ModelMeshPart xPart = new ModelMeshPart();
                xPart.IndexBuffer = indexB;
                xPart.VertexBuffer = vertexB;
                xPart.NumVertices = verticies.Count;
                xPart.PrimitiveCount = verticies.Count;
                xPart.StartIndex = 0;
                xPart.VertexOffset = 0;

                Model returnModel = new Model(graphics, new List<ModelBone>(), new List<ModelMesh>() { new ModelMesh(graphics, new List<ModelMeshPart>() { xPart }) });
                returnModel.Meshes[0].MeshParts[0].Effect = e;//will need to be able to generate model files from
                //collections of modifiable meshes later

                return returnModel;
                //return new Model(graphics, new List<ModelBone>(), new List<ModelMesh>());
            }
                      }
                  }";
            #endregion
            StringBuilder GenerateBuildString = new StringBuilder();
            GenerateBuildString.Append(usingStatements);
            GenerateBuildString.Append(NameSpaceDecleration);

            string VertexTypeName = "Vertex";//so over our generate loop just add usages
            List<List<string>> ValuesList = new List<List<string>>();
            foreach (var VertexElement in declaration.GetVertexElements())
            {
                List<string> elementValues = new List<string>();
                //VertexElement v = new VertexElement(offset, VertexElementFormat.Format, VertexElementUsage.Usage, UseINdex)
                elementValues.Add(VertexElement.Offset.ToString());
                elementValues.Add("VertexElementFormat." + VertexElement.VertexElementFormat.ToString());
                elementValues.Add("VertexElementUsage." + VertexElement.VertexElementUsage.ToString());
                elementValues.Add(VertexElement.UsageIndex.ToString());
                ValuesList.Add(elementValues);

                VertexTypeName += VertexElement.VertexElementUsage.ToString();
            }

            
            string StructBody =
                  @"public struct [NAME] : IVertexType 
                    {
                        [FIELDS]                        
                        public [NAME]([ARGUMENTS])
                        {
                            [ASSIGNMENTS]
                        }                        
                        public static readonly VertexDeclaration VertexDeclaration = new VertexDeclaration([ELEMENT_LIST])
                        VertexDeclaration IVertexType.VertexDeclaration get=>VertexDeclaration;
                    }";

            StringBuilder StructBodyBuilder = new StringBuilder(StructBody);           

            
           
            StringBuilder FieldNames = new StringBuilder();
            StringBuilder ConstructorArguments = new StringBuilder();
            StringBuilder ConstructorAssignments = new StringBuilder();            
            StringBuilder VertexElementParams = new StringBuilder();
            StringBuilder VertexBuildParams = new StringBuilder();
            string VertexElementConstant = "new VertexElement([V0],[V1],[V2],[V3])";
            foreach(var valueSource in ValuesList)
            {
                FieldNames.Append("Public " + valueSource[1] + " " + valueSource[2] + valueSource[3] +";"+"\n");//this should be all for field names
                //now down here constructor
                if (ValuesList.IndexOf(valueSource) != 0)
                    ConstructorArguments.Append("," + valueSource[1] + " _" + valueSource[2] + valueSource[3]);
                //the format should be Public Format UsageUsageIndex;
                //so say public Vector3 Position0; 
                else
                {
                    ConstructorArguments.Append(valueSource[1] + " _" + valueSource[2] + valueSource[3]);//if first argument don't have seperation
                }

                ConstructorAssignments.Append(valueSource[2] + valueSource[3] + " = _" + valueSource[2] + valueSource[3] + ";\n");//yeaahh, we'll try it in a bit.  
                
                if(ValuesList.IndexOf(valueSource)!=0)
                {
                    //VertexElementParams.Append("new VertexElement(" + valueSource[0] + "," + valueSource[1] + ",")
                    VertexElementParams.Append(",");
                }
                StringBuilder elementBuild = new StringBuilder(VertexElementConstant);
                elementBuild.Replace("[V0]", valueSource[0]);
                elementBuild.Replace("[V1]", valueSource[1]);
                elementBuild.Replace("[V2]", valueSource[2]);
                elementBuild.Replace("[V3]", valueSource[3]);
                VertexElementParams.Append(elementBuild.ToString());
                //so the general idea is hIter.data.TryGet[UsageName](usageIndex) or hIter.vertex.Position
                if(ValuesList.IndexOf(valueSource)!=0)
                {
                    VertexBuildParams.Append(",");
                }
                if (valueSource[2] == "Position")
                {
                    VertexBuildParams.Append("hIter.Vertex.Position");
                }
                else
                {
                    VertexBuildParams.Append(string.Format("hiter.Data.TryGet{0}({1})", valueSource[2], valueSource[3]));
                }
            }            
            StructBodyBuilder.Replace("[NAME]", VertexTypeName);
            StructBodyBuilder.Replace("[FIELDS]", FieldNames.ToString());
            StructBodyBuilder.Replace("[ARGUMENTS]", ConstructorArguments.ToString());
            StructBodyBuilder.Replace("[ASSIGNMENTS]", ConstructorAssignments.ToString());
            StructBodyBuilder.Replace("[ELEMENT_LIST]", VertexElementParams.ToString());//alright that should be all of the struct

            GenerateBuildString.Replace("[STRUCT_INSERT]", StructBodyBuilder.ToString());
            GenerateBuildString.Replace("[VTYPe]", VertexTypeName);
            GenerateBuildString.Replace("[VERT_ARGUMENTS]", VertexBuildParams.ToString());
            //this doesn't need to be here.
            //I should get. 
            //so this whole process should happen once somewhere when processing verticies and then back here its just, finding it.
            //because we're gonna generate a lot of duplicate code otherwise.
            //and compiling this can't be great.
            //though I'm not gonna get a lot of examples I guess.

            //but. All the effective code is proper. 
            //the only real actually.
            //moving this somewhere else means I can build all this during engine time like this and just. Call it at runtime.

            //now the function

            //lets just write a function inside of this and then turn it into a string

            string usingStatements2 =
                  @"using System.Collections.Generic;
                    using Microsoft.Xna.Framework;
                    using Microsoft.Xna.Framework.Graphics;
                    using Microsoft.Xna.Framework.Graphics.PackedVector; 
                    using Face = BaseEngine.MobileMeshes.MobileMesh.Face;
                    using Edge = BaseEngine.MobileMeshes.MobileMesh.Edge;
                    using Vertex = BaseEngine.MobileMeshes.MobileMesh.Vertex;
                    using HalfEdge = BaseEngine.MobileMeshes.MobileMesh.HalfEdge;
                    ";

            CSharpCodeProvider provider = new CSharpCodeProvider();
            CompilerParameters parameters = new CompilerParameters();

            parameters.ReferencedAssemblies.Add("System.dll");
            parameters.ReferencedAssemblies.Add("Microsoft.Xna.Framework.dll");
            parameters.ReferencedAssemblies.Add("BaseEngine.dll");

            parameters.GenerateInMemory = true;
            parameters.GenerateExecutable = false;

            CompilerResults results =  provider.CompileAssemblyFromSource(parameters, GenerateBuildString.ToString());

            if(results.Errors.HasErrors)
            {
                StringBuilder sb = new StringBuilder();

                foreach(CompilerError error in results.Errors)
                {
                    sb.AppendLine(string.Format("Error ({0}): {1}", error.ErrorNumber, error.ErrorText));
                }
                throw new InvalidOperationException(sb.ToString());

                
            }

            Assembly assembly = results.CompiledAssembly;
            Type program = assembly.GetType("BaseEngine.MobileMeshes.ExtendedType");
            MethodInfo Generate = program.GetMethod("Generate" + VertexTypeName);

            return (Model)Generate.Invoke(null, new object[] { graphics, this, e });

            return new Model(graphics, new List<ModelBone>(), new List<ModelMesh>());
        }


    //so we can generate a list of
    //lets make a list of the fields being worked with using the vertex element data
    //I need to rebuild vertex data container to use lists
    //each index of a list is a layer so that.
    //when adding a layer you go over each half edge and add
    //he 


    //namespace BaseEngine.MobileMeshes
    //{
    //    public struct 
    //}
    //struct VertexBuildType;



    //Model GenerateModel(GraphicsDevice graphics, VertexDeclaration vertType)
    //{
    //    IndexBuffer indexB;
    //    VertexBuffer vertexB;
    //    List<IVertexType> verticies = new List<IVertexType>();
    //    List<short> indicies = new List<short>();

    //    foreach(Face f in Faces)
    //    {
    //        HalfEdge hIter = f.HalfEdge;
    //        do
    //        {
    //            //alright lets
    //        } while (hIter != f.HalfEdge);
    //    }

    //    indexB = new IndexBuffer(graphics, typeof(short), indicies.Count, BufferUsage.None);
    //    indexB.SetData<short>(indicies.ToArray());
    //    vertexB = new VertexBuffer(graphics, vertType, verticies.Count, BufferUsage.None);
    //    //vertexB.SetData(verticies.ToArray());

    //}

    public struct VertexData//Update
        {
            //public Vector3 Position { get; set; }//god just move this back to, to vertex its so much easier to manage position changes with one reference
            //public Vector3 Normal { get; set; }
            public List<Vector3> Normal { get; set; }
            //public Vector2 UV { get; set; }
            public List<Vector3> BiNormal { get; set; }

            public List<Vector3> Tangent { get; set; }

            public List<Vector2> UV { get; set; }
            //public Color Color { get; set; }
            public List<Color> Color { get; set; }
           
            public List<Byte4> BlendIndicies { get; set; }
            //alright
            public List<Vector4> BlendWeights { get; set; }

            //public VertexData()
            //{
            //    Normal = new List<Vector3>();
            //    BiNormal = new List<Vector3>();
            //    Tangent = new List<Vector3>();
            //    UV = new List<Vector2>();
            //    Color = new List<Color>();
            //    BlendIndicies = new List<Byte4>();
            //    BlendWeights = new List<Vector4>();
            //}

            public Vector3 TryGetNormal(int layer)
            {
                if(Normal.Count > layer)
                {
                    return Normal[layer];
                }
                return Vector3.Zero;
            }
            public Vector3 TryGetBiNormal(int layer)
            {
                if (BiNormal.Count > layer)
                {
                    return BiNormal[layer];
                }
                return Vector3.Zero;
            }
            public Vector3 TryGetTangent(int layer)
            {
                if (Tangent.Count > layer)
                {
                    return Tangent[layer];
                }
                return Vector3.Zero;
            }
            public Vector2 TryGetTextureCoordinate(int layer)
            {                
                if (UV.Count > layer)
                {
                    return UV[layer];
                }
                return Vector2.Zero;
            }
            public Color TryGetColor(int layer)
            {
                if (Color.Count > layer)
                {
                    return Color[layer];
                }
                return Microsoft.Xna.Framework.Color.White;
            }
            public Byte4 TryGetBlendIndices(int layer)
            {
                if (BlendIndicies.Count > layer)
                {
                    return BlendIndicies[layer];
                }
                return new Byte4(0,0,0,0);
            }
            public Vector4 TryGetBlendWeight(int layer)
            {
                if (BlendWeights.Count > layer)
                {
                    return BlendWeights[layer];
                }
                return Vector4.Zero;
            }
        }
               
        public ushort AvailableIndex = 0;//increment with each vertex add
        public List<Vertex> Verticies = new List<Vertex>(sizeof(ushort));
        public List<Edge> Edges = new List<Edge>();
        public List<Face> Faces = new List<Face>();

        public List<HalfEdge> HalfEdges = new List<HalfEdge>();

        public MobileMesh()
        {

        }
        //public MobileMesh(List<Vertex> V, List<Edge> E, List<Face> F)
        //{

        //}
        public MobileMesh(List<Vertex> V, List<Edge> E, List<Face> F, List<HalfEdge> H)
        {
            Verticies = V;
            Edges = E;
            Faces = F;
            HalfEdges = H;
        }
        public MobileMesh(List<Vector3> verticies, List<List<ushort>> polygons, VertexDataBinding bindings)
        {
            Generate(verticies, polygons, bindings);
        }
        //lets try, the half edge model, even though I sort of want non manifold to be possible for later?
        public abstract class HalfEdgeElement
        {
            public HalfEdge HalfEdge { get; set; }
            //public Vertex Vertex { get; set; }
            //public Edge Edge { get; set; }
            //public Face Face { get; set; }
            public abstract BoundingBox bounds();
            public abstract Vector3 centroid();
            //axes?

            internal void GenerateMaxMin(Vector3 point, ref Vector3 min, ref Vector3 max)
            {
                if (point.X < min.X)
                    min.X = point.X;

                if (point.Y < min.Y)
                    min.X = point.Y;

                if (point.Z < min.Z)
                    min.X = point.Z;
                if (point.X > max.X)
                    min.X = point.X;

                if (point.Y > max.Y)
                    min.X = point.Y;

                if (point.Z < min.Z)
                    min.X = point.Z;
            }
        }
        public class HalfEdge //: HalfEdgeElement
        {
            //public static HalfEdge BoundaryHEdge = new HalfEdge() { isBoundary = true };
            //public static HalfEdge BoundaryEdge(HalfEdge twin,)

            /// <summary>
            /// next edge in the faces winding order
            /// </summary>
            public HalfEdge next { get; set; }
            /// <summary>
            /// previous edge in the winding order
            /// </summary>
            public HalfEdge previous { get; set; }

            /// <summary>
            /// adjacent half edge linked to the next polygon.
            /// </summary>
            public HalfEdge twin { get; set; }
            /// <summary>
            /// Starting Verticy of the Edge
            /// </summary>
            public Vertex Vertex { get; set; }
            /// <summary>
            /// Edge Associated with the half edge
            /// </summary>
            public Edge Edge { get; set; }
            /// <summary>
            /// Face containing the half edge
            /// </summary>
            public Face Face { get; set; }
            //public override BoundingBox bounds()
            //{
            //    return Edge.bounds();
            //}
            //public override Vector3 centroid()
            //{
            //    return Edge.centroid();
            //}
            public VertexData Data;
            public bool isBoundary { get; set; }
            public HalfEdge()
            {
                Data = new VertexData();
            }
        }
        public class Vertex : HalfEdgeElement
        {
            private Vector3 boundsScale = new Vector3(.1f, .1f, .1f);
            //public Vector3 Position { get; private set; }
            //public ushort Index { get; private set; }
            //public HalfEdge hEdge { get; private set; }//immeditaely notice these should inheret some base class
            //public Vertex(Vector3 position,ushort index)
            //{
            //    this.Index = index;
            //    this.Position = position;
            //}
            public Vector3 Position;
            public Vertex(Vector3 pos)
            {
                Position = pos;
            }

            public override Vector3 centroid()
            {
                //return HalfEdge.Data.Position;
                return Position;
            }
            public override BoundingBox bounds()
            {
                //return new BoundingBox(Position - boundsScale, Position + boundsScale);
                return new BoundingBox(Position - boundsScale, Position + boundsScale);
            }

            //other vertex data, to build maybe?
        }
        public class Edge : HalfEdgeElement
        {
            // public HalfEdge hEdge { get; private set; }
            //hmm what does edge information look like? hm
            public override BoundingBox bounds()
            {
                Vector3 min = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);
                Vector3 max = new Vector3(float.MinValue, float.MinValue, float.MinValue);

                GenerateMaxMin(HalfEdge.Vertex.Position, ref min, ref max);
                GenerateMaxMin(HalfEdge.twin.Vertex.Position, ref min, ref max);

                return new BoundingBox(min, max);
                //return new BoundingBox(hEdge.Vertex.Position, hEdge.twin.Vertex.Position);

            }
            public override Vector3 centroid()
            {
                Vector3 average;

                average = HalfEdge.Vertex.Position + HalfEdge.twin.Vertex.Position;
                return average / 2;
            }
        }
        public class Face : HalfEdgeElement
        {
            //public static Face BoundaryFace = new Face() { isBoundary = true
            //,
            //HalfEdge = HalfEdge.BoundaryHEdge,            
            //};
            public Vector3 Normal;
            public Plane FacePlane;
            public int Degree;

            //public HalfEdge hEdge { get; private set; }

            public override BoundingBox bounds()
            {
                Vector3 min = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);
                Vector3 max = new Vector3(float.MinValue, float.MinValue, float.MinValue);

                HalfEdge h = HalfEdge;
                do
                {
                    GenerateMaxMin(h.Vertex.Position, ref min, ref max);
                    h = h.next;

                } while (h != HalfEdge);

                return new BoundingBox(min, max);
            }
            public override Vector3 centroid()
            {
                Vector3 average = Vector3.Zero;
                int magnitude = 0;
                HalfEdge h = HalfEdge;
                do
                {
                    average += h.Vertex.Position;
                    magnitude += 0;
                    h = h.next;

                } while (h != HalfEdge);

                return average / magnitude;
            }
            public bool isBoundary { get; set; }
            //maybe a plane. for selection at least or like. 
            //like for the bsp csp css whatever thing. binary space partitioning constructive solid geometry
        }

        protected struct IndexPair
        {
            ushort A;
            ushort B;
            public IndexPair(ushort a, ushort b)
            {
                A = a;
                B = b;
            }
        }

        public class VertexDataBinding
        {
            public struct VertexPolygonPair
            {
                public Vector3 vertex;
                public List<ushort> polygon;

                public VertexPolygonPair(Vector3 V, List<ushort> P)
                {
                    vertex = V;
                    polygon = P;
                }
            }
            Dictionary<VertexPolygonPair, VertexData> vertexBinding = new Dictionary<VertexPolygonPair, VertexData>();

            public VertexData this[Vector3 v, List<ushort> p]
            {
                get { return vertexBinding[new VertexPolygonPair(v, p)]; }
            }

            public void Add(Vector3 V, List<ushort> P, VertexData data)
            {
                vertexBinding.Add(new VertexPolygonPair(V, P), data);
            }
        }
        public void Generate(List<Vector3> verticies, List<List<ushort>> polygons, VertexDataBinding bindings)
        {
            ushort counter = 0;
            foreach (Vector3 vector in verticies)
            {
                //Verticies.Add(new Vertex(vector, counter));
                Verticies.Add(new Vertex(vector));
                counter++;
            }

            List<IndexPair> indexPairs = new List<IndexPair>();
            Dictionary<IndexPair, HalfEdge> pairToHEdge = new Dictionary<IndexPair, HalfEdge>();
            

            //now we can construct, polgyons I think
            foreach (List<ushort> polygon in polygons)
            {
                Face face = new Face();
                List<HalfEdge> faceHalfEdges = new List<HalfEdge>();
                for (int i = 0; i < polygon.Count; i++)
                {
                    Edge e = new Edge();//the edge 
                    ushort vertA = polygon[i];
                    ushort vertB = polygon[(i + 1) % polygon.Count];
                    IndexPair AB = new IndexPair(vertA, vertB);
                    //this half edge already exists, edge has 3 faces
                    if (pairToHEdge.ContainsKey(AB))
                    {
                        return;
                    }
                    //from this we can construct an edge, particularly a half edge
                    //half edge would at least be easier
                    HalfEdge h = new HalfEdge()
                    {
                        Vertex = Verticies[vertA],
                        Edge = e,
                        Face = face,
                        Data = bindings[verticies[vertA], polygon]
                    };

                    Verticies[vertA].HalfEdge = h;
                    face.HalfEdge = h;

                    //okay so we need to check for existance of twin before this
                    IndexPair BA = new IndexPair(vertB, vertA);
                    if (!pairToHEdge.ContainsKey(BA))
                    {

                        e.HalfEdge = h;
                        //this aint elegant but eh it works and shouldn't be too slow
                        //defining halfEdge

                        HalfEdge boundary = new HalfEdge()
                        {
                            Edge = e,
                            Vertex = Verticies[vertB],
                            Face = new Face() { isBoundary = true },
                            isBoundary = true,
                            twin = h,
                            Data = bindings[verticies[vertA], polygon],
                            //next = boundary,
                            //previus = boundary
                            //shouldn't need data?
                        };
                        boundary.next = boundary;
                        boundary.previous = boundary;
                        //define boundary edge twin
                        h.twin = boundary;
                        //h.twin = new HalfEdge()
                        //{
                        //    Edge = e,
                        //    Vertex = Verticies[vertB],
                        //    Face = Face.BoundaryFace,
                        //    isBoundary = true,
                        //    twin = h,                            
                        //};
                        //iif I define half Edge as the FaceBoundary HalfEdge...hmm
                        //set boundary next to self
                        // h.twin.next = h.twin;

                        Edges.Add(e);//only add edges when its first generated
                    }
                    else // ContainsHalfEdgeCondition
                    {
                        var existingEdge = pairToHEdge[BA];
                        h.twin = existingEdge; //set twin
                        h.Edge = existingEdge.Edge; //unset boundary halfEdge //oh no this is setting existing edge
                        existingEdge.twin = h;//this is unsetting boundary hEdge

                    }
                    pairToHEdge.Add(AB, h);
                    faceHalfEdges.Add(h);

                    HalfEdges.Add(h);
                }
                //set next
                for (int i = 0; i < faceHalfEdges.Count; i++)
                {

                    faceHalfEdges[i].next = faceHalfEdges[(i + 1) % faceHalfEdges.Count];
                }
                //set previous
                for (int i = faceHalfEdges.Count; i > 0; i--)
                {
                    //this shouuuuld work?
                    faceHalfEdges[i % faceHalfEdges.Count].previous = faceHalfEdges[i - 1];
                }

                Faces.Add(face);
            }
        }
        public void PropogateFacePlanes()
        {
            foreach (Face f in Faces)
            {
                HalfEdge hIter = f.HalfEdge;

                var ab = hIter.Vertex.Position - hIter.next.Vertex.Position;
                var cb = hIter.next.next.Vertex.Position - hIter.next.Vertex.Position;

                ab.Normalize();
                cb.Normalize();
                f.Normal = Vector3.Cross(ab, cb);

                //f.FacePlane = new Plane(hIter.Vertex.Position, hIter.previous.Vertex.Position, hIter.previous.previous.Vertex.Position);
            }
        }
        public void PropogateDegree()
        {
            foreach (Face face in Faces)
            {
                int xDegree = 0;
                HalfEdge hIter = face.HalfEdge;
                do
                {
                    xDegree++;
                    hIter = hIter.next;

                } while (hIter != face.HalfEdge & xDegree < 10);

                face.Degree = xDegree;

                if (xDegree >= 10)
                {
                    throw new Exception("UnResolvedFaceLoop");
                }


            }

        }
        public void PropogateNormals()
        {
            //PropogateFacePlanes();
            PropogatePlanes();

            foreach (Vertex v in Verticies)
            {
                int xDegree = 0;
                Vector3 average = Vector3.Zero;
                HalfEdge hIter = v.HalfEdge;
                do
                {
                    //average += hIter.twin.Vertex.Position;
                    average += Vector3.Normalize(hIter.Face.FacePlane.Normal);
                    hIter = hIter.twin.next;
                    xDegree++;

                } while (hIter != v.HalfEdge);
                //average = average / xDegree;
                hIter = v.HalfEdge;
                do
                {
                    for (int i = 0; i < hIter.Data.Normal.Count; i++)
                    {
                        hIter.Data.Normal[i] = Vector3.Normalize(average);
                    }
                    hIter = hIter.twin.next;
                } while (hIter != v.HalfEdge);
            }
        }
        public void PropogateNormals(int normalLayer)
        {
            //PropogateFacePlanes();
            PropogatePlanes();

            foreach (Vertex v in Verticies)
            {
                int xDegree = 0;
                Vector3 average = Vector3.Zero;
                HalfEdge hIter = v.HalfEdge;
                do
                {
                    //average += hIter.twin.Vertex.Position;
                    average += Vector3.Normalize(hIter.Face.FacePlane.Normal);
                    hIter = hIter.twin.next;
                    xDegree++;

                } while (hIter != v.HalfEdge);
                //average = average / xDegree;
                hIter = v.HalfEdge;
                do
                {
                    if(normalLayer< hIter.Data.Normal.Count )
                        hIter.Data.Normal[normalLayer] = Vector3.Normalize(average);
                    
                    hIter = hIter.twin.next;
                } while (hIter != v.HalfEdge);
            }
        }

        void PropogatePlanes()
        {
            foreach (Face f in Faces)
            {
                f.FacePlane = new Plane(f.HalfEdge.previous.Vertex.Position, f.HalfEdge.next.Vertex.Position, f.HalfEdge.Vertex.Position);
            }
        }
        public void PropogateFaceNormals()
        {
            PropogatePlanes();
            //PropogateFacePlanes();
            foreach (Face f in Faces)
            {
                HalfEdge hIter = f.HalfEdge;
                do
                {
                    //hIter.Data.Normal = Vector3.Normalize(f.FacePlane.Normal);
                    //hIter.Data.Normal = Vector3.Normalize(f.Normal);
                    //hIter.Data.Normal = Vector3.UnitZ;
                    for (int i = 0; i < hIter.Data.Normal.Count; i++)
                    {
                        hIter.Data.Normal[i] = f.FacePlane.Normal;
                    }
                    hIter = hIter.next;
                } while (hIter != f.HalfEdge);
            }
        }
        public void PropogateFaceNormals(int normalLayer)
        {
            PropogatePlanes();
            //PropogateFacePlanes();
            foreach (Face f in Faces)
            {
                HalfEdge hIter = f.HalfEdge;
                do
                {
                    //hIter.Data.Normal = Vector3.Normalize(f.FacePlane.Normal);
                    //hIter.Data.Normal = Vector3.Normalize(f.Normal);
                    //hIter.Data.Normal = Vector3.UnitZ;
                    if(normalLayer < hIter.Data.Normal.Count)
                        hIter.Data.Normal[normalLayer] = f.FacePlane.Normal;
                    hIter = hIter.next;
                } while (hIter != f.HalfEdge);
            }
        }

        List<Edge> SubdivisonEdges = new List<Edge>(); //AddHereForoperationsthatgenerate edges during subdiv
        List<Face> triangulationFaces = new List<Face>();
        private void TriangulateInternal()
        {
            List<Face> originalFaceList = new List<Face>();
            originalFaceList.AddRange(Faces);
            foreach (Face f in originalFaceList)
            {
                Face actFace = f;
                do
                {
                    actFace = SplitFace(f.HalfEdge);
                } while (actFace.Degree > 3);
            }
        }
        public MobileMesh Triangulate()
        {
            return this;
        }

        VertexData LerpData(VertexData A, VertexData B, float amount)
        {
            VertexData returnData = new VertexData();
            for(int i =0;i<A.Normal.Count;i++)
            {
                returnData.Normal.Add(Vector3.Lerp(A.Normal[i], B.Normal[i], amount));
            }
            for (int i = 0; i < A.Color.Count; i++)
            {
                returnData.Color.Add(Color.Lerp(A.Color[i], B.Color[i], amount));
            }
            for (int i = 0; i < A.UV.Count; i++)
            {
                returnData.UV.Add(Vector2.Lerp(A.UV[i], B.UV[i], amount));
            }
            for (int i = 0; i < A.BiNormal.Count; i++)
            {
                returnData.BiNormal.Add(Vector3.Lerp(A.BiNormal[i], B.BiNormal[i], amount));
            }

            return returnData;
        }

        public MobileMesh LoopSubdivison()
        {
            SubdivisonEdges = new List<Edge>();
            MobileMesh returnCache = new MobileMesh();
            //do subdiv here?

            List<Vertex> createdVerticies = new List<Vertex>();


            List<Edge> startEdges = new List<Edge>();
            //List<Vector3> newPositions = new List<Vector3>();

            startEdges.AddRange(Edges);

            foreach (Edge e in startEdges)
            {
                createdVerticies.Add(SplitEdge2(e));//,newPositions[startEdges.IndexOf(e)]));
                foreach (Face f in Faces)
                {

                    HalfEdge hIter = f.HalfEdge;
                    do
                    {
                        hIter = hIter.next;
                    } while (hIter != f.HalfEdge);

                }
            }


            foreach (Edge e in SubdivisonEdges)
            {
                //so conditions needs one vert in one vert out.
                //so SO god damn so we need to 
                // bool containsBoth = (createdVerticies.Contains(e.HalfEdge.Vertex) && createdVerticies.Contains(e.HalfEdge.twin.Vertex));
                //bool containsNeither = ((!createdVerticies.Contains(e.HalfEdge.Vertex)) && (!createdVerticies.Contains(e.HalfEdge.Vertex)));


                if (!(createdVerticies.Contains(e.HalfEdge.Vertex) && createdVerticies.Contains(e.HalfEdge.twin.Vertex)))
                {
                    EdgeFlip2(e);
                }
            }

            //this should do it?????
            return this;
        }
        public Vertex SplitEdge2(Edge e)
        {

            Vertex newVertex = new Vertex(e.centroid());
            Edge newEdgeA = new Edge();
            Edge newEdgeB = new Edge();

            HalfEdge A = e.HalfEdge;
            HalfEdge B = A.twin;


            //VertexData centerDataA = new VertexData()
            //{
            //    //Position = e.centroid(),
            //    Color = Color.Lerp(A.Data.Color, A.next.Data.Color, .5f),
            //    UV = Vector2.Lerp(A.Data.UV, A.next.Data.UV, .5f),
            //    Normal = Vector3.Normalize(Vector3.Lerp(A.Data.Normal, A.next.Data.Normal, .5f))
            //};
            VertexData centerDataA = LerpData(A.Data, B.Data, .5f);
            //VertexData centerDataB = new VertexData()
            //{
            //    //Position = e.centroid(),
            //    Color = Color.Lerp(B.Data.Color, B.next.Data.Color, .5f),
            //    UV = Vector2.Lerp(B.Data.UV, B.next.Data.UV, .5f),
            //    Normal = Vector3.Normalize(Vector3.Lerp(B.Data.Normal, B.next.Data.Normal, .5f))
            //};
            VertexData centerDataB = LerpData(A.Data, B.Data, .5f);

            HalfEdge AA = new HalfEdge()
            {
                Vertex = newVertex,
                Edge = newEdgeA,
                Face = A.Face,
                Data = centerDataA,
                previous = A,
                next = A.next,
                twin = B,
                isBoundary = A.isBoundary
            };
            HalfEdge BB = new HalfEdge
            {
                Vertex = newVertex,
                Edge = newEdgeB,
                Face = B.Face,
                Data = centerDataB,
                previous = B,
                next = B.next,
                twin = A,
                isBoundary = B.isBoundary
            };
            //back propogate winding
            AA.next.previous = AA;
            AA.previous.next = AA;
            BB.next.previous = BB;
            BB.previous.next = BB;
            //prep new edges and twins.
            AA.twin.twin = AA;
            BB.twin.twin = BB;
            AA.twin.Edge = AA.Edge;
            BB.twin.Edge = BB.Edge;
            //remove old edge
            Edges.Remove(e);

            newEdgeA.HalfEdge = AA;
            newEdgeB.HalfEdge = BB;
            newVertex.HalfEdge = AA;
            //Add All New Items
            Verticies.Add(newVertex);
            Edges.Add(newEdgeA);
            Edges.Add(newEdgeB);
            HalfEdges.Add(AA);
            HalfEdges.Add(BB);

            //now we need to split the faces
            SplitFace(AA);
            SplitFace(BB);

            return newVertex;
            //so you want the edges and their twins to , no you don't want them the same what.
            //fuck
            //why didn't this cause any problems on the first loop

        }

        Face SplitFace(HalfEdge hEdge)
        {
            if (hEdge.Face.isBoundary)
            {
                return new Face();
            }

            Face existingFace = hEdge.Face;
            Face newFaceA = new Face();
            Face newFaceB = new Face();

            Edge jointEdge = new Edge();

            HalfEdge A;
            HalfEdge B;//lets define our half edges

            //okay so SO no new verticies, but one new face and one new edge

            A = new HalfEdge()
            {
                Vertex = hEdge.next.next.Vertex,
                Edge = jointEdge,
                Face = newFaceA,
                Data = hEdge.next.next.Data,
                next = hEdge,
                previous = hEdge.next,
            };

            B = new HalfEdge()
            {
                Vertex = hEdge.Vertex,
                Edge = jointEdge,
                Face = newFaceB,
                Data = hEdge.Data,
                next = hEdge.next.next,
                previous = hEdge.previous
            };
            //okay no this works??? ahh???
            A.twin = B;
            B.twin = A;

            A.next.previous = A;
            A.previous.next = A;
            B.next.previous = B;
            B.previous.next = B;//uhhhh yeah? yeah

            newFaceA.HalfEdge = A;
            newFaceB.HalfEdge = B;

            HalfEdge hIter = A;
            do
            {
                hIter.Face = newFaceA;
                hIter = hIter.next;
            } while (hIter != A);
            hIter = B;
            do
            {
                hIter.Face = newFaceB;
                hIter = hIter.next;
            } while (hIter != B);

            //existingFace.HalfEdge = A;
            //newFace.HalfEdge = B;
            jointEdge.HalfEdge = B;

            Faces.Add(newFaceA);
            Faces.Add(newFaceB);
            Faces.Remove(existingFace);
            Edges.Add(jointEdge);
            SubdivisonEdges.Add(jointEdge);
            HalfEdges.Add(A);
            HalfEdges.Add(B);


            //adding for triangulation, return larger degree face
            //or if equal degree return
            int degree = 0;
            hIter = newFaceA.HalfEdge;
            do
            {
                degree++;
                hIter = hIter.next;
            } while (hIter != newFaceA.HalfEdge);
            newFaceA.Degree = degree;
            hIter = newFaceB.HalfEdge;
            degree = 0;
            do
            {
                degree++;
                hIter = hIter.next;
            } while (hIter != newFaceB.HalfEdge);
            newFaceB.Degree = degree;


            if (newFaceA.Degree > newFaceB.Degree)
            {
                return newFaceA;
            }
            else
            {
                return newFaceB;
            }

        }

        public void EdgeFlip2(Edge e)
        {
            //ensure face on either end is associated with existing half edge
            //so when we loop through later
            //we have a solid point of reference
            HalfEdge A = e.HalfEdge;
            A.Face.HalfEdge = A;
            HalfEdge B = e.HalfEdge.twin;
            B.Face.HalfEdge = B;

            //define all new pointers
            HalfEdge AStarNext = A.next.next;
            HalfEdge AStarPRevious = A.twin.next;
            Vertex AStarVertex = A.twin.next.next.Vertex;
            VertexData AStarVertexData = A.twin.next.next.Data;

            HalfEdge BStarNext = B.next.next;
            HalfEdge BStarPrevious = B.twin.next;
            Vertex BStarVertex = B.twin.next.next.Vertex;
            VertexData BStarVertexData = B.twin.next.next.Data;

            //assigne all new pointers
            A.next = AStarNext;
            A.previous = AStarPRevious;
            B.next = BStarNext;
            B.previous = BStarPrevious;
            //this section will only resolve on tris
            //
            AStarPRevious.previous = AStarNext;
            AStarNext.next = AStarPRevious;
            BStarPrevious.previous = BStarNext;
            BStarNext.next = BStarPrevious;
            //to resolve on higher order polygons, 
            //create single looped polygon by looping references
            //then reinsert edges

            A.next.previous = A;
            A.previous.next = A;
            B.next.previous = A;
            B.previous.next = B;

            A.Vertex = AStarVertex;
            A.Data = AStarVertexData;
            B.Vertex = BStarVertex;
            B.Data = BStarVertexData;
            //propogate faces to half edges
            Face FA = A.Face;
            Face FB = B.Face;
            HalfEdge hIter = A;
            do
            {
                hIter.Face = FA;
                hIter = hIter.next;
            } while (hIter != A);
            hIter = B;
            do
            {
                hIter.Face = FB;
                hIter = hIter.next;
            } while (hIter != B);



        }

        public void Sphereize(float radius)
        {
            Vector3 center = Vector3.Zero;

            foreach (Vertex v in Verticies)
            {
                //center += v.HalfEdge.Data.Position;
                center += v.Position;

            }
            center /= Verticies.Count;


            //now we just go back over every verticy find the, direction a vertex is from the center

            foreach (Vertex v in Verticies)
            {
                v.Position = Vector3.Normalize(v.Position - center) * radius;
            }//uhh, yeah thats it
        }

        //I don't now how to do this programatically
        //I guess actually, don't do this here
        //create external functions that take a geometry cache (rename that what is that)
        //and return the needed vertex type, with default input values oh yeah,
        //the big problem is data assurance and if you just pass default values and build from this your fine I guess?
        //public void BuildDrawable() - don't do this here
    }
    //class SphereDiamondPrimitiveRef : DrawablePrimitive<VertexPositionNormalColorTexture>
    //{

    //    private List<short> indexList = new List<short>();
    //    private List<VertexPositionNormalColorTexture> vertexList = new List<VertexPositionNormalColorTexture>();
    //    public SphereDiamondPrimitiveRef(float radius, int divisions, Color color)
    //    {
    //        vertexList.Add(new VertexPositionNormalColorTexture(Vector3.UnitZ, Vector3.UnitZ, color, new Vector2(.5f, .5f)));//0
    //        vertexList.Add(new VertexPositionNormalColorTexture(Vector3.UnitX, Vector3.UnitX, color, new Vector2(1, .5f)));//1
    //        vertexList.Add(new VertexPositionNormalColorTexture(Vector3.UnitY, Vector3.UnitY, color, new Vector2(.5f, 1)));//2
    //        vertexList.Add(new VertexPositionNormalColorTexture(-Vector3.UnitX, -Vector3.UnitX, color, new Vector2(0, .5f)));//3
    //        vertexList.Add(new VertexPositionNormalColorTexture(-Vector3.UnitY, -Vector3.UnitY, color, new Vector2(.5f, 0)));//4
    //        vertexList.Add(new VertexPositionNormalColorTexture(-Vector3.UnitZ, -Vector3.UnitZ, color, new Vector2(0, 0)));//5
    //        vertexList.Add(new VertexPositionNormalColorTexture(-Vector3.UnitZ, -Vector3.UnitZ, color, new Vector2(1, 0)));//6
    //        vertexList.Add(new VertexPositionNormalColorTexture(-Vector3.UnitZ, -Vector3.UnitZ, color, new Vector2(0, 1)));//7
    //        vertexList.Add(new VertexPositionNormalColorTexture(-Vector3.UnitZ, -Vector3.UnitZ, color, new Vector2(1, 1)));//8

    //        AddFace(2, 1, 0);//0
    //        AddFace(3, 2, 0);//1
    //        AddFace(4, 3, 0);//2
    //        AddFace(1, 4, 0);//3
    //        AddFace(3, 4, 5);//4
    //        AddFace(4, 1, 6);//5
    //        AddFace(2, 3, 7);//6
    //        AddFace(1, 2, 8);//7

    //        vertexCount = 9;
    //        primitiveCount = 8;
    //        primitiveType = PrimitiveType.TriangleList;
    //        vertices = vertexList.ToArray();
    //        indices = indexList.ToArray();

    //    }
    //    public override void Initialize(GraphicsDevice graphicsDevice)
    //    {
    //        effect = new BasicEffect(graphicsDevice);
    //        effect.VertexColorEnabled = true;
    //        effect.EnableDefaultLighting();
    //    }


    //    public void AddFace(short i1, short i2, short i3)
    //    {
    //        indexList.Add(i1);
    //        indexList.Add(i2);
    //        indexList.Add(i3);

    //    }
    
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseEngine.Assets
{
    public struct Asset
    {
        public string Name;
        public string Path;
        public IAssetTag Tag;
        public Asset(string name, string path, IAssetTag tag)
        {
            Name = name;
            Path = path;
            Tag = tag;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;

namespace BaseEngine.Assets
{
    public class TextureAsset : IAssetTag
    {
        dynamic IAssetTag.Load(string name, Microsoft.Xna.Framework.Content.ContentManager content)
        {
            return content.Load<Texture>(name);
            
        }
    }
    
}

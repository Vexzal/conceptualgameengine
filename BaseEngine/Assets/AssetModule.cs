﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;

namespace BaseEngine.Assets
{
    //is this a servic?
    public class AssetModule
    {
        ContentManager Content { get; set; }
        List<Asset> Assets = new List<Asset>();
        Dictionary<string, Asset> AssetCollection = new Dictionary<string, Asset>();
        public AssetModule(ContentManager content)
        {
            Content = content;
        }
        public void InitLoad()
        {
            foreach (Asset a in AssetCollection.Values)
            {
                a.Tag.Load(a.Path, Content);
            }
        }

        public dynamic this[string name]
        {
            get
            {
                if (AssetCollection.ContainsKey(name))
                {
                    return AssetCollection[name].Tag.Load(AssetCollection[name].Path, Content);
                }
                return null;
            }
        }
        public void Unload()
        {
            Content.Unload();
        }
        public void AddAsset(Asset asset)
        {
            AssetCollection.Add(asset.Name, asset);
        }
    }
}

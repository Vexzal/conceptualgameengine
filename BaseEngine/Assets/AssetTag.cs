﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;

namespace BaseEngine.Assets
{
    public interface IAssetTag
    {
        dynamic Load(string name, ContentManager content);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace BaseEngine.Assets
{
    public class FontAsset : IAssetTag
    {
        dynamic IAssetTag.Load(string name, ContentManager content)
        {
            return content.Load<SpriteFont>(name);
        }
    }
}

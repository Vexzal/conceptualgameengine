﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseEngine.Components;
using Microsoft.Xna.Framework;

namespace BaseEngine.Logic
{
    public class Behavior : Component
    {
        public Behavior()
        {

        }        
        public virtual void Update(GameTime gameTime)
        {

        }
        public virtual void LateUpdate(GameTime gameTime)
        {

        }
    }
}

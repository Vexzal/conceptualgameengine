﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseEngine.Components;
using BaseEngine.Extensions;
using Microsoft.Xna.Framework;

namespace BaseEngine
{
    
    public class GameObject : INode<GameObject>
    {
        const string NeedsInitialized = "Initialize";
        public string Name;
        
        public Dictionary<Type, Component> components= new Dictionary<Type, Component>();

        public GameObject Parent
        {
            get => parent;
        }
        public List<GameObject> Children
        { 
            get => children;
        }
        GameObject parent;
        List<GameObject> children = new List<GameObject>();
        public GameObject()
        {
            Name = "";
            AddComponent(new TransformComponent());
        }
        public GameObject(string name)
        {
            Name = name;
            AddComponent(new TransformComponent());
        }
        public void AddComponent(Component component)
        {
            component.gameObject = this;
            
            components.Add(component.GetType(),component);
            if (component.OverridesMethod(NeedsInitialized))
                InitializeComponents += component.Initialize;
            //component.Initialize(); TODO - Move initialization to runtime.            
        }
        public void RemoveComponent(Component component)
        {
            component.gameObject = null;
            //Scene.Components.R//this won't work this messes up the whole ordering of components which screws up the indicies.
            components.Remove(component.GetType());
        }
        public T GetComponent<T>() where T : Component
        {            
            //dictionary 
            if(components.ContainsKey(typeof(T)))
            {
                return (T)components[typeof(T)];
            }            
            return default(T);            
        }
        public bool GetComponent<T>(ref T component) where T: Component
        {
            if(components.ContainsKey(typeof(T)))
            {
                component = (T)components[typeof(T)];
                return true;
            }
            //component = default(T);
            return false;
        }
        //we should do parent clearing here
        public void SetParent(GameObject go) 
        {
            if(parent!= null)
            {
                parent.GetComponent<TransformComponent>().ComponentUpdated -= this.GetComponent<TransformComponent>().ParentUpdated;
                parent.RemoveChild(this);
            }

            parent = go;

            var parentTransform = parent.GetComponent<TransformComponent>();
            parentTransform.ComponentUpdated += this.GetComponent<TransformComponent>().ParentUpdated;
            var thisTransform = (TransformComponent)this.GetComponent<TransformComponent>();
            thisTransform.SetTransform(thisTransform.GetWorld() * Matrix.Invert(parentTransform.GetWorld()));
        }
        public void RemoveParent()
        {
            //remove parent and set return object 
            parent.GetComponent<TransformComponent>().ComponentUpdated -= this.GetComponent<TransformComponent>().ParentUpdated;
            parent = null;
        }
        
        public void AddChild(GameObject go)
        {
            if (go == parent)
                return;//parent is equal to set children which is, bad! really
            children.Add(go);
            go.SetParent(this);
        }
        public void RemoveChild(GameObject go)
        {
            go.RemoveParent();
            children.Remove(go);
        }
        public void Initialize()
        {
            InitializeComponents?.Invoke();
        }
        private delegate void Initializable();
        private event Initializable InitializeComponents;
        public delegate void GameObjectUpdate(GameObjectUpdateArguments e);
        public event GameObjectUpdate GameObjectUpdated;//don't need to, invoke this yet I don't think

    }
    public struct GameObjectUpdateArguments
    {
        public GameObject GameObject { get; private set; }

        public GameObjectUpdateArguments(GameObject go)
        {
            GameObject = go;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Input;

namespace BaseEngine.Extensions
{
    public static class EngineExtensions
    {
        public static bool HasMethod(this object objectToCheck, string methodName)
        {
            var type = objectToCheck.GetType();
            return type.GetMethod(methodName) != null;
        }
        public static bool OverridesMethod(this object objectToCheck, string methodName)
        {
            if (!objectToCheck.HasMethod(methodName))
                return false;
            var xType = objectToCheck.GetType();
            var xMethod = xType.GetMethod(methodName);

            if (xMethod.IsVirtual && xMethod.DeclaringType == xType)
                return true;

            return false;
        }
        public static bool InheritsFrom(this object objectToCheck, Type inheretType)
        {
            return inheretType.IsAssignableFrom(objectToCheck.GetType());
        }
        public static bool IsButtonUp(this MouseState state, MouseButton button)
        {
            return !state.IsButtonDown(button);
        }
        public static bool IsButtonDown(this MouseState state, MouseButton button)
        {
            if (button == MouseButton.Left)
            {
                if (state.LeftButton == ButtonState.Pressed)
                    return true;
            }
            if (button == MouseButton.Middle)
            {
                if (state.MiddleButton == ButtonState.Pressed)
                    return true;
            }
            if (button == MouseButton.Right)
            {
                if (state.RightButton == ButtonState.Pressed)
                    return true;
            }
            if (button == MouseButton.xButton1)
            {
                if (state.XButton1 == ButtonState.Pressed)
                    return true;
            }
            if (button == MouseButton.xButton2)
            {
                if (state.XButton2 == ButtonState.Pressed)
                    return true;
            }
            return false;
        }
        public static float TexelWidth(this Microsoft.Xna.Framework.Graphics.Texture2D target)
        {
            return 1f / (float)target.Width;
        }
        public static float TexelHeight(this Microsoft.Xna.Framework.Graphics.Texture2D target)
        {
            return 1f / (float)target.Height;
        }
        public static void AddRange<T>(this ICollection<T>target, IEnumerable<T> source)
        {
            if (target == null)
                throw new ArgumentNullException(nameof(target));
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            foreach(var element in source)
            {
                target.Add(element);
            }
        }
    }
}

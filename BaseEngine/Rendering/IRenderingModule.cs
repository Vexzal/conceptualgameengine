﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace BaseEngine.Rendering
{
    public interface IRenderingModule
    {
        short drawPriority { get; }
        ICamera Camera { get; set; }
        GraphicsDevice graphics { get; set; }
        RenderTargetManager TargetManager { get; set; }//if we're. Going to have all this. we could probably just. Hmm. use this as THE Interface for drawing and also inheret this in the  Deferred render pipeline
        //oh that would be so cool you could just slot in whatever you want that would be so cool
        void Load(Scene gameScene);
        void Draw(GameTime gameTime);
    }
}

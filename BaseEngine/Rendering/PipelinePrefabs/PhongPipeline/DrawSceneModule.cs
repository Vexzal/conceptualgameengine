﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace BaseEngine.Rendering.Prefabs.PhongPipeline
{
    class DrawSceneModule : IRenderingModule
    {
        public short drawPriority { get; set; }

        public ICamera Camera { get; set; }
        public GraphicsDevice graphics { get; set; }
        public SpriteBatch spriteBatch { get; set; }
        public RenderTargetManager TargetManager { get; set; }

        public SpriteFont Font { get; set; }
        public int TargetDraw = 4;

        public void Draw(GameTime gameTime)
        {
            graphics.SetRenderTarget(null);
            spriteBatch.Begin();
            if(TargetDraw ==0)
            {
                spriteBatch.Draw((RenderTarget2D)TargetManager.NRTLookup[NameConstants.GBColor].RenderTarget,Vector2.Zero,Color.White);
                spriteBatch.DrawString(Font, "Deffered: Color", Vector2.Zero, Color.White);
            }
            if (TargetDraw == 1)
            {
                spriteBatch.Draw((RenderTarget2D)TargetManager.NRTLookup[NameConstants.GBNormal].RenderTarget, Vector2.Zero, Color.White);
                spriteBatch.DrawString(Font, "Deffered: Normal", Vector2.Zero, Color.White);
            }
            if (TargetDraw == 2)
            {
                spriteBatch.Draw((RenderTarget2D)TargetManager.NRTLookup[NameConstants.GBDepth].RenderTarget, Vector2.Zero, Color.White);
                spriteBatch.DrawString(Font, "Deffered: Depth", Vector2.Zero, Color.White);
            }
            if (TargetDraw == 3)
            {
                spriteBatch.Draw((RenderTarget2D)TargetManager.NRTLookup[NameConstants.LightMap].RenderTarget, Vector2.Zero, Color.White);
                spriteBatch.DrawString(Font, "Deffered: Lights", Vector2.Zero, Color.White);
            }
            if (TargetDraw == 4)
            {
                spriteBatch.Draw((RenderTarget2D)TargetManager.NRTLookup[NameConstants.CompositeMap].RenderTarget, Vector2.Zero, Color.White);
                spriteBatch.DrawString(Font, "Deffered: Composite", Vector2.Zero, Color.White);
            }
            spriteBatch.End();


        }

        public void Load(Scene gameScene)
        {
            drawPriority = 5;
            //throw new NotImplementedException();
        }
    }
}

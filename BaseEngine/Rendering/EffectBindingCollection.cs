﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace BaseEngine.Rendering
{
    public class xEffectBindingCollection : IEnumerable<EffectBinding>
    {
        private EffectBinding[] _bindings;
        private Dictionary<string, int> _indexLookup;

        internal xEffectBindingCollection(EffectParameterCollection parameters)
        {
            _bindings = new EffectBinding[parameters.Count]; 
            _indexLookup = new Dictionary<string, int>(_bindings.Length);

            for(int i =0;i < _bindings.Length;i++)
            {
                if (parameters[i].Elements.Count < 0)
                {
                    if (parameters[i].ParameterClass == EffectParameterClass.Matrix)
                    {
                        //do add effectbinding
                        _bindings[i] = new MatrixBinding(parameters[i].Name, parameters[i]);
                    }
                    if (parameters[i].ParameterClass == EffectParameterClass.Scalar)
                    {
                        if (parameters[i].ParameterType == EffectParameterType.Single)
                        {
                         _bindings[i] =new FloatBinding(parameters[i].Name, parameters[i]);
                        }
                    }
                    if (parameters[i].ParameterClass == EffectParameterClass.Vector)
                    {

                    }
                    if (parameters[i].ParameterClass == EffectParameterClass.Object)
                    {
                        if (parameters[i].ParameterType == EffectParameterType.Texture2D)
                        {
                            _bindings[i] =  new TextureBinding(parameters[i].Name, parameters[i]);
                        }
                    }
                }
                else
                {
                    if (parameters[i].ParameterClass == EffectParameterClass.Matrix)
                    {
                        _bindings[i] = new MatrixArrayBinding(parameters[i].Name, parameters[i]);
                    }
                }
            }
        }
        public int Count
        {
            get { return _bindings.Length; }
        }
        public EffectBinding this[int index]
        {
            get { return _bindings[index]; }
        }
        public EffectBinding this[string name]
        {
            get
            {
                int index;
                if (_indexLookup.TryGetValue(name, out index))
                    return _bindings[index];
                return null;
            }
        }
        public IEnumerator<EffectBinding> GetEnumerator()
        {
            return ((IEnumerable<EffectBinding>)_bindings).GetEnumerator();            
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _bindings.GetEnumerator();
        }
    }
}

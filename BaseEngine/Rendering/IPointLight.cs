﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace BaseEngine.Rendering
{
    public interface IPointLight
    {
        Matrix World { get; }
        Vector3 Position { get; }
        float Radius { get; }
        Color Color { get; }
        float Intensity { get; }
    }
}

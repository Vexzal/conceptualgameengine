﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace BaseEngine.Rendering
{
    //consider going in and changing View as a fixed value 
    //thats updated whenever other values are changed
    //this should just be in, in the memorly leak code really
    //the base engine code doesn't need this anymore the interface is implmented
    public class EditorCamera : ICamera
    {

        public float xAngel;
        public float zAngel;

        Matrix focus;
        Matrix pivot;
        Matrix arm;
        Matrix camera;
        public Vector3 Position
        {
            get => Vector3.Transform(new Vector3(0, -(float)distance, 0), pivot);
        }
        public Matrix View
        {
            get => Matrix.CreateLookAt(
                Vector3.Transform(
                    Vector3.Transform(
                        Vector3.Zero,
                        Matrix.CreateTranslation(0, -(float)distance, 0) * pivot),focus),
                focus.Translation,
                Up);
        }

        public Matrix Projection
        {
            get => Matrix.CreatePerspectiveFieldOfView(FOV, aspect, nearClipPlane, farClipPlane);
        }
        //camera settings
        public float distance;
        public Vector3 Up { get; set; }
        public Vector3 AbsUp { get; private set; }

        public Vector3 FocusPoint
        {
            get { return focus.Translation; }
            set { focus.Translation = value; }
        }

        //projection settings
        public float FOV { get; set; }
        public float nearClipPlane { get; set; }
        public float farClipPlane { get; set; }
        public float aspect { get; set; }
        public EditorCamera(GraphicsDevice device, float distance)
        {
            xAngel = -45;
            zAngel = 45;
            focus = Matrix.Identity;
            aspect = device.Viewport.AspectRatio;
            FOV = MathHelper.ToRadians(30);
            nearClipPlane = 1;
            farClipPlane = 100;
            this.distance = distance;
            Up = Vector3.UnitZ;
            AbsUp = Vector3.UnitZ;
            pivot = Matrix.CreateRotationX(MathHelper.ToRadians(xAngel)) * Matrix.CreateRotationZ(MathHelper.ToRadians(zAngel));//Matrix.CreateFromYawPitchRoll(0, MathHelper.ToRadians(xAngel), MathHelper.ToRadians(zAngel));
            arm = Matrix.CreateTranslation(new Vector3(0, -distance, 0));
            camera = Matrix.CreateLookAt(Vector3.UnitY, Vector3.Zero, Up);
        }
        public void UpdatePivot()
        {
            //pivot = Matrix.CreateFromYawPitchRoll(
            //    0,
            //    MathHelper.ToRadians(xAngel),
            //    MathHelper.ToRadians(zAngel));
            
            pivot = Matrix.CreateRotationX(MathHelper.ToRadians(xAngel))
                * Matrix.CreateRotationY(0)
                * Matrix.CreateRotationZ(MathHelper.ToRadians(zAngel));
            Up = Vector3.Transform(AbsUp, pivot);
            
        }
        public void UpdateProjection()
        {

        }
        public void UpdateCamera()
        {
            camera = Matrix.CreateLookAt(Vector3.UnitY, Vector3.Zero, Up);

        }
        public Vector3 GetCameraZ()
        {
            Matrix getView = View;
            return Vector3.Normalize(new Vector3(getView.M13, getView.M23, getView.M33));
        }
        public Vector3 GetCameraY()
        {
            Matrix getView = View;
            return Vector3.Normalize(new Vector3(getView.M12, getView.M22, getView.M32));
        }
        public Vector3 GetCameraX()
        {
            Matrix getView = View;
            return Vector3.Normalize(new Vector3(getView.M11, getView.M21, getView.M31));
        }
        public Vector3 GetCameraPosition()
        {
           return Vector3.Transform(Vector3.Zero,
                    Matrix.CreateTranslation(0, -(float)distance, 0) * pivot);
        }
        public BoundingFrustum CameraFrustrum
        {
            get { return new BoundingFrustum(View * Projection); }
        }
    }
}


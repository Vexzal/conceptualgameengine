﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace BaseEngine.Rendering
{
    public interface IModelMesh
    {
        Model DrawModel { get;  }
        Effect ModelEffect { get; }
        Matrix WorldTransform { get; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseEngine.Extensions;
using Microsoft.Xna.Framework.Graphics;
using RenderTargetLookup = BaseEngine.Rendering.RenderTargetManager.LookupContainer<Microsoft.Xna.Framework.Graphics.RenderTargetBinding>;

namespace BaseEngine.Rendering
{
    public class RenderTargetManager
    {
        public class LookupContainer<T>
        {
            internal Dictionary<string, int> indexLookup = new Dictionary<string, int>();
            internal List<T> lookupTable = new List<T>();

            public T this[string value]
            {
                get
                {
                    if (indexLookup.ContainsKey(value))
                    {
                        return lookupTable[indexLookup[value]];
                    }
                    return default(T);
                }
            }
            public List<string> Lookups
            {
                get => indexLookup.Keys.ToList();
            }
            public bool Contains(string name)
            {
                return indexLookup.ContainsKey(name);
            }
        }

        /// <summary>
        /// lookup multiple rendertarget collections
        /// </summary>
        public LookupContainer<RenderTargetBinding[]> MRTLookup = new LookupContainer<RenderTargetBinding[]>();
        /// <summary>
        /// lookup Named Render Targets
        /// </summary>
        public LookupContainer<RenderTargetBinding> NRTLookup = new LookupContainer<RenderTargetBinding>();
        #region Constructors
        public RenderTargetManager(GBufferMap gMap)
        {
            MRTBind(gMap);
        }
        public RenderTargetManager(NamedRenderTarget target)//see about replacing this with a struct instead of whatever the heck this is now
        {
            RTBind(target);
        }
        public RenderTargetManager(List<GBufferMap> gMaps)
        {
            foreach (var map in gMaps)
            {
                MRTBind(map);
            }
        }
        public RenderTargetManager(List<NamedRenderTarget> targetList)
        {
            foreach (var target in targetList)
            {
                RTBind(target);
            }
        }
        public RenderTargetManager(GBufferMap gMap, List<NamedRenderTarget> targetList)
        {
            MRTBind(gMap);
            foreach (var target in targetList)
            {
                RTBind(target);
            }
        }
        public RenderTargetManager(List<GBufferMap> gMaps, List<NamedRenderTarget> targetList)
        {
            foreach (var map in gMaps)
            {
                MRTBind(map);
            }
            foreach (var target in targetList)
            {
                RTBind(target);
            }
        }
        #endregion

        public void Bind()
        {
            UpdateBindings?.Invoke(NRTLookup);
        }

        #region internal bindings
        private void RTBind(NamedRenderTarget target)
        {
            NRTLookup.lookupTable.Add(target.Target);
            NRTLookup.indexLookup.Add(target.Name, NRTLookup.lookupTable.IndexOf(target.Target));
        }

        //so this is adding 2 things we remap this to add stuff to the MRT lookup and the NRT lookup
        private void MRTBind(GBufferMap mapping)
        {
            if (MRTLookup.Contains(mapping.Name))
                return;
            //alright so we'll have a temp list here for binding

            List<RenderTargetBinding> holdList = new List<RenderTargetBinding>();
            Dictionary<string, int> holdLookup = new Dictionary<string, int>();
            int i = 0;
            foreach (var map in mapping.targets)
            {
                //holdList.Add(new NamedRenderTarget(map.Name, new RenderTargetBinding(map.RenderTarget)));//see if I can just implement named rendertarget later
                holdList.Add(map.RenderTarget);
                holdLookup.Add(map.Name, i);
                foreach (var content in map.Contents)
                {
                    /**map is pre definned so thats done.
                     * //hmm this is a. a problem I guyess mix name with usage
                     * //eugh no that won't work, booo.//come back to this. 
                     * so to be specific dictionaries are unique keys and agnostic values. so everything needs a unique name. so Keeping up with the names is going to be important.asking for unique names isn't that tenuous but it'll be a programming nightmare later on**/
                    holdLookup.Add(content, i);
                }
                i++;
            }
            i = MRTLookup.lookupTable.Count;

            //MRTLookup.lookupTable.Add(holdList.ToDictionary(x => x.Key, x => x.Value).Values.ToArray());
            MRTLookup.lookupTable.Add(holdList.ToArray());
            MRTLookup.indexLookup.Add(mapping.Name, i);

            NRTLookup.lookupTable.AddRange(holdList);
            NRTLookup.indexLookup.AddRange(holdLookup);

        }
        #endregion

        public event RenderTargetUpdate UpdateBindings;//update this to return whole binding collection instead of a dictionary

    }
    public delegate void RenderTargetUpdate(RenderTargetLookup bindings);
}

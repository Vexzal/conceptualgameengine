﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;

namespace BaseEngine.Rendering
{
    public struct NamedRenderTarget
    {
        public string Name;
        public RenderTargetBinding Target;
        public NamedRenderTarget(string name, RenderTargetBinding target)
        {
            Name = name;
            Target = target;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace BaseEngine.Rendering
{
    public interface ICamera
    {
        Matrix View { get; }
        Matrix Projection { get; }
        BoundingFrustum CameraFrustrum { get; }
        Vector3 Position { get; }
    }
}

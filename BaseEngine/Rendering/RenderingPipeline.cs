﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using BaseEngine.Components;
using System;
namespace BaseEngine.Rendering
{
    [Obsolete]
    public interface IRenderingPipeline
    {
        Camera Camera { get; set; }
        System.Collections.Generic.List<MeshComponent> models { get; set; }
        void Load();
        //public abstract void ApplyTransform(TransformComponent transform);
        //public abstract void SetGBuffer();
        //public abstract void UnSetGBuffer();
        //public abstract void ClearGBuffer();
        //void DrawGBuffer();
        void Update(GameTime gameTime);
        void Draw(GameTime gameTime);
    }
}

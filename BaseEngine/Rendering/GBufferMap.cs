﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;

namespace BaseEngine.Rendering
{
    public class GBufferMap
    {
        public string Name;
        public struct RenderTargetDefinition
        {
            public string Name;
            public RenderTarget2D RenderTarget { get; set; }
            public List<string> Contents { get; set; }
            public RenderTargetDefinition(string name, RenderTarget2D target, List<string> contents)
            {
                Name = name;
                RenderTarget = target;
                Contents = contents;
            }
        }
        //this defines gbuffer mappings, so thats all this is going to do. 
        //on top of hat I need the targets for other render passes. 
        //
        public List<RenderTargetDefinition> targets = new List<RenderTargetDefinition>();

        public void AddMap(string name, RenderTarget2D target, List<string> contents)
        {
            targets.Add(new RenderTargetDefinition(name, target, contents));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RenderTargetLookup = BaseEngine.Rendering.RenderTargetManager.LookupContainer<Microsoft.Xna.Framework.Graphics.RenderTargetBinding>;

namespace BaseEngine.Rendering
{
    public interface IRenderTargetDependant
    {
        void BindTargets(RenderTargetLookup bindings);
    }
}

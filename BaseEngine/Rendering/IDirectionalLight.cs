﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace BaseEngine.Rendering
{
    public interface IDirectionalLight
    {
        Vector3 Direction { get; }
        Color Color{get;}
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace BaseEngine.Rendering
{
    //this is weird but its one of the few like constantly useful things to have
    //hold onto it
    public static class Quad
    {
        static VertexPositionTexture[] verts =
            new VertexPositionTexture[4] {
                new VertexPositionTexture(new Vector3(-1,-1,0), new Vector2(0,1)),
                new VertexPositionTexture(new Vector3(-1,1,0), new Vector2(0,0)),
                new VertexPositionTexture(new Vector3(1,-1,0), new Vector2(1,1)),
                new VertexPositionTexture(new Vector3(1,1,0), new Vector2(1,0))};
        static short[] ind = new short[6] { 0, 1, 2, 2, 1, 3 };

        public static void Draw(GraphicsDevice graphics)
        {
            graphics.DrawUserIndexedPrimitives(
                PrimitiveType.TriangleList,
                verts,
                0,
                4,
                ind,
                0,
                2);
        }
    }
}

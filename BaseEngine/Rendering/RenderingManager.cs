﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace BaseEngine.Rendering
{
    
    public class GenericRenderPipeline : IRenderingModule, IDisposable
    {
        string Name { get; set; }
        //need to solve, gbuffer issue since using deferred rendering by default is like fine. 
        //so like can you blend gbuffers into 
        //well lets just see how the fastest way to get gbuffer arrays from another 
        //okay what if we prefab mrt lists.
        //yeah, yeah!!!
        public RenderTargetManager TargetManager { get; set; }

        public short drawPriority { get; set; }
        ICamera camera;
        public ICamera Camera
        {
            get => camera;
            set
            {
                camera = value;
                //this is a short term solution it doesn't work in the long run when you have multiple cameras like when rendering shadow maps or secondary scenes
                //then again this only exists to add the, the editor camera.
                //I guess I could name it a component. I'll get to that
                foreach(IRenderingModule mod in RenderingModules)
                {
                    mod.Camera = value;
                }
            }
        }
        public GraphicsDevice graphics { get; set; }

        public IRenderingModule[] Modules
        {
            get => RenderingModules.ToArray();
        }
        

        List<IRenderingModule> RenderingModules = new List<IRenderingModule>();
        public void Add(params IRenderingModule[] Modules)
        {
            foreach(IRenderingModule module in Modules)
            {
                RenderingModules.Add(module);
            }
        }

        public void Sort()
        {
            RenderingModules.Sort(ModuleCompare);            
        }//so this could just be loaded in, load .
        int ModuleCompare<T>(T x, T y) where T: IRenderingModule
        {
            if (x.drawPriority < y.drawPriority)
                return -1;
            if (x.drawPriority == y.drawPriority)
                return 0;
            if (x.drawPriority > y.drawPriority)
                return 1;
            return 0;
        }
        public void Draw(GameTime gameTime)
        {
            foreach (IRenderingModule module in RenderingModules)
            {
                module.Draw(gameTime);
            }
        }

        public void Load(Scene gameScene)
        {
            drawPriority = -1;//does not order.
            //Sort();

            foreach(GameObject go in gameScene.gameObjects)
            {
                if(go.components.Keys.Contains( typeof(BaseEngine.Components.Camera)))
                {
                    Camera = go.GetComponent<BaseEngine.Components.Camera>();
                    break;
                }
            }

            foreach(IRenderingModule module in RenderingModules)
            {
                
                module.TargetManager = TargetManager;
                //don't pass the, the, the camera, each module might want its own.
                module.Camera = Camera;
                module.graphics = graphics;
                //nah pass everything by default for safety 
                //then overload
                module.Load(gameScene);
            }
            TargetManager.Bind();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}

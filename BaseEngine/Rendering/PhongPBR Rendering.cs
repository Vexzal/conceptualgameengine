﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using BaseEngine.Components;

namespace BaseEngine.Rendering
{
    //~~so a note a deferred shader has two sets of values~~
    //stuff about deferred shaders
    //expected outputs/inputs of the material itself, * needs code side representation
    //the rendertargets those outputs write to * needs code side representation
    //the calculations for turning material inputs into outputs *shader code
    //the calculations for turning rendertargets to drawn image *shader code

    //like so an idea of, in generating your renderer.
    //you would make a list of inputs you need or want, and then 
    //you would have to assign those to channels on the render targets
    //and assign those targets formats.

    /// <summary>
    /// an implementation of the phong deferred rendering from catalinzima.com
    /// http://www.catalinzima.com/xna/tutorials/deferred-rendering-in-xna/
    /// </summary>
    ///
    //the whole rendering pipeline system is outdated, remove it
    [Obsolete]
    public class PhongDeferredRendering : IRenderingPipeline //create an inhereted class for a general pipeline
    {
        public Camera Camera { get; set; }
        CoreGame game;

        RenderTarget2D colorRT;//Holds color and specular Intensity
        RenderTarget2D normalRT;//holds normal and specular power
        RenderTarget2D depthRT;// holds zdepth
        Effect ClearBuffer;
        Effect EngineConstants;
        public List<MeshComponent> models { get; set; }
        List<MeshComponent> drawModels = new List<MeshComponent>();
        List<TransformBind> worldParameters = new List<TransformBind>();
        List<EffectParameter> viewParameters = new List<EffectParameter>();
        List<EffectParameter> projectionParameters = new List<EffectParameter>();
        List<TransformBind> wvpParameters = new List<TransformBind>();
        List<EffectParameter> deltaParameters = new List<EffectParameter>(); // none of this works

        //#region EngineConstantParameterNames
        #region PipelineConstantParameterNames
        string WorldConstant = "World";
        string ViewConstant = "View";
        string ProjectionConstant = "Projection";
        string WVPConstant = "WorldViewProjection";
        string TimeConstant = "deltaTime";
        #endregion

        //Effect gbufferEffect; //move to model, add lighting effects here


        public PhongDeferredRendering(CoreGame game)
        {
            this.game = game;
            
        }

        public void Load()
        {
            int backBufferWidth = game.GraphicsDevice.PresentationParameters.BackBufferWidth;
            int backBufferHeight = game.GraphicsDevice.PresentationParameters.BackBufferHeight;

            //colorRT = new RenderTarget2D(GraphicsDevice, backBufferWidth, backBufferHeight);
            colorRT = new RenderTarget2D(game.GraphicsDevice, backBufferWidth, backBufferHeight, false, SurfaceFormat.Color, DepthFormat.Depth16);
            normalRT = new RenderTarget2D(game.GraphicsDevice, backBufferWidth, backBufferHeight, false, SurfaceFormat.Color, DepthFormat.Depth16);
            depthRT = new RenderTarget2D(game.GraphicsDevice, backBufferWidth, backBufferHeight, false, SurfaceFormat.Single, DepthFormat.Depth16);

            ClearBuffer = game.Content.Load<Effect>(@"GBufferEffects\ClearGBufferPhong");
            //EngineConstants = game.Content.Load<Effect>(@"ModelEffects\EngineConstants");
            //deltaTime = EngineConstants.Parameters["deltaTime"];
            //effectWorld = EngineConstants.Parameters["World"];
            //effectView = EngineConstants.Parameters["View"];
            //effectProjection = EngineConstants.Parameters["Projection"];
            //effectWVP = EngineConstants.Parameters["WorldViewProjection"];
            foreach(MeshComponent m in models)
            {
                var e = m.Effect;
                if(e[WorldConstant] != null)
                {
                    worldParameters.Add(new TransformBind(m.Transform, e[WorldConstant]));
                }
                if (e[ViewConstant] != null)
                {
                     viewParameters.Add(e[ViewConstant]);
                }
                if (e[ProjectionConstant] != null)
                {
                    projectionParameters.Add(e[ProjectionConstant]);
                }
                if (e[WVPConstant] != null)
                {
                    wvpParameters.Add(new TransformBind(m.Transform, e[WVPConstant]));
                }
                if (e[TimeConstant] != null)//wouldn't that just be an out of range exception? hmm
                {
                    deltaParameters.Add( e[TimeConstant]);//wait how does this work.
                }                
            }
        }
        
        public void ApplyTransform(TransformComponent transform)
        {
            //    effectWorld.SetValue(transform.AbsoluteTransform);
            //    effectView.SetValue(Camera.View);
            //    effectProjection.SetValue(Camera.Projection);
            //    effectWVP.SetValue(transform.AbsoluteTransform * Camera.View * Camera.Projection);
        }
        public void Update(GameTime gameTime)
        {
            //deltaTime.SetValue((float)gameTime.ElapsedGameTime.TotalSeconds)
            foreach(MeshComponent component in models)
            {
                foreach(ModelMesh mesh in component.mesh.Meshes)
                {
                    if(Camera.CameraFrustrum.Intersects(mesh.BoundingSphere))
                    {
                        drawModels.Add(component);
                        break;
                    }
                }
            }
            
        }
        public void SetGBuffer()
        {
            game.GraphicsDevice.SetRenderTargets(colorRT, normalRT, depthRT);            
        }
        public void UnSetGBuffer()
        {
            game.GraphicsDevice.SetRenderTarget(null);
        }
        VertexPositionTexture[] verts =
            new VertexPositionTexture[4] {
                new VertexPositionTexture(new Vector3(0,0,0), new Vector2(0,0)),
                new VertexPositionTexture(new Vector3(0,1,0), new Vector2(0,1)),
                new VertexPositionTexture(new Vector3(1,0,0), new Vector2(1,0)),
                new VertexPositionTexture(new Vector3(1,1,0), new Vector2(1,1))};
        short[] ind = new short[6] { 0, 1, 2, 2,1,3 };
        public void ClearGBuffer()
        {
            ClearBuffer.Techniques[0].Passes[0].Apply();
            //clear a quad here or a tri?
            game.GraphicsDevice.DrawUserIndexedPrimitives<VertexPositionTexture>(PrimitiveType.TriangleList,
                verts, 0, 4, ind, 0, 2);
        }

        public void DrawGBuffer()
        {
            game.spriteBatch.Begin();
            game.spriteBatch.Draw(colorRT, Vector2.Zero, Color.White);
            game.spriteBatch.End();
        }
        
        public void Draw(GameTime gameTime)
        {
            foreach (EffectParameter p in deltaParameters)
            {
                p.SetValue((float)gameTime.ElapsedGameTime.TotalSeconds);
            }
            foreach (TransformBind b in worldParameters)
            {
                b.parameter.SetValue(b.transform.GetParentedTransform());
            }
            foreach (EffectParameter p in viewParameters)
            {
                p.SetValue(Camera.View);
            }
            foreach (EffectParameter p in projectionParameters)
            {
                p.SetValue(Camera.Projection);
            }
            foreach (TransformBind b in wvpParameters)
            {
                b.parameter.SetValue(b.transform.GetParentedTransform() * Camera.View * Camera.Projection);
            }

            SetGBuffer();
            ClearGBuffer();
            foreach(MeshComponent model in drawModels)
            {
                DrawModel(model.mesh);
            }
            UnSetGBuffer();
            DrawGBuffer();
        }
        void DrawModel(Model model)
        {
            foreach(ModelMesh mesh in model.Meshes)
            {
                //mesh.Draw();
                for (int i = 0; i < mesh.MeshParts.Count; i++)
                {
                    var part = mesh.MeshParts[i];
                    var effect = part.Effect;

                    if (part.PrimitiveCount > 0)
                    {
                        game.GraphicsDevice.SetVertexBuffer(part.VertexBuffer);
                        game.GraphicsDevice.Indices = part.IndexBuffer;

                        for (int j = 0; j < effect.CurrentTechnique.Passes.Count; j++)
                        {
                            effect.CurrentTechnique.Passes[j].Apply();
                            game.GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, part.VertexOffset, part.StartIndex, part.PrimitiveCount);
                        }
                    }
                }
            }
        }


    }
    struct TransformBind
    {
        public TransformComponent transform;
        public EffectParameter parameter;
        public TransformBind(TransformComponent _transform, EffectParameter _parameter)
        {
            transform = _transform;
            parameter = _parameter;
        }
    }

}

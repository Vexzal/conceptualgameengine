﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseEngine;
using BaseEngine.Components;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace BaseEngine.Rendering
{

    //proposal add a base module class modules derive from
    //move processing of components into the module 
    //rendering also needs sub behaviours, for different contexts.
    //so like I Need to render 2d stuff to render targets for the screen or textures first.
    //then I need to 

        //currently using an enabled and disabled camera, don't do this. 
        //use find ways to pair objects to specific cameras, and abstract the render target to a rendering system.

    //redefine as services //modules are modulare components plugged in and out, services are things that manage modules, o. okay.
    public class RenderingService : Service, IRenderingService
    {
        CoreGame game;
        public IRenderingModule primaryModule { get; set; }

        public RenderingService(CoreGame _game)
        {
            game = _game;
        }
        public override void Load(Scene gameScene)
        {            
            primaryModule.Load(gameScene);            
        }                                            

        public void Draw(GameTime gameTime)
        {
            //int leaveneltFortnight;

            primaryModule.Draw(gameTime);
           
        }                             
    }
}

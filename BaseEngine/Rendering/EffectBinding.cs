﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
namespace BaseEngine.Rendering
{
    public class EffectBinding
    {
        public string Name;
        protected EffectParameter _parameter;
        public EffectBinding(string name, EffectParameter parameter)
        {
            Name = name;
            _parameter = parameter;
        }
        public virtual void Bind()
        {

        }
        public void Bind(float value)
        {

        }
        
    }
    public class FloatBinding : EffectBinding
    {
        IntPtr floatptr;
        public FloatBinding(string name, EffectParameter parameter) :
            base(name, parameter)
        {
        }       
        public SetBinding Binding;
        public override void Bind()
        {
            unsafe
            {
                _parameter.SetValue(*(float*)floatptr);
            }
            return;
            _parameter.SetValue(Binding());
        }
        public void Bind(float value)
        {           
        }        
        
        public delegate float SetBinding();
    }
    public class TextureBinding : EffectBinding
    {
        public TextureBinding(string name, EffectParameter parameter) :
           base(name, parameter)
        {}
        public SetBinding Binding;
        public override void Bind()
        {
            _parameter.SetValue(Binding());
        }
        public delegate Texture2D SetBinding();
    }
    public class MatrixBinding : EffectBinding
    {        
        public MatrixBinding(string name, EffectParameter parameter) :
            base(name, parameter)
        {}
        public SetBinding Binding;
        public override void Bind()
        {
            _parameter.SetValue(Binding());
        }
        public delegate Matrix SetBinding();
    }
    public class MatrixArrayBinding : EffectBinding
    {
        public MatrixArrayBinding(string name, EffectParameter parameter):
            base(name,parameter)
        {}
        public SetBinding Binding;
        public override void Bind()
        {
            _parameter.SetValue(Binding());
        }
        public delegate Matrix[] SetBinding();
    }
    
    
}

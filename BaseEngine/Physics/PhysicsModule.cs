﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

using BEPUphysics;

namespace BaseEngine.Physics
{
    [Obsolete]
    public class PhysicsModule : Service
    {
        CoreGame game;
        //public Space physicsSpace { get; private set; }
        IPhysicsModule Module;
        //List<PhysicsComponent> entities = new List<PhysicsComponent>();

        public PhysicsModule(CoreGame _game)
        {
            game = _game;
            //physicsSpace = new Space();
            //physicsSpace.ForceUpdater.Gravity = new BEPUutilities.Vector3(0, 0, -9.81f);
            
        }
        public override void Load()
        {
            //foreach (GameObject go in gameScene.gameObjects)
            //{
            //    foreach (PhysicsComponent entity in go.components.Values.OfType<PhysicsComponent>())
            //    {
            //        entities.Add(entity);
            //        LoadScene += entity.Load;
            //        //physics.AddEntity(entity);
            //    }
            //}
            //if (LoadScene!=null)
            //{
            //    LoadScene(physicsSpace);
            //}
            Module.Load(gameScene);
        }

        //scene management
        public void GameObjectAdded(GameObject go)
        {
            //if (go.GetComponent<PhysicsComponent>() != null)
            if(go.GetComponent<PhysicsComponent>() is IPhysicsComponent)
            {//this whole check is a problem with the interface configuration.
                Module.Add((IPhysicsComponent)go.GetComponent<PhysicsComponent>());
                //physicsSpace.Add(go.GetComponent<PhysicsComponent>().Body);
                //meshEntities.Add(go.GetComponent<MeshComponent>());
            }
        }
        public void GameObjectRemoved(GameObject go)
        {
            //var comp = go.GetComponent<MeshComponent>();
            var comp = go.GetComponent<PhysicsComponent>();
            //if (comp != null)
            if(comp is IPhysicsComponent)
            {
                //hmm.
                
                //this is assuming this class needs to contain everything when really I could just load physics entirely as a seperate module. service? yeah service.
                //because force checking against IPhysics component kind of sucks
                //removing bepu physics as a dependant on the base engine is gonna be better in the long run.

                //gets me thinking go look at, enable and disable of physics and where that is
                //physicsSpace.Remove(comp.Body);
                Module.Remove((IPhysicsComponent)comp);
                //if (physicsSpace.Remove(comp))
                //{
                //    meshEntities.Remove(comp);
                //}
            }
        }

        public PhysicsModule(CoreGame _game,BEPUutilities.Threading.IParallelLooper threadLooper)
        {
            game = _game;
            //physicsSpace = new Space(threadLooper);
        }
        //[Obsolete]
        //public void AddEntity(PhysicsComponent component)
        //{
        //    entities.Add(component);
        //    //component.GetBody(physicsSpace);
        //    //LoadScene += component.Load;
        //    physicsSpace.Add(component.Body);
            
        //}
        //[Obsolete]
        //public void AddEntityGroup(List<PhysicsComponent> components)
        //{
        //    foreach(PhysicsComponent component in components)
        //    {
        //        AddEntity(component);
        //    }
        //}

        public void Update(GameTime gameTime)
        {
            //physicsSpace.Update((float)gameTime.ElapsedGameTime.TotalSeconds);
        }
        delegate void Loading(Space space);
        event Loading LoadScene;
        
    }
}

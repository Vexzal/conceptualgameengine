﻿
using BEPUphysics.CollisionShapes;
using BEPUphysics.CollisionShapes.ConvexShapes;

namespace BaseEngine.Physics
{
    public static class DefaultShapes
    {
        static public BoxShape DefaultBox = new BoxShape(1, 1, 1);
        static public CapsuleShape DefaultCapsule = new CapsuleShape(2, .5f);
        static public ConeShape DefaultCone = new ConeShape(2, 1);
        static public CylinderShape DefaultCylinder = new CylinderShape(2, 1);
        static public SphereShape DefaultSphere = new SphereShape(1);
        
    }
}

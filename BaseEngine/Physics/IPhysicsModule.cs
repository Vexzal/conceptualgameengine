﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace BaseEngine.Physics
{
    interface IPhysicsModule
    {
        void Add(IPhysicsComponent component);
        void Remove(IPhysicsComponent component);
        void Load(Scene GameScene);
        void Update(GameTime gameTime);
    }

    interface IPhysicsComponent
    {

    }
    interface IBepuPhysicsComponent : IPhysicsComponent
    {
        BEPUphysics.ISpaceObject SpaceBody {get;set;}
    }
}

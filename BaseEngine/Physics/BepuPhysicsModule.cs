﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using BEPUphysics;
using BaseEngine.Components;

namespace BaseEngine.Physics
{
    class BepuPhysicsModule : IPhysicsModule
    {
        Space BepuSpace;
        public BepuPhysicsModule()
        {
            BepuSpace = new Space();
            BepuSpace.ForceUpdater.Gravity = new BEPUutilities.Vector3(0, 0, -9.81f);
        }

        public void Add(IPhysicsComponent Component)
        {
            BepuSpace.Add(((IBepuPhysicsComponent)Component).SpaceBody);
            //throw new NotImplementedException();
        }
        public void Remove(IPhysicsComponent Component)
        {
            BepuSpace.Remove(((IBepuPhysicsComponent)Component).SpaceBody);
            //throw new NotImplementedException();
        }
        public void Load(Scene GameScene)
        {
            foreach(GameObject go in GameScene.gameObjects)
            {
                foreach(Component c in go.components.Values)
                {
                    if(c is IBepuPhysicsComponent)
                    {
                        Add((IBepuPhysicsComponent)c);
                        break;
                    }
                }
            }
            //throw new NotImplementedException();
        }

        public void Update(GameTime gameTime)
        {
            BepuSpace.Update((float)gameTime.ElapsedGameTime.TotalSeconds);
            
        }

    }
}

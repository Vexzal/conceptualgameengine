﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseEngine.Components;

namespace BaseEngine
{
    public struct TwoKey<T,U>
    {
        public T TKey { get; set; }
        public U UKey { get; set; }
        public TwoKey(T t, U u)
        {
            TKey = t;
            UKey = u;
        }
    }
    public class TwoKeyCollection<T,U,V>
    {
        
        Dictionary<TwoKey<T, U>, V> collection = new Dictionary<TwoKey<T, U>, V>();

        public V this[T t,U u]
        {
            get
            {
                TwoKey<T, U> key = new TwoKey<T, U>(t, u);
                if(collection.ContainsKey(key))
                {
                    return collection[key];
                }
                return default(V);
            }
        }
        public void Add(T t, U u, V v)
        {
            collection.Add(new TwoKey<T, U>(t, u), v);
        }
    }

    public class Scene 
    {
        public string Name;
        public List<GameObject> gameObjects = new List<GameObject>();
        //centralizing components        

        //public TwoKeyCollection<GameObject, Type, Component> components = new TwoKeyCollection<GameObject, Type, Component>();
         
        //would this have its own asset system.
        //it probably must.
        //hmm. 
        //so the big problem is I've never worked out how loading a scene works
        //building one yeah but actually building one?
        //hmm. //okay lets do a thing. 
        
        //okay so lets think. I'm considering storing a list of all components in here, and then processing them through here.
        //and then storing indicies in the game objects themselves
        //and then in here I also need a pairing for assets and attatching them to game objects.
        //or.
        //hmm.
        //so the actual thing is I Need to be able to manage multiple collections of things at once
        //like a
        //well mm , hmm, like the player would be a prefab that goes everywhere.
        //so you'd need several asset collections I guess for the various things that go in and out of here.
        //or well.
        //hmm.
        //eughhhh
        //game objects are collections of objects in the scene.
        //game objects contain components that define behaviours.
        //so like lets say the game mode but you want someone to switch to editor mode.
        //what are the actual components that change
        //the input manager, hooked up to different bindings, the 
        //so game modes are really subsets of the scene, using different objects and states from within it.
        //that, helps a lot I guess.
        //it also means loading happens through game modes.
        //oh okay sow aht if I did that component thing but through the game modes.
        //
        //event system.               
        public Scene(string name)
        {
            Name = name;
        }
        public void AddGameObject(GameObject gameObj)
        {
            gameObjects.Add(gameObj);
            
            GameObjectAdded?.Invoke(gameObj);
        }
        public void AddGameObjects(params GameObject[] gos)
        {
            foreach(var go in gos)
            {
                gameObjects.Add(go);
                GameObjectAdded?.Invoke(go);
            }
        }
        public void RemoveGameObject(GameObject gameObj)
        {
            gameObjects.Remove(gameObj);
            GameObjectRemoved?.Invoke(gameObj);

        }

        public delegate void Initialize();
        public event Initialize InitializeScene;
        public delegate void OnGameObjectAdded(GameObject gameObject);
        public event OnGameObjectAdded GameObjectAdded;
        public delegate void OnGameObjectRemoved(GameObject gameObject);
        public event OnGameObjectRemoved GameObjectRemoved;
        public void Init()
        {
            if (InitializeScene != null)
                InitializeScene();
        }
        public void Initial()
        {
            foreach(GameObject go in gameObjects)
            {
                go.Initialize();
            }
        }
        
    }
}

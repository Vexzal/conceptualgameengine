﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseEngine
{
    public static class EngineServices
    {
        public static void ExpandArray<T>(ref T[] Array)
        {
            //if (ExpandFactor <= 1)
                //throw new InvalidOperationException("Expand Factor must be greater than 1");
            T[] arrayStorage = Array;


            var size = Array.Length + Array.Length / 2;
            size = (size + 63) & (~63);

            Array = new T[size];
            arrayStorage.CopyTo(Array, 0);
            arrayStorage = null;
        }
        public static void ExpandArray<T>(ref T[] Array, int ExpandLenght)
        {
            var size = Array.Length + ExpandLenght;
            T[] arrayStorage = Array;

            Array = new T[size];
            arrayStorage.CopyTo(Array, 0);
            arrayStorage = null;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseEngine.Input
{
    enum PressEventType
    {
        Up,
        Down,
        Pressed,
        Released
    }

    public class ButtonBinding : IInputBinding
    {
        internal string Name { get; private set; }
        string IInputBinding.Name { get => Name; }
            
        public string Device { get; set; }
        
        BindingType bType = BindingType.Button;
        BindingType IInputBinding.bType { get => bType; }

        //no key reference
        public ButtonBinding(string name)
        {
            Name = name;
        }
        internal void CallEvent(PressEventType type)
        {
            switch (type)
            {
                case PressEventType.Down:
                    Down?.Invoke();
                    break;
                case PressEventType.Up:
                    Up?.Invoke();
                    break;
                case PressEventType.Pressed:
                    Press?.Invoke();
                    break;
                case PressEventType.Released:
                    Release?.Invoke();
                    break;
            }
        }
        public event InputKeyDelegate Down;
        public event InputKeyDelegate Up;

        public event InputKeyDelegate Press;
        public event InputKeyDelegate Release;
    }
}

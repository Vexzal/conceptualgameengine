﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;

namespace BaseEngine.Input
{
    //figure out how to extend all this to other input types
    //since monogame has evertyhign very seperated

    //so the idea is, each input context would have its own one of these
    //right so different contexts can have custom input controls.

    class InputBindingMap
    {
        bool Active;
        Dictionary<Type, List<IInputBindingKey>> _Bindings = new Dictionary<Type, List<IInputBindingKey>>();

        public List<IInputBindingKey> DeviceBindings(Type device)
        {
            return _Bindings[device];
        }

        public void AddBinding<T>(IInputBindingKey key)
        {
            _Bindings[typeof(T)].Add(key);
        }
    }

    public class InputState2
    {
        Dictionary<string, EventDispatcher> Devices = new Dictionary<string, EventDispatcher>();
        public void Update()
        {
            //update all devices
            foreach(var Device in Devices.Values)
            {
                Device.Update();
            }
            //now update each map
        }
    }

    public class InputState
    {
        List<IKeyPressUpdate> KeyEventDispatchers = new List<IKeyPressUpdate>();
        List<IAxisUpdate> AxisEventDispatchers = new List<IAxisUpdate>();
        List<IFloatUpdate> FloatEventDispatchers = new List<IFloatUpdate>();

        Dictionary<Type, EventDispatcher> eventDispatchers = new Dictionary<Type, EventDispatcher>();
        Dictionary<Type, Type> eventDispatcherArgumentMap = new Dictionary<Type, Type>();
        //first type is type of dispatcher second type is type of arguments.
        //this is just verification stuff its not necessary but I want it.

        //Dictionary<string, InputBinding> pressBindings = new Dictionary<string, InputBinding>();
        Dictionary<string, IInputBinding> bindings = new Dictionary<string, IInputBinding>();


        //this won't work with including axis types but we'll, get to that
        public AxisBinding GetAxis(string name)
        {
            if(bindings.ContainsKey(name))
            {
                if(bindings[name].bType == BindingType.Axis)
                {
                    return (AxisBinding)bindings[name];
                }
            }
            return null;
        }
        public ButtonBinding GetButton(string name)
        {
            if(bindings.ContainsKey(name))
            {
                if(bindings[name].bType == BindingType.Button)
                {
                    return (ButtonBinding)bindings[name];
                }
            }
            return null;
        }
        public FloatBinding GetFloat(string name)
        {
            if(bindings.ContainsKey(name))
            {
                if (bindings[name].bType == BindingType.Float)
                    return (FloatBinding)bindings[name];
            }
            return null;
        }
        //public InputBinding this[string name]
        //{
        //    get
        //    {
        //        if(pressBindings.Keys.Contains(name))
        //        {
        //            return pressBindings[name];
        //        }
        //        return null;
        //        //return new InputBind("");//returning null will cause runtime errors which is...easier to fix actually so fuck this                
        //    }
        //}
        public void AddDispatcher(EventDispatcher dispatcher)
        {
            var dispatchType = dispatcher.GetType();

            eventDispatchers.Add(dispatchType, dispatcher);
            eventDispatcherArgumentMap.Add(dispatchType, dispatcher.KeyArgumentType);
            if(dispatchType.GetInterfaces().Contains(typeof(IKeyPressUpdate)))
            {
                KeyEventDispatchers.Add((IKeyPressUpdate)dispatcher);
            }
            if(dispatchType.GetInterfaces().Contains(typeof(IAxisUpdate)))
            {
                AxisEventDispatchers.Add((IAxisUpdate)dispatcher);
            }//uhhhh this should work yeah??
            if(dispatchType.GetInterfaces().Contains(typeof(IFloatUpdate)))
            {
                FloatEventDispatchers.Add((IFloatUpdate)dispatcher);
            }
        }

        public void AddBinding<T>(IInputBindingKey binding) where T: EventDispatcher
        {
            var dispatcherType = typeof(T);
            var argumentsType = binding.GetType();

            if(eventDispatchers.ContainsKey(dispatcherType))
            {
                if(argumentsType == eventDispatcherArgumentMap[dispatcherType] )
                {
                    eventDispatchers[dispatcherType].AddBinding(binding);

                }
            }
            if(!bindings.ContainsValue(binding.binding))
            {
                bindings.Add(binding.binding.Name, binding.binding);
            }
        }

        public void Update()
        {
            foreach(EventDispatcher dispatcher in eventDispatchers.Values)
            {
                dispatcher.Update();//make sure everything is current
            }

            foreach(IInputBinding binding in bindings.Values)
            {
                foreach(IKeyPressUpdate keyUpdate in KeyEventDispatchers)
                {
                    if(binding.bType == BindingType.Button)
                        keyUpdate.TryUpdate((ButtonBinding)binding);
                }
                foreach(IAxisUpdate axisUpdate in AxisEventDispatchers)
                {
                    if(binding.bType == BindingType.Axis)
                        axisUpdate.TryUpdate((AxisBinding)binding);
                }
                foreach(IFloatUpdate floatUdpate in FloatEventDispatchers)
                {
                    if(binding.bType == BindingType.Float)
                        floatUdpate.TryUpdate((FloatBinding)binding);
                }
            }
            //then we'll do all the event updates


            //foreach (InputBinding binding in pressBindings.Values)
            //{
            //    foreach (EventDispatcher dispatcher in eventDispatchers.Values)
            //    {
            //        dispatcher.TryUpdate(binding);
            //    }
            //}
        }  
    }

   
    //alright so the idea is different input states have different input events
    //

    [Flags]
    enum KeyboardModifiers
    {
        // or, we do a 
        Shift,
        Alt,
        Ctrl
    }
    enum ControllerType
    {
        Keyboard,
        Gamepad,
        Touch,

    }
    //enum EventType
    //{
    //    Up,
    //    Down,
    //    Pressed,
    //    Released
    //}

    class InputBindingScratch
    {
        string Name;//binding name
        Microsoft.Xna.Framework.Input.Buttons BUTTON;
        Microsoft.Xna.Framework.Input.Keys KeyboardKeys;
        Microsoft.Xna.Framework.Input.Touch.GestureType GESTURE;
        

        public void XAlt()
        {
            
        }
        

    }
       
    public delegate void InputKeyDelegate();
    public delegate void InputAxisDelegate(Vector2 v);
    public delegate void InputSensorDelegate(Vector3 v);
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseEngine.Input
{
    
    public class FloatBinding : IInputBinding
    {
        internal string Name { get; private set; }
        string IInputBinding.Name { get => Name; }

        BindingType bindingType = BindingType.Float;
        BindingType IInputBinding.bType { get => bindingType; }

        public FloatBinding(string name)
        {
            Name = name;
        }
        internal void CallEvent(float value)
        {
            FloatVal?.Invoke(value);
        }

        public event InputFloatDelegate FloatVal;
        

    }
    public delegate void InputFloatDelegate(float v);
}

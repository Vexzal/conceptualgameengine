﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
//I need to be able to move, the camera first.
//so lets move the camera.
//that'll be a quick certainty test

namespace BaseEngine.Input
{
    //okay this won't work/
    //so lets say what the list is. 
    //what does it need
    //you need to be able to assign callbacks to events
    //so you need to be able to call for a input name 
    //so you get    
    
    [Flags]
    public enum Modifiers
    {
        Null = 0,
        Shift = 1<<0,
        Ctrl = 1<<1,
        Alt = 1<<2,
    }
    public abstract class EventDispatcher
    {
        public string Name;
        public Type KeyArgumentType;
        //public abstract void TryUpdate(InputBinding binding);
        public abstract void Update();
        
        public abstract void AddBinding(IInputBindingKey bind);
    }
    public class KeyboardEventDispatcher : EventDispatcher, IKeyPressUpdate
    {
        Dictionary<string, KeyboardKeyCombo> keyBindings = new Dictionary<string, KeyboardKeyCombo>();
        KeyboardState OldState;
        KeyboardState State;
        //public Type KeyArgumentType { get; private set; }
        Modifiers ModifierState = Modifiers.Null;


        public KeyboardEventDispatcher(string name)
        {
            Name = name;
            KeyArgumentType = typeof(KeyboardBind);
        }

        public void Add(ButtonBinding bind, KeyboardKeyCombo key)
        {
            keyBindings.Add(bind.Name, key);
        }
        public override void AddBinding(IInputBindingKey bind)
        {
            var binding = (KeyboardBind)bind;
            keyBindings.Add(binding.BindingName, binding.BindingCombo);
        }
        public override void Update()
        {
            OldState = State;
            State = Keyboard.GetState();

            ModifierState = Modifiers.Null;
            if (State.IsKeyDown(Keys.LeftShift) || State.IsKeyDown(Keys.RightShift))
                ModifierState |= Modifiers.Shift;
            if (State.IsKeyDown(Keys.LeftAlt) || State.IsKeyDown(Keys.RightAlt))
                ModifierState |= Modifiers.Alt;
            if (State.IsKeyDown(Keys.LeftControl) || State.IsKeyDown(Keys.RightControl))
                ModifierState |= Modifiers.Alt;
        }
        public void TryUpdate(ButtonBinding binding)
        {            
            if (keyBindings.ContainsKey(binding.Name))
            {
                if(State.IsKeyDown(keyBindings[binding.Name].BindingKey) && keyBindings[binding.Name].BindingModifier == ModifierState)
                {
                    if(OldState.IsKeyUp(keyBindings[binding.Name].BindingKey))
                    {
                        binding.CallEvent(PressEventType.Pressed);
                    }
                    binding.CallEvent(PressEventType.Down);
                }
                if (State.IsKeyUp(keyBindings[binding.Name].BindingKey) && keyBindings[binding.Name].BindingModifier == ModifierState)
                {
                    if(OldState.IsKeyDown(keyBindings[binding.Name].BindingKey))
                    {
                        binding.CallEvent(PressEventType.Released);
                    }
                    binding.CallEvent(PressEventType.Up);
                }
                //if (State.IsKeyDown(keyBindings[binding.Name].Binkdi) && OldState.IsKeyUp(keyBindings[binding.Name]))
                //{
                //    binding.CallEvent(PressEventType.Pressed);
                //}
                //if (State.IsKeyUp(keyBindings[binding.Name]) && OldState.IsKeyDown(keyBindings[binding.Name]))
                //{
                //    binding.CallEvent(PressEventType.Released);
                //}
            }
        }
    }

    public struct KeyboardBind : IInputBindingKey
    {
        public IInputBinding binding {get;set ;}
        public string BindingName;
        public KeyboardKeyCombo BindingCombo;

        public string Device { get; private set; }
        public KeyboardBind(ButtonBinding binding, KeyboardKeyCombo key)
        {
            Device = "Keyboard";
            this.binding = binding;
            BindingName = binding.Name;
            BindingCombo = key;
        }
    }
    public struct KeyboardKeyCombo
    {
        public Keys BindingKey;
        public Modifiers BindingModifier;
        public KeyboardKeyCombo(Keys key, Modifiers modifiers)
        {
            BindingKey = key;
            BindingModifier = modifiers;
        }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace BaseEngine.Input
{
    //so I like how this is set up but I want an additional layer of abstraction where I can just add, seperate update listeners.
    //to a single device.
    //right now its a device and all the bindings
    //so I did this in a way I don't like so I'll make a second one, beecause of course
    public class MouseEventDispatcher : EventDispatcher, IKeyPressUpdate, IAxisUpdate, IFloatUpdate
    {

        MouseState OldState;
        MouseState State;

        Modifiers ModifierState = Modifiers.Null;

        Dictionary<string, MouseButtonCombo> keyBindings = new Dictionary<string, MouseButtonCombo>();
        List<string> axisBindings = new List<string>();
        List<string> floatBindings = new List<string>();
        List<InputBindingMap> maps = new List<InputBindingMap>();

        public MouseEventDispatcher(string name)
        {
            Name = name;
            KeyArgumentType = typeof(MouseBinding);
            
        }
        public void tryUpdate2()
        {
            foreach(var map in maps)
            {
                foreach(var binding in map.DeviceBindings(this.GetType()))
                {
                    if(binding is MouseBinding)
                    {

                    }
                }
            }
        }

        public override void AddBinding(IInputBindingKey bind)
        {
            var binding = (MouseBinding)bind;
            if(binding.IsAxis)
            {
                axisBindings.Add(binding.Name);                
            }
            else if(binding.IsFloat)
            {
                floatBindings.Add(binding.Name);
            }
            else
            {
                keyBindings.Add(binding.Name, binding.Button);
            }
        }

        public override void Update()
        {
            OldState = State;
            State = Mouse.GetState();

            var KState = Keyboard.GetState();

            ModifierState = Modifiers.Null;

            if (KState.IsKeyDown(Keys.LeftShift) || KState.IsKeyDown(Keys.RightShift))
                ModifierState |= Modifiers.Shift;
            if (KState.IsKeyDown(Keys.LeftAlt) || KState.IsKeyDown(Keys.RightAlt))
                ModifierState |= Modifiers.Alt;
            if (KState.IsKeyDown(Keys.LeftControl) || KState.IsKeyDown(Keys.RightControl))
                ModifierState |= Modifiers.Shift;
        }

        public void TryUpdate(ButtonBinding b)
        {
            if(keyBindings.ContainsKey(b.Name))
            {
                var keyVal = keyBindings[b.Name];
                if (IsButtonDown(keyVal.BindButton) && (ModifierState == keyVal.BindModifiers))
                {
                    if(IsOldButtonUp(keyVal.BindButton))
                    {
                        b.CallEvent(PressEventType.Pressed);
                    }
                    b.CallEvent(PressEventType.Down);
                }
                if(IsButtonUp(keyVal.BindButton) && (ModifierState == keyVal.BindModifiers))
                {
                    if(IsOldButtonDown(keyVal.BindButton))
                    {
                        b.CallEvent(PressEventType.Released);
                    }
                    b.CallEvent(PressEventType.Released);
                }               
            }
        }
        private bool IsButtonDown(MouseButton button)
        {
            if (button == MouseButton.Left)
            {
                if (State.LeftButton == ButtonState.Pressed)
                    return true;
            }
            if (button == MouseButton.Middle)
            {
                if (State.MiddleButton == ButtonState.Pressed)
                    return true;
            }
            if (button == MouseButton.Right)
            {
                if (State.RightButton == ButtonState.Pressed)
                    return true;
            }
            if (button == MouseButton.xButton1)
            {
                if (State.XButton1 == ButtonState.Pressed)
                    return true;
            }
            if (button == MouseButton.xButton2)
            {
                if (State.XButton2 == ButtonState.Pressed)
                    return true;
            }
            return false;
        }
        private bool IsButtonUp(MouseButton button)
        {
            return !IsButtonDown(button);
        }

        private bool IsOldButtonDown(MouseButton button)
        {
            if (button == MouseButton.Left)
            {
                if (OldState.LeftButton == ButtonState.Pressed)
                    return true;
            }
            if (button == MouseButton.Middle)
            {
                if (OldState.MiddleButton == ButtonState.Pressed)
                    return true;
            }
            if (button == MouseButton.Right)
            {
                if (OldState.RightButton == ButtonState.Pressed)
                    return true;
            }
            if (button == MouseButton.xButton1)
            {
                if (OldState.XButton1 == ButtonState.Pressed)
                    return true;
            }
            if (button == MouseButton.xButton2)
            {
                if (OldState.XButton2 == ButtonState.Pressed)
                    return true;
            }
            return false;
        }
        public bool IsOldButtonUp(MouseButton button)
        {
            return !IsOldButtonDown(button);
        }

        public void TryUpdate(AxisBinding b)
        {
            if(axisBindings.Contains(b.Name))
            {
                b.CallEvent(State.Position.ToVector2());
            }
        }
        public void TryUpdate(FloatBinding b)
        {
            if (floatBindings.Contains(b.Name))
                b.CallEvent(State.ScrollWheelValue);
        }
    }
    public struct MouseButtonCombo
    {
        public MouseButton BindButton;
        public Modifiers BindModifiers;
        public MouseButtonCombo(MouseButton button, Modifiers modifier)
        {
            BindButton = button;
            BindModifiers = modifier;
        }
    }
    //public struct MouseClickBinding : IInputBindingKey
    //{
    //    //so we're left with the problem that gets lost 
    //    //when 
    //}
    //so I have this as a, single thing and it could easily be the three or whatever.
    public struct MouseBinding : IInputBindingKey
    {
        public string Name;
        public string Device { get; private set; }
        public MouseButtonCombo Button;
        public bool IsAxis;
        public bool IsFloat;        
        public IInputBinding binding { get; set; }
        

        public MouseBinding(FloatBinding binding)
        {
            Name = binding.Name;
            //Button = MouseButton.Left;
            Device = "Mouse";
            Button = new MouseButtonCombo(MouseButton.Left, Modifiers.Null);
            IsAxis = false;
            IsFloat = true;
            this.binding = binding;
        }
        public MouseBinding(AxisBinding binding)
        {
            Name = binding.Name;
            Device = "Mouse";
            Button = new MouseButtonCombo(MouseButton.Left, Modifiers.Null);
            IsAxis = true;
            IsFloat = false;
            this.binding = binding;
        }
        public MouseBinding(ButtonBinding binding, MouseButtonCombo button)
        {
            Name = binding.Name;
            Device = "Mouse";
            Button = button;
            IsAxis = false;
            IsFloat = false;
            this.binding = binding;
        }

    }
}

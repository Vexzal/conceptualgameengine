﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace BaseEngine.Input
{
    //so the idea is
    //we have input handler
    /*
     *command map //list 
     *  list of input bindings
     *  active bool
     *input handler
     *  list of command maps
     *  devices
     * 
     * device
     *   device info
     *   update//updates device info
     *   binding update//pass a binding and it gets parsed and updated
     */
    //writing this incase I just want a delta event later
    enum AxisEventType
    {
        Still,
        Moved
    }

    public class AxisBinding :IInputBinding
    {
        public string Name { get; private set; }

        public string Device { get; set; }

        string IInputBinding.Name { get => Name; }

        BindingType bType = BindingType.Axis;
        
        BindingType IInputBinding.bType { get => bType; }

        public AxisBinding(string name)
        {
            Name = name;
        }
        internal void CallEvent(Vector2 axis)
        {
            Axis?.Invoke(axis);
        }

        public event InputAxisDelegate Axis;
    }
}

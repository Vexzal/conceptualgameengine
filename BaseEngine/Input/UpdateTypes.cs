﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseEngine.Input
{
    interface IKeyPressUpdate
    {
        void TryUpdate(ButtonBinding b);
    }
    interface IAxisUpdate
    {
        void TryUpdate(AxisBinding b);
    }
    interface IFloatUpdate
    {
        void TryUpdate(FloatBinding b);
    }

}

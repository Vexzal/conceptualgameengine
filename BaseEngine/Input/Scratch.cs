﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using static BaseEngine.Input.CommandBuilder;
using BaseEngine.Extensions;

namespace BaseEngine.Input
{


    //lets startfrom scratch and put all of it here

    //current
    public abstract class InputDevice
    {
        public string Name;
        public abstract void Update();
        public abstract void UpdateBinding(IKeyBinding binding);
    }
    //current
    public class InputHandler
    {
        Dictionary<string, InputDevice> Devices = new Dictionary<string, InputDevice>();

        List<IMapping> Maps = new List<IMapping>();

        public void AddDevice(InputDevice device)
        {
            Devices.Add(device.Name, device);

        }
        public void AddCommandMap(IMapping map)
        {
            Maps.Add(map);
        }

        public void Update(GameTime gameTime)
        {
            //update all devices
            foreach (var Device in Devices.Values)
            {
                Device.Update();
            }
            foreach (var Map in Maps)
            {
                if (Map.Active)
                {
                    Map.Update(ref Devices);
                    //foreach (var Binding in Map)
                    //{
                    //    Devices[Binding.Device].UpdateBinding(Binding);
                    //}
                }
            }
            //now update each map
        }
        //we should update this to have a list of command listeners that pull from whats pulled from the command map
        //yeah that totally makes sense its the whole string thing
        //so that command strings can be responded to per frame.
        //what does modifiers look like.
    }


    public interface IMapping /*: IEnumerable<ICommand>*/
    {
        bool Active { get; set; }
        void Update(ref Dictionary<string, InputDevice> devices);

    }

    public class CommandMap : IMapping
    {
        public bool Active { get; set; }
        Dictionary<string, ICommandDispatcher> bindings = new Dictionary<string, ICommandDispatcher>();
        List<IKeyBinding> KeyBindings = new List<IKeyBinding>();

        public void AddBinding(ICommandDispatcher binding)
        {
            bindings.Add(binding.Name, binding);//should add remove and dictionary I guess
        }
        public InputCommand GetToggle(string Name)
        {
            if (bindings.ContainsKey(Name))
            {
                if (bindings[Name] is InputCommand)
                    return (InputCommand)bindings[Name];
            }
            return null;

        }
        public AxisBinding GetAxis(string Name)
        {
            if (bindings.ContainsKey(Name))
            {
                if (bindings[Name] is AxisBinding)
                    return (AxisBinding)bindings[Name];
            }
            return null;

        }



        public void ToggleEnable()
        {
            Active = !Active;
        }
        public void Disable()
        {
            Active = false;
        }
        public void Enable()
        {
            Active = true;
        }



        public void Update(ref Dictionary<string, InputDevice> devices)
        {
            //foreach(var binding in bindings.Values)
            //{
            //    foreach (var bbinding in binding.KeyBindings)
            //    {
            //        //devices[bbinding.Device].UpdateBinding(bbinding);
            //        devices[bbinding.Device].UpdateBinding(bbinding);
            //    }
            //}
            foreach (var binding in KeyBindings)
            {
                devices[binding.Device].UpdateBinding(binding);
            }
        }
    }
    public enum JoyconAux
    {
        JoystickX,
        JoystickY,
        GyroX,
        GyroY,
        GyroZ,
        AccelX,
        AccelY,
        AccelZ
    }
    public enum JoyconOneAux
    {
        JoystickX,
        JoystickY,
        GyroX,
        GyroY,
        GyroZ,
        AccelX,
        AccelY,
        AccelZ
    }
    public enum JoyconTwoAux
    {
        JoystickX,
        JoystickY,
        GyroX,
        GyroY,
        GyroZ,
        AccelX,
        AccelY,
        AccelZ
    }
    public enum JoyconThreeAux
    {
        JoystickX,
        JoystickY,
        GyroX,
        GyroY,
        GyroZ,
        AccelX,
        AccelY,
        AccelZ
    }
    public enum JoyconFourAux
    {
        JoystickX,
        JoystickY,
        GyroX,
        GyroY,
        GyroZ,
        AccelX,
        AccelY,
        AccelZ
    }
    public enum JoyconOneButtons 
    {
        DownY = 1 << 0,
        UpX = 1 << 1,
        RightB = 1 << 2,
        LeftA = 1 << 3,
        SR = 1 << 4,
        SL = 1 << 5,
        LR = 1 << 6,
        ZLR = 1 << 7,
        Minus = 1 << 8,
        Plus = 1 << 9,
        StickR = 1 << 10,
        StickL = 1 << 11,
        Home = 1 << 12,
        Capture = 1 << 13
    }
    public enum JoyconTwoButtons
    {
        DownY = 1 << 0,
        UpX = 1 << 1,
        RightB = 1 << 2,
        LeftA = 1 << 3,
        SR = 1 << 4,
        SL = 1 << 5,
        LR = 1 << 6,
        ZLR = 1 << 7,
        Minus = 1 << 8,
        Plus = 1 << 9,
        StickR = 1 << 10,
        StickL = 1 << 11,
        Home = 1 << 12,
        Capture = 1 << 13
    }
    public enum JoyconThreeButtons
    {
        DownY = 1 << 0,
        UpX = 1 << 1,
        RightB = 1 << 2,
        LeftA = 1 << 3,
        SR = 1 << 4,
        SL = 1 << 5,
        LR = 1 << 6,
        ZLR = 1 << 7,
        Minus = 1 << 8,
        Plus = 1 << 9,
        StickR = 1 << 10,
        StickL = 1 << 11,
        Home = 1 << 12,
        Capture = 1 << 13
    }
    public enum JoyconFourButtons
    {
        DownY = 1 << 0,
        UpX = 1 << 1,
        RightB = 1 << 2,
        LeftA = 1 << 3,
        SR = 1 << 4,
        SL = 1 << 5,
        LR = 1 << 6,
        ZLR = 1 << 7,
        Minus = 1 << 8,
        Plus = 1 << 9,
        StickR = 1 << 10,
        StickL = 1 << 11,
        Home = 1 << 12,
        Capture = 1 << 13
    }

    public class JoyconInputDevice : InputDevice
    {
        JoyconIndex playerIndex;
        JoyconState state;
        Type keyTypeButton;
        Type keyTypeAux;


        public JoyconInputDevice(JoyconIndex jIndex)
        {
            Name = "Joycon";//this is the problem.
            //this assumes 
            //how do I bind a command to an individual controller.
            //because the systmem I have here assumes single device input
            //or IE each key type only matches to one device.
            //put joycon info in keybinding.
            //validTypes.Add(typeof(JoyconButtons), Name);
            //validTypes.Add(typeof(JoyconAux), Name);
            playerIndex = jIndex;
            if(jIndex == JoyconIndex.JoyOne)
            {
                Name += "One";
                validTypes.Add(typeof(JoyconOneButtons), Name);
                validTypes.Add(typeof(JoyconOneAux), Name);
                keyTypeButton = typeof(JoyconOneButtons);
                keyTypeAux = typeof(JoyconOneAux);
                //I oh right modify the name
                
            }
            if (jIndex == JoyconIndex.JoyTwo)
            {
                Name += "Two";
                validTypes.Add(typeof(JoyconTwoButtons), Name);
                validTypes.Add(typeof(JoyconTwoAux), Name);
                keyTypeButton = typeof(JoyconTwoButtons);
                keyTypeAux = typeof(JoyconTwoAux);
                //I oh right modify the name

            }
            if (jIndex == JoyconIndex.JoyThree)
            {
                Name += "Three";
                validTypes.Add(typeof(JoyconThreeButtons), Name);
                validTypes.Add(typeof(JoyconThreeAux), Name);
                keyTypeButton = typeof(JoyconThreeButtons);
                keyTypeAux = typeof(JoyconThreeAux);
                //I oh right modify the name

            }
            if (jIndex == JoyconIndex.JoyFour)
            {
                Name += "Four";
                validTypes.Add(typeof(JoyconFourButtons), Name);
                validTypes.Add(typeof(JoyconFourAux), Name);
                keyTypeButton = typeof(JoyconFourButtons);
                keyTypeAux = typeof(JoyconFourAux);
                //I oh right modify the name
            }


        }
        public override void Update()
        {
            state = Joycon.GetState(playerIndex);
        }
        public override void UpdateBinding(IKeyBinding binding)
        {
            if (binding.Dispatcher is ToggleCommandDispatcher)
            {
                var dispatcher = binding.Dispatcher as ToggleCommandDispatcher;
                if (binding.Key.GetType() == keyTypeButton)
                {
                    if (state.IsButtonDown((JoyconButtons)binding.Key))
                        dispatcher.InvokeDown();
                    else
                        dispatcher.InvokeUp();
                }
                if(binding.Key.GetType() == keyTypeAux )
                {
                    if((JoyconAux)binding.Key == JoyconAux.JoystickX)
                    {
                        if (state.ThumbStick.X != 0)
                            dispatcher.InvokeDown();
                        else
                            dispatcher.InvokeUp();
                    }
                    if ((JoyconAux)binding.Key == JoyconAux.JoystickY)
                    {
                        if (state.ThumbStick.Y != 0)
                            dispatcher.InvokeDown();
                        else
                            dispatcher.InvokeUp();
                    }
                    if ((JoyconAux)binding.Key == JoyconAux.GyroX)
                    {
                        if (state.Gyroscope.pitch != 0)
                            dispatcher.InvokeDown();
                        else
                            dispatcher.InvokeUp();
                    }
                    if ((JoyconAux)binding.Key == JoyconAux.GyroY)
                    {
                        if (state.Gyroscope.roll != 0)
                            dispatcher.InvokeDown();
                        else
                            dispatcher.InvokeUp();
                    }
                    if ((JoyconAux)binding.Key == JoyconAux.GyroZ)
                    {
                        if (state.Gyroscope.yaw != 0)
                            dispatcher.InvokeDown();
                        else
                            dispatcher.InvokeUp();
                    }

                }
            }
            if(binding.Dispatcher is AxisCommandDispatcher)
            {
                var dispater = binding.Dispatcher as AxisCommandDispatcher;
                if (binding.Key.GetType() == keyTypeButton)
                {
                    
                    if (state.IsButtonDown((JoyconButtons)binding.Key))
                        dispater.InvokeAxis(1 * binding.Scale);
                    else
                        dispater.InvokeAxis(0);
                }
                else
                {
                    if((JoyconAux)binding.Key == JoyconAux.JoystickX)
                    {
                        dispater.InvokeAxis(state.ThumbStick.X * binding.Scale);
                    }
                    if ((JoyconAux)binding.Key == JoyconAux.JoystickY)
                    {
                        dispater.InvokeAxis(state.ThumbStick.Y * binding.Scale);
                    }
                    if ((JoyconAux)binding.Key == JoyconAux.GyroX)
                    {
                        dispater.InvokeAxis(state.Gyroscope.pitch * binding.Scale);
                    }
                    if ((JoyconAux)binding.Key == JoyconAux.GyroY)
                    {
                        dispater.InvokeAxis(state.Gyroscope.roll * binding.Scale);
                    }
                    if ((JoyconAux)binding.Key == JoyconAux.GyroZ)
                    {
                        dispater.InvokeAxis(state.Gyroscope.yaw * binding.Scale);
                    }
                    if ((JoyconAux)binding.Key == JoyconAux.AccelX)
                    {
                        dispater.InvokeAxis(state.Acclerometer.x * binding.Scale);
                    }
                    if ((JoyconAux)binding.Key == JoyconAux.AccelY)
                    {
                        dispater.InvokeAxis(state.Acclerometer.y * binding.Scale);
                    }
                    if ((JoyconAux)binding.Key == JoyconAux.AccelZ)
                    {
                        dispater.InvokeAxis(state.Acclerometer.z * binding.Scale);
                    }
                }
            }
        }
    }
    //public class JoyconPairInputDevice : InputDevice
    //{
    //    JoyconPairState state;
    //    public JoyconPairInputDevice()
    //    {
    //        Name = "JoyconPair"
    //    }
    //
    public enum JoyconPairAux
    {
        LeftStickX,
        LeftStickY,
        RightStickX,
        RightStickY,
        LeftGyroX,
        LeftGyroY,
        LeftGyroZ,
        RightGyroX,
        RightGyroY,
        RightGyroZ,
        LeftAccelX,
        LeftAccelY,
        LeftAccelZ,
        RightAccelX,
        RightAccelY,
        RightAccelZ
    }
    public class JoyconPairInputDevice : InputDevice
    {
        JoyconPairState state;
        public JoyconPairInputDevice()
        {
            Name = "JoyconPair";
            if (Joycon.GetState(JoyconIndex.JoyOne).LeftRight)
                JoyconPair.AddDevice(JoyconIndex.JoyOne, JoyconIndex.JoyTwo);
            else
                JoyconPair.AddDevice(JoyconIndex.JoyTwo, JoyconIndex.JoyOne);
            validTypes.Add(typeof(JPairButton), Name);
        }
        public override void Update()
        {
            state = JoyconPair.GetState(JoyconPairPlayer.PairOne);
        }
        public override void UpdateBinding(IKeyBinding binding)
        {
            
        }
    }

    public class KeyboardInputDevice : InputDevice
    {
        //I want single state events only
        //per frame events are a hastle and mostly unecessary
        //and if I need them I can pull from axis events
        //I might come to 
        KeyboardState oldState;
        KeyboardState State;
        public KeyboardInputDevice()
        {
            Name = "Keyboard";

            validTypes.Add(typeof(Keys), Name);//yeah thats it for                                                      
        }
        public override void Update()
        {
            oldState = State;
            State = Keyboard.GetState();
        }

        public override void UpdateBinding(IKeyBinding binding)
        {
            if (binding.Dispatcher is ToggleCommandDispatcher)
            {
                var val = binding.Dispatcher as ToggleCommandDispatcher;
                var Key = (Keys)binding.Key;
                if (State.IsKeyDown(Key) && oldState.IsKeyUp(Key))
                    val.InvokeDown();
                if(State.IsKeyUp(Key) && State.IsKeyDown(Key))
                    val.InvokeUp();
            }
            if (binding.Dispatcher is AxisCommandDispatcher)
            {
                var val = binding.Dispatcher as AxisCommandDispatcher;
                var key = (Keys)binding.Key;
                if (State.IsKeyDown(key))
                    val.InvokeAxis(1 * binding.Scale);
                else
                {
                    val.InvokeAxis(0 * binding.Scale);
                }
            }
            throw new NotImplementedException();
        }
    }
    //worry about all the nitty gritty later I guess
    

    public enum MouseAux
    {
        MouseWheel,
        MouseX,
        MouseY
    }

    public class MouseInputDevice :InputDevice
    {
        MouseState oldState;
        MouseState State;
        Point OldPosition;
        public MouseInputDevice()
        {
            Name = "Mouse";
            //this is kind of nicer because it doesn't require custom types
            //or well highly custom types
            //you can just use eisting enums and build when you need them.
            //but also it keeps the device string to the device constructor
            //instead of asking it to match between several types
            validTypes.Add(typeof(MouseButton), Name);
            validTypes.Add(typeof(MouseAux), Name);
        }

        public override void Update()
        {
            OldPosition = State.Position;
            oldState = State;
            State = Mouse.GetState();
            //throw new NotImplementedException();
        }

        public override void UpdateBinding(IKeyBinding binding)
        {
            if(binding.Dispatcher is ToggleCommandDispatcher)
            {
                var val = binding.Dispatcher as ToggleCommandDispatcher;
                if(binding.Key is MouseButton)
                {
                    if (State.IsButtonDown((MouseButton)binding.Key) && oldState.IsButtonUp((MouseButton)binding.Key))
                        val.InvokeDown();
                    if (State.IsButtonUp((MouseButton)binding.Key) && oldState.IsButtonDown((MouseButton)binding.Key))
                        val.InvokeUp();
                }
                if(binding.Key is MouseAux)
                {
                    var Key = (MouseAux)binding.Key;
                    if(Key == MouseAux.MouseWheel)
                    {
                        if (State.ScrollWheelValue != 0)
                            val.InvokeDown();
                        else
                            val.InvokeUp();
                    }
                    if(Key == MouseAux.MouseX )
                    {
                        //frick, you need old state info.
                        //I'll just save old position
                        if (OldPosition.X - State.Position.X != 0)
                            val.InvokeDown();
                        else
                            val.InvokeUp();

                    }
                    if(Key == MouseAux.MouseY)
                    {
                        if (OldPosition.Y - State.Position.Y != 0)
                            val.InvokeDown();
                        else
                            val.InvokeUp();
                    }                    
                }                
            }
            if(binding.Dispatcher is AxisCommandDispatcher)
            {
                var val = binding.Dispatcher as AxisCommandDispatcher;
                if(binding.Key is MouseButton)
                {
                    //if (IsButtonDown((MouseButton)binding.Key))
                    if(State.IsButtonDown((MouseButton)binding.Key))
                        val.InvokeAxis(1 * binding.Scale);
                    else
                        val.InvokeAxis(0 * binding.Scale);
                }
                if(binding.Key is MouseAux)
                {
                    var key = (MouseAux)binding.Key;
                    if (key == MouseAux.MouseWheel)
                        val.InvokeAxis(State.ScrollWheelValue * binding.Scale);
                    if(key == MouseAux.MouseX)
                    {
                        val.InvokeAxis((State.X - OldPosition.X) * binding.Scale);
                    }
                    if(key == MouseAux.MouseY)
                    {
                        val.InvokeAxis((State.Y - OldPosition.Y) * binding.Scale);
                    }
                        
                }
            }

            //if(binding is MouseButton2)
            //{
            //    var b = binding as MouseButton2;

            //    if (IsButtonUp(b.Button))
            //        b.InvokeUp();
            //    if (IsButtonDown(b.Button))
            //        b.InvokeDown();
            //}
            //if (binding is MouseAxis)
            //{
            //    var b = binding as MouseAxis;
            //    b.InvokeAxis(State.Position.ToVector2());
            //}
            //if (binding is MouseWheel)
            //{
            //    var b = binding as MouseWheel;
            //    b.InvokeFloat(State.ScrollWheelValue);
            //}
            //throw new NotImplementedException();
        }                
    }

     
    class TempInputScope
    {
        InputHandler handler;
        CommandMap Map;
        public TempInputScope()
        {
            handler = new InputHandler();
            handler.AddDevice(new KeyboardInputDevice());
            handler.AddDevice(new MouseInputDevice());
            Map = new CommandMap();
            handler.AddCommandMap(Map);
        }

        public void FigureBindings()
        {
            //Map.AddBinding(KeyboardButton.DownBinding("Void",Keys.A,ButtonFunc));
            //Map.AddBinding(new MouseWheel("Wheelies"));
            //Map.AddBinding(new MouseAxis("Axel"));
            //Map.AddBinding(Command("",Keys.A));//needs name.

            Map.GetAxis("Axel").Axis += AxisSetting;
        }
        public void ButtonFunc()
        {
            var stopX = 0;
        }
        public void AxisSetting(Vector2 v)
        {

        }
    }
    public delegate void PressDelegate();
    public delegate void AxisDelegate(double axis);
    
    public interface ICommand
    {
        string Device { get; }
        string Name { get; set; }
        object Key { get; }
        KeyInfo Binding { get; set; }

    }
    public struct DeviceBinding
    {
        public string Device { get; set; }
        public object Key { get; set; }
        public float Scale { get; set; }
        public DeviceBinding(string device, object key, float scale = 0)
        {
            Device = device;
            Key = key;
            Scale = scale;
        }
    }
    public interface ICommandEvent
    {
        //hmm
        //I guess list of stuff
        string Name { get; set; }
        List<ICommand> bindings { get; set; }
    }
    public class CommandDispatcher
    {
        public string Name { get; set; }
        public List<ICommand> bindings = new List<ICommand>();

        public CommandDispatcher(string name)
        {
            Name = name;  

        }
    }

    //what about something with a parent child structure
    //like
    //You have the top level named command dispatcher
    //and you key bindings 
    //and key bindings contain a reference to their command dispatcher
   
    public interface ICommandDispatcher
    {
        string Name { get; set; }
        //List<IKeyBinding> KeyBindings { get; }
        //void AddBinding(IKeyBinding binding);
    }
    public class AxisCommandDispatcher : ICommandDispatcher
    {
        public string Name { get; set; }
        //Dictionary<object, IKeyBinding> Bindings = new Dictionary<object, IKeyBinding>();
        public AxisCommandDispatcher(string name)
        {
            Name = name;
        }
        //public void AddBinding(IKeyBinding binding)
        //{
        //    if (Bindings.ContainsKey(binding.Key))
        //        return;
        //    Bindings.Add(binding.Key, binding);
        //    binding.Dispatcher = this;
        //}
        //public void RemoveBinding(KeyBinding binding)
        //{
        //    if (Bindings.ContainsKey(binding.Key))
        //        Bindings.Remove(binding.Key);
        //}
        //public List<IKeyBinding> KeyBindings
        //{
        //    get => Bindings.Values.ToList();
        //}
        public event AxisDelegate Axis;
        public void InvokeAxis(double value)
        {
            Axis?.Invoke(value);
        }

    }
    public interface IKeyBinding
    {

        string Device { get; }
        object Key { get; }
        double Scale { get; }
        //object DeviceIndex { get; set; }
        ICommandDispatcher Dispatcher { get; set; }
    }
    
    public struct KeyBinding : IKeyBinding
    {
       // public object DeviceIndex { get; set; }
        public object Key { get; set; }
        public string Device { get;set; }
        public double Scale { get; set; }
        public ICommandDispatcher Dispatcher { get; set; }
        public KeyBinding(object key, string device,ICommandDispatcher dispatcher)
        {
            Key = key;
            Device = device;
            Dispatcher = dispatcher;
            Scale = 0;
        }
        public KeyBinding(object key, string device, double scale,ICommandDispatcher dispatcher)
        {
            Key = key;
            Device = device;
            Dispatcher = dispatcher;
            Scale = scale;
        }
    }
    public class ToggleCommandDispatcher : ICommandDispatcher
    {
        public string Name { get; set; }
        //Dictionary<object,IKeyBinding> Bindings = new Dictionary<object, IKeyBinding>();
        //yeaah>?
        public ToggleCommandDispatcher(string name)
        {
            Name = name;
        }
        //public void AddBinding(IKeyBinding binding)
        //{
        //    if (Bindings.ContainsKey(binding.Key))
        //        return;
        //    Bindings.Add(binding.Key, binding);
        //    binding.Dispatcher = this;
        //}
        //public void RemoveBinding(KeyBinding binding)
        //{
        //    if (!Bindings.ContainsKey(binding.Key))
        //        return;            
        //    Bindings.Remove(binding);            
        //}
        //public List<IKeyBinding> KeyBindings
        //{
        //    get => Bindings.Values.ToList(); 
        //}

        public void InvokeUp()
        {
            Up?.Invoke();
        }
        public void InvokeDown()
        {
            Down?.Invoke();
        }

        public event PressDelegate Up;
        public event PressDelegate Down;
    }
     //so you pass the command and it invokes the dispatcher from within the device.
     //eughhhhh
     //I don't know I don't like that either.
     //the thing about this that sucks is I know what I want to do here i just need it to work already god damnit.
    public class InputCommand : ICommand
    {
        public string Device { get => Binding.Device; }
        public object Key { get => Binding.Key; }
        public string Name { get; set; }
        public KeyInfo Binding { get; set; }
        
        //public List<ICommand> bindings { get; set; }

        public InputCommand(string name,string device, object key)
        {
            Name = name;
            //Device = device;
            //Key = key;
            Binding = new KeyInfo(key, device);
            //bindings = new List<ICommand>();
        }
        public void InvokeDown()
        {
            Press?.Invoke();

        }
        public void InvokeUp()
        {
            Unpress?.Invoke();
        }

        public event PressDelegate Press;
        public event PressDelegate Unpress;
    }
    public struct KeyInfo
    {
        public object Key;
        public string Device;
        public KeyInfo(object key, string device)
        {
            Key = key;
            Device = device;
        }
    }
    public class AxisCommand : ICommand
    {
        public string Device { get => Binding.Device; }
        public float Scale;
        public string Name { get; set; }
        public object Key { get => Binding.Key; }
        public KeyInfo Binding { get; set; }
        //public List<DeviceBinding> bindings { get; set; }

        public AxisCommand(string device, object key, float scale)
        {
            //Name = name;
            //Device = device;
            //Key = key;
            Scale = scale;
            //bindings = new List<DeviceBinding>();
            Binding = new KeyInfo(key, device);
        }
        public void InvokeAxis(float val)
        {
            Axis?.Invoke(val);
        }
        public event AxisDelegate Axis;

    }
    //hmm  
    public class CommandBuilder
    {
        //missing modifier checks. 
        //
        
        public static Dictionary<System.Type, string> validTypes = new Dictionary<System.Type, string>(); //then you need to. attatch keys to 
        //input from -1 to 1;
        //okay so my problem is accessing modifier states outside of buttons
        //like with axis inputs and such.
        //which is weird? I guess.
        //well wait no.
        //I'm imagining having axis inputs in like its own veiled thing but also just...have an axis command or whatever that takes the thing.
        //because. well because all that is is firing events from a state manager right?
        delegate void InputReturn(int i);
        //hmm. I don't think I want to just instantiate one. 
        //why not just keep this static or whatever
        //sure its a singleton and thats awful or whatever but who cares, it makes it easy
        //information I Need is device associated with type
        /// <param name="keys"></param>
        List<InputCommand> inputs = new List<InputCommand>();
        //public CommandBuilder(params object[] keys)
        //{
        //    foreach (var key in keys)
        //    {
        //        if (!validTypes.Keys.Contains(key.GetType()))
        //            throw new System.Exception("Key Must Be of Valid Type: " + typesString());
        //        inputs.Add(new InputCommand("",validTypes[key.GetType()], key));
        //    }
        //    //then generate a list of device names and inputs?
        //}
        public static bool VerifyKey(object key)
        {
            return (!validTypes.Keys.Contains(key.GetType()));
        }
        //could just return this alone
        public static string ValidateKey(object key)
        {
            if (validTypes.Keys.Contains(key.GetType()))
            {
                return validTypes[key.GetType()];
            }
            return "-1";
        }
        public static CommandDispatcher Command(string name, float scale, params object[] keys)
        {
            //if (!(validTypes.Keys.Contains(key.GetType())))
            //throw new Exception("invalid key type");
            //var ret = new AxisCommand(name, validTypes[key.GetType()], key, scale);
            var ret = new CommandDispatcher(name);
            foreach(var key in keys)
            {
                if (!validTypes.Keys.Contains(key.GetType()))
                    throw new Exception("" + key.GetType() + " is not a valid Key Type");
                ret.bindings.Add(new AxisCommand(validTypes[key.GetType()], key,scale));
            }
            //this is wrong
            //if the point is to bind multiple keys to the same event this is wrong
            //because the event is still stored and fired from the key.
            //So each command needs to be added seperately
            return ret;
        }
        //public static CommandDispatcher Command(string name, params object[] keys)
        //{
        //    //if (!(validTypes.Keys.Contains(key.GetType())))
        //        //throw new Exception("invalid key type");
        //    //if(scale == 0)
        //    //{
        //    //var ret = new InputCommand(name, validTypes[key.GetType()], key);
        //    var ret = new CommandDispatcher(name);
        //    //ret.bindings.AddRange(Bindings(key))
        //    foreach(var key in keys)
        //    {
        //        if (!validTypes.Keys.Contains(key.GetType()))
        //            throw new Exception("" + key.GetType() + " is not a valid Key Type");
        //        ret.bindings.Add(new InputCommand(validTypes[key.GetType()], key));
        //    }

        //    //Bindings(ret, key);
        //    return ret;
        //    //}
           
        //}
        //static void Bindings(InputCommand command, params object[] keys)
        //{           
        //    foreach(var key in keys)
        //    {
        //        if (!(validTypes.Keys.Contains(key.GetType())))
        //            throw new Exception("invalid key type");
        //        command.bindings.Add(new InputCommand(validTypes[key.GetType()], key));
        //    }            
        //}
        //static void Bindings(AxisCommand command,float scale, params object[] keys)
        //{
        //    foreach(var key in keys)
        //    {
        //        if (!(validTypes.Keys.Contains(key.GetType()))
        //            throw new Exception("invalid key type");
        //        command.bindings.Add(new DeviceBinding(validTypes[key.GetType()], key, scale));
        //    }
        //}
        private string typesString()
        {
            string ret = "";
            foreach (var type in validTypes.Keys)
            {
                ret += type.Name;
                ret += " ";
            }
            return ret;
        }
    }
}

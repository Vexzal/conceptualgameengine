﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
namespace BaseEngine.Input
{
    public enum BindingType
    {
        Button,
        Axis,         
        Float,
    }
    public interface IInputBinding
    {
        string Name { get;}
        //string Device { get; }
        BindingType bType { get; }
    }
    public interface IInputBindingKey
    {
        IInputBinding binding{get;set;}
        string Device { get; }
    }
    
}

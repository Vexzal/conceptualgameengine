﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using BaseEngine.Rendering;

namespace BaseEngine.Components
{
    public class PointLightComponent : Component, IPointLight
    {
        public float Radius { get; set; }
        public float Intensity { get; set; }
        public Color Color { get; set; }
        TransformComponent transform;
        
        public Matrix World
        {
            get => Matrix.CreateTranslation(transform.GetParentedTransform().Translation) * Matrix.CreateScale(Radius);
        }

        public Vector3 Position
        {
            get => transform.Position;
        }

        public override void Initialize()
        {
            transform = gameObject.GetComponent<TransformComponent>();
        }
    }
}

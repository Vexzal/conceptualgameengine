﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using ConversionHelper;

namespace BaseEngine.Components
{
    public class TransformComponent : Component
    {
        //private TransformContainer transform;

        //public TransformContainer Transform
        //{
        //    get { return transform; }
        //}

        Vector3 _position;
        Quaternion _rotation;
        Vector3 _scale;
        public Vector3 Position
        {
            get => _position;
            set { _position = value; CallUpdate(); }
        }
        public Quaternion Rotation
        {
            get => _rotation;
            set { _rotation = value; CallUpdate(); }
        }
        public Vector3 Scale
        {
            get => _scale;
            set { _scale = value; CallUpdate(); }
        }
        //I don't want to actually calculate all this per frame, //lets just update them on a dirty flag or as other things are set.
        public Matrix Transform
        {
            get => Matrix.CreateScale(_scale) * Matrix.CreateFromQuaternion(_rotation) * Matrix.CreateTranslation(_position);
            /**set
            {

                _scale = new Vector3(
                    new Vector3(value.M11, value.M12, value.M13).Length(),
                    new Vector3(value.M21, value.M22, value.M23).Length(),
                    new Vector3(value.M31, value.M32, value.M33).Length());
                _position = new Vector3(value.M41, value.M42, value.M43);
                var NormalizedRotMatrix = value * Matrix.Invert(Matrix.CreateScale(_scale));
                NormalizedRotMatrix.M41 = 0;
                NormalizedRotMatrix.M42 = 0;
                NormalizedRotMatrix.M43 = 0;
                _rotation = Quaternion.CreateFromRotationMatrix(NormalizedRotMatrix);
                //this is a bad idea? 

            }*/

            set
            {
                _position = new Vector3(value.M41, value.M42, value.M43);
                _rotation = Quaternion.CreateFromRotationMatrix(GetRotationMatrix(value));
                _scale = GetScale(value);
            }
        }
        

        //public TransformContainer()
        //{
        //    position = Vector3.Zero;
        //    rotation = Quaternion.Identity;
        //    scale = Vector3.One;
        //} 
        //public Matrix GetWorld()
        //{
        //    return Transform;
        //}
        public void SetTransform(Matrix m)
        {
            //okay this doesn't need to be done here I guess?
            //Vector3 translation = Matrix.CreateTranslation(m.M41, m.M42, m.M43);
            //Matrix rotation = GetRotationMatrix(m);
            _position = new Vector3(m.M41, m.M42, m.M43);
            _rotation = Quaternion.CreateFromRotationMatrix(GetRotationMatrix(m));
            _scale = GetScale(m);
        }
        private Vector3 GetScale(Matrix M)
        {
            Vector3 iHat = new Vector3(M.M11, M.M12, M.M13);
            Vector3 jHat = new Vector3(M.M21, M.M22, M.M23);
            Vector3 kHat = new Vector3(M.M31, M.M32, M.M33);

            return new Vector3(iHat.Length(), jHat.Length(), kHat.Length());
        }
        private Matrix GetRotationMatrix(Matrix m)
        {
            Vector3 iHat = Vector3.Normalize(new Vector3(m.M11, m.M12, m.M13));
            Vector3 jHat = Vector3.Normalize(new Vector3(m.M21, m.M22, m.M23));
            Vector3 kHat = Vector3.Normalize(new Vector3(m.M31, m.M32, m.M33));

            return new Matrix(
                new Vector4(iHat, 0),
                new Vector4(jHat, 0),
                new Vector4(kHat, 0),
                new Vector4(0, 0, 0, 1));

        }
        //this should be in the physics component.
        public BEPUutilities.Matrix GetPhysicsTransform()
        {
            Matrix rotMatrix = GetRotationMatrix(Transform);
            rotMatrix.Translation = _position;
            return MathConverter.Convert(rotMatrix);
        }

        public TransformComponent()            
        {
            // transform = new TransformContainer();
            //position = Vector3.Zero;
            //Rotation = Quaternion.Identity; 
            //scale = Vector3.One;
            _position = Vector3.Zero;
            _rotation = Quaternion.Identity;
            _scale = Vector3.One;
        }
        public Matrix GetWorld()
        {
            Matrix Parent = Matrix.Identity;
            if (gameObject.Parent != null)
                Parent = gameObject.Parent.GetComponent<TransformComponent>().GetWorld();
            //return Parent * transform.GetWorld();
            return Parent * Transform;
        }
        public TransformComponent(GameObject _gameObject)
        {
            gameObject = _gameObject;
            //transform = new TransformContainer();
        }
        public override void Initialize()
        {
        }        
        public Matrix GetParentedTransform()
        {
            //please work
            if(this.gameObject.Parent != null)
            {
                return GetWorld() * gameObject.Parent.GetComponent<TransformComponent>().GetParentedTransform();
            }
            return GetWorld();
        }

        internal override void ParentUpdated(ComponentUpdateArguments e)
        {
            CallUpdate();//update when parent transform updates
        }
    }
}

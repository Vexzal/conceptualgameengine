﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseEngine;

namespace BaseEngine.Components
{
    public abstract class Component
    {
        public object Tag { get; set; }
        public GameObject gameObject;
        public bool Enabled { get; set; }
        public virtual void Initialize() { }      
        public Component()
        {
            Enabled = true;
        }
        public Component(GameObject _gameObject)
        {         
            gameObject = _gameObject;
        }
        public delegate void ComponentUpdate(ComponentUpdateArguments a);
        public event ComponentUpdate ComponentUpdated;
        internal virtual void CallUpdate()//this is bad?
        {
            ComponentUpdated?.Invoke(new ComponentUpdateArguments(this.GetType(), this, gameObject));
        }
        internal virtual void ParentUpdated(ComponentUpdateArguments e) { }
        
        
    }
    public struct ComponentUpdateArguments
    {
        public Type ComponentType { get; private set; }
        public Component Component { get; private set; }
        public GameObject GameObject { get; private set; }

        public ComponentUpdateArguments(Type componentType, Component component, GameObject gameObject)
        {
            ComponentType = componentType;
            Component = component;
            GameObject = gameObject;            
        }        
    }
}

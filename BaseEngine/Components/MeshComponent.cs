﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using BaseEngine.Rendering;

namespace BaseEngine.Components
{
    public class MeshComponent : Component, IModelMesh
    {
        Model Mesh;
        public Model mesh
        {
            get => Mesh;
            set
            {
                Mesh = value;
                CallUpdate();
            }
        }
        public Model DrawModel
        {
            get => Mesh;
        }

        public Vector3[] vPositionList
        {
            get
            {
                List<Vector3> vReturn = new List<Vector3>();
                foreach (ModelMesh mMesh in Mesh.Meshes)
                {
                    foreach (ModelMeshPart mPart in mMesh.MeshParts)
                    {
                        Vector3[] VList = new Vector3[mPart.NumVertices];
                        mPart.VertexBuffer.GetData<Vector3>(0, VList, 0, mPart.NumVertices, mPart.VertexBuffer.VertexDeclaration.VertexStride);
                        var dec = mPart.VertexBuffer.VertexDeclaration;
                        vReturn.AddRange(VList);
                    }
                }
                return vReturn.ToArray();
            }
        }
        public EffectComponent Effect {get;set;}
        public Effect ModelEffect
        {
            get => Effect.Effect;
        }
        TransformComponent transform;
        public TransformComponent Transform
        {
            get { return transform; }
            set { transform = value;
                CallUpdate();
            }
        }
        public Matrix WorldTransform
        {
            get => transform.GetParentedTransform();
        }

        public MeshComponent()
        {
            
        }
        
        //public void Instantiate()
        //{
        //    transform = gameObject.GetComponent<TransformComponent>().Transform;
        //}
        public override void Initialize()
        {
            transform = gameObject.GetComponent<TransformComponent>();
            Effect = gameObject.GetComponent<EffectComponent>();
            
        }
        //rethink this maybe?
        //a lot of components might not need to be added to a service
        //transform doesn't
        //render and physics do but they only need to be added to a specific service
        //
        
    }
}

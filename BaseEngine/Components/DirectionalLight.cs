﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using BaseEngine.Rendering;

namespace BaseEngine.Components
{
    class DirectionalLight : Component, IDirectionalLight
    {
        TransformComponent transform;
        public Vector3 Direction
        {
            get => Vector3.Transform(-Vector3.UnitZ, transform.Rotation);
        }
        public Color Color { get; set; }

        public override void Initialize()
        {
            transform = gameObject.GetComponent<TransformComponent>();
        }
    }
}

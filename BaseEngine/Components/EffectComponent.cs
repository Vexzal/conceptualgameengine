﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseEngine;
using BaseEngine.Rendering;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace BaseEngine.Components
{
    public class EffectComponent : Component
    {
        TransformComponent transform;
        //List<EffectBinding> effectBindings;
        //public EffectBindingCollection bindings;        
        public EffectParameterCollection bindings { get; private set; }
        public Effect Effect { get; set; }
        public EffectComponent()
        {
            //needsInitialization = true;
        }
        public override void Initialize()
        {
            transform = gameObject.GetComponent<TransformComponent>();
            //bindings = new EffectBindingCollection(Effect.Parameters);
            bindings = Effect.Parameters;
            /**foreach(EffectParameter parameter in effect.Parameters)
            {
                if(parameter.Elements.Count </ 0)
                {
                    if(parameter.ParameterClass == EffectParameterClass.Matrix)
                    {
                        //do add effectbinding
                        effectBindings.Add(new MatrixBinding(parameter.Name, parameter));
                    }
                    if(parameter.ParameterClass == EffectParameterClass.Scalar)
                    {
                        if(parameter.ParameterType == EffectParameterType.Single)
                        {
                            effectBindings.Add(new FloatBinding(parameter.Name, parameter));
                        }
                    }
                    if(parameter.ParameterClass == EffectParameterClass.Vector)
                    {
                        
                    }
                    if(parameter.ParameterClass == EffectParameterClass.Object)
                    {
                        if(parameter.ParameterType == EffectParameterType.Texture2D)
                        {
                            effectBindings.Add(new TextureBinding(parameter.Name, parameter));
                        }
                    }                    
                }
                else
                {
                    if(parameter.ParameterClass == EffectParameterClass.Matrix)
                    {
                        effectBindings.Add(new MatrixArrayBinding(parameter.Name, parameter));
                    }
                }
            }*/
        }
        public EffectParameter this[string index]
        {
            get => bindings[index];
        }
        //if I have ^ d I need v
        public void SetValue(string name, float value)
        {
            Effect.Parameters[name].SetValue(value);
        }
        public void SetValue(string name, Texture2D value)
        {
            Effect.Parameters[name].SetValue(value);
        }
        public void SetValue(string name, Texture3D value)
        {
            Effect.Parameters[name].SetValue(value);
        }
        public void SetValue(string name, TextureCube value)
        {
            Effect.Parameters[name].SetValue(value);
        }
        public void SetValue(string name, Matrix value)
        {
            Effect.Parameters[name].SetValue(value);
        }
        public void SetValue(string name, Matrix[] value)
        {
            Effect.Parameters[name].SetValue(value);
        }
        public void SetValue(string name, Vector2 value)
        {
            Effect.Parameters[name].SetValue(value);
        }
        public void SetValue(string name, Vector3 value)
        {
            Effect.Parameters[name].SetValue(value);
        }        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseEngine.Rendering;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace BaseEngine.Components
{
    //make this a base class, that you create different behavior cameras from
    //or, just make a camera container, maybe, no.
    //okay so different cameras need different things
    // so just, hae a normal camera, and then have a camera controller.
    //soo.
    public class Camera : Component, ICamera
    {
        RenderTarget2D renderTarget;

        //Vector3 distance;
        public IRenderingPipeline Pipeline;
        bool enabled;
        public bool Enambled
        {
            get { return enabled; }
            set { enabled = value;
                if (CameraChange != null && value == true)
                    CameraChange(this);
            }
             
        }
        public Vector3 Position
        {
            get => transform.Position;
        }
        public Vector3 Up;
        //Vector3 Target;
        public TransformComponent transform;
        public TransformComponent target;
        //TransformContainer offset;
        public bool perspective;
        public float fov;
        public float aspect;
        public float nearClipPlane;
        public float farClipPlane;
        
        public Camera()
        {
            
        }
        public override void Initialize()
        {
            transform = gameObject.GetComponent<TransformComponent>();
        }
        public Matrix View
        {
            get { return Matrix.CreateLookAt(transform.Position, target.Position, Up); }
        }
        public Matrix Projection
        {
            get { return Matrix.CreatePerspectiveFieldOfView(fov, aspect, nearClipPlane, farClipPlane); }
        }
        public BoundingFrustum CameraFrustrum
        {
            get { return new BoundingFrustum(View * Projection); }
        }
        public delegate void changeCamera(Camera camera);
        public event changeCamera CameraChange;
        
    }
}

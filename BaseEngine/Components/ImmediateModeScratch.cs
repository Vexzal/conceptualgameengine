﻿


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using static BaseEngine.EngineServices;
using BaseEngine.Logic;

namespace BaseEngine.Components
{


    //alright so. 
    //heres immediate problems
    //managing windows, rectangles, 
    //keeping track of ui dependant info, "retaining" ui info.
    //keeping track of window order. 
    //
    //lets start with like a, draw rect

    
    //do I need this or can I use two anchors
    //like normal
    //its all a lot of inputs.
    //I guess thats unavoidable sort of.
   
    //no way to set widget data thats not horribly messy without public so make it public
    //wait what no of course there is god hold on
    //just add
    //fuck I'm almost out of time.
    //okay hold on there is no reason not to make context data public because it is only used within the widget calls anyway.

    public struct DrawCommandStorage
    {
        public Rectangle Rect;
        public Rectangle Clip;
        public string StyleKey;
        public Color Color;
        public string Text;
        public string StateKey;
        public Texture2D Texture;
        //style key accesses. Ithink this is it huh.//no texture huh. I guess thats grabbed from style.
        //yeah thats convinient.
        public DrawCommandStorage(Rectangle rect, Rectangle clip, Color color, string text, string styleKey, string stateKey, Texture2D texture = null)
        {
            Rect = rect;
            Clip = clip;
            StyleKey = styleKey;
            Color = color;
            Text = text;
            StateKey = stateKey;
            Texture = texture;
        }

            
        //hmm.
    }
    //style data contains all the 
    //drawing data and positioning data.
    public struct StyleInfo
    {
        float margin;//I'm worried this isn't needed but it basically is
        Vector2 topAnchor;
        Vector2 botAnchor;
        Vector2 topOffset;
        Vector2 botOffset;

    }
        //alright the transition to static is done I think whats the list of improvements  Istill want.
        //I want better way to access parent offset.
        //I need to reexamine parenting and docking and window data as a whole the whole thing feels wrong.
        //Idea is storing a list of window data in Windows.
        //clean up the component
        //input polling clearly
    
    public class StyleGuide
    {
        Dictionary<string, SomeStyleClass> StyleData = new Dictionary<string, SomeStyleClass>();//where object shoulod be some style class
        
        public SomeStyleClass Style(string WidgetType)
        {//could ust. Like Might as well do the thing?
            SomeStyleClass ret;
            if(StyleData.TryGetValue(WidgetType,out ret))
            {
                return ret;
            }
            ret = new SomeStyleClass();//really throw exception
            throw new Exception("Undefined Widget Type");
            return ret;
        }
        public void AddStyle(string key, SomeStyleClass style)
        {
            StyleData.Add(key, style);
        }
        //so generally 
        //we want bounds rectangle from
       
    }
    //return index key.
    public unsafe delegate void UIVerticiesDelegate( float margin, Rectangle rectangle,out VertexPositionColorTexture[] verticies, out string indexKey);
    public unsafe delegate void UIIndiciesDelegate(out int[] indicies);
    public enum Alignment
    {
        LeftAligned,
        CenterAlinged,
        RightAligned
    }
    public class SomeStyleClass
    {
        int TopMargin;
        int BotMargin;
        int LeftMargin;
        int RightMargin;
        public int margin;//used to keep from regenning vertex  generation code.
        public Alignment Alignment;
        public int MarginWidth => margin * 2;
        public int MarginHeight => TopMargin + BotMargin;
        
        int MinHeight;
        int MinWidth;

        Vector2 TopAnchor;//these can be seet in any manner of ways.
        Vector2 BotAnchor;

        public UIVerticiesDelegate Hot;
        public UIVerticiesDelegate Inactive;
        public UIVerticiesDelegate Clicked;
        public UIIndiciesDelegate  Indicies;
        //I should ust return. a drawing collection.
        public  void GrabVerticies(DrawCommandStorage command,out VertexPositionColorTexture[] v, out string IndexKey)
        {
            switch(command.StateKey)
            {
                //case MemoryConstants.InnactiveStateID:
                    //return Inactive.Invoke(command, margin, command.Rect, ref v, ref i);                                        
                case MemoryConstants.HotStateID:
                    //return Hot.Invoke(command, margin, command.Rect, ref v, ref i);
                    Hot.Invoke(margin, command.Rect, out v, out IndexKey);
                    break;
                case MemoryConstants.ClickedStateID:
                    //return Clicked.Invoke(command, margin, command.Rect, ref v, ref i);
                    Clicked.Invoke(margin, command.Rect, out v, out IndexKey);
                    break;
                default:
                    Inactive.Invoke(margin, command.Rect, out v, out IndexKey);
                    break;
            }
        }
        public void GrabIndicies(out int[] i)
        {
            Indicies.Invoke(out i); 
        }
        //what do I want. hmm. out.
        //getting size style data. hmm
        //Rectangle BuildSource(Vector2 position, Vector2 size)//hmm.
        //{
        //    //so this one is easy 
        //    Vector2 BotPosition = position + size;
        //}

        bool Rectangles(Rectangle source, Rectangle parent, out Rectangle parentClipped,out Rectangle clipped)
        {
            //this should be in the style data
            parentClipped = new Rectangle();
            clipped = new Rectangle();

            source.Width = Math.Min(source.Width, MinWidth);
            source.Height = Math.Min(source.Height, MinHeight);

            if (!parent.Contains(source))
            {
                return false;
            }
            //fix min max instead of closing I guess?
            //if(source.Size.X < MarginWidth || source.Size.Y < MarginHeight)
            //{
            //    return false;
            //}
            //this is all wrong. fuck.
            parentClipped.X = Math.Max(source.X, parent.X);
            parentClipped.Y = Math.Max(source.Y, parent.Y);
            //this is wrong - just using width like its a bottom corner instead of a fixed size value.
            //okay hold on this is fine.
            //I think this is what you do?
            //I think this is still wrong.
            parentClipped.Width = Math.Min(source.Width,  source.Width + (parent.X + parent.Width - (parentClipped.X + source.Width)));
            parentClipped.Height = Math.Min(source.Height, source.Height + (parent.Y + parent.Height - (parentClipped.Y + source.Height)));

            //we need to do the check again. wait no we don't we just ensured it contains the .
            //but it could be too small.
            clipped.X = parentClipped.X + margin  ;
            clipped.Y = parentClipped.Y + margin;
            clipped.Width = parentClipped.Width - MarginWidth;
            clipped.Height = parentClipped.Height - MarginHeight;



            return true;
            
        }
        public Rectangle GetClip(Rectangle source)
        {
            Rectangle rect = new Rectangle();
            rect.X = source.X + margin;
            rect.Y = source.Y + margin;
            rect.Width = source.Width - MarginWidth;
            rect.Height = source.Height - MarginWidth;
            return rect;
        }
        //draw data

        void GetDrawData()
        {

        }

    }
    //a class for managing cursor data for gui
    public class Cursor
    {
        public string Name;//name for owning device.probably not nesccesary but I want it.
        public string Hot;
        public string FrameHot;
        public string FrameActive;
        public string Active;
        public Point Position;
        public ButtonState State;
    }

    
    //could produce odd behaviour with overlapping cursor states.
    class WidgetState
    {
        public bool Hot { get; private set; }
        public bool Active { get; private set; }
        public bool Clicked { get; private set; }
        public void SetActive(bool active)
        {
            if (Active)
                return;
            Active = active;
        }
        public void SetHot(bool hot)
        {
            if (Hot)
                return;
            Hot = hot;
        }
        public void SetClicked(bool clicked)
        {
            if (Clicked)
                return;
            Clicked = clicked;
        }
        public void Reset()
        {
            Hot = Active = Clicked = false;
        }
        

    }
    public static partial class MemoryConstants
    {
        public const string WindowID = "#WINDOW";
        public const string ButtonID = "#BUTTON";
        public const string InnactiveStateID = "#INACTIVE";
        public const string HotStateID = "#HOT";
        public const string ClickedStateID = "#CLICKED";
    }
    public static partial class MemoryGui
    {
        public const string WindowTag = "##Window";
        public const string ButtonTag = "##Button";


        static WidgetState state = new WidgetState();

        public static void BeginCanvas()
        {
            Context.ClearContainer();
            if(Context.Container.FirstOfFrame)
            {
                Context.Container.FirstOfFrame = false;
                Context.Container.VerticalOffset = 0;
            }
        }
        
        //public static ImmediateModeScratch Context;
        public static IImmediateModeContext Context;
        public static bool SetContext(ImmediateModeScratch scratch)
        {
            //skip disabled contexts
            
            if(scratch.Enabled)
            {
                Context = scratch;
                return true;
            }
            return false;
        }
        //alright now actual widget functions.
        #region Widgets
        const string WindowID = "#WINDOW";
        //window thats just append or default.       b
        public static void Window(string id)
        {
            //so we need
            //a fair warning is this could cause all manner of hell if you screw up id's
            var Data = Context.GetWidget<Canvas>(id + MemoryConstants.WindowID);

            //hmm// I think this is fine?
            //all that docking check can be done in the main function.
            Window(id,Data.LastFramePosition ,Data.LastFrameSize);
            //generally you'd check for docked state. etc.
            //throw new NotImplementedException("Switch for current window state");
        }
        public static void Window(string id, Point position, Point size)
        {
            var label = id.Split('#')[0];
            id += MemoryConstants.WindowID;
           
            Context.Container = Context.GetWidget<Canvas>(id);
            if (!Context.Windows.Contains(Context.Container))
                Context.Windows.Add(Context.Container);

           
            //figure out size info hmm.
            if(Context.Container.FirstOfFrame)
            {
                Context.Container.FirstOfFrame = false;
                Context.Container.Bounds = new Rectangle(position, size);
                Context.Container.ClippingRectangle = Context.Style.Style(MemoryConstants.WindowID).GetClip(Context.Container.Bounds);
                Context.Container.VerticalOffset = Context.Style.Style(MemoryConstants.WindowID).margin;
                if(!(size.X + size.Y < 60))
                {
                    //calculate stuff I guess.
                    //oh I should have an exist bool in the canvas.

                    //Cursor 
                    state.Reset();
                    bool NotOverlap = true; // necessary for windows since they can be called in a different order than their drawn.
                    //using a ternary operator.
                    

                    for (int i = 0; i < Context.Cursors.Length; i++)
                    {
                        var cursor = Context.Cursors[i];
                        NotOverlap = true;
                        //this is baad. if should be true I'm full length of windows past window.

                        
                        if (Context.Container.Bounds.Contains(cursor.Position) )
                        {
                            for (int j = Context.Windows.IndexOf(Context.Container) + 1; j < Context.Windows.Count; j++)
                            {
                                NotOverlap = !Context.Windows[j].Bounds.Contains(cursor.Position);
                                if (!NotOverlap)
                                    break;
                            }
                            if (NotOverlap)
                            {
                                cursor.FrameHot = id;
                                state.SetHot(cursor.Hot == id);
                                if (cursor.Active == "" && cursor.State == ButtonState.Pressed)
                                {
                                    //state.SetActive( true);
                                    cursor.FrameActive = id;
                                }
                                //if (cursor.Active == id)
                                //{
                                //    state.SetActive(true);
                                //}
                            }
                            else
                            {
                                state.SetHot(cursor.Hot == id);
                            }
                            state.SetActive(cursor.Active == id);
                        }
                        state.SetClicked(cursor.Active == id && cursor.Hot == id && cursor.State == ButtonState.Pressed); 
                        //so we need. size stuff. we'll fix that later.
                    }
                    //render 
                    //window doesn't really have seperate render states so.
                    Context.Container.drawBuffer.Add(new DrawCommandStorage(Context.Container.Bounds, Context.Container.Bounds,Color.White, label, MemoryConstants.WindowID, ""));
                    if (state.Hot)
                    {
                        if (state.Active)
                        {

                        }
                        else
                        {

                        }
                    }
                    else
                    {

                    }
                    //focus
                    if (state.Clicked)
                        Context.WantsFocus = Context.Container;

                }
                
                    //closed or non existant.
                    //should really be. hmm. actually if its less than the added 
            }

        }

        /// old window call
       

        //auto layouyt with style
        public static bool Button(string id)
        {
            //very different;
            //lets grab style.
            var label = id.Split('#')[0];
            var Style = Context.Style.Style(MemoryConstants.ButtonID);
            var height = Context.Container.VerticalOffset;
            //so lets get. button size.
            var textDimensions = Context.Font.MeasureString(label);
            //so now that we have.
            textDimensions.X += Style.margin * 2;
            textDimensions.Y += Style.margin * 2;
            //so
            Point outPosition;
            Point outSize = textDimensions.ToPoint() ;
            //so the current problem is how do you handle same line stuff.
            //well this is the ongoing problem with imgui is 
            //is that widgets have no real information about eachother during the frame they'redrawn.\
            //so I'm going to finish doing all of this in here for now but.
            //I could spend forever compromomising the immediate mode design to accomodate this problem
            //but there is a stage of the program that knows all layout information, and thats the user end and the calling end.
            //so passing in layout data as a layer on top of the gui.
            //so you can mix simple calls and complex layout calls.
            switch(Style.Alignment)
            {
                case Alignment.CenterAlinged:
                    {
                        var halfContainerSize = Context.Container.Bounds.Size.ToVector2() / 2;
                        var halfTargetSize = textDimensions / 2;
                        outPosition = Context.Container.Bounds.Location + new Point((int)(halfContainerSize.X - halfTargetSize.X), height);

                    }
                    break;
                case Alignment.LeftAligned:
                    {
                        //this doesn't need a lot of offsets huh.
                        outPosition = Context.Container.Bounds.Location + new Point(Context.Style.Style(MemoryConstants.WindowID).margin, height);
                        
                    }
                    break;
                case Alignment.RightAligned:
                    {
                        outPosition = Context.Container.Bounds.Location + new Point(
                            Context.Container.Bounds.Size.X - outSize.X - Context.Style.Style(MemoryConstants.WindowID).margin, height);
                    }
                    break;
                default:
                    outPosition = Point.Zero;
                    break;
            }
            Context.Container.VerticalOffset += outSize.Y + 5;
            return Button(id, outPosition, outSize);

            throw new NotImplementedException();
        }
        //use anchoring offsets.
        public static bool Button(string id, Vector2 minAnchor, Vector2 maxAnchor, Point topOffset, Point botOffset)
        {
            var PositionA = Context.Container.Bounds.Location + ((minAnchor * Context.Container.Bounds.Size.ToVector2()).ToPoint() + topOffset );
            var PositionB = Context.Container.Bounds.Location + ((maxAnchor * Context.Container.Bounds.Size.ToVector2()).ToPoint() + botOffset);

            var Size = PositionB - PositionA;

            return Button(id, PositionA, Size);

            throw new NotImplementedException();
        }
        /// Obsolte Verswion of the call
        

        public static bool Button(string id, Point position, Point size)
        {
            id = id + MemoryConstants.ButtonID;
            Rectangle source = new Rectangle(position, size);
            Rectangle clippedSource = source;
            clippedSource.X = Math.Max(clippedSource.X, Context.Container.ClippingRectangle.X);
            clippedSource.Y = Math.Max(clippedSource.Y, Context.Container.ClippingRectangle.Y);
            clippedSource.Width = Math.Min(clippedSource.Width, Context.Container.ClippingRectangle.Width);
            clippedSource.Height = Math.Min(clippedSource.Height, Context.Container.ClippingRectangle.Height);
            //button doesn't really have widet data

            state.Reset();
            for (int i = 0; i < Context.Cursors.Length; i++)
            {
                var cursor = Context.Cursors[i];
                bool NotOverlap = true;
                if (clippedSource.Contains(cursor.Position))
                {
                    for (int j = Context.Windows.IndexOf(Context.Container) + 1; j < Context.Windows.Count; j++)
                    {
                        NotOverlap = !Context.Windows[j].Bounds.Contains(cursor.Position);
                        if (!NotOverlap)
                            break;
                    }
                    //Context.FrameHot = id;
                    if (NotOverlap)
                    {
                        cursor.FrameHot = id;//hmm how do I. oh I need to.
                                             //state.SetHot(true);

                        state.SetHot(cursor.Hot == id);
                        //if (Context.Active == "" && Context.ClickHappen)
                        //{
                        //    Context.Active = id;
                        //}
                        //if(cursor.Active == null)
                        //{
                        //    throw new Exception();
                        //}
                        if (cursor.Active == "" && cursor.State == ButtonState.Pressed)
                        {
                            //cursor.Active = id;//I was going to store all this in a data structure but I could just store it here.
                            //by here I mean the static class. or the function?
                            cursor.FrameActive = id;

                        }
                    }
                    else
                    {
                        state.SetHot(cursor.Hot == id); 
                    }
                    state.SetActive(cursor.Active == id);
                }
                state.SetClicked(cursor.State == ButtonState.Pressed && cursor.Hot == id && cursor.Active == id);
                if(cursor.State == ButtonState.Pressed && cursor.Hot == id && cursor.Active == id)
                {
                    cursor.FrameActive = null;
                }


            }
            //a good point is you don't want to render per cursor.
            //so how do I store render state.
            //render            
            if (state.Hot)
            {
                if (state.Active)
                {
                    Context.Container.drawBuffer.Add(new DrawCommandStorage(source,Context.Container.ClippingRectangle, Color.Gray, id.Split('#')[0], MemoryConstants.ButtonID, MemoryConstants.ClickedStateID));
                }
                else
                {
                    Context.Container.drawBuffer.Add(new DrawCommandStorage(source,Context.Container.ClippingRectangle, Color.Blue, id.Split('#')[0], MemoryConstants.ButtonID, MemoryConstants.HotStateID));
                }
            }
            else
            {
                Context.Container.drawBuffer.Add(new DrawCommandStorage(source,Context.Container.ClippingRectangle, Color.White, id.Split('#')[0], MemoryConstants.ButtonID, MemoryConstants.InnactiveStateID));
            }
            

            //if (Context.ClickHappen && Context.Hot == id && Context.Active == id)
            if(state.Clicked)
                return true;
            return false;


        }

        public static bool Button(string id, string text)
        {
            //so. mm
            //lets do centered and we'll. work back from there?
            //we need size from the 
            
            //we need to. 
            Vector2 Size = Context.Font.MeasureString(text);
            //Size plus margin
            Size += new Vector2(20, 20);
            //get position so half rectangle plus half 
            Vector2 Position =
                new Vector2(0, 0);


            return false;
        }
        
        #endregion
    }
    //active data is used wrong reread tutorial
    //store active container data.
    //manage parenting solely through docking.
    //do I wanna pull this out of.
    //I don't know.
    //hmmmm
    
    public interface IImmediateModeContext
    {
        Cursor[] Cursors { get; }
        //string Hot { get; set; }
        //string Active { get; set; }
        Canvas Container { get; set; }
        //string FrameHot { get; set; }
        int VertexOffset{ get; }
        int IndexOffset { get; }
        Dictionary<string,object> Widgets { get; }
        List<Canvas> Windows { get; }
        //void AddDrawData(VertexPositionColorTexture[] v, int[] i);//hmm.
        void ClearContainer();//so you can draw items to screen without window.
        StyleGuide Style { get; }
        T GetWidget<T>(string id) where T: new();
        //bool ClickHappen { get; }
        //Point MousePos { get; }
        SpriteFont Font { get; }
        Canvas WantsFocus { set; }
    }

    //public interface IImmediateModeRendering
    //{
    //    VertexPositionColorTexture[] Verticies { get; set; }
    //    int[] Indicies { get; set; }
    //    //void ExpandArray(int targetLength);
    //    int GetOrCreateIndexOffset(string indexID);//hmm
    //    int AddIndexOffset(string indexID, int offset);
    //    //int AvailableVIndex { get; set; }
    //    //int AvailableIIndex { get; set; }
        
    //}
    
   

    public class ImmediateModeScratch : BaseEngine.Logic.Behavior, IImmediateModeContext
    {

        public RenderTarget2D Target { get; private set; }
        Canvas DefaultCanvas;
        Canvas WantsFocus;
        Canvas IImmediateModeContext.WantsFocus { set => WantsFocus = value; }
        public void SetTarget(RenderTarget2D target)
        {
            Target = target; 
            
            DefaultCanvas = new Canvas() { Bounds = target.Bounds,ClippingRectangle = target.Bounds };
            //throw new NotImplementedException("Implement canvas size stuff for default canvas");
        }
        //this is better.
        Dictionary<string, object> ObjectCollection = new Dictionary<string, object>();
        T IImmediateModeContext.GetWidget<T>(string id)
        {
            object ret;
            if(ObjectCollection.TryGetValue(id,out ret))
            {
                return (T)ret;
            }
            ObjectCollection.Add(id, new T());//YEAHHHHHHH
            //MOTHER FUCK
            return (T)ObjectCollection[id];
        }
        [Updatable(UpdateStage.SystemPolling)]
        public override void Update(GameTime gameTime)
        {
            //TODO Clear Command list or vertex list per frame;
            //I think generating all the verticies will be fine for now.
            //it should be in the draw call but I also ...don't...wanna.
            var Context = this as IImmediateModeContext;

            //(this as IImmediateModeContext).FrameHot = "";
             drawCommands.Clear();

            for (int i = 0; i < Context.Cursors.Length; i++)
            {
                Context.Cursors[i].FrameHot = "";

            }
            List<Canvas> inactiveWindows = new List<Canvas>();

            DefaultCanvas?.drawBuffer.Clear();
            if(DefaultCanvas != null)
                DefaultCanvas.FirstOfFrame = true;
            foreach(var window in Windows)
            {
                //not called last frame remove from stack
                if (!window.FirstOfFrame)
                {
                    //var windowData = window.WidgetStorage as Canvas;
                    window.drawBuffer.Clear();
                    window.FirstOfFrame = true;
                }
                else
                {
                    inactiveWindows.Add(window);
                }
            }
            foreach(var window in inactiveWindows)
            {
                Windows.Remove(window);
            }
            
            

            foreach (var window in Windows)
            {
                window.FirstOfFrame = true;
            }
        }
        //hmm
        //get rid of overrides and just use, skimming.
        //for attributes.
        public override void LateUpdate(GameTime gameTime)
        {
            var Context = this as IImmediateModeContext;

            for (int i = 0; i < Context.Cursors.Length; i++)
            {
                if (Context.Cursors[i].State == ButtonState.Released)
                {
                    //Context.Cursors[i].Active = "";
                    Context.Cursors[i].FrameActive = "";
                }
                else
                {
                    if (Context.Cursors[i].FrameActive == "")
                    {
                        Context.Cursors[i].FrameActive = null;
                    }
                }
                Context.Cursors[i].Hot = Context.Cursors[i].FrameHot;
                Context.Cursors[i].Active = Context.Cursors[i].FrameActive;
            }
            if(WantsFocus != null)
            {
                Windows.Remove(WantsFocus);
                Windows.Add(WantsFocus);
            }
            WantsFocus = null;
        }
        public ImmediateModeScratch(Game g)
        {
            vBuffer = new VertexBuffer(g.GraphicsDevice, VertexPositionColorTexture.VertexDeclaration, vBufferSize, BufferUsage.None);
            iBuffer = new IndexBuffer(g.GraphicsDevice, IndexElementSize.ThirtyTwoBits, iBufferSize, BufferUsage.None);
            //Widgets = new Dictionary<string, RectangleStorage>();
            Windows = new List<Canvas>();
        }


        //public Dictionary<string, Cursor> Cursors = new Dictionary<string, Cursor>();
        public void SetCursor(string id,Point position, ButtonState state)
        {
            int index;
            if (!CursorIndex.TryGetValue(id, out index))
            {
                //so if it failed we need to generate a 
                //so. 
                if(freeIndex > Cursors.Length-1)
                {
                    //expand 
                    //ExpandArray(ref Cursors);
                    ExpandArray(ref Cursors, 1);

                }
                Cursors[freeIndex] = new Cursor();
                CursorIndex.Add(id, freeIndex);
                index = freeIndex;
                freeIndex++;                
            }
            var active = Cursors[index];
            active.Position = position;
            active.State = state;
            
        }
        /// <summary>
        /// used to track available index of cursors.
        /// </summary>
        int freeIndex = 0;
        /// <summary>
        /// store index lookup of cursors.
        /// </summary>
        Dictionary<string, int> CursorIndex = new Dictionary<string, int>();
        /// <summary>
        /// cursors collection. 
        /// Default is 2 because of number of joysticks, which is a likely input device.
        /// plus its the most imaginable at once outside of a multiplayer context.
        /// </summary>
        Cursor[] Cursors = new Cursor[0];
        Cursor[] IImmediateModeContext.Cursors { get => Cursors; }//hmm. this'll cause a whole lot of array conversions.

        //string IImmediateModeContext.Hot { get; set; }  
        Canvas IImmediateModeContext.Container { get; set; }
        //string IImmediateModeContext.FrameHot { get; set; }
        //string IImmediateModeContext.Active { get; set; }
        Dictionary<string,object> IImmediateModeContext.Widgets => ObjectCollection;
        List<Canvas> IImmediateModeContext.Windows => Windows;
        int IImmediateModeContext.VertexOffset => verticies.Length;
        int IImmediateModeContext.IndexOffset => indicies.Length;
        public StyleGuide Style { get; set; }
        //void IImmediateModeContext.AddDrawData(VertexPositionColorTexture[] v, int[] i)
        //{
        //    verticies.AddRange(v);
        //    indicies.AddRange(i);
        //}
        void IImmediateModeContext.ClearContainer()
        {
            (this as IImmediateModeContext).Container = DefaultCanvas;
        }
        //bool IImmediateModeContext.ClickHappen => clickHappened;
        //Point IImmediateModeContext.MousePos => MousePos;
        SpriteFont IImmediateModeContext.Font => font;
        public SpriteFont font;//text drawing I have to. to reimpliment this huh.

        //string hot, active;//hmm lets just store the id I guess
        public /*RectangleStorage*/string frameHot;
        //hmm.//Do I want this here or in.
        public bool clickHappened = false;
        public Point MousePos = Point.Zero;


        // top level parent should be canvas info.
        //canvas info should be in guiBegin.
        //public void SetContext(int width, int height)
        //{
        //    //var hold = container;
        //    //while(hold.id != "Context##Context")
        //    //{
        //    //    hold = container.parent;
        //    //}
        //}
        //Dictionary<string, RectangleStorage> Widgets;//retained info for setting through frame data, and cross frame data for say drag and drop and text boxes and scroll bars
        public List<Canvas> Windows = new List<Canvas>();
        public List<RectangleStorage> WindowsOld;
        //order info for undocked windows.
        //List<drawInfo>();
        //public int VertexOffset
        //{
        //    get => verticies.Count;

        //}
        //public int IndexOffset
        //{
        //    get => indicies.Count;
        //}
        //I don't want to just replicate dear imgui.

        int vBufferSize = 0;
        int iBufferSize = 0;
        //store graphics device.
        struct indexTuple
        {
            public int offset;
            public int primitiveCount;
        }
        Dictionary<string, indexTuple> indexLocationStorage = new Dictionary<string, indexTuple>();
        
        VertexPositionColorTexture[] _verticies;
        string styleKey;
        int[] _indicies;
        int vertexIndex = 0;
        int indiciesIndex = 0;
        indexTuple tuple;
        public DrawCommand ProcessStorage(DrawCommandStorage storage)
        {
            DrawCommand command = new DrawCommand();
            //drawCommands.Add( style.Style(command.StyleKey).GenerateFromKey(command, ref verticies, ref indicies));
            //should be struct.
            command.clippingRect = storage.Clip;
            command.color = storage.Color;
            command.texture = storage.Texture;//
                                              //now we need geometry
            SomeStyleClass styleClass = (this as IImmediateModeContext).Style.Style(storage.StyleKey);
            styleClass.GrabVerticies(storage, out _verticies, out styleKey);
            //set verticies here
            //HERE
            command.vertexCount = _verticies.Length;
            command.vertexOffset = vertexIndex;
            //so for simplicity;
            //we can worry about optimizing later

            //while(vertexIndex + _verticies.Length > verticies.Length)
            //{
            //    ExpandArray(ref verticies);//I think the logic in this function is wrong. because any size under 2 will not grow.
            //}
            if (vertexIndex + _verticies.Length > verticies.Length)
            {
                ExpandArray(ref verticies, vertexIndex + _verticies.Length - verticies.Length);
            }
            _verticies.CopyTo(verticies, vertexIndex);//alright so FIRST.
            vertexIndex += _verticies.Length;
            //HERE


            if (indexLocationStorage.TryGetValue(styleKey, out tuple))
            {
                command.indexOffset = tuple.offset;
                command.primitiveCount = tuple.primitiveCount;
            }
            else
            {
                styleClass.GrabIndicies(out _indicies);
                //process some indicies here
                command.indexOffset = indiciesIndex;
                command.primitiveCount = _indicies.Length / 3;
                indexLocationStorage.Add(styleKey, new indexTuple() { offset = command.indexOffset, primitiveCount = command.primitiveCount });

                //so now we do the same.
                //while(indiciesIndex + _indicies.Length > indicies.Length)
                //{
                //    ExpandArray(ref indicies);
                //}
                if (indiciesIndex + _indicies.Length > indicies.Length)
                {
                    ExpandArray(ref indicies, indiciesIndex + _indicies.Length - indicies.Length);
                }
                _indicies.CopyTo(indicies, indiciesIndex);
                indiciesIndex += _indicies.Length;
            }
            return command;
        }
        public unsafe void BeforeDraw(GraphicsDevice device)
        {                       
            vertexIndex = 0;
            indiciesIndex = 0;
            indexTuple tuple;
            indexLocationStorage.Clear();
            
            foreach(DrawCommandStorage storage in DefaultCanvas?.drawBuffer)
            {
                drawCommands.Add(ProcessStorage(storage));
            }

            foreach(Canvas canvas in Windows)
            {
                foreach(DrawCommandStorage storage in canvas.drawBuffer)
                {

                    

                    drawCommands.Add(ProcessStorage(storage));

                    //so the thing.
                    //the thing is.
                    //I'm not generating draw commands from here.
                }
            }
            
            if(verticies.Length > vBufferSize)//no we need to. generate vertex collections first.
            {
                vBuffer?.Dispose();
                vBufferSize = (int)(verticies.Length);
                vBuffer = new VertexBuffer(device, VertexPositionColorTexture.VertexDeclaration, vBufferSize, BufferUsage.None);
            }
            if(indicies.Length > iBufferSize)
            {
                iBuffer?.Dispose();
                iBufferSize = (int)(indicies.Length);
                iBuffer = new IndexBuffer(device, IndexElementSize.ThirtyTwoBits, iBufferSize, BufferUsage.None);
            }
            if(verticies.Length > 0)
                vBuffer.SetData(verticies);
            if (indicies.Length > 0)
                iBuffer.SetData<int>(indicies);


            //foreach (var window in Windows)
            //{
            //    throw new Exception("SHould be generating vertex data here.");
            //    //var windowData = window.WidgetStorage as Canvas;
            //    drawCommands.AddRange(window.drawBuffer);
                

            //}

           
            


        }
        List<string> existingIds = new List<string>();
        public unsafe void AfterDraw()
        {
            //this should be. hmm. this should be in the update loop right?
            var Context = (this as IImmediateModeContext).Cursors;
            //clear all data
            //actually huh I don't have to clear the array huh.
            fixed (VertexPositionColorTexture* v = verticies)
            {

                for(int i = 0;i<verticies.Length;i++)
                {
                    v[i] = new VertexPositionColorTexture();
                }
            }
            fixed (int* ind = indicies)
            {
                for (int i = 0; i < indicies.Length; i++)
                {
                    ind[i] = 0;
                }
            }


            for (int i = 0; i < Context.Length; i++)
            {
                Context[i].Hot = Context[i].FrameHot;
                drawCommands.Clear();
            }
        }

        ///art idea fork broom with a cooking or food witch.
        
        //public List<VertexPositionColorTexture> verticies = new List<VertexPositionColorTexture>();
        //public List<int> indicies = new List<int>();
        public VertexBuffer vBuffer;
        public IndexBuffer iBuffer;

        public VertexPositionColorTexture[] verticies = new VertexPositionColorTexture[0];
        public int[] indicies = new int[0];
       



        public List<DrawCommand> drawCommands = new List<DrawCommand>();
        public RenderTarget2D target;
                
    }

    //Oh I don't need this?
    //why don't I just return 
    //I don't know where to get vertex data and index data pushed into the list.

    /// <summary>
    /// don't know what to name this properly
    /// its for storing data so you don't have to do
    /// geometry generation every frame for every window.
    /// </summary>    
    public struct DrawingCollection
    {
        public VertexPositionColorTexture[] verticies;
        public int[] indicies;
        string ownerID;
        public Rectangle rect;
    }
    public static class StyleBuilder
    {
        static Dictionary<string, DrawingCollection> idBuffer = new Dictionary<string, DrawingCollection>();
        //doing this without style info is, fine alright
        public static DrawingCollection BuildText(string text, Color color,Rectangle rect, SpriteFont font)
        {
            bool firstGlyphOfLine = true;
            var glyphs = font.GetGlyphs();
            DrawingCollection ret = new DrawingCollection();
            ret.verticies = new VertexPositionColorTexture[text.Length * 4];
            ret.indicies = new int[text.Length * 6];

            float TexelWidth = (float)1 / font.Texture.Width;
            float TexelHeight = (float)1 / font.Texture.Height;
            Vector2 offset = rect.Location.ToVector2();

            for (int i = 0; i < text.Length; i++)
            {
                
                var glyph = glyphs[text[i]];
                if (firstGlyphOfLine)
                {
                    offset.X += Math.Max(glyph.LeftSideBearing, 0);
                    firstGlyphOfLine = false;
                }
                else
                {
                    offset.X += font.Spacing + glyph.LeftSideBearing;
                }
                var characterOffset = offset;
                characterOffset.X += glyph.Cropping.X;
                characterOffset.Y += glyph.Cropping.Y;


                Vector2 TopPosition = new Vector2(characterOffset.X, characterOffset.Y);
                Vector2 BotPosition = new Vector2(TopPosition.X + glyph.BoundsInTexture.Width, TopPosition.Y + glyph.BoundsInTexture.Height);
                Vector2 TopUV = new Vector2(glyph.BoundsInTexture.X * TexelWidth, glyph.BoundsInTexture.Y * TexelHeight);
                Vector2 BotUV = new Vector2((glyph.BoundsInTexture.X + glyph.BoundsInTexture.Width) * TexelWidth, (glyph.BoundsInTexture.Y + glyph.BoundsInTexture.Height) * TexelHeight);

                //ret.verticies[0 + (i * 4)] = new VertexPositionColorTexture(new Vector3(characterOffset,0),Color.White,new Vector2(glyph.BoundsInTexture.X * TexelWidth, glyph.BoundsInTexture.Height * TexelHeight));

                //ret.verticies[1 + (i * 4)] = new VertexPositionColorTexture(new Vector3(characterOffset.X + glyph.BoundsInTexture.Width, characterOffset.Y, 0), Color.White, new Vector2((glyph.BoundsInTexture.X + glyph.BoundsInTexture.Width) * TexelWidth, glyph.BoundsInTexture.Y * TexelHeight));
                //ret.verticies[2 + (i * 4)] =  new VertexPositionColorTexture(new Vector3(characterOffset.X, characterOffset.Y + glyph.BoundsInTexture.Height, 0), Color.White, new Vector2(glyph.BoundsInTexture.X * TexelWidth, (glyph.BoundsInTexture.Y + glyph.BoundsInTexture.Height) * TexelHeight));
                //ret.verticies[3 + (i * 4)] = new VertexPositionColorTexture(new Vector3(characterOffset.X + glyph.BoundsInTexture.Width, characterOffset.Y + glyph.BoundsInTexture.Height, 0), Color.White, new Vector2((glyph.BoundsInTexture.X + glyph.BoundsInTexture.Width) * TexelWidth, (glyph.BoundsInTexture.Y + glyph.BoundsInTexture.Height) * TexelHeight));

                ret.verticies[0 + (i * 4)] = new VertexPositionColorTexture(new Vector3(TopPosition.X, TopPosition.Y, 0), Color.White, new Vector2(TopUV.X, TopUV.Y));
                ret.verticies[1 + (i * 4)] = new VertexPositionColorTexture(new Vector3(TopPosition.X, BotPosition.Y, 0), Color.White, new Vector2(TopUV.X, BotUV.Y));
                ret.verticies[2 + (i * 4)] = new VertexPositionColorTexture(new Vector3(BotPosition.X, TopPosition.Y,0), Color.White, new Vector2(BotUV.X, TopUV.Y));
                ret.verticies[3 + (i * 4)] = new VertexPositionColorTexture(new Vector3(BotPosition.X, BotPosition.Y, 0), Color.White, new Vector2(BotUV.X, BotUV.Y));

                ret.indicies[0 + (i * 6)] = 0 + (i * 4);/* { 0, 1, 2, 2, 1, 3 };*/
                ret.indicies[1 + (i * 6)] = 1 + (i * 4);
                ret.indicies[2 + (i * 6)] = 2 + (i * 4);
                ret.indicies[3 + (i * 6)] = 2 + (i * 4);
                ret.indicies[4 + (i * 6)] = 1 + (i * 4);
                ret.indicies[5 + (i * 6)] = 3 + (i * 4);

                offset.X += glyph.WidthIncludingBearings;

                if(offset.X > rect.X + rect.Width)
                {
                    break;
                }
                
            }
            return ret;
        }

        public static DrawingCollection BuildRoundedFrame(int indexOffset,string id,Rectangle rect, float margin) 
        {
            DrawingCollection ret = new DrawingCollection();
            ret.verticies = new VertexPositionColorTexture[76];
            //okay so lets say. margins are 15 each?
            //16 would make rounding easier. 
            //so we have to generate.circles.            
            //if (idBuffer.ContainsKey(id))
            //{
            //    ret = idBuffer[id];
            //    if (ret.rect.Size == rect.Size && ret.rect.Location == rect.Location)
            //    {
            //        return ret;
            //    }
            //}
            //else
            //{
            //    idBuffer.Add(id, new DrawingCollection());
            //    ret = idBuffer[id];
            //    ret.rect = rect;
            //}
            #region MeshDefinition;
            ret.verticies[0] = new VertexPositionColorTexture(new Vector3(0.4756137f * margin + rect.X, 0.3774018f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[1] = new VertexPositionColorTexture(new Vector3(0.4521645f * margin + rect.X, 0.384515f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[2] = new VertexPositionColorTexture(new Vector3(0.4999999f * margin + rect.X, 0.4999999f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[3] = new VertexPositionColorTexture(new Vector3(0.4305537f * margin + rect.X, 0.3960662f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[4] = new VertexPositionColorTexture(new Vector3(0.4116116f * margin + rect.X, 0.4116116f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[5] = new VertexPositionColorTexture(new Vector3(0.3960662f * margin + rect.X, 0.4305536f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[6] = new VertexPositionColorTexture(new Vector3(0.384515f * margin + rect.X, 0.4521645f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[7] = new VertexPositionColorTexture(new Vector3(0.3774018f * margin + rect.X, 0.4756136f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[8] = new VertexPositionColorTexture(new Vector3(0.3749999f * margin + rect.X, 0.4999999f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[9] = new VertexPositionColorTexture(new Vector3(0.125f * margin + rect.X + rect.Width - margin, 0.4999998f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[10] = new VertexPositionColorTexture(new Vector3(0.1225981f * margin + rect.X + rect.Width - margin, 0.4756135f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[11] = new VertexPositionColorTexture(new Vector3(-1.192093E-07f * margin + rect.X + rect.Width - margin, 0.4999999f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[12] = new VertexPositionColorTexture(new Vector3(0.1154848f * margin + rect.X + rect.Width - margin, 0.4521644f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[13] = new VertexPositionColorTexture(new Vector3(0.1039336f * margin + rect.X + rect.Width - margin, 0.4305535f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[14] = new VertexPositionColorTexture(new Vector3(0.0883882f * margin + rect.X + rect.Width - margin, 0.4116114f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[15] = new VertexPositionColorTexture(new Vector3(0.06944609f * margin + rect.X + rect.Width - margin, 0.3960661f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[16] = new VertexPositionColorTexture(new Vector3(0.04783511f * margin + rect.X + rect.Width - margin, 0.3845149f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[17] = new VertexPositionColorTexture(new Vector3(0.02438605f * margin + rect.X + rect.Width - margin, 0.3774017f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[18] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X + rect.Width - margin, 0.3749999f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[19] = new VertexPositionColorTexture(new Vector3(0.4999999f * margin + rect.X, 0.3749999f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[20] = new VertexPositionColorTexture(new Vector3(0.09754443f * margin + rect.X + rect.Width - margin, 0.009607226f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[21] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X + rect.Width - margin, 0f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[22] = new VertexPositionColorTexture(new Vector3(0.191341f * margin + rect.X + rect.Width - margin, 0.03805995f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[23] = new VertexPositionColorTexture(new Vector3(0.2777846f * margin + rect.X + rect.Width - margin, 0.08426481f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[24] = new VertexPositionColorTexture(new Vector3(0.3535529f * margin + rect.X + rect.Width - margin, 0.1464462f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[25] = new VertexPositionColorTexture(new Vector3(0.4157345f * margin + rect.X + rect.Width - margin, 0.2222144f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[26] = new VertexPositionColorTexture(new Vector3(0.4619396f * margin + rect.X + rect.Width - margin, 0.3086578f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[27] = new VertexPositionColorTexture(new Vector3(0.4903926f * margin + rect.X + rect.Width - margin, 0.4024543f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[28] = new VertexPositionColorTexture(new Vector3(0.5f * margin + rect.X + rect.Width - margin, 0.4999995f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[29] = new VertexPositionColorTexture(new Vector3(0.009607375f * margin + rect.X, 0.4024548f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[30] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X, 0.5f * margin + rect.Y, 0), new Color(0.4588235f, 0.4588235f, 0.4588235f, 1f), Vector2.Zero);
            ret.verticies[31] = new VertexPositionColorTexture(new Vector3(0.03806025f * margin + rect.X, 0.3086583f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[32] = new VertexPositionColorTexture(new Vector3(0.08426517f * margin + rect.X, 0.2222149f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[33] = new VertexPositionColorTexture(new Vector3(0.1464466f * margin + rect.X, 0.1464466f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[34] = new VertexPositionColorTexture(new Vector3(0.2222149f * margin + rect.X, 0.0842652f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[35] = new VertexPositionColorTexture(new Vector3(0.3086583f * margin + rect.X, 0.03806025f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[36] = new VertexPositionColorTexture(new Vector3(0.4024549f * margin + rect.X, 0.009607375f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[37] = new VertexPositionColorTexture(new Vector3(0.5f * margin + rect.X, 0f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[38] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X, 0f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[39] = new VertexPositionColorTexture(new Vector3(0.3749999f * margin + rect.X, -1.192093E-07f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[40] = new VertexPositionColorTexture(new Vector3(0.4999999f * margin + rect.X, -1.192093E-07f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[41] = new VertexPositionColorTexture(new Vector3(0.5f * margin + rect.X + rect.Width - margin, -4.768372E-07f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[42] = new VertexPositionColorTexture(new Vector3(0.125f * margin + rect.X + rect.Width - margin, -2.384186E-07f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[43] = new VertexPositionColorTexture(new Vector3(-1.192093E-07f * margin + rect.X + rect.Width - margin, -1.192093E-07f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[44] = new VertexPositionColorTexture(new Vector3(0.3774018f * margin + rect.X, 0.02438617f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[45] = new VertexPositionColorTexture(new Vector3(0.384515f * margin + rect.X, 0.04783535f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[46] = new VertexPositionColorTexture(new Vector3(0.3960662f * margin + rect.X, 0.06944621f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[47] = new VertexPositionColorTexture(new Vector3(0.4116116f * margin + rect.X, 0.0883882f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[48] = new VertexPositionColorTexture(new Vector3(0.4305537f * margin + rect.X, 0.1039336f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[49] = new VertexPositionColorTexture(new Vector3(0.4521645f * margin + rect.X, 0.1154848f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[50] = new VertexPositionColorTexture(new Vector3(0.4756137f * margin + rect.X, 0.1225981f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[51] = new VertexPositionColorTexture(new Vector3(0.5f * margin + rect.X, 0.1249999f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[52] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X + rect.Width - margin, 0.1249999f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[53] = new VertexPositionColorTexture(new Vector3(0.02438629f * margin + rect.X + rect.Width - margin, 0.1225981f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[54] = new VertexPositionColorTexture(new Vector3(0.04783535f * margin + rect.X + rect.Width - margin, 0.1154848f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[55] = new VertexPositionColorTexture(new Vector3(0.06944633f * margin + rect.X + rect.Width - margin, 0.1039336f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[56] = new VertexPositionColorTexture(new Vector3(0.08838832f * margin + rect.X + rect.Width - margin, 0.0883882f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[57] = new VertexPositionColorTexture(new Vector3(0.1039337f * margin + rect.X + rect.Width - margin, 0.06944609f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[58] = new VertexPositionColorTexture(new Vector3(0.115485f * margin + rect.X + rect.Width - margin, 0.04783523f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[59] = new VertexPositionColorTexture(new Vector3(0.1225982f * margin + rect.X + rect.Width - margin, 0.02438617f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[60] = new VertexPositionColorTexture(new Vector3(0.4903927f * margin + rect.X + rect.Width - margin, 0.09754467f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[61] = new VertexPositionColorTexture(new Vector3(0.4619399f * margin + rect.X + rect.Width - margin, 0.1913414f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[62] = new VertexPositionColorTexture(new Vector3(0.415735f * margin + rect.X + rect.Width - margin, 0.2777848f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[63] = new VertexPositionColorTexture(new Vector3(0.3535537f * margin + rect.X + rect.Width - margin, 0.3535532f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[64] = new VertexPositionColorTexture(new Vector3(0.2777853f * margin + rect.X + rect.Width - margin, 0.4157346f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[65] = new VertexPositionColorTexture(new Vector3(0.1913419f * margin + rect.X + rect.Width - margin, 0.4619397f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[66] = new VertexPositionColorTexture(new Vector3(0.09754539f * margin + rect.X + rect.Width - margin, 0.4903926f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[67] = new VertexPositionColorTexture(new Vector3(2.384186E-07f * margin + rect.X + rect.Width - margin, 0.5f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[68] = new VertexPositionColorTexture(new Vector3(0.402455f * margin + rect.X, 0.4903927f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[69] = new VertexPositionColorTexture(new Vector3(0.5000002f * margin + rect.X, 0.5f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[70] = new VertexPositionColorTexture(new Vector3(0.3086584f * margin + rect.X, 0.4619398f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[71] = new VertexPositionColorTexture(new Vector3(0.2222149f * margin + rect.X, 0.4157348f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[72] = new VertexPositionColorTexture(new Vector3(0.1464466f * margin + rect.X, 0.3535534f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[73] = new VertexPositionColorTexture(new Vector3(0.08426517f * margin + rect.X, 0.2777851f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[74] = new VertexPositionColorTexture(new Vector3(0.03806022f * margin + rect.X, 0.1913416f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[75] = new VertexPositionColorTexture(new Vector3(0.009607345f * margin + rect.X, 0.09754515f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.indicies = new int[342] { 0 , 1 , 2 , 1 , 3 , 2 , 3 , 4 , 2 , 4 , 5 , 2 , 5 , 6 , 2 , 6 , 7 , 2 , 7 , 8 , 2 , 9 , 10 , 11 , 10 , 12 , 11 , 12 , 13 , 11 , 13 , 14 , 11 , 14 , 15 , 11 , 15 , 16 , 11 , 16 , 17 , 11 , 17 , 18 , 11 , 19 , 0 , 2 , 20 , 21 , 18 , 20 , 18 , 17 , 22 , 20 , 17 , 22 , 17 , 16 , 23 , 22 , 16 , 23 , 16 , 15 , 24 , 23 , 15 , 24 , 15 , 14 , 25 , 24 , 14 , 25 , 14 , 13 , 26 , 25 , 13 , 26 , 13 , 12 , 27 , 26 , 12 , 27 , 12 , 10 , 28 , 27 , 10 , 28 , 10 , 9 , 29 , 30 , 8 , 29 , 8 , 7 , 31 , 29 , 7 , 31 , 7 , 6 , 32 , 31 , 6 , 32 , 6 , 5 , 33 , 32 , 5 , 33 , 5 , 4 , 34 , 33 , 4 , 34 , 4 , 3 , 35 , 34 , 3 , 35 , 3 , 1 , 36 , 35 , 1 , 36 , 1 , 0 , 37 , 36 , 0 , 37 , 0 , 19 , 30 , 38 , 39 , 30 , 39 , 8 , 8 , 39 , 40 , 8 , 40 , 2 , 41 , 28 , 9 , 41 , 9 , 42 , 42 , 9 , 11 , 42 , 11 , 43 , 21 , 37 , 19 , 21 , 19 , 18 , 18 , 19 , 2 , 18 , 2 , 11 , 40 , 43 , 11 , 40 , 11 , 2 , 39 , 44 , 40 , 44 , 45 , 40 , 45 , 46 , 40 , 46 , 47 , 40 , 47 , 48 , 40 , 48 , 49 , 40 , 49 , 50 , 40 , 50 , 51 , 40 , 52 , 53 , 43 , 53 , 54 , 43 , 54 , 55 , 43 , 55 , 56 , 43 , 56 , 57 , 43 , 57 , 58 , 43 , 58 , 59 , 43 , 59 , 42 , 43 , 60 , 41 , 42 , 60 , 42 , 59 , 61 , 60 , 59 , 61 , 59 , 58 , 62 , 61 , 58 , 62 , 58 , 57 , 63 , 62 , 57 , 63 , 57 , 56 , 64 , 63 , 56 , 64 , 56 , 55 , 65 , 64 , 55 , 65 , 55 , 54 , 66 , 65 , 54 , 66 , 54 , 53 , 67 , 66 , 53 , 67 , 53 , 52 , 68 , 69 , 51 , 68 , 51 , 50 , 70 , 68 , 50 , 70 , 50 , 49 , 71 , 70 , 49 , 71 , 49 , 48 , 72 , 71 , 48 , 72 , 48 , 47 , 73 , 72 , 47 , 73 , 47 , 46 , 74 , 73 , 46 , 74 , 46 , 45 , 75 , 74 , 45 , 75 , 45 , 44 , 38 , 75 , 44 , 38 , 44 , 39 , 69 , 67 , 52 , 69 , 52 , 51 , 51 , 52 , 43 , 51 , 43 , 40 , };
            #endregion
            return ret;
            //lets now do a thing I guess.
            throw new NotImplementedException();

        }

        public static DrawingCollection BuildInvertedFrame(int indexOffset, string id, Rectangle rect, float margin)
        {
            DrawingCollection ret = new DrawingCollection();
            ret.verticies = new VertexPositionColorTexture[76];
            #region MeshDefinition
            ret.verticies[0] = new VertexPositionColorTexture(new Vector3(0.4756137f * margin + rect.X, 0.3774018f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[1] = new VertexPositionColorTexture(new Vector3(0.4521645f * margin + rect.X, 0.384515f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[2] = new VertexPositionColorTexture(new Vector3(0.4999999f * margin + rect.X, 0.4999999f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[3] = new VertexPositionColorTexture(new Vector3(0.4305537f * margin + rect.X, 0.3960662f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[4] = new VertexPositionColorTexture(new Vector3(0.4116116f * margin + rect.X, 0.4116116f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[5] = new VertexPositionColorTexture(new Vector3(0.3960662f * margin + rect.X, 0.4305536f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[6] = new VertexPositionColorTexture(new Vector3(0.384515f * margin + rect.X, 0.4521645f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[7] = new VertexPositionColorTexture(new Vector3(0.3774018f * margin + rect.X, 0.4756136f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[8] = new VertexPositionColorTexture(new Vector3(0.3749999f * margin + rect.X, 0.4999999f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[9] = new VertexPositionColorTexture(new Vector3(0.125f * margin + rect.X + rect.Width - margin, 0.4999998f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[10] = new VertexPositionColorTexture(new Vector3(0.1225981f * margin + rect.X + rect.Width - margin, 0.4756135f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[11] = new VertexPositionColorTexture(new Vector3(-1.192093E-07f * margin + rect.X + rect.Width - margin, 0.4999999f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[12] = new VertexPositionColorTexture(new Vector3(0.1154848f * margin + rect.X + rect.Width - margin, 0.4521644f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[13] = new VertexPositionColorTexture(new Vector3(0.1039336f * margin + rect.X + rect.Width - margin, 0.4305535f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[14] = new VertexPositionColorTexture(new Vector3(0.0883882f * margin + rect.X + rect.Width - margin, 0.4116114f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[15] = new VertexPositionColorTexture(new Vector3(0.06944609f * margin + rect.X + rect.Width - margin, 0.3960661f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[16] = new VertexPositionColorTexture(new Vector3(0.04783511f * margin + rect.X + rect.Width - margin, 0.3845149f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[17] = new VertexPositionColorTexture(new Vector3(0.02438605f * margin + rect.X + rect.Width - margin, 0.3774017f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[18] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X + rect.Width - margin, 0.3749999f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[19] = new VertexPositionColorTexture(new Vector3(0.4999999f * margin + rect.X, 0.3749999f * margin + rect.Y, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[20] = new VertexPositionColorTexture(new Vector3(0.09754443f * margin + rect.X + rect.Width - margin, 0.009607226f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[21] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X + rect.Width - margin, 0f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[22] = new VertexPositionColorTexture(new Vector3(0.191341f * margin + rect.X + rect.Width - margin, 0.03805995f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[23] = new VertexPositionColorTexture(new Vector3(0.2777846f * margin + rect.X + rect.Width - margin, 0.08426481f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[24] = new VertexPositionColorTexture(new Vector3(0.3535529f * margin + rect.X + rect.Width - margin, 0.1464462f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[25] = new VertexPositionColorTexture(new Vector3(0.4157345f * margin + rect.X + rect.Width - margin, 0.2222144f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[26] = new VertexPositionColorTexture(new Vector3(0.4619396f * margin + rect.X + rect.Width - margin, 0.3086578f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[27] = new VertexPositionColorTexture(new Vector3(0.4903926f * margin + rect.X + rect.Width - margin, 0.4024543f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[28] = new VertexPositionColorTexture(new Vector3(0.5f * margin + rect.X + rect.Width - margin, 0.4999995f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[29] = new VertexPositionColorTexture(new Vector3(0.009607375f * margin + rect.X, 0.4024548f * margin + rect.Y, 0), new Color(0.9960784f, 0.9960784f, 0.9960784f, 1f), Vector2.Zero);
            ret.verticies[30] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X, 0.5f * margin + rect.Y, 0), new Color(0.9960784f, 0.9960784f, 0.9960784f, 1f), Vector2.Zero);
            ret.verticies[31] = new VertexPositionColorTexture(new Vector3(0.03806025f * margin + rect.X, 0.3086583f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[32] = new VertexPositionColorTexture(new Vector3(0.08426517f * margin + rect.X, 0.2222149f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[33] = new VertexPositionColorTexture(new Vector3(0.1464466f * margin + rect.X, 0.1464466f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[34] = new VertexPositionColorTexture(new Vector3(0.2222149f * margin + rect.X, 0.0842652f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[35] = new VertexPositionColorTexture(new Vector3(0.3086583f * margin + rect.X, 0.03806025f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[36] = new VertexPositionColorTexture(new Vector3(0.4024549f * margin + rect.X, 0.009607375f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[37] = new VertexPositionColorTexture(new Vector3(0.5f * margin + rect.X, 0f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[38] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X, 0f * margin + rect.Y + rect.Height - margin, 0), new Color(0.9882353f, 0.9882353f, 0.9882353f, 1f), Vector2.Zero);
            ret.verticies[39] = new VertexPositionColorTexture(new Vector3(0.3749999f * margin + rect.X, -1.192093E-07f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[40] = new VertexPositionColorTexture(new Vector3(0.4999999f * margin + rect.X, -1.192093E-07f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[41] = new VertexPositionColorTexture(new Vector3(0.5f * margin + rect.X + rect.Width - margin, -4.768372E-07f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[42] = new VertexPositionColorTexture(new Vector3(0.125f * margin + rect.X + rect.Width - margin, -2.384186E-07f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[43] = new VertexPositionColorTexture(new Vector3(-1.192093E-07f * margin + rect.X + rect.Width - margin, -1.192093E-07f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[44] = new VertexPositionColorTexture(new Vector3(0.3774018f * margin + rect.X, 0.02438617f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[45] = new VertexPositionColorTexture(new Vector3(0.384515f * margin + rect.X, 0.04783535f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[46] = new VertexPositionColorTexture(new Vector3(0.3960662f * margin + rect.X, 0.06944621f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[47] = new VertexPositionColorTexture(new Vector3(0.4116116f * margin + rect.X, 0.0883882f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[48] = new VertexPositionColorTexture(new Vector3(0.4305537f * margin + rect.X, 0.1039336f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[49] = new VertexPositionColorTexture(new Vector3(0.4521645f * margin + rect.X, 0.1154848f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[50] = new VertexPositionColorTexture(new Vector3(0.4756137f * margin + rect.X, 0.1225981f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[51] = new VertexPositionColorTexture(new Vector3(0.5f * margin + rect.X, 0.1249999f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[52] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X + rect.Width - margin, 0.1249999f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[53] = new VertexPositionColorTexture(new Vector3(0.02438629f * margin + rect.X + rect.Width - margin, 0.1225981f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[54] = new VertexPositionColorTexture(new Vector3(0.04783535f * margin + rect.X + rect.Width - margin, 0.1154848f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[55] = new VertexPositionColorTexture(new Vector3(0.06944633f * margin + rect.X + rect.Width - margin, 0.1039336f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[56] = new VertexPositionColorTexture(new Vector3(0.08838832f * margin + rect.X + rect.Width - margin, 0.0883882f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[57] = new VertexPositionColorTexture(new Vector3(0.1039337f * margin + rect.X + rect.Width - margin, 0.06944609f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[58] = new VertexPositionColorTexture(new Vector3(0.115485f * margin + rect.X + rect.Width - margin, 0.04783523f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[59] = new VertexPositionColorTexture(new Vector3(0.1225982f * margin + rect.X + rect.Width - margin, 0.02438617f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[60] = new VertexPositionColorTexture(new Vector3(0.4903927f * margin + rect.X + rect.Width - margin, 0.09754467f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[61] = new VertexPositionColorTexture(new Vector3(0.4619399f * margin + rect.X + rect.Width - margin, 0.1913414f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[62] = new VertexPositionColorTexture(new Vector3(0.415735f * margin + rect.X + rect.Width - margin, 0.2777848f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[63] = new VertexPositionColorTexture(new Vector3(0.3535537f * margin + rect.X + rect.Width - margin, 0.3535532f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[64] = new VertexPositionColorTexture(new Vector3(0.2777853f * margin + rect.X + rect.Width - margin, 0.4157346f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[65] = new VertexPositionColorTexture(new Vector3(0.1913419f * margin + rect.X + rect.Width - margin, 0.4619397f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[66] = new VertexPositionColorTexture(new Vector3(0.09754539f * margin + rect.X + rect.Width - margin, 0.4903926f * margin + rect.Y + rect.Height - margin, 0), new Color(0.9960784f, 0.9960784f, 0.9960784f, 1f), Vector2.Zero);
            ret.verticies[67] = new VertexPositionColorTexture(new Vector3(2.384186E-07f * margin + rect.X + rect.Width - margin, 0.5f * margin + rect.Y + rect.Height - margin, 0), new Color(0.9960784f, 0.9960784f, 0.9960784f, 1f), Vector2.Zero);
            ret.verticies[68] = new VertexPositionColorTexture(new Vector3(0.402455f * margin + rect.X, 0.4903927f * margin + rect.Y + rect.Height - margin, 0), new Color(0.9960784f, 0.9960784f, 0.9960784f, 1f), Vector2.Zero);
            ret.verticies[69] = new VertexPositionColorTexture(new Vector3(0.5000002f * margin + rect.X, 0.5f * margin + rect.Y + rect.Height - margin, 0), new Color(0.9960784f, 0.9960784f, 0.9960784f, 1f), Vector2.Zero);
            ret.verticies[70] = new VertexPositionColorTexture(new Vector3(0.3086584f * margin + rect.X, 0.4619398f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[71] = new VertexPositionColorTexture(new Vector3(0.2222149f * margin + rect.X, 0.4157348f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[72] = new VertexPositionColorTexture(new Vector3(0.1464466f * margin + rect.X, 0.3535534f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[73] = new VertexPositionColorTexture(new Vector3(0.08426517f * margin + rect.X, 0.2777851f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[74] = new VertexPositionColorTexture(new Vector3(0.03806022f * margin + rect.X, 0.1913416f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[75] = new VertexPositionColorTexture(new Vector3(0.009607345f * margin + rect.X, 0.09754515f * margin + rect.Y + rect.Height - margin, 0), new Color(0.9960784f, 0.9960784f, 0.9960784f, 1f), Vector2.Zero);
            ret.indicies = new int[342] { 0 , 1 , 2 , 1 , 3 , 2 , 3 , 4 , 2 , 4 , 5 , 2 , 5 , 6 , 2 , 6 , 7 , 2 , 7 , 8 , 2 , 9 , 10 , 11 , 10 , 12 , 11 , 12 , 13 , 11 , 13 , 14 , 11 , 14 , 15 , 11 , 15 , 16 , 11 , 16 , 17 , 11 , 17 , 18 , 11 , 19 , 0 , 2 , 20 , 21 , 18 , 20 , 18 , 17 , 22 , 20 , 17 , 22 , 17 , 16 , 23 , 22 , 16 , 23 , 16 , 15 , 24 , 23 , 15 , 24 , 15 , 14 , 25 , 24 , 14 , 25 , 14 , 13 , 26 , 25 , 13 , 26 , 13 , 12 , 27 , 26 , 12 , 27 , 12 , 10 , 28 , 27 , 10 , 28 , 10 , 9 , 29 , 30 , 8 , 29 , 8 , 7 , 31 , 29 , 7 , 31 , 7 , 6 , 32 , 31 , 6 , 32 , 6 , 5 , 33 , 32 , 5 , 33 , 5 , 4 , 34 , 33 , 4 , 34 , 4 , 3 , 35 , 34 , 3 , 35 , 3 , 1 , 36 , 35 , 1 , 36 , 1 , 0 , 37 , 36 , 0 , 37 , 0 , 19 , 30 , 38 , 39 , 30 , 39 , 8 , 8 , 39 , 40 , 8 , 40 , 2 , 41 , 28 , 9 , 41 , 9 , 42 , 42 , 9 , 11 , 42 , 11 , 43 , 21 , 37 , 19 , 21 , 19 , 18 , 18 , 19 , 2 , 18 , 2 , 11 , 40 , 43 , 11 , 40 , 11 , 2 , 39 , 44 , 40 , 44 , 45 , 40 , 45 , 46 , 40 , 46 , 47 , 40 , 47 , 48 , 40 , 48 , 49 , 40 , 49 , 50 , 40 , 50 , 51 , 40 , 52 , 53 , 43 , 53 , 54 , 43 , 54 , 55 , 43 , 55 , 56 , 43 , 56 , 57 , 43 , 57 , 58 , 43 , 58 , 59 , 43 , 59 , 42 , 43 , 60 , 41 , 42 , 60 , 42 , 59 , 61 , 60 , 59 , 61 , 59 , 58 , 62 , 61 , 58 , 62 , 58 , 57 , 63 , 62 , 57 , 63 , 57 , 56 , 64 , 63 , 56 , 64 , 56 , 55 , 65 , 64 , 55 , 65 , 55 , 54 , 66 , 65 , 54 , 66 , 54 , 53 , 67 , 66 , 53 , 67 , 53 , 52 , 68 , 69 , 51 , 68 , 51 , 50 , 70 , 68 , 50 , 70 , 50 , 49 , 71 , 70 , 49 , 71 , 49 , 48 , 72 , 71 , 48 , 72 , 48 , 47 , 73 , 72 , 47 , 73 , 47 , 46 , 74 , 73 , 46 , 74 , 46 , 45 , 75 , 74 , 45 , 75 , 45 , 44 , 38 , 75 , 44 , 38 , 44 , 39 , 69 , 67 , 52 , 69 , 52 , 51 , 51 , 52 , 43 , 51 , 43 , 40 , };
            #endregion
            return ret;
        }
        //hmm.what does this actually look like. 
        //whats the number I want to do for this.
        public static DrawingCollection BuildTabbedFrame(int indexOffset, string id, Rectangle rect, float margin)
        {
            throw new NotImplementedException();
        }

        public static DrawingCollection BuildBeveledFrame(int indexOffset, string id, Rectangle rect, float margin)
        {
            DrawingCollection ret = new DrawingCollection();
            ret.verticies = new VertexPositionColorTexture[8];
            //if (idBuffer.ContainsKey(id))
            //{
            //    ret = idBuffer[id];
            //    if (ret.rect.Size == rect.Size && ret.rect.Location == rect.Location)
            //    {
            //        return ret;
            //    }
            //}
            //else
            //{
            //    idBuffer.Add(id, new DrawingCollection());
            //    ret = idBuffer[id];
            //    ret.rect = rect;
            //}
            //define mesh
            #region MeshDefinition
            ret.verticies[0] = new VertexPositionColorTexture(new Vector3(0.5f * margin + rect.X, 0.5f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[1] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X + rect.Width - margin, 0.5f * margin + rect.Y, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[2] = new VertexPositionColorTexture(new Vector3(0.5f * margin + rect.X + rect.Width - margin, 0f * margin + rect.Y, 0), new Color(0.003921569f, 0.003921569f, 0.003921569f, 1f), Vector2.Zero);
            ret.verticies[3] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X, 0f * margin + rect.Y, 0), new Color(0f, 0f, 0f, 1f), Vector2.Zero);
            ret.verticies[4] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X, 0.5f * margin + rect.Y + rect.Height - margin, 0), new Color(0f, 0f, 0f, 1f), Vector2.Zero);
            ret.verticies[5] = new VertexPositionColorTexture(new Vector3(0.5f * margin + rect.X, 0f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[6] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X + rect.Width - margin, 0f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[7] = new VertexPositionColorTexture(new Vector3(0.5f * margin + rect.X + rect.Width - margin, 0.5f * margin + rect.Y + rect.Height - margin, 0), new Color(0f, 0f, 0f, 1f), Vector2.Zero);
            ret.indicies = new int[30] { 0 , 1 , 2 , 0 , 2 , 3 , 3 , 4 , 5 , 3 , 5 , 0 , 1 , 6 , 7 , 1 , 7 , 2 , 6 , 1 , 0 , 6 , 0 , 5 , 4 , 7 , 6 , 4 , 6 , 5 , };
            #endregion

            return ret;


        }
        public static DrawingCollection BuildCloseButton(int indexOffset, string id, Rectangle rect, float margin)
        {
            DrawingCollection ret = new DrawingCollection();
            ret.verticies = new VertexPositionColorTexture[18];
            //if (idBuffer.ContainsKey(id))
            //{
            //    ret = idBuffer[id];
            //    if (ret.rect.Size == rect.Size && ret.rect.Location == rect.Location)
            //    {
            //        return ret;
            //    }
            //}
            //else
            //{
            //    idBuffer.Add(id, new DrawingCollection());
            //    ret = idBuffer[id];
            //    ret.rect = rect;
            //}

            //define mesh

            #region MeshDefinition
            ret.verticies[0] = new VertexPositionColorTexture(new Vector3(0.09754443f * margin + rect.X + rect.Width - margin, 0.009607226f * margin + rect.Y, 0), new Color(0.5019608f, 0f, 0f, 1f), Vector2.Zero);
            ret.verticies[1] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X + rect.Width - margin, 0f * margin + rect.Y, 0), new Color(0.5019608f, 0f, 0f, 1f), Vector2.Zero);
            ret.verticies[2] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X + rect.Width - margin, 0.3749999f * margin + rect.Y, 0), new Color(1f, 0f, 0f, 1f), Vector2.Zero);
            ret.verticies[3] = new VertexPositionColorTexture(new Vector3(0.02438605f * margin + rect.X + rect.Width - margin, 0.3774017f * margin + rect.Y, 0), new Color(1f, 0f, 0f, 1f), Vector2.Zero);
            ret.verticies[4] = new VertexPositionColorTexture(new Vector3(0.191341f * margin + rect.X + rect.Width - margin, 0.03805995f * margin + rect.Y, 0), new Color(0.5019608f, 0f, 0f, 1f), Vector2.Zero);
            ret.verticies[5] = new VertexPositionColorTexture(new Vector3(0.04783511f * margin + rect.X + rect.Width - margin, 0.3845149f * margin + rect.Y, 0), new Color(1f, 0f, 0f, 1f), Vector2.Zero);
            ret.verticies[6] = new VertexPositionColorTexture(new Vector3(0.2777846f * margin + rect.X + rect.Width - margin, 0.08426481f * margin + rect.Y, 0), new Color(0.5019608f, 0f, 0f, 1f), Vector2.Zero);
            ret.verticies[7] = new VertexPositionColorTexture(new Vector3(0.06944609f * margin + rect.X + rect.Width - margin, 0.3960661f * margin + rect.Y, 0), new Color(1f, 0f, 0f, 1f), Vector2.Zero);
            ret.verticies[8] = new VertexPositionColorTexture(new Vector3(0.3535529f * margin + rect.X + rect.Width - margin, 0.1464462f * margin + rect.Y, 0), new Color(0.5019608f, 0f, 0f, 1f), Vector2.Zero);
            ret.verticies[9] = new VertexPositionColorTexture(new Vector3(0.0883882f * margin + rect.X + rect.Width - margin, 0.4116114f * margin + rect.Y, 0), new Color(1f, 0f, 0f, 1f), Vector2.Zero);
            ret.verticies[10] = new VertexPositionColorTexture(new Vector3(0.4157345f * margin + rect.X + rect.Width - margin, 0.2222144f * margin + rect.Y, 0), new Color(0.5019608f, 0f, 0f, 1f), Vector2.Zero);
            ret.verticies[11] = new VertexPositionColorTexture(new Vector3(0.1039336f * margin + rect.X + rect.Width - margin, 0.4305535f * margin + rect.Y, 0), new Color(1f, 0f, 0f, 1f), Vector2.Zero);
            ret.verticies[12] = new VertexPositionColorTexture(new Vector3(0.4619396f * margin + rect.X + rect.Width - margin, 0.3086578f * margin + rect.Y, 0), new Color(0.5019608f, 0f, 0f, 1f), Vector2.Zero);
            ret.verticies[13] = new VertexPositionColorTexture(new Vector3(0.1154848f * margin + rect.X + rect.Width - margin, 0.4521644f * margin + rect.Y, 0), new Color(1f, 0f, 0f, 1f), Vector2.Zero);
            ret.verticies[14] = new VertexPositionColorTexture(new Vector3(0.4903926f * margin + rect.X + rect.Width - margin, 0.4024543f * margin + rect.Y, 0), new Color(0.5019608f, 0f, 0f, 1f), Vector2.Zero);
            ret.verticies[15] = new VertexPositionColorTexture(new Vector3(0.1225981f * margin + rect.X + rect.Width - margin, 0.4756135f * margin + rect.Y, 0), new Color(1f, 0f, 0f, 1f), Vector2.Zero);
            ret.verticies[16] = new VertexPositionColorTexture(new Vector3(0.5f * margin + rect.X + rect.Width - margin, 0.4999995f * margin + rect.Y, 0), new Color(0.5019608f, 0f, 0f, 1f), Vector2.Zero);
            ret.verticies[17] = new VertexPositionColorTexture(new Vector3(0.125f * margin + rect.X + rect.Width - margin, 0.4999998f * margin + rect.Y, 0), new Color(1f, 0f, 0f, 1f), Vector2.Zero);
            ret.indicies = new int[48] { 0 , 1 , 2 , 0 , 2 , 3 , 4 , 0 , 3 , 4 , 3 , 5 , 6 , 4 , 5 , 6 , 5 , 7 , 8 , 6 , 7 , 8 , 7 , 9 , 10 , 8 , 9 , 10 , 9 , 11 , 12 , 10 , 11 , 12 , 11 , 13 , 14 , 12 , 13 , 14 , 13 , 15 , 16 , 14 , 15 , 16 , 15 , 17 , };
            #endregion

            return ret;

        }
        public static DrawingCollection BuildResizeButton(int indexOffset, string id, Rectangle rect, float margin)
        {
            DrawingCollection ret = new DrawingCollection();
            ret.verticies = new VertexPositionColorTexture[18];
            //this completely falls apart when adding stuff so scrap it.
            //or well.
            //it'd be pretty easy to 
            //okay no it wouldn't I could just rebuild index data but I'd need radically different understandings of where stuff is stored in the vertex buffer.
            //and when that moves I can't track it.
            //if (idBuffer.ContainsKey(id))
            //{
            //    ret = idBuffer[id];
            //    if (ret.rect.Size == rect.Size && ret.rect.Location == rect.Location)
            //    {
            //        return ret;
            //    }
            //}
            //else
            //{
            //    idBuffer.Add(id, new DrawingCollection());
            //    ret = idBuffer[id];
            //    ret.rect = rect;
            //}

            //define mesh
            #region MeshDefinition
            ret.verticies[0] = new VertexPositionColorTexture(new Vector3(0.4903927f * margin + rect.X + rect.Width - margin, 0.09754467f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[1] = new VertexPositionColorTexture(new Vector3(0.5f * margin + rect.X + rect.Width - margin, -4.768372E-07f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[2] = new VertexPositionColorTexture(new Vector3(0.125f * margin + rect.X + rect.Width - margin, -2.384186E-07f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[3] = new VertexPositionColorTexture(new Vector3(0.1225982f * margin + rect.X + rect.Width - margin, 0.02438617f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[4] = new VertexPositionColorTexture(new Vector3(0.4619399f * margin + rect.X + rect.Width - margin, 0.1913414f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[5] = new VertexPositionColorTexture(new Vector3(0.115485f * margin + rect.X + rect.Width - margin, 0.04783523f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[6] = new VertexPositionColorTexture(new Vector3(0.415735f * margin + rect.X + rect.Width - margin, 0.2777848f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[7] = new VertexPositionColorTexture(new Vector3(0.1039337f * margin + rect.X + rect.Width - margin, 0.06944609f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[8] = new VertexPositionColorTexture(new Vector3(0.3535537f * margin + rect.X + rect.Width - margin, 0.3535532f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[9] = new VertexPositionColorTexture(new Vector3(0.08838832f * margin + rect.X + rect.Width - margin, 0.0883882f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[10] = new VertexPositionColorTexture(new Vector3(0.2777853f * margin + rect.X + rect.Width - margin, 0.4157346f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[11] = new VertexPositionColorTexture(new Vector3(0.06944633f * margin + rect.X + rect.Width - margin, 0.1039336f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[12] = new VertexPositionColorTexture(new Vector3(0.1913419f * margin + rect.X + rect.Width - margin, 0.4619397f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[13] = new VertexPositionColorTexture(new Vector3(0.04783535f * margin + rect.X + rect.Width - margin, 0.1154848f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[14] = new VertexPositionColorTexture(new Vector3(0.09754539f * margin + rect.X + rect.Width - margin, 0.4903926f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[15] = new VertexPositionColorTexture(new Vector3(0.02438629f * margin + rect.X + rect.Width - margin, 0.1225981f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.verticies[16] = new VertexPositionColorTexture(new Vector3(2.384186E-07f * margin + rect.X + rect.Width - margin, 0.5f * margin + rect.Y + rect.Height - margin, 0), new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f), Vector2.Zero);
            ret.verticies[17] = new VertexPositionColorTexture(new Vector3(0f * margin + rect.X + rect.Width - margin, 0.1249999f * margin + rect.Y + rect.Height - margin, 0), new Color(1f, 1f, 1f, 1f), Vector2.Zero);
            ret.indicies = new int[48] { 0 , 1 , 2 , 0 , 2 , 3 , 4 , 0 , 3 , 4 , 3 , 5 , 6 , 4 , 5 , 6 , 5 , 7 , 8 , 6 , 7 , 8 , 7 , 9 , 10 , 8 , 9 , 10 , 9 , 11 , 12 , 10 , 11 , 12 , 11 , 13 , 14 , 12 , 13 , 14 , 13 , 15 , 16 , 14 , 15 , 16 , 15 , 17 , };
            #endregion

            return ret;
        }
        delegate DrawingCollection BuildWindow(Rectangle r);
    }
    struct DrawCollection
    {
        public VertexBuffer Buffer;
        public IndexBuffer Index;
    }
    //storage information for windows and widgets
    //hmm
    //I don't want this.
    //lets make it internal because I want extendability but not ust full access.
    public class RectangleStorage
    {
        public string id;//this is for equality distinction, not usability
        public double Depth;//if I turn off thre depth buffer I probably don't need this. I just need data order inserted properly.
        //I could probably use this to track widget depth or something for like "how many strings down for overlap does this need to be.
        public Rectangle Rectangle;
        public object WidgetStorage;
        public string StyleKey;
        public bool isContainer;
        public Rectangle ClippingRectangle;//aww hell.
        //okay so I, have now stumbled on to doing htings the way imgui does.
        //because I hate myself I guess.
        //having a clipping rect is a real quick way to bypass some stuff.
        //i still want to clip within the application but thats...hard.
        //
        //public Point Position
        //{
        //    get
        //    {
        //        //this should always be true.
        //        if(WidgetStorage is Canvas)
        //        {
        //            return ((Canvas)WidgetStorage).topMarfgin + Rectangle.Location;
        //        }
        //        else
        //        {
        //            return Rectangle.Location;
        //        }
        //    }
        //}

        public RectangleStorage(string id)
        {
           this.id = id;
            isContainer = false;
        }
        public RectangleStorage(string id,bool container )
        {
            this.id = id;
            isContainer = container;
        }
        //huh. oh well also
        //I put parenting here but it yelled at me
        //This should be a class actually what am I doing
        //there we go., it was a struct, which wouldn't retain constancy between lists. 
        public RectangleStorage parent;
    }
    //what if I store draw data here, and then construct everything at the end using the window order. 
    //yeah. cool.
    public class ScrollValue
    {
        int ScrollState;
        int ScrollMin;
        int ScrollMax;
        int Width;
        int Height;
    }
    public class Canvas
    {
        public Canvas()
        {
            
        }
        public bool Active { get; set; }
        public Point StartingPosition
        {
            get => ClippingRectangle.Location;
        }
        public Point LastFramePosition { get; set; }
        public Point LastFrameSize { get; set; }         
        public Rectangle Bounds;
        public Rectangle ClippingRectangle;//hmm       
        //scroll data
        public int ScrollState;
        public int ScrollMin;
        public int ScrollMax;
        //multi frame appending
        public bool FirstOfFrame = true;

        //public List<DrawCommand> drawBuffer;
        public List<DrawCommandStorage> drawBuffer = new List<DrawCommandStorage>();
        //I don't need this...here. huh.
        //okay so outset to some general 
       
        //should probably keep in style data
        public int Padding;
        /// <summary>
        /// vertical offset for auto positioning of widgets.
        /// </summary>
        public int VerticalOffset;
    }

    public struct DrawCommand
    {
        public int vertexOffset;
        public int vertexCount;
        public int indexOffset;
        //public int indexLength;
        public int primitiveCount;
        public Color color;
        public Texture texture;
        public Rectangle clippingRect;        
    }

    struct drawInfo
    {
        VertexPositionColorTexture[] verticies;
        //or is it
        public int VertexOffset;//wait do I need these??
        public int VertexCount;//oh wait yeah I do fuck. Fuck.
        int[] indicies;
        //what if I don't care and just set all the things instead of a big list. god that would be nice ;w;
        //by that, later me, I mean a big list of vertex buffers and index buffers.
        //or hell just the vertex indec lists. 
        //because a big batch of the same stuff makes doing effect stuff harder.
        
        public int indexOffset;
        public int IndexLength;
        Rectangle ClippingRectangle;

        //what else I guess
        //effects?
        //hmm.
        //I can easily sort for effect later I guess??

        Effect effect;
        Texture2D texture;
        Color color;
    }



    //alright lets work out.
    //I was thinking of treating windows and containsers as instead of a thing in like a call its determined by an attribute, 
    //so the function the calls are stored in is a window,
    //this way ordering and occlusion is handled automatically with sorting instead of just the window itself.
    public class MemoryCanvas : System.Attribute
    {
        public string Name;//this isn't strictly needed huh, you can use the. the function name sort of.
        public int Order;
        public MemoryCanvas(string name, int order = 0)
        {
            Name = name;
            Order = order;
        }
    }

    public class ImguiStorage
    {
        [MemoryCanvas("FirstWindow")]
        public void ImguiFunc()
        {
            MemoryGui.Button("Some Button");
        }
    }
}

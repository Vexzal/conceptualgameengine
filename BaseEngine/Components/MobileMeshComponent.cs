﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseEngine.MobileMeshes;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace BaseEngine.Components
{
    class MobileMeshComponent : Component//create a seperate mesh interface for the renderer
    {
        public EffectComponent Effect { get; set; }
        public MobileMesh MMesh { get; set; }
        public Model Model
        {
            get => MobileMesh.BuildModel(MMesh, Effect.Effect.GraphicsDevice, VertexPosition.VertexDeclaration,Effect.Effect);
        }
        //public Model Mesh
        //{
        //    //get=>MMesh.MeshModel
        //}
         

        

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework.Graphics;

namespace BaseEngine.Input2
{
    class Input_Device_Binding
    {
    }
    
    class MouseDeviceHandler
    {
        public string Name;
        MouseState state;


    }
    class InputBinding
    {
        
    }
    class InputBindingContainer
    {
        
    }
    //lets start with a device
    public class KeyboardUpdater
    {
        KeyboardState state;
        string stuff;
        public void Update()
        {
            state = Keyboard.GetState();
            //stuff = state.GetPressedKeys().ToString();
            foreach(var key in state.GetPressedKeys())
            {
                stuff += key;
            }
        }
    }
    public class TouchUpdater
    {
        TouchCollection panelState;
        DeviceState returnState = new DeviceState();
        public void Update()
        {
            panelState = Microsoft.Xna.Framework.Input.Touch.TouchPanel.GetState();

            foreach(var touch in panelState)
            {
                returnState.Device = "Touch";
                returnState.ID = touch.Id.ToString();
                returnState.DeviceInputs = touch.State.ToString();
                //I need to know more about touch.
            }
        }
    }
    public class MouseDevice
    {
        string m = " ";//I don't, I don't actually need this do I?
        //actually there is a definite problem because
        //all I'm getting is boolstates from pressed buttons
        //and string concatting doesn't work well with axis inputs I want
        //
        MouseState mouseState;
        DeviceState returnState = new DeviceState();
        public void Update()
        {
            mouseState = Mouse.GetState();
            returnState.Device = "mouse";
            returnState.ID = "-1";
            returnState.DeviceInputs = mouseState.LeftButton.ToString() + m + mouseState.RightButton + m + mouseState.MiddleButton + m + mouseState.XButton1 + m + mouseState.XButton2 + m + "Pos;"+mouseState.Position + m + mouseState.ScrollWheelValue; 
        }
    }
    //so
    //alright my problem is behaviour
    //how do you decide when to exclude inputs because of other inputs?
    //chaining button inputs etc.
    //if you don't want modifiers you have an implicit not pressed interest.
    //I guess???
    //part of the problem is 
    //well lets just define modifiers.
    //the second problem is more abstract stuff. 
    //well okay I have all of this and I want a single command dispatcher that outputs 
    //ahhhhhh
    //okay no lets go back to what I actually wanted.
    //I wanted independent command groups.
    //so.
    //so.

    public struct DeviceState
    {
        /// <summary>
        /// Type of Device sending the state
        /// </summary>
        public string Device;
        /// <summary>
        /// ID of device sending the input
        /// (used to differentiate multiple attatched devices)
        /// </summary>
        public string ID;
        /// <summary>
        /// Concatonated Command String
        /// </summary>
        public string DeviceInputs;
        public DeviceState(string _device, string _deviceID, string _deviceInputs)
        {
            Device = _device;
            ID = _deviceID;
            DeviceInputs = _deviceInputs;
        }
    }
}

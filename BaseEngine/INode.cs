﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseEngine
{
    public interface INode<T>
    {
        T Parent { get; }
        List<T> Children { get; }
        void AddChild(T node);
        void SetParent(T node);
        void RemoveChild(T node);
        void RemoveParent();
    }
}
